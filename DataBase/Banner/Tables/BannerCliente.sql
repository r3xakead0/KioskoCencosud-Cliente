﻿CREATE TABLE [Banner].[BannerCliente] (
    [IdBannerCliente] INT          IDENTITY (1, 1) NOT NULL,
    [IdBanner]        INT          NOT NULL,
    [TipoDocumento]   VARCHAR (10) NOT NULL,
    [NroDocumento]    VARCHAR (20) NOT NULL,
    [FechaRegistro]   DATETIME     NOT NULL,
    [Sincronizado]    BIT          CONSTRAINT [DF_BannerCliente_Sincronizado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BannerCliente] PRIMARY KEY CLUSTERED ([IdBannerCliente] ASC),
    CONSTRAINT [FK_BannerCliente_Banner] FOREIGN KEY ([IdBanner]) REFERENCES [Maestro].[Banner] ([IdBanner])
);

