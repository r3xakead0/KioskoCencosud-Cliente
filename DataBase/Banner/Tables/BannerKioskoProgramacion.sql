﻿CREATE TABLE [Banner].[BannerKioskoProgramacion] (
    [IdBannerKioskoProgramacion] INT      IDENTITY (1, 1) NOT NULL,
    [IdBannerKiosko]             INT      NOT NULL,
    [Fecha]                      DATE     NOT NULL,
    [HoraInicio]                 TIME (7) NOT NULL,
    [HoraFin]                    TIME (7) NOT NULL,
    [FechaRegistro]              DATETIME NOT NULL,
    [Sincronizado]               BIT      CONSTRAINT [DF_BannerKioskoProgramacion_Sincronizado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BannerKioskoProgramacion] PRIMARY KEY CLUSTERED ([IdBannerKioskoProgramacion] ASC),
    CONSTRAINT [FK_BannerKioskoProgramacion_BannerKiosko] FOREIGN KEY ([IdBannerKiosko]) REFERENCES [Banner].[BannerKiosko] ([IdBannerKiosko])
);

