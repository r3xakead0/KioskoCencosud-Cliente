﻿CREATE TABLE [Banner].[BannerKiosko] (
    [IdBannerKiosko] INT           IDENTITY (1, 1) NOT NULL,
    [IdBanner]       INT           NOT NULL,
    [Tienda]         VARCHAR (50)  NOT NULL,
    [Hostname]       VARCHAR (120) NOT NULL,
    [FechaRegistro]  DATETIME      NOT NULL,
    [Sincronizado]   BIT           CONSTRAINT [DF_BannerKiosko_Sincronizado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BannerKiosko] PRIMARY KEY CLUSTERED ([IdBannerKiosko] ASC),
    CONSTRAINT [FK_BannerKiosko_Banner] FOREIGN KEY ([IdBanner]) REFERENCES [Maestro].[Banner] ([IdBanner])
);

