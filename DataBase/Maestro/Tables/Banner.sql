﻿CREATE TABLE [Maestro].[Banner] (
    [IdBanner]          INT           IDENTITY (1, 1) NOT NULL,
    [IdCampanha]        INT           NOT NULL,
    [Nombre]            VARCHAR (50)  NOT NULL,
    [Imagen]            VARCHAR (255) NOT NULL,
    [Prioridad]         INT           NOT NULL,
    [Tiempo]            INT           NOT NULL,
    [InicioVigencia]    DATE          NOT NULL,
    [FinalVigencia]     DATE          NOT NULL,
    [Activo]            BIT           NOT NULL,
    [Tipo]              CHAR (1)      NOT NULL,
    [VistasLimite]      INT           NOT NULL,
    [VistasActual]      INT           NOT NULL,
    [FechaRegistro]     DATETIME      NOT NULL,
    [CodigosMateriales] VARCHAR (MAX) NOT NULL,
    [Sincronizado]      BIT           CONSTRAINT [DF_Banner_Sincronizado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED ([IdBanner] ASC),
    CONSTRAINT [FK_Banner_Campanha] FOREIGN KEY ([IdCampanha]) REFERENCES [Maestro].[Campanha] ([IdCampanha])
);

