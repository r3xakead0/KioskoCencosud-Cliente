﻿CREATE TABLE [Maestro].[Campanha] (
    [IdCampanha]    INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]        VARCHAR (50)  NOT NULL,
    [Descripcion]   VARCHAR (255) NOT NULL,
    [Activo]        BIT           NOT NULL,
    [FechaRegistro] DATETIME      NOT NULL,
    [Sincronizado]  BIT           CONSTRAINT [DF_Campanha_Sincronizado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Campanha] PRIMARY KEY CLUSTERED ([IdCampanha] ASC)
);

