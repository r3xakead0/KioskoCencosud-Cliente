﻿CREATE FUNCTION [dbo].[fn_split]
(	
	@List nvarchar(MAX),
	@SplitOn nvarchar(100)
)
RETURNS @RtnValue table (	
	Value varchar(100)
)
AS
BEGIN
	While (Charindex(@SplitOn,@List)>0)
	Begin 
		Insert Into @RtnValue (value)
		Select 
			Value = ltrim(rtrim(Substring(@List,1,Charindex(@SplitOn,@List)-1))) 
		Set @List = REPLACE(REPLACE(ltrim(rtrim(
		Substring(@List,Charindex(@SplitOn,@List)+len(@SplitOn),len(@List))
		)),CHAR(13) + CHAR(10),''),CHAR(9),'')
	End 
	
	if ltrim(rtrim(@List))<>''
	 begin
		Insert Into @RtnValue (Value)
		Select Value = REPLACE(REPLACE(ltrim(rtrim(@List)),CHAR(13) + CHAR(10),''),CHAR(9),'')
     end
    Return
END