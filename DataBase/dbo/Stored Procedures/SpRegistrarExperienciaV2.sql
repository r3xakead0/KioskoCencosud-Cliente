﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Registrar la experiencia del Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
--  Testing
--      EXEC SpRegistrarExperienciaV2 1, '43453754', 4, 'OFIW', 'G200603PR504'
CREATE PROC [dbo].[SpRegistrarExperienciaV2]
(
	@intTipDoc		INT = 0,			-- ID del Tipo de Documento ( 0 : NA | 1 : DNI | 2 : OTRO )
	@strNroDoc		VARCHAR(12),		-- Numero de Documento,
	@intExperiencia INT = 0,			-- ID de Experiencia ( 0 : NA | 1 : No |  2 : Algo |  3 : Me Gusto |  4 : Me Encanto )
	@strTienda		VARCHAR(4) = '',	-- Codigo de Tienda
	@strHostname	VARCHAR(120) = ''	-- Nombre de Host
)
AS
BEGIN

	INSERT INTO Auditoria.Clientes_Experiencia(TipoDoc, NumDoc, Experiencia, FechaRegistro, Tienda, Hostname) 
	VALUES (@intTipDoc, @strNroDoc, @intExperiencia, GETDATE(), @strTienda, @strHostname)
	
	DECLARE	@REGISTRADO AS BIT
	SELECT	@REGISTRADO = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	
	SELECT	@REGISTRADO AS Registrado

END