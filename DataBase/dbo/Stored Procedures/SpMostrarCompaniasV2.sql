﻿-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Lista de todas las Compañias
--  Change history
--      21/12/2018  - Agregar comentarios - Afu 
CREATE PROC [dbo].[SpMostrarCompaniasV2]
AS
BEGIN
	SELECT	DISTINCT 
			UPPER(Compania) AS Compania 
	FROM	Perfil.Config WITH(NOLOCK)
	ORDER BY Compania
END