﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Validar si existe el Cliente y si es su cumpleaños
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpValidarClienteV2]
@strNroDoc AS VARCHAR(20),
@strAmbito AS VARCHAR(10)
AS
BEGIN 

	DECLARE @existeCliente AS BIT
	SELECT	@existeCliente = CASE WHEN COUNT(T0.NumDoc) > 0 THEN 1 ELSE 0 END
	FROM	Maestro.Clientes_Cupones T0 WITH(NOLOCK)
	WHERE	T0.NumDoc = @strNroDoc
	
	DECLARE @esCumpleanhos AS BIT
	SET		@esCumpleanhos = 0
	
	DECLARE @cuponCumpleanhos AS BIT
	SET		@cuponCumpleanhos = 0

	IF(@existeCliente = 1)
	BEGIN
	
		--Guardar sesion
		UPDATE	Maestro.Clientes_Cupones 
		SET		FEC_ULTSESION = GETDATE(),
				FEC_REGISTRO = GETDATE() 
		WHERE	NUMDOC = @strNroDoc
		
		SELECT	@esCumpleanhos = CASE WHEN COUNT(T0.NumDoc) > 0 THEN 1 ELSE 0 END
		FROM	Maestro.Clientes_Cupones T0 WITH(NOLOCK)
		WHERE	T0.NumDoc = @strNroDoc
		AND		DAY(T0.fec_nacimiento) = DAY(GETDATE())
		AND		MONTH(T0.fec_nacimiento) = MONTH(GETDATE())
		
		IF(@esCumpleanhos = 1)
		BEGIN
			
			SELECT	@cuponCumpleanhos = CASE WHEN COUNT(T0.NumDoc) > 0 THEN 1 ELSE 0 END
			FROM	Cupon.SuperCupones_Disponibles T0 WITH(NOLOCK)
			WHERE	T0.TipoCupon = 1 
			AND		T0.CantCuponesDispo = 1
			AND		T0.NumDoc = @strNroDoc 
			AND		T0.ambito = @strAmbito 
			
		END
		
	END

	SELECT	@existeCliente AS Valido,
			@esCumpleanhos AS EsCumpleanhos,
			@cuponCumpleanhos AS CuponCumpleanhos,
			GETDATE() AS FechaHoraConsulta

END