﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Registrar los datos de sesión del Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpRegistrarSesionV2]
(
	@strId						VARCHAR(40),		-- Id unico de sesion
	@strTienda					VARCHAR(4),			-- Codigo de Tienda
	@strHostname				VARCHAR(120),		-- Nombre de Host
	@intTipDoc					INT = 0,			-- ID del Tipo de Documento ( 0 : NA | 1 : DNI | 2 : OTRO )
	@strNroDoc					VARCHAR(12),		-- Numero de Documento
	@strInicio					VARCHAR(20),		-- Fecha y Hora de inicio de sesion (yyyy-MM-dd HH:mm:ss)
	@strFin						VARCHAR(20),		-- Fecha y Hora de fin de sesion (yyyy-MM-dd HH:mm:ss)
	@bitMisCupones				BIT = 0,			-- Selecciono la opcion de mis cupones
	@bitMiSuperCupon			BIT = 0,			-- Selecciono la opcion de super cupones
	@bitMarcasSeleccionadas		BIT = 0,			-- Selecciono la opcion de cupones de marcas
	@bitArbolDeCupones			BIT = 0,			-- Selecciono la opcion de arbos de cupones
	@bitCuponesDelLugar			BIT = 0,			-- Selecciono la opcion de cupones de lugar
	@bitRuletaDeCupones			BIT = 0,			-- Selecciono la opcion de ruleta de cupones
	@bitMisPuntosBonus			BIT = 0,			-- Selecciono la opcion de mis puntos bonus
	@bitMisInvitaciones			BIT = 0,			-- Selecciono la opcion de mis invitaciones
	@bitCuponesComerciales		BIT = 0,			-- Selecciono la opcion de cupones comerciales
	@strCuponesImpresos			VARCHAR(MAX) = '',	-- Codigos de cupones impresos separados por comas
	@strCuponesVistos			VARCHAR(MAX) = '',	-- Codigos de cupones vistos separados por comas
	@strBannersVistos			VARCHAR(MAX) = ''	-- Codigos de Banners vistos separados por comas
)
AS
BEGIN

	DECLARE	@dtInicio AS DATETIME
	DECLARE	@dtFin AS DATETIME
	
	SET @dtInicio = CONVERT(VARCHAR(20), @strInicio, 120)
	--SET @dtFin = CONVERT(VARCHAR(20), @strFin, 120)
	SET @dtFin = GETDATE()

	INSERT INTO Auditoria.Clientes_Sesion (Id, 
								Tienda, Hostname, 
								TipoDoc, NumDoc, 
								Inicio, Fin, 
								OpcionMisCupones, OpcionMiSuperCupon, OpcionMarcasSeleccionadas, 
								OpcionArbolDeCupones, OpcionCuponesDelLugar, OpcionRuletaDeCupones, 
								OpcionMisPuntosBonus, OpcionMisInvitaciones, OpcionCuponesComerciales, 
								CuponesImpresos, CuponesVistos, BannersVistos)
	VALUES (@strId, 
			@strTienda, @strHostname, 
			@intTipDoc, @strNroDoc, 
			@dtInicio, @dtFin, 
			@bitMisCupones, @bitMiSuperCupon, @bitMarcasSeleccionadas, 
			@bitArbolDeCupones, @bitCuponesDelLugar, @bitRuletaDeCupones, 
			@bitMisPuntosBonus, @bitMisInvitaciones, @bitCuponesComerciales,
			@strCuponesImpresos, @strCuponesVistos, @strBannersVistos)

	DECLARE	@REGISTRADO AS BIT
	SELECT	@REGISTRADO = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	
	SELECT	@REGISTRADO AS Registrado
END