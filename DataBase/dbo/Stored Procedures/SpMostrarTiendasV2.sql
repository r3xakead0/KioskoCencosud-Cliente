﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Lista de todas las Tiendas y Kioskos
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpMostrarTiendasV2]
AS
BEGIN
	SELECT  UPPER(T0.Compania) AS Compania,
			UPPER(T0.Tienda) AS Tienda,
			UPPER(T0.Hostname) AS Hostname 
	FROM	Perfil.Config T0 WITH(NOLOCK)
	GROUP BY T0.Compania, 
			T0.Tienda,
			T0.Hostname
	ORDER BY T0.Tienda
END