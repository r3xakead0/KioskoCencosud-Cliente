﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos del Perfil del Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpObtenerPerfilClienteV2] 
(
	@intTipDoc	INT = 0,		-- ID del Tipo de Documento ( 0 : NA | 1 : DNI | 2 : OTRO )
	@strNroDoc	VARCHAR(12),	-- Numero de Documento
	@strAmbito	VARCHAR(20)		-- Nombre de Empresa (WONG o METRO)
)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @esCumpleanhos AS BIT
	DECLARE @cuponCumpleanhos AS BIT
	
	SELECT	@esCumpleanhos = CASE WHEN COUNT(T0.NumDoc) > 0 THEN 1 ELSE 0 END
	FROM	Maestro.Clientes_Cupones T0 WITH(NOLOCK)
	WHERE	T0.NumDoc = @strNroDoc
	AND		DAY(T0.fec_nacimiento) = DAY(GETDATE())
	AND		MONTH(T0.fec_nacimiento) = MONTH(GETDATE())
	
	IF (@esCumpleanhos = 1)
	BEGIN
	
		SELECT	@cuponCumpleanhos = CASE WHEN COUNT(T0.NumDoc) > 0 THEN 1 ELSE 0 END
		FROM	[Cupon].[SuperCupones_Disponibles] T0 WITH(NOLOCK)
		WHERE	T0.TipoCupon = 1 
		AND		T0.CantCuponesDispo = 1
		AND		T0.NumDoc = @strNroDoc 
		AND		T0.ambito = @strAmbito 
		
	END 

	SELECT	TipoDocumento,
			NumeroDocumento,
			TiendaAdmitidas,
			Ambito,
			OpcionMisCupones,
			CASE WHEN @cuponCumpleanhos = 1 THEN @cuponCumpleanhos ELSE OpcionMiSuperCupon END AS OpcionMiSuperCupon,
			OpcionMarcasSeleccionadas,
			OpcionArbolDeCupones,
			OpcionCuponesDelLugar,
			OpcionRuletaDeCupones,
			OpcionMisPuntosBonus,
			OpcionMisInvitaciones,
			OpcionCuponesComerciales
	FROM	Perfil.PerfilCliente WITH(NOLOCK)
	WHERE	TipoDocumento = @intTipDoc
	AND		NumeroDocumento = @strNroDoc
	AND		Ambito = @strAmbito
END