﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Lista de Banners por Kiosko y/o Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
--  Testing
--      EXEC SpMostrarBannersV2 'OFIW', 'G200603PR504', '012345678'
--      EXEC SpMostrarBannersV2 'OFIW', 'G200603PR504'
--      EXEC SpMostrarBannersV2 'OFIW', 'G200603PR504', '43453754'
--      EXEC SpMostrarBannersV2 'OFIW', 'G200603PR501'
--      EXEC SpMostrarBannersV2 'OFIM', 'G200603PR504'
CREATE Proc [dbo].[SpMostrarBannersV2] 
(
	@strTienda		VARCHAR (5),		-- Codigo de Tienda, 
	@strHostname	VARCHAR (20),		-- Hostname del Kiosco 
	@strNroDoc		VARCHAR(12) = ''	-- Numero de Documento
)
AS 
BEGIN

	DECLARE	@FechaHora AS DATETIME
	DECLARE	@Fecha AS DATE
	DECLARE	@Hora AS TIME
	
	SET @FechaHora = GETDATE()
	--SET @FechaHora = '2018-10-07 09:00:00'
	SET @Fecha = CAST(@FechaHora AS DATE)
	SET @Hora = CAST(@FechaHora AS TIME)
	
	IF @strNroDoc = '' 
	BEGIN
	
		SELECT	T0.IdBanner,
				T0.Nombre,
				T0.Imagen,
				ROW_NUMBER() OVER (ORDER BY T0.Prioridad, T0.Tiempo ASC) AS Orden,
				T0.Prioridad,
				T0.Tiempo
		FROM	Maestro.Banner T0 
		INNER JOIN Banner.BannerKiosko T1 ON T1.IdBanner = T0.IdBanner
		INNER JOIN Banner.BannerKioskoProgramacion T2 ON T2.IdBannerKiosko = T1.IdBannerKiosko
		WHERE	T0.Activo = 1
		AND		T1.Tienda = @strTienda
		AND		T1.Hostname = @strHostname
		AND		@FechaHora BETWEEN T0.InicioVigencia AND T0.FinalVigencia 
		AND		T2.Fecha = @Fecha
		AND		@Hora BETWEEN T2.HoraInicio AND T2.HoraFin  
		AND		(T0.Tipo = 'A' OR T0.Tipo = 'E')
		ORDER BY T0.Prioridad, T0.Tiempo ASC

	END
	ELSE
	BEGIN
		
		SELECT	T0.IdBanner,
				T0.Nombre,
				T0.Imagen,
				ROW_NUMBER() OVER (ORDER BY T0.Prioridad, T0.Tiempo ASC) AS Orden,
				T0.Prioridad,
				T0.Tiempo
		FROM	Maestro.Banner T0 
		INNER JOIN Banner.BannerKiosko T1 ON T1.IdBanner = T0.IdBanner
		INNER JOIN Banner.BannerKioskoProgramacion T2 ON T2.IdBannerKiosko = T1.IdBannerKiosko
		INNER JOIN Banner.BannerCliente T3 ON T3.IdBanner = T1.IdBanner 
		WHERE	T0.Activo = 1
		AND		T1.Tienda = @strTienda
		AND		T1.Hostname = @strHostname
		AND		@FechaHora BETWEEN T0.InicioVigencia AND T0.FinalVigencia 
		AND		T2.Fecha = @Fecha
		AND		@Hora BETWEEN T2.HoraInicio AND T2.HoraFin  
		AND		T3.NroDocumento = @strNroDoc  
		AND		(T0.Tipo = 'A' OR T0.Tipo = 'I')
		ORDER BY T0.Prioridad, T0.Tiempo ASC
		
	END

END