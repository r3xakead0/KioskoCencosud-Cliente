﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Actualizar datos de configuración del Kiosko
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpActualizarConfigTiendaV2]
@strTienda AS VARCHAR(50),
@strHostname AS VARCHAR(120),
@intLongPapel AS DECIMAL(12,2),
@intLongCupon AS DECIMAL(12,2),
@intTotImpresiones AS DECIMAL(12,2),
@intRestImpresiones AS DECIMAL(12,2),
@strTelefono AS VARCHAR(50),
@strEmail AS VARCHAR(250)
AS
BEGIN
	UPDATE	Perfil.Config 
	SET		LongPapel = @intLongPapel,
			LongCupon = @intLongCupon,
			TotImpresiones = @intTotImpresiones,
			RestImpresiones = @intRestImpresiones,
			Telefono = @strTelefono,
			Email = @strEmail
	WHERE	Tienda = @strTienda
	AND		Hostname = @strHostname
END