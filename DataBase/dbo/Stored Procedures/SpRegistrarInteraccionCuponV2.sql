﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Registrar la interacción del Cupón
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
--  Testing
--      EXEC SpRegistrarInteraccionCuponV2 '43453754', 134580, 1, 1, 'OFIW'
--      EXEC SpRegistrarInteraccionCuponV2 '71844756', 134202, 1, 1, 'H005'
CREATE PROC [dbo].[SpRegistrarInteraccionCuponV2]
(
	@strNroDoc	VARCHAR(12),	-- Numero de Documento,
	@intCupon	BIGINT,			-- ID de Codigo de Campaña
	@intGrupo	INT,			-- ID del Grupo de Cupones 
	@intFlag	INT = 0,		-- ID de Interaccion ( 0 : NA | 1 : Interaccion | 2 : Imprimir )
	@strTienda	VARCHAR(50)		-- Codigo de Tienda
)
AS
BEGIN

	DECLARE @ambito AS VARCHAR(10)
	SELECT	@ambito = UPPER(Cadena) 
	FROM	Maestro.Tiendas WITH(NOLOCK) 
	WHERE	CodTienda = substring(@strTienda,1,4)

	IF @intGrupo = 1 -- Mis Cupones
	BEGIN

		IF @intFlag = 1 --Interaccion
		BEGIN
			IF (@ambito = 'WONG')
			BEGIN
				UPDATE	Cupon.Cupones_Disponibles_WONG 
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1 
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon 
			END
			ELSE IF (@ambito = 'METRO')
			BEGIN
				UPDATE	Cupon.Cupones_Disponibles_METRO
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1 
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon 
			END
		END

		ELSE IF @intFlag = 2 --Imprimir
		BEGIN
		
			IF (@ambito = 'WONG')
			BEGIN
				UPDATE	Cupon.Cupones_Disponibles_WONG 
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1, 
						FechaImpresion = GETDATE(),
						TiendaImpresion	= @strTienda
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon
			END
			ELSE IF (@ambito = 'METRO')
			BEGIN
				UPDATE	Cupon.Cupones_Disponibles_METRO
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1, 
						FechaImpresion = GETDATE(),
						TiendaImpresion	= @strTienda
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon
			END
		END
		
	END

	IF @intGrupo = 2 -- Super Cupones
	BEGIN
		IF @intFlag = 1 --Interaccion
		BEGIN
			UPDATE	Cupon.SuperCupones_Disponibles
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1 
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
		END

		ELSE IF @intFlag = 2 --Imprimir
		BEGIN
			UPDATE	Cupon.SuperCupones_Disponibles
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1, 
					FechaImpresion = GETDATE(),
					TiendaImpresion	= @strTienda
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
		END
				
	END

	IF @intGrupo = 3 -- Cupones de Marca
	BEGIN
		IF @intFlag = 1 --Interaccion
		BEGIN
			UPDATE	Cupon.Cupones_Marcas_Seleccionadas
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1 
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
		END

		ELSE IF @intFlag = 2 --Imprimir
		BEGIN
			UPDATE	Cupon.Cupones_Marcas_Seleccionadas
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1, 
					FechaImpresion = GETDATE(),
					TiendaImpresion	= @strTienda
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
		END
	END

	IF @intGrupo = 4 -- Cupones de Lugar
	BEGIN

		IF (@intFlag = 1) --Interaccion
		BEGIN
		
			UPDATE	Cupon.[Cupones_Otros_Establecimientos] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
			
		END
		
		ELSE IF (@intFlag = 2) --Imprimir
		BEGIN
		
			UPDATE	Cupon.[Cupones_Otros_Establecimientos] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1, 
					FechaImpresion = GETDATE(),
					TiendaImpresion	= @strTienda
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
							
		END

	END	

	--IF @intGrupo = 5 -- Cupones de Ruleta
	--BEGIN
	--END

	--IF @intGrupo = 6 -- Cupones de Arbol
	BEGIN
	
		IF (@intFlag = 1) --Interaccion
		BEGIN
		
			UPDATE	Cupon.[Arbol_Cupones_Disponibles] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1 
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
			
		END
		
		ELSE IF (@intFlag = 2) --Imprimir
		BEGIN
		
			UPDATE	Cupon.[Arbol_Cupones_Disponibles] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1, 
					FechaImpresion = GETDATE(),
					TiendaImpresion	= @strTienda
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
							
		END

	END	

	--IF @intGrupo = 7 -- Bonus
	--BEGIN
	--END

	IF @intGrupo = 8 -- Cupones de Invitacion
	BEGIN

		IF (@intFlag = 1) --Interaccion
		BEGIN
		
			UPDATE	Cupon.[Cupones_Invitaciones] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1 
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
			
		END
		
		ELSE IF (@intFlag = 2) --Imprimir
		BEGIN
		
			UPDATE	Cupon.[Cupones_Invitaciones] 
			SET		UltFechaInteraccion	= GETDATE(),
					Interaccion	= ISNULL(Interaccion,0)	+ 1, 
					FechaImpresion = GETDATE(),
					TiendaImpresion	= @strTienda
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon 
							
		END

	END

	IF @intGrupo = 9 -- Cupones Comerciales
	BEGIN

		IF @intFlag = 1 --Interaccion
		BEGIN

			IF (@ambito = 'WONG')
			BEGIN
				UPDATE	Cupon.Cupones_Comerciales_WONG
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1 
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon 
			END
			ELSE IF (@ambito = 'METRO')
			BEGIN
				UPDATE	Cupon.Cupones_Comerciales_METRO
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1 
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon 
			END

		END

		ELSE IF @intFlag = 2 --Imprimir
		BEGIN
		
			IF (@ambito = 'WONG')
			BEGIN
				UPDATE	Cupon.Cupones_Comerciales_WONG 
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1, 
						FechaImpresion = GETDATE(),
						TiendaImpresion	= @strTienda
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon
			END
			ELSE IF (@ambito = 'METRO')
			BEGIN
				UPDATE	Cupon.Cupones_Comerciales_METRO
				SET		UltFechaInteraccion	= GETDATE(),
						Interaccion	= ISNULL(Interaccion,0)	+ 1, 
						FechaImpresion = GETDATE(),
						TiendaImpresion	= @strTienda
				WHERE	NumDoc		= @strNroDoc 
				AND		CodCampania	= @intCupon
			END

		END

	END

	DECLARE	@REGISTRADO AS BIT
	SET @REGISTRADO = 1
	--SELECT	@REGISTRADO = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	
	SELECT	@REGISTRADO AS Registrado	
END