﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Lista de Cupones del Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu 
--  Testing
--      EXEC SpMostrarCuponesV2 '43453754',1,'WONG','T104'
--      EXEC SpMostrarCuponesV2 '71844756',1,'METRO','H001'
--      EXEC SpMostrarCuponesV2 '43453754',2,'WONG','T104', 2
CREATE PROC [dbo].[SpMostrarCuponesV2]
@strNroDoc AS VARCHAR(20),
@intGrupo AS INT,
@strAmbito AS VARCHAR(10),
@strTienda AS VARCHAR(10),
@intTipo AS INT = 0
AS
BEGIN

	DECLARE	@MostrarCupones AS TABLE (
		IdKiosko BIGINT,
		CodCampania BIGINT,
		Descuento VARCHAR(100),
		Mensaje1 VARCHAR(50),
		Mensaje2 VARCHAR(50),
		Mensaje3 VARCHAR(50),
		Imagen VARCHAR(255),
		Descripcion VARCHAR(MAX),
		Prioridad INT,
		Disponibles INT,
		Redimidos INT,
		FechaRedencion VARCHAR(10),
		HoraRedencion VARCHAR(10),
		ExclusivoApp BIT
	)

	IF @intGrupo = 1 -- Mis Cupones
	BEGIN

		IF UPPER(@strAmbito) = 'WONG' 
		BEGIN
			INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
			SELECT	TOP 32
					IdKiosko,
					CodCampania,
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					CodImagen AS Imagen,
					Legal AS Descripcion,
					Prioridad,
					CantCuponesDispo AS Disponibles,
					0 AS Redimidos,
					'' AS FechaRedencion,
					'' AS HoraRedencion, 
					ISNULL(ExclusivoApp,0) AS ExclusivoApp
			FROM	Cupon.Cupones_Disponibles_WONG WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		Ambito = @strAmbito
			AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
			AND		CantCuponesDispo = 1 
			AND		GETDATE() 
					BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
					AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
			ORDER BY Prioridad, 
					FechaFin	
			
		END

		IF UPPER(@strAmbito) = 'METRO' 
		BEGIN
			INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
			SELECT	TOP 32
					IdKiosko,
					CodCampania,
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					CodImagen AS Imagen,
					Legal AS Descripcion,
					Prioridad,
					CantCuponesDispo AS Disponibles,
					0 AS Redimidos,
					'' AS FechaRedencion,
					'' AS HoraRedencion, 
					ISNULL(ExclusivoApp,0) AS ExclusivoApp
			FROM	Cupon.Cupones_Disponibles_METRO WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		Ambito = @strAmbito
			AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
			AND		CantCuponesDispo = 1 
			AND		GETDATE() 
					BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
					AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
			ORDER BY Prioridad, 
					FechaFin	
		END
	END

	IF @intGrupo = 2 -- Super Cupones
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 1
				IdKiosko,
				CodCampania,
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				CodImagen AS Imagen,
				Legal AS Descripcion,
				0 AS Prioridad,
				CantCuponesDispo AS Disponibles,
				0 AS Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion, 
				0 AS ExclusivoApp
		FROM	Cupon.SuperCupones_Disponibles WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		Ambito = @strAmbito
		AND		TipoCupon = @intTipo
		AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		CantCuponesDispo = 1 
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY Prioridad, 
				FechaFin	
	END

	IF @intGrupo = 3 -- Cupones de Marca
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 32
				IdKiosko,
				CodCampania,
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				CodImagen AS Imagen,
				Legal AS Descripcion,
				Prioridad,
				CantCuponesDispo AS Disponibles,
				0 AS Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion, 
				0 AS ExclusivoApp
		FROM	Cupon.Cupones_Marcas_Seleccionadas WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		Ambito = @strAmbito
		AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		CantCuponesDispo = 1 
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY Prioridad, 
				FechaFin	
	END

	IF @intGrupo = 4 -- Cupones de Lugar
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 32
				IdKiosko,
				CodCampania,
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				CodImagen AS Imagen,
				Legal AS Descripcion,
				Prioridad,
				CantCuponesDispo AS Disponibles,
				0 AS Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion, 
				0 AS ExclusivoApp
		FROM	Cupon.Cupones_Otros_Establecimientos WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		Ambito = @strAmbito
		AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		CantCuponesDispo = 1 
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY Prioridad, 
				FechaFin
	END
	--IF @intGrupo = 5 -- Cupones de Ruleta
	--BEGIN
		--Falta Implementar
	--END	
	IF @intGrupo = 6 -- Cupones de Arbol
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 32
				T0.idKiosko AS IdKiosko,
				T0.CodCampania,
				T0.Descuento,
				T0.Mensaje AS Mensaje1,
				T0.Mensaje1 AS Mensaje2,
				T0.Mensaje2 AS Mensaje3,
				T0.CodImagen AS Imagen,
				'' AS Descripcion,
				T0.Prioridad,
				T0.CantCuponesDispo AS Disponibles,
				T0.Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion,
				0 AS ExclusivoApp
		FROM	Cupon.Arbol_Cupones_Disponibles T0 WITH(NOLOCK)
		WHERE	T0.NumDoc = @strNroDoc 
		AND		T0.Ambito = @strAmbito 
		and		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY T0.Prioridad, 
				T0.FechaFin	
	END

	IF @intGrupo = 7 -- Cupones con Bonus
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 32
				IdKiosko,
				CodCampania,
				Descuento,
				Mensaje1,
				Mensaje2,
				Mensaje3,
				CodImagen AS Imagen,
				Legal AS Descripcion,
				Prioridad,
				CantCuponesDispo AS Disponibles,
				0 AS Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion, 
				ISNULL(ExclusivoApp,0) AS ExclusivoApp
		FROM	Cupon.Cupones_Bonus WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		Ambito = @strAmbito
		AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		CantCuponesDispo = 1 
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY Prioridad, 
				FechaFin	
	END	

	IF @intGrupo = 8 -- Cupones de Invitacion
	BEGIN
		INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
		SELECT	TOP 32
				IdKiosko,
				CodCampania,
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				CodImagen AS Imagen,
				Legal AS Descripcion,
				Prioridad,
				CantCuponesDispo AS Disponibles,
				0 AS Redimidos,
				'' AS FechaRedencion,
				'' AS HoraRedencion, 
				0 AS ExclusivoApp
		FROM	Cupon.Cupones_Invitaciones WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		Ambito = @strAmbito
		AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
		AND		CantCuponesDispo = 1 
		AND		GETDATE() 
				BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
				AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
		ORDER BY Prioridad, 
				FechaFin	
	END

	IF @intGrupo = 9 -- Cupones Comerciales
	BEGIN

		IF UPPER(@strAmbito) = 'WONG' 
		BEGIN
			INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
			SELECT	TOP 32
					IdKiosko,
					CodCampania,
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					CodImagen AS Imagen,
					Legal AS Descripcion,
					Prioridad,
					CantCuponesDispo AS Disponibles,
					0 AS Redimidos,
					'' AS FechaRedencion,
					'' AS HoraRedencion, 
					0 AS ExclusivoApp
			FROM	Cupon.Cupones_Comerciales_WONG WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
			AND		CantCuponesDispo = 1 
			AND		GETDATE() 
					BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
					AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
			ORDER BY Prioridad, 
					FechaFin	
			
		END

		IF UPPER(@strAmbito) = 'METRO' 
		BEGIN
			INSERT INTO @MostrarCupones (IdKiosko,CodCampania,Descuento,Mensaje1,Mensaje2,Mensaje3,Imagen,Descripcion,Prioridad,Disponibles,Redimidos,FechaRedencion,HoraRedencion,ExclusivoApp)
			SELECT	TOP 32
					IdKiosko,
					CodCampania,
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					CodImagen AS Imagen,
					Legal AS Descripcion,
					Prioridad,
					CantCuponesDispo AS Disponibles,
					0 AS Redimidos,
					'' AS FechaRedencion,
					'' AS HoraRedencion, 
					0 AS ExclusivoApp
			FROM	Cupon.Cupones_Comerciales_METRO WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		Kiosko.FnValidaTienda(MD_Permitidos,@strTienda) = 1
			AND		CantCuponesDispo = 1 
			AND		GETDATE() 
					BETWEEN DATEADD(DAY, 0 - VisualizacionAnticipada, CAST(FechaIni AS DATETIME) + CAST(HoraIni AS DATETIME))
					AND CAST(FechaFin AS DATETIME) + CAST(HoraFin AS DATETIME) 
			ORDER BY Prioridad, 
					FechaFin	
		END
	END

	SELECT	IdKiosko,
			CodCampania,
			Descuento,
			ISNULL(Mensaje1,'') AS Mensaje1,
			ISNULL(Mensaje2,'') AS Mensaje2,
			ISNULL(Mensaje3,'') AS Mensaje3,
			Imagen,
			Descripcion,
			Prioridad,
			Disponibles,
			Redimidos,
			FechaRedencion,
			HoraRedencion, 
			ExclusivoApp
	FROM	@MostrarCupones

END