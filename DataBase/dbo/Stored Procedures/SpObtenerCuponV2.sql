﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos del Cupón
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
--  Testing
--      EXEC SpObtenerCuponV2 '012345678',136269,2,2
--      EXEC SpObtenerCuponV2 '43453754',134526,1
--      EXEC SpObtenerCuponV2 '71844756',137466,2,2
CREATE PROC [dbo].[SpObtenerCuponV2]
(
	@strNroDoc	VARCHAR(20),	-- Numero de Documento
	@intCupon	BIGINT,			-- ID de Codigo de Campaña
	@intGrupo	INT,			-- ID del Grupo de Cupones 
	@intTipo	INT = 0			--  Tipo de Super Cupon ( 0 : NA | 1 : Cupon Cumpleaños | 2 : Super Cupon )
)
AS
BEGIN
 
	DECLARE @BonusPrincipalSeriado AS VARCHAR(20)
	DECLARE @BonusTitularSeriado AS VARCHAR(20)

	SELECT	TOP 1 
			@BonusPrincipalSeriado = BonusPrincipal,
			@BonusTitularSeriado = BonusTitular 
	FROM	Maestro.Clientes_Cupones WITH(NOLOCK) 
	WHERE	NumDoc = @strNroDoc

	DECLARE	@ObtenerCupon AS TABLE (
		Descuento VARCHAR(100),
		Mensaje1 VARCHAR(50),
		Mensaje2 VARCHAR(50),
		Mensaje3 VARCHAR(50),
		Ambito VARCHAR(50),
		TipoLegal INT,
		MensajeLegal VARCHAR(MAX),
		Imagen VARCHAR(500),
		CodigoBarra VARCHAR(50),
		CodigoCanal INT,
		FlagSeriado INT,
		BonusSeriado VARCHAR(20),
		BonusTitularSeriado VARCHAR(100),
		CodigoOnline VARCHAR(50),
		Vigencia VARCHAR(255),
		Tipo INT,
		CodEmision BIGINT
	)

	IF @intGrupo = 1 -- Mis Cupones
	BEGIN

		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				'WONG' AS Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				ISNULL(CodEmision,0) AS CodEmision
		FROM	Cupon.Cupones_Disponibles_WONG WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
		AND		ISNULL(ExclusivoApp,0) = 0

		IF NOT EXISTS(SELECT Descuento FROM @ObtenerCupon)
		BEGIN
			INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
			SELECT	TOP 1
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					'METRO' AS Ambito,
					TipoMensajeLegal AS TipoLegal,
					Legal AS MensajeLegal,
					LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
					CodBarraOffline AS CodigoBarra,
					FlagCanal AS CodigoCanal,
					Seriado AS FlagSeriado,
					BonusSeriado,
					@BonusTitularSeriado AS BonusTitularSeriado,
					CodOnline AS CodigoOnline,
					'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
					0 AS Tipo,
					ISNULL(CodEmision,0) AS CodEmision
			FROM	Cupon.Cupones_Disponibles_METRO WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		CodCampania = @intCupon  
			AND		CantCuponesDispo = 1
			AND		ISNULL(ExclusivoApp,0) = 0
		END
	END

	IF @intGrupo = 2 -- Super Cupones
	BEGIN

		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				TipoCupon AS Tipo,
				ISNULL(CodEmision,0) AS CodEmision
		FROM	Cupon.SuperCupones_Disponibles WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
		AND		TipoCupon = ISNULL(@intTipo, 0)

	END

	IF @intGrupo = 3 -- Cupones de Marca
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				ISNULL(CodEmision,0) AS CodEmision
		FROM	Cupon.Cupones_Marcas_Seleccionadas WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
	END
	
	IF @intGrupo = 4 -- Cupones de Lugar
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				0 AS CodEmision
		FROM	Cupon.Cupones_Otros_Establecimientos WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
	END

	--IF @intGrupo = 5 -- Cupones de Ruleta
	--BEGIN
		--Falta Implementar
	--END	

	IF @intGrupo = 6 -- Cupones de Arbol
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				0 AS CodEmision
		FROM	Cupon.Arbol_Cupones_Disponibles WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
	END

	IF @intGrupo = 7 -- Cupones con Bonus
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Mensaje1,
				Mensaje2,
				Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				0 AS CodEmision
		FROM	Cupon.Cupones_Bonus WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
	END	

	IF @intGrupo = 8 -- Cupones de Invitacion
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				ISNULL(CodEmision,0) AS CodEmision
		FROM	Cupon.Cupones_Invitaciones WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1
	END
	
	IF @intGrupo = 9 -- Cupones Comerciales
	BEGIN
		INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
		SELECT	TOP 1
				Descuento,
				Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
				Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
				Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
				'WONG' AS Ambito,
				TipoMensajeLegal AS TipoLegal,
				Legal AS MensajeLegal,
				LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
				CodBarraOffline AS CodigoBarra,
				FlagCanal AS CodigoCanal,
				Seriado AS FlagSeriado,
				BonusSeriado,
				@BonusTitularSeriado AS BonusTitularSeriado,
				CodOnline AS CodigoOnline,
				'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
				0 AS Tipo,
				ISNULL(CodEmision,0) AS CodEmision
		FROM	Cupon.Cupones_Comerciales_WONG WITH(NOLOCK)
		WHERE	NumDoc = @strNroDoc 
		AND		CodCampania = @intCupon  
		AND		CantCuponesDispo = 1

		IF NOT EXISTS(SELECT Descuento FROM @ObtenerCupon)
		BEGIN
			INSERT INTO @ObtenerCupon (Descuento,Mensaje1,Mensaje2,Mensaje3,Ambito,TipoLegal,MensajeLegal,Imagen,CodigoBarra,CodigoCanal,FlagSeriado,BonusSeriado,BonusTitularSeriado,CodigoOnline,Vigencia,Tipo,CodEmision)
			SELECT	TOP 1
					Descuento,
					Kiosko.FnMensajesCupon(Mensaje,1) AS Mensaje1,
					Kiosko.FnMensajesCupon(Mensaje,2) AS Mensaje2,
					Kiosko.FnMensajesCupon(Mensaje,3) AS Mensaje3,
					'METRO' AS Ambito,
					TipoMensajeLegal AS TipoLegal,
					Legal AS MensajeLegal,
					LEFT(CodImagen,LEN(CodImagen) - 4) + '-ISO' + RIGHT(CodImagen,4) AS Imagen,
					CodBarraOffline AS CodigoBarra,
					FlagCanal AS CodigoCanal,
					Seriado AS FlagSeriado,
					BonusSeriado,
					@BonusTitularSeriado AS BonusTitularSeriado,
					CodOnline AS CodigoOnline,
					'VALIDA DEL ' + CONVERT(VARCHAR(10),FechaIni,103) + ' AL ' + CONVERT(VARCHAR(10),FechaFin,103) AS Vigencia,
					0 AS Tipo,
					ISNULL(CodEmision,0) AS CodEmision
			FROM	Cupon.Cupones_Comerciales_METRO WITH(NOLOCK)
			WHERE	NumDoc = @strNroDoc 
			AND		CodCampania = @intCupon  
			AND		CantCuponesDispo = 1
		END
	END

	SELECT	Descuento,
			ISNULL(Mensaje1,'') AS Mensaje1,
			ISNULL(Mensaje2,'') AS Mensaje2,
			ISNULL(Mensaje3,'') AS Mensaje3,
			Ambito,
			TipoLegal,
			MensajeLegal,
			Imagen,
			CodigoBarra,
			CodigoCanal,
			FlagSeriado,
			BonusSeriado,
			BonusTitularSeriado,
			CodigoOnline,
			Vigencia,
			Tipo,
			CodEmision 
	FROM	@ObtenerCupon

END