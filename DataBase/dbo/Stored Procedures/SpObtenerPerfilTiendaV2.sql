﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos del Perfil de la Tienda
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpObtenerPerfilTiendaV2] 
(
	@strTienda	VARCHAR (4)	-- Codigo de Tienda	
)
AS
BEGIN
	SELECT	TiendaId,
			Ambito,
			OpcionMisCupones,
			OpcionMiSuperCupon,
			OpcionMarcasSeleccionadas,
			OpcionArbolDeCupones,
			OpcionCuponesDelLugar,
			OpcionRuletaDeCupones,
			OpcionMisPuntosBonus,
			OpcionMisInvitaciones,
			OpcionCuponesComerciales 
	FROM	Perfil.PerfilTienda WITH(NOLOCK)
	WHERE	TiendaId = @strTienda
END