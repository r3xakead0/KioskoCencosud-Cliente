﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Quitar la disponibilidad del Cupón
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
--  Testing
--      EXEC SpQuitarDisponibilidadCuponV2 '43453754',134580,1, '2400029492387'
CREATE PROC [dbo].[SpQuitarDisponibilidadCuponV2]
(
	@strNroDoc		VARCHAR(12),		-- Numero de Documento,
	@intCupon		BIGINT,				-- ID de Codigo de Campaña
	@intGrupo		INT,				-- ID del Grupo de Cupones 
	@intTipo		INT = 0,			-- Tipo de Super Cupon ( 0 : NA | 1 : Cupon Cumpleaños | 2 : Super Cupon )
	@strCodBarra	VARCHAR(50) = ''	-- Codigo de Barra
)
AS
BEGIN

	DECLARE @RPTA AS BIT
	SET @RPTA = 1

	IF @intGrupo = 1 -- Mis Cupones
	BEGIN
		UPDATE	Cupon.Cupones_Disponibles_WONG 
		SET		CantCuponesDispo = 0,
				FechaImpresion = GETDATE(),
				CodBarraOffline = @strCodBarra
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 

		UPDATE	Cupon.Cupones_Disponibles_METRO
		SET		CantCuponesDispo = 0,
				FechaImpresion = GETDATE(),
				CodBarraOffline = @strCodBarra
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 
	END

	IF @intGrupo = 2 -- Super Cupones
	BEGIN
		UPDATE	Cupon.SuperCupones_Disponibles
		SET		CantCuponesDispo = 0,
				FechaImpresion = GETDATE(),
				CodBarraOffline = @strCodBarra
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 
		AND		TipoCupon = @intTipo
	END

	IF @intGrupo = 3 -- Cupones de Marca
	BEGIN
		UPDATE	Cupon.Cupones_Marcas_Seleccionadas
		SET		CantCuponesDispo = 0,
				FechaImpresion = GETDATE(),
				CodBarraOffline = @strCodBarra
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 
	END

	IF @intGrupo = 4 -- Cupones de Lugar
	BEGIN
		UPDATE	Cupon.[Cupones_Otros_Establecimientos] 
		SET		CantCuponesDispo = 0, 
				CodBarraOffline = @strCodBarra 
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 
	END

	--IF @intGrupo = 5 -- Cupones de Ruleta
	--BEGIN
	--END

	--IF @intGrupo = 6 -- Cupones de Arbol
	BEGIN
		UPDATE	Cupon.[Arbol_Cupones_Disponibles] 
		SET		CantCuponesDispo = 0, 
				CodBarraOffline = @strCodBarra 
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc  
	END	

	--IF @intGrupo = 7 -- Bonus
	--BEGIN
	--END

	IF @intGrupo = 8 -- Cupones de Invitacion
	BEGIN
		UPDATE	Cupon.Cupones_Invitaciones
		SET		CantCuponesDispo = 0,
				FechaImpresion = GETDATE(),
				CodBarraOffline = @strCodBarra
		WHERE	CodCampania = @intCupon
		AND		NumDoc = @strNroDoc 
	END

	IF (@intGrupo = 9) --Cupones Comerciales
	BEGIN
	
		IF (
			SELECT	COUNT(1)
			FROM	Cupon.[Cupones_Comerciales_WONG] WITH(NOLOCK)
			WHERE	NumDoc		= @strNroDoc 
			AND		CodCampania	= @intCupon ) > 0
		BEGIN
		
			UPDATE	Cupon.[Cupones_Comerciales_WONG] 
			SET		CantCuponesDispo = 0,
					CodBarraOffline = @strCodBarra 
			WHERE	NumDoc = @strNroDoc
			AND		CodCampania	= @intCupon 
			
		END
		ELSE
		BEGIN
		
			UPDATE	Cupon.[Cupones_Comerciales_METRO] 
			SET		CantCuponesDispo = 0,
					CodBarraOffline = @strCodBarra 
			WHERE	NumDoc = @strNroDoc
			AND		CodCampania	= @intCupon 
			
		END

	END	

	--SELECT	@RPTA = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	SELECT @RPTA AS Registrado
END