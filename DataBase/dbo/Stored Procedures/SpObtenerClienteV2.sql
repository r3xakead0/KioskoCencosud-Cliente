﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos del cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpObtenerClienteV2]
@strNroDoc AS VARCHAR(20) -- Numero de Documento
AS
BEGIN 
	SELECT	TOP 1 
			TipoDoc AS TipoDocumento,
			NumDoc AS NumeroDocumento,
			Nombres,
			Cant_METRO AS CuponesAsignadosMetro,
			Cant_WONG AS CuponesAsignadosWong
	FROM	Maestro.Clientes_Cupones WITH(NOLOCK)
	WHERE	NumDoc = @strNroDoc
END