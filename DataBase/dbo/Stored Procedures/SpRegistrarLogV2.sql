﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Registrar el mensaje de error en el Log
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpRegistrarLogV2]
(
	@strTienda		VARCHAR(4),		-- Codigo de Tienda
	@strHostname	VARCHAR(120),	-- Nombre de Host
	@intTipo		TINYINT,		-- Tipo de Error ( 1 : Tecnico | 2 : Usuario )
	@strMensaje		VARCHAR(255),	-- Mensaje de Log
	@strDescripcion	VARCHAR(MAX)	-- Descripcion detallada de Log
)
AS
BEGIN

	INSERT INTO Auditoria.Kiosko_Log (Tienda,Hostname,Tipo,Fecha,Mensaje,Descripcion)
	VALUES (@strTienda,@strHostname,@intTipo,GETDATE(),@strMensaje,@strDescripcion)

	DECLARE	@REGISTRADO AS BIT
	SELECT	@REGISTRADO = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	
	SELECT	@REGISTRADO AS Registrado
END