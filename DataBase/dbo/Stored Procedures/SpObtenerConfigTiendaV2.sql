﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos de la configuración del Kiosko
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpObtenerConfigTiendaV2]
(
	@strTienda		VARCHAR(4),	-- Codigo de Tienda
	@strHostname	VARCHAR(120)-- Nombre de Host
)
AS
BEGIN
	SELECT	TOP 1 
			UPPER(Compania) AS Compania,
			UPPER(Tienda) AS Tienda,
			Hostname,
			Ubicacion,
			CAST(LongPapel AS INTEGER) AS LongPapel,
			CAST(LongCupon AS INTEGER) AS LongCupon,
			CAST(TotImpresiones AS INTEGER) AS TotImpresiones,
			CAST(RestImpresiones AS INTEGER) AS RestImpresiones,
			Telefono,
			Email,
			Servidor,
			Puerto, 
			EmailSalida,
			Pass,
			PassAplicacion 
	FROM	Perfil.Config WITH(NOLOCK)
	WHERE	Activo = 1 
	AND		Tienda = @strTienda
	AND		Hostname = @strHostname
END