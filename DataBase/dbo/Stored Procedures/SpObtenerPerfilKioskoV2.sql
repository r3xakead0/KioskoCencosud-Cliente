﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Obtener datos del Perfil del Kiosko
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpObtenerPerfilKioskoV2] 
(
	@strTienda	VARCHAR (4),	-- Codigo de Tienda	
	@strHostname VARCHAR(120)	-- Nombre de Host	
)
AS
BEGIN
	SELECT	T0.TiendaKioskoId,
			T0.TiendaId,
			T0.Ambito,
			T0.HostName,
			T0.DireccionIP,
			T0.Zona,
			T0.ImagenBanner,
			T0.FechaIniBanner,
			T0.FechaFinBanner,
			T1.ImpresoraId,
			T1.Marca,
			T1.Modelo,
			T1.MargenIzquierdoX,
			T1.MargenSuperiorY,
			T1.Ancho,
			T1.Alto
	FROM	Perfil.PerfilKiosko T0 WITH(NOLOCK)
	INNER JOIN	Perfil.Impresora T1 WITH(NOLOCK) ON T1.ImpresoraId = T0.ImpresoraId
	WHERE	T0.TiendaId = @strTienda
	AND		T0.HostName = @strHostname	 
END