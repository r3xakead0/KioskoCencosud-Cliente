﻿
-- Stored Procedure
--    Author:           Afu Tse
--    Create date:      21/12/2018
--    Description:      Contador de Vistas de Banners por Cliente
--  Change history
--      21/12/2018  - Agregar comentarios - Afu Tse
CREATE PROC [dbo].[SpContarBannersV2]
(
	@strBannersVistos	VARCHAR(MAX)	-- Codigos de Banners vistos separados por comas
)
AS 
BEGIN

	DECLARE	@REGISTRADO AS BIT
	SET @REGISTRADO = 0

	IF LEN(@strBannersVistos) > 0
	BEGIN
		UPDATE	Maestro.Banner 
		SET		VistasActual = VistasActual + 1
		WHERE	IdBanner IN (SELECT Value FROM dbo.fn_split(@strBannersVistos,','))

		SELECT	@REGISTRADO = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END 
	END

	SELECT	@REGISTRADO AS Registrado

END