﻿CREATE TABLE [Auditoria].[Clientes_Sesion] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [Tienda]                    VARCHAR (50)     NOT NULL,
    [Hostname]                  VARCHAR (120)    NOT NULL,
    [TipoDoc]                   INT              NOT NULL,
    [NumDoc]                    VARCHAR (20)     NOT NULL,
    [Inicio]                    DATETIME         NOT NULL,
    [Fin]                       DATETIME         NOT NULL,
    [OpcionMisCupones]          BIT              NOT NULL,
    [OpcionMiSuperCupon]        BIT              NOT NULL,
    [OpcionMarcasSeleccionadas] BIT              NOT NULL,
    [OpcionArbolDeCupones]      BIT              NOT NULL,
    [OpcionCuponesDelLugar]     BIT              NOT NULL,
    [OpcionRuletaDeCupones]     BIT              NOT NULL,
    [OpcionMisPuntosBonus]      BIT              NOT NULL,
    [OpcionMisInvitaciones]     BIT              NOT NULL,
    [CuponesImpresos]           VARCHAR (MAX)    NULL,
    [CuponesVistos]             VARCHAR (MAX)    NULL,
    [OpcionCuponesComerciales]  BIT              NULL,
    [BannersVistos]             VARCHAR (MAX)    NULL
);

