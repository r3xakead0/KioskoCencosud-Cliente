﻿CREATE TABLE [Auditoria].[Kiosko_Log] (
    [Tienda]      VARCHAR (50)  NOT NULL,
    [Hostname]    VARCHAR (120) NOT NULL,
    [Tipo]        TINYINT       NOT NULL,
    [Fecha]       DATETIME      NOT NULL,
    [Mensaje]     VARCHAR (255) NOT NULL,
    [Descripcion] VARCHAR (MAX) NOT NULL
);

