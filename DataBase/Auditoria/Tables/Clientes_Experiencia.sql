﻿CREATE TABLE [Auditoria].[Clientes_Experiencia] (
    [TipoDoc]       INT           NOT NULL,
    [NumDoc]        VARCHAR (20)  NOT NULL,
    [Experiencia]   INT           NOT NULL,
    [FechaRegistro] DATETIME      NOT NULL,
    [Tienda]        VARCHAR (50)  NULL,
    [Hostname]      VARCHAR (120) NULL
);

