﻿CREATE TABLE [Auditoria].[Kiosko_Monitoreo] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Empresa]   VARCHAR (50)  NOT NULL,
    [Tienda]    VARCHAR (50)  NOT NULL,
    [Hostname]  VARCHAR (120) NOT NULL,
    [Ip]        VARCHAR (20)  NOT NULL,
    [Conexion]  BIT           NOT NULL,
    [Version]   VARCHAR (20)  NOT NULL,
    [Sesion]    VARCHAR (20)  NULL,
    [Impreso]   INT           NOT NULL,
    [Restante]  INT           NOT NULL,
    [FechaHora] DATETIME      NOT NULL
);

