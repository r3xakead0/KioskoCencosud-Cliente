﻿CREATE TABLE [Perfil].[PerfilTienda] (
    [TiendaId]                  VARCHAR (50)   NULL,
    [Ambito]                    VARCHAR (8000) NULL,
    [OpcionMisCupones]          TINYINT        NULL,
    [OpcionMiSuperCupon]        TINYINT        NULL,
    [OpcionMarcasSeleccionadas] TINYINT        NULL,
    [OpcionArbolDeCupones]      TINYINT        NULL,
    [OpcionCuponesDelLugar]     TINYINT        NULL,
    [OpcionRuletaDeCupones]     TINYINT        NULL,
    [OpcionMisPuntosBonus]      TINYINT        NULL,
    [OpcionMisInvitaciones]     TINYINT        NULL,
    [OpcionCuponesComerciales]  TINYINT        NULL
);

