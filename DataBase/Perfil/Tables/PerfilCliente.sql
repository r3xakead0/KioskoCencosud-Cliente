﻿CREATE TABLE [Perfil].[PerfilCliente] (
    [TipoDocumento]             INT            NULL,
    [NumeroDocumento]           VARCHAR (20)   NULL,
    [TiendaAdmitidas]           VARCHAR (5000) NULL,
    [Ambito]                    VARCHAR (8000) NULL,
    [OpcionMisCupones]          BIT            NULL,
    [OpcionMiSuperCupon]        BIT            NULL,
    [OpcionMarcasSeleccionadas] BIT            NULL,
    [OpcionArbolDeCupones]      BIT            NULL,
    [OpcionCuponesDelLugar]     BIT            NULL,
    [OpcionRuletaDeCupones]     BIT            NULL,
    [OpcionMisPuntosBonus]      BIT            NULL,
    [OpcionMisInvitaciones]     BIT            NULL,
    [OpcionCuponesComerciales]  BIT            NULL
);


GO
CREATE NONCLUSTERED INDEX [IDX_cliente]
    ON [Perfil].[PerfilCliente]([NumeroDocumento] ASC);

