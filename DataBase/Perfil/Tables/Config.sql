﻿CREATE TABLE [Perfil].[Config] (
    [id]              INT             IDENTITY (1, 1) NOT NULL,
    [Compania]        VARCHAR (50)    NULL,
    [Tienda]          VARCHAR (50)    NULL,
    [Ubicacion]       VARCHAR (100)   NULL,
    [LongPapel]       DECIMAL (10, 2) NULL,
    [LongCupon]       DECIMAL (10, 2) NULL,
    [TotImpresiones]  DECIMAL (10, 2) NULL,
    [RestImpresiones] DECIMAL (10, 2) NULL,
    [Telefono]        VARCHAR (50)    NULL,
    [Email]           VARCHAR (250)   NULL,
    [Servidor]        VARCHAR (150)   NULL,
    [Puerto]          INT             NULL,
    [EmailSalida]     VARCHAR (250)   NULL,
    [Pass]            VARCHAR (250)   NULL,
    [PassAplicacion]  VARCHAR (6)     NULL,
    [Hostname]        VARCHAR (120)   NULL,
    [Activo]          BIT             NOT NULL,
    CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED ([id] ASC)
);

