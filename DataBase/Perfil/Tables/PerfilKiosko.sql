﻿CREATE TABLE [Perfil].[PerfilKiosko] (
    [TiendaKioskoId] INT            IDENTITY (1, 1) NOT NULL,
    [TiendaId]       VARCHAR (50)   NULL,
    [Ambito]         VARCHAR (8000) NULL,
    [HostName]       VARCHAR (120)  NULL,
    [DireccionIP]    VARCHAR (120)  NULL,
    [Zona]           VARCHAR (120)  NULL,
    [ImagenBanner]   VARCHAR (8000) NULL,
    [FechaIniBanner] DATETIME       NULL,
    [FechaFinBanner] DATETIME       NULL,
    [ImpresoraId]    INT            NULL
);

