﻿CREATE TABLE [Perfil].[Impresora] (
    [ImpresoraId]      INT           IDENTITY (1, 1) NOT NULL,
    [Marca]            VARCHAR (120) NULL,
    [Modelo]           VARCHAR (120) NULL,
    [MargenIzquierdoX] INT           NULL,
    [MargenSuperiorY]  INT           NULL,
    [Ancho]            INT           NULL,
    [Alto]             INT           NULL,
    CONSTRAINT [PK_Impresora] PRIMARY KEY NONCLUSTERED ([ImpresoraId] ASC)
);

