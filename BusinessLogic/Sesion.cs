﻿using System;
using DA = Cupones.DataAccess;
using DE = Cupones.DataEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Sesion : Base
    {

        private string tienda = "";
        private string hostname = "";

        public Sesion(string tienda, string hostname)
        {
            this.tienda = tienda;
            this.hostname = hostname;
        }

        public bool RegistrarSesion(Guid id, 
                                    DateTime inicio, DateTime fin, 
                                    string nroDocumento, int tipoDocumento,
                                    bool OpcionMisCupones, bool OpcionMiSuperCupon,
                                    bool OpcionMarcasSeleccionadas, bool OpcionArbolDeCupones,
                                    bool OpcionCuponesDelLugar, bool OpcionRuletaDeCupones,
                                    bool OpcionMisPuntosBonus, bool OpcionMisInvitaciones,
                                    bool OpcionCuponesComerciales,
                                    string CuponesImpresos, string CuponesVistos,
                                    string BannersVistos)


        {
            bool rpta = false;
            try
            {
                string formato = "yyyy-MM-dd HH:mm:ss";
                string strInicio = inicio.ToString(formato);
                string strFin = fin.ToString(formato);
                string strGuid = id.ToString();
                rpta = fuente.RegistrarSesion(this.tienda, this.hostname, strGuid, tipoDocumento, nroDocumento, strInicio, strFin, 
                                                OpcionMisCupones, OpcionMiSuperCupon,
                                                OpcionMarcasSeleccionadas, OpcionArbolDeCupones,
                                                OpcionCuponesDelLugar, OpcionRuletaDeCupones,
                                                OpcionMisPuntosBonus, OpcionMisInvitaciones,
                                                OpcionCuponesComerciales,
                                                CuponesImpresos, CuponesVistos,
                                                BannersVistos);
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
