﻿using System;
using System.Globalization;
using DA = Cupones.DataAccess;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Perfiles : Base
    {
        
        /// <summary>
        /// Obtener los datos del perfil de tienda
        /// </summary>
        /// <param name="tienda">Codigo de la Tienda</param>
        /// <returns></returns>
        public BE.PerfilTienda ObtenerPerfilTienda(string tienda)
        {
            BE.PerfilTienda bePerfilTienda = null;
            try
            {
                var dePerfilTienda = fuente.ObtenerPerfilTienda(tienda);

                if (dePerfilTienda != null)
                {
                    bePerfilTienda = new BE.PerfilTienda();
                    bePerfilTienda.TiendaId = dePerfilTienda.TiendaId;
                    bePerfilTienda.Ambito = dePerfilTienda.Ambito;
                    bePerfilTienda.OpcionMisCupones = dePerfilTienda.OpcionMisCupones;
                    bePerfilTienda.OpcionMiSuperCupon = dePerfilTienda.OpcionMiSuperCupon;
                    bePerfilTienda.OpcionMarcasSeleccionadas = dePerfilTienda.OpcionMarcasSeleccionadas;
                    bePerfilTienda.OpcionArbolDeCupones = dePerfilTienda.OpcionArbolDeCupones;
                    bePerfilTienda.OpcionCuponesDelLugar = dePerfilTienda.OpcionCuponesDelLugar;
                    bePerfilTienda.OpcionRuletaDeCupones = dePerfilTienda.OpcionRuletaDeCupones;
                    bePerfilTienda.OpcionMisPuntosBonus = dePerfilTienda.OpcionMisPuntosBonus;
                    bePerfilTienda.OpcionMisInvitaciones = dePerfilTienda.OpcionMisInvitaciones;
                    bePerfilTienda.OpcionCuponesComerciales = dePerfilTienda.OpcionCuponesComerciales;
                }

                return bePerfilTienda;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Obtener los datos del perfil del cliente
        /// </summary>
        /// <param name="tipoDocumento">Codigo del tipo de documento</param>
        /// <param name="nroDocumento">Numero de documento</param>
        /// <param name="ambito">WONG o METRO</param>
        /// <returns></returns>
        public BE.PerfilCliente ObtenerPerfilCliente(int tipoDocumento, string nroDocumento, string ambito)
        {
            BE.PerfilCliente bePerfilCliente = null;
            try
            {
                var dePerfilCliente = fuente.ObtenerPerfilCliente(tipoDocumento, nroDocumento, ambito);
                if (dePerfilCliente != null)
                {
                    bePerfilCliente = new BE.PerfilCliente();
                    bePerfilCliente.TipoDocumento = int.Parse(dePerfilCliente.TipoDocumento);
                    bePerfilCliente.NumeroDocumento = dePerfilCliente.NumeroDocumento;
                    bePerfilCliente.TiendaAdmitidas = dePerfilCliente.TiendaAdmitidas;
                    bePerfilCliente.Ambito = dePerfilCliente.Ambito;
                    bePerfilCliente.OpcionMisCupones = dePerfilCliente.OpcionMisCupones;
                    bePerfilCliente.OpcionMiSuperCupon = dePerfilCliente.OpcionMiSuperCupon;
                    bePerfilCliente.OpcionMarcasSeleccionadas = dePerfilCliente.OpcionMarcasSeleccionadas;
                    bePerfilCliente.OpcionArbolDeCupones = dePerfilCliente.OpcionArbolDeCupones;
                    bePerfilCliente.OpcionCuponesDelLugar = dePerfilCliente.OpcionCuponesDelLugar;
                    bePerfilCliente.OpcionRuletaDeCupones = dePerfilCliente.OpcionRuletaDeCupones;
                    bePerfilCliente.OpcionMisPuntosBonus = dePerfilCliente.OpcionMisPuntosBonus;
                    bePerfilCliente.OpcionMisInvitaciones = dePerfilCliente.OpcionMisInvitaciones;
                    bePerfilCliente.OpcionCuponesComerciales = dePerfilCliente.OpcionCuponesComerciales;
                }

                return bePerfilCliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener los datos del perfil del kiosko
        /// </summary>
        /// <param name="tipoDocumento"></param>
        /// <param name="nroDocumento"></param>
        /// <param name="ambito"></param>
        /// <returns></returns>
        public BE.PerfilKiosko ObtenerPerfilKiosko(string tienda, string hostname)
        {
            BE.PerfilKiosko bePerfilKiosko = null;

            try
            {
                var dePerfilKiosko = fuente.ObtenerPerfilKiosko(tienda, hostname);
                if (dePerfilKiosko != null)
                {

                    bePerfilKiosko = new BE.PerfilKiosko();

                    bePerfilKiosko.TiendaKioskoId = dePerfilKiosko.TiendaKioskoId;
                    bePerfilKiosko.TiendaId = dePerfilKiosko.TiendaId;
                    bePerfilKiosko.Ambito = dePerfilKiosko.Ambito;
                    bePerfilKiosko.HostName = dePerfilKiosko.HostName;
                    bePerfilKiosko.DireccionIP = dePerfilKiosko.DireccionIP;
                    bePerfilKiosko.Zona = dePerfilKiosko.Zona;

                    //Banner
                    bePerfilKiosko.RutaImagenBannerTienda = dePerfilKiosko.ImagenBanner;

                    string format = "yyyy-MM-dd HH:mm:ss";
                    var fecIni = DateTime.ParseExact(dePerfilKiosko.FechaIniBanner, format, CultureInfo.InvariantCulture);
                    bePerfilKiosko.FechaIniBanner = fecIni;
                    var fecFin = DateTime.ParseExact(dePerfilKiosko.FechaFinBanner, format, CultureInfo.InvariantCulture);
                    bePerfilKiosko.FechaFinBanner = fecFin;

                    //Impresora
                    bePerfilKiosko.ImpresoraId = dePerfilKiosko.ImpresoraId;
                    if (bePerfilKiosko.ImpresoraId > 0)
                    {
                        bePerfilKiosko.Impresora = new BE.Impresora();
                        bePerfilKiosko.Impresora.Marca = dePerfilKiosko.Marca;
                        bePerfilKiosko.Impresora.Modelo = dePerfilKiosko.Modelo;
                        bePerfilKiosko.Impresora.MargenIzquierdoX = dePerfilKiosko.MargenIzquierdoX;
                        bePerfilKiosko.Impresora.MargenSuperiorY = dePerfilKiosko.MargenSuperiorY;
                        bePerfilKiosko.Impresora.Ancho = dePerfilKiosko.Ancho;
                        bePerfilKiosko.Impresora.Alto = dePerfilKiosko.Alto;
                    }

                }

                return bePerfilKiosko;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
