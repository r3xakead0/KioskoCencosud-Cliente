﻿using System;
using DA = Cupones.DataAccess;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Experiencias : Base
    {

        /// <summary>
        /// Guarda la Experiencia del cliente
        /// </summary>
        /// <param name="ambito"></param>
        /// <param name="codTienda"></param>
        /// <returns></returns>
        public bool GuardaExperiencia(int tipoDoc, string nroDoc, int idExperiencia, string tienda, string hostname)
        {
            bool rpta = false;
            try
            {
                rpta = fuente.RegistrarExperiencia(tipoDoc, nroDoc, idExperiencia, tienda, hostname);
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
