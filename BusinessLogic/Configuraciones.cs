﻿using System;
using System.Collections.Generic;
using System.Linq;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Configuraciones : Base
    {

        /// <summary>
        /// Obtener la lista de companias
        /// </summary>
        /// <returns></returns>
        public List<BE.Companhia> ListarCompanhias()
        {
            var lstCompanhia = new List<BE.Companhia>();
            try
            {
                var lstDeCompanhia = fuente.MostrarCompanias();
                foreach (var deCompanhia in lstDeCompanhia)
                {
                    var companhia = new BE.Companhia();
                    companhia.Codigo = deCompanhia.Compania;
                    companhia.Nombre = deCompanhia.Compania;
                    lstCompanhia.Add(companhia);
                }
                return lstCompanhia;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la lista de tiendas
        /// </summary>
        /// <returns></returns>
        public List<BE.Tienda> ListarTiendas(string companhia = "")
        {
            var lstTienda = new List<BE.Tienda>();
            try
            {
                var lstDeTienda = fuente.MostrarTiendas();

                if (companhia.Length > 0)
                    lstDeTienda = lstDeTienda.Where(x => x.Compania.Equals(companhia)).ToList();

                foreach (var deTienda in lstDeTienda)
                {
                    var tienda = new BE.Tienda();
                    tienda.Codigo = deTienda.Tienda;
                    tienda.Nombre = deTienda.Tienda;
                    tienda.Empresa = deTienda.Compania;
                    lstTienda.Add(tienda);
                }
                return lstTienda;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene el codigo de tienda a partir del hostname
        /// </summary>
        /// <param name="hostname">Hostname del kiosko</param>
        /// <returns></returns>
        public string ObtenerTienda(string hostname)
        {
            string codTienda = "";
            try
            {
                string strHostname = hostname.Trim().ToUpper();

                var lstDeTienda = fuente.MostrarTiendas();
                var deTienda = lstDeTienda.FirstOrDefault(x => x.Hostname.Equals(strHostname));

                if (deTienda != null)
                {
                    codTienda = deTienda.Tienda.Trim();
                }

                return codTienda;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la configuracion de la tienda
        /// </summary>
        /// <param name="codTienda">Codigo de Tienda</param>
        /// <param name="hostname">Nombre del Host</param>
        /// <returns></returns>
        public BE.Configuracion Obtener(string codTienda, string hostname)
        {
            BE.Configuracion beConfiguracion = null;
            try
            {

                DE.ConfiguracionEntity deConfiguracion = fuente.ObtenerConfiguracion(codTienda, hostname);

                if (deConfiguracion != null)
                {
                    beConfiguracion = new BE.Configuracion();

                    beConfiguracion.Compania = deConfiguracion.Compania;
                    beConfiguracion.Tienda = deConfiguracion.Tienda;
                    beConfiguracion.Hostname = deConfiguracion.Hostname;
                    beConfiguracion.Ubicacion = deConfiguracion.Ubicacion;

                    beConfiguracion.TecnicoTelefono = deConfiguracion.TecnicoTelefono;
                    beConfiguracion.TecnicoEmail = deConfiguracion.TecnicoEmail;
                    
                    beConfiguracion.EmailServidor = deConfiguracion.EmailServidor;
                    beConfiguracion.EmailPuerto = deConfiguracion.EmailPuerto;
                    beConfiguracion.EmailCuenta = deConfiguracion.EmailCuenta;
                    beConfiguracion.EmailClave = deConfiguracion.EmailClave;

                    beConfiguracion.AplicacionClave = deConfiguracion.AplicacionClave;
                }

                return beConfiguracion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza la configuracion de la tienda
        /// </summary>
        /// <param name="beConfiguracion">Datos de Configuracion</param>
        /// <returns></returns>
        public bool Actualizar(BE.Configuracion beConfiguracion)
        {
            bool rpta = false;
            try
            {
                if (beConfiguracion != null)
                {

                    rpta = fuente.ActualizarConfiguracion(beConfiguracion.Tienda, 
                                                        beConfiguracion.Hostname, 
                                                        beConfiguracion.TecnicoTelefono, 
                                                        beConfiguracion.TecnicoEmail);
                }
                
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
