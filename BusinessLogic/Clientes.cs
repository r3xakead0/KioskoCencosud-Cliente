﻿using System;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Clientes : Base
    {

        /// <summary>
        /// Valida el numero de DNI y OTRO numero de documento
        /// </summary>
        /// <param name="nroDoc">Numero de documento de identidad</param>
        /// <param name="ambito">WONG o METRO</param>
        /// <param name="esCumpleanhos">Indica si hoy es su cumpleaños</param>
        /// <param name="tieneCupon">Indica si tiene cupon de cumpleaños </param>
        /// <param name="fechaHoraConsulta">Obtiene la fecha y hora de la consulta</param>
        /// <returns></returns>
        public bool ValidarCliente(string nroDoc, string ambito, out bool esCumpleanhos, out bool tieneCupon, out DateTime fechaHoraConsulta)
        {
            bool rpta = false;
            bool esCumple = false;
            bool conCupon = false;
            DateTime fechaHora = DateTime.Now;

            try
            {
                DE.ValidarEntity validar = fuente.ValidarCliente(nroDoc, ambito);
                if (validar != null)
                {
                    rpta = validar.Valido;
                    esCumple = validar.EsCumpleanhos;
                    conCupon = validar.CuponCumpleanhos;
                    fechaHora = validar.FechaHoraConsulta;
                }

                esCumpleanhos = esCumple;
                tieneCupon = conCupon;
                fechaHoraConsulta = fechaHora;

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener datos del cliente mediente el numero de identidad
        /// </summary>
        /// <param name="nroDoc">Numero de documento de identidad</param>
        /// <returns></returns>
        public BE.Cliente ObtenerCliente(string nroDoc)
        {
            BE.Cliente beCliente = null;
            try
            {
                DE.ClienteEntity cliente = fuente.ObtenerCliente(nroDoc);

                if (cliente != null)
                {
                    beCliente = new BE.Cliente();
                    beCliente.NroDocumento = cliente.NumeroDocumento.Trim();
                    beCliente.TipoDocumento = int.Parse(cliente.TipoDocumento);
                    beCliente.NompleCompleto = cliente.Nombres.Trim();
                    beCliente.CuponesAsignadosMetro = cliente.CuponesAsignadosMetro;
                    beCliente.CuponesAsignadosWong = cliente.CuponesAsignadosWong;
                }

                return beCliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener el nombre completo del cliente
        /// </summary>
        /// <param name="nroDoc">Numero de documento de identidad</param>
        /// <returns></returns>
        public string ObtenerNombreCliente(string nroDoc)
        {
            string nombreCompleto = "";
            try
            {
                var cliente = this.ObtenerCliente(nroDoc);

                if (cliente != null)
                    nombreCompleto = cliente.NompleCompleto;

                return nombreCompleto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
