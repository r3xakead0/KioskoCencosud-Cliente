﻿using System;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;
using DA = Cupones.DataAccess;
using Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Usuarios
    {

        public DA.IAcceso fuente = null;

        public Usuarios()
        {
            IUnityContainer container = new UnityContainer();
            container.LoadConfiguration();

            this.fuente = container.Resolve<DA.IAcceso>();
        }

        public bool Acceso(string dominio, string usuario, string clave)
        {
            bool rpta = false;

            try
            {
                rpta = fuente.Login(dominio, usuario, clave);
              
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
}
