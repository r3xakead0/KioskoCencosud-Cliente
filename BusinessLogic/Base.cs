﻿using System.Configuration;
using Unity;
using Microsoft.Practices.Unity.Configuration;
using DA = Cupones.DataAccess;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Base
    {

        public DA.IDatos fuente = null;

        public Base()
        {
            IUnityContainer container = new UnityContainer();
            container.LoadConfiguration();

            this.fuente = container.Resolve<DA.IDatos>();
        }

    }
}
