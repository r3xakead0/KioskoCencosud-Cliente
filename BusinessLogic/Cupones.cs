﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DA = Cupones.DataAccess;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Cupones : Base
    {

        /// <summary>
        /// Obtener la lista de cupones
        /// </summary>
        /// <param name="nroDoc">Numero de Documentos</param>
        /// <param name="idGrupo">Id del Grupo</param>
        /// <param name="ambito">WONG o METRO</param>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <returns></returns>
        public List<BE.Cupon> ListarCupones(string nroDoc, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            var lstBeCupon = new List<BE.Cupon>();
            try
            {
                var lstDeCupon = fuente.MostrarCupones(nroDoc, idGrupo, ambito, tienda, tipoCupon);

                string strFormatoFechaHora = "yyyy-MM-dd HH:mm:ss";
                CultureInfo provider = CultureInfo.InvariantCulture;

                foreach (var deCupon in lstDeCupon)
                {
                    var cupon = new BE.Cupon();

                    cupon.Id = deCupon.IdKiosko;
                    cupon.CodCampania = deCupon.CodCampania;
                    cupon.Descuento = deCupon.Descuento;
                    cupon.Mensaje1 = deCupon.Mensaje1;
                    cupon.Mensaje2 = deCupon.Mensaje2;
                    cupon.Mensaje3 = deCupon.Mensaje3;
                    cupon.ImagenPath = deCupon.Imagen;
                    cupon.Imagen = null;
                    cupon.Descripcion = deCupon.Descripcion;
                    cupon.Prioridad = deCupon.Prioridad;
                    cupon.CantidadCuponesDisponibles = deCupon.CantidadDisponibles;
                    cupon.Redimidos = deCupon.CantidadRedimidos;
                    cupon.FechaHoraRedencion = null;

                    string fechaRedencion = deCupon.FechaRedencion.Trim();
                    string horaRedencion = deCupon.HoraRedencion.Trim();
                    if (fechaRedencion.Length > 0 && horaRedencion.Length > 0)
                    {
                        string strFechaHoraRedencion = fechaRedencion + " " + horaRedencion;
                        DateTime fechaHoraRedencion;
                        bool fechaValida = DateTime.TryParseExact(strFechaHoraRedencion,
                                                                strFormatoFechaHora,
                                                                provider,
                                                                DateTimeStyles.None,
                                                                out fechaHoraRedencion);
                        if (fechaValida)
                            cupon.FechaHoraRedencion = (DateTime?)fechaHoraRedencion;
                    }

                    cupon.ExclusivoApp = deCupon.ExclusivoApp;

                    lstBeCupon.Add(cupon);
                }

                return lstBeCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener el detalle del cupon
        /// </summary>
        /// <param name="nroDoc"></param>
        /// <param name="cupon"></param>
        /// <param name="codGrupo"></param>
        /// <returns></returns>
        public BE.DetalleCupon ObtenerDetalleCupon(string nroDoc, int cupon, int idGrupo, int tipoCupon = 0)
        {
            try
            {
                BE.DetalleCupon beDetalleCupon = null;
                try
                {

                    var deDetalleCupon = fuente.ObtenerCupon(nroDoc, cupon, idGrupo, tipoCupon);

                    if (deDetalleCupon != null)
                    {
                        beDetalleCupon = new BE.DetalleCupon();

                        beDetalleCupon.TipoDocumento = 0;
                        beDetalleCupon.NroDocumento = nroDoc;
                        beDetalleCupon.Descuento = deDetalleCupon.Descuento;
                        beDetalleCupon.Mensaje1 = deDetalleCupon.Mensaje1;
                        beDetalleCupon.Mensaje2 = deDetalleCupon.Mensaje2;
                        beDetalleCupon.Mensaje3 = deDetalleCupon.Mensaje3;
                        beDetalleCupon.Ambito = deDetalleCupon.Ambito;
                        beDetalleCupon.Tienda = "";
                        beDetalleCupon.LegalTipo = deDetalleCupon.TipoLegal;
                        beDetalleCupon.LegalMensaje = deDetalleCupon.MensajeLegal;
                        beDetalleCupon.ImagenPath = deDetalleCupon.Imagen;
                        beDetalleCupon.CodigoBarra = deDetalleCupon.CodigoBarra.Trim();
                        beDetalleCupon.FlagCanal = deDetalleCupon.CodigoCanal;
                        beDetalleCupon.FlagSeriado = deDetalleCupon.FlagSeriado;
                        beDetalleCupon.BonusPrincipalSeriado = deDetalleCupon.BonusPrincipalSeriado;
                        beDetalleCupon.BonusTitularSeriado = deDetalleCupon.BonusTitularSeriado;
                        beDetalleCupon.CodigoOnline = deDetalleCupon.CodigoOnline;
                        beDetalleCupon.Vigencia = deDetalleCupon.Vigencia;
                        beDetalleCupon.Tipo = deDetalleCupon.Tipo;
                        beDetalleCupon.CodEmision = deDetalleCupon.CodEmision; 
                    }

                    return beDetalleCupon;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Registrar la impresion
        /// </summary>
        /// <param name="nroDoc">Numero de Documento</param>
        /// <param name="cupon">ID de Codigo de Campaña</param>
        /// <param name="idGrupo">ID del Grupo de Cupones </param>
        /// <param name="tipoCupon">Tipo de Super Cupon ( 0 : NA | 1 : Cupon Cumpleaños | 2 : Super Cupon )</param>
        /// <returns></returns>
        public bool QuitarDisponibilidad(string nroDoc, int cupon, int idGrupo, int tipoCupon = 0, string codBarra = "")
        {
            bool rpta = false;
            try
            {
                rpta = fuente.QuitarDisponibilidadCupon(nroDoc, cupon, idGrupo, tipoCupon, codBarra);

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Registrar la interaccion con el Cupon
        /// </summary>
        /// <param name="nroDoc">Numero de Documento</param>
        /// <param name="cupon">ID de Codigo de Campaña</param>
        /// <param name="idGrupo">ID del Grupo de Cupones </param>
        /// <param name="idInteraccion">ID de Interaccion ( 0 : NA | 1 : Interaccion | 2 : Imprimir )</param>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <returns></returns>
        public bool RegistrarInteraccion(string nroDoc, int cupon, int idGrupo, int idInteraccion, string tienda)
        {
            bool rpta = false;
            try
            {
                rpta = fuente.RegistrarInteraccionCupon(nroDoc, cupon, idGrupo, idInteraccion, tienda);

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerCodigoBarra(string bonus, string nroDoc, string cupon, string hostname, string tienda)
        {
            string rpta = "";
            try
            {
                if (cupon.Length > 0)
                {
                    var cupones = new string[1];
                    cupones[0] = cupon;

                    var rptas = new DA.Pangui().CuponesSeriados(bonus, nroDoc, cupones, hostname, tienda);

                    rpta = rptas[0];
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] ObtenerCodigoBarra(string bonus, string nroDoc, string[] cupones, string hostname, string tienda)
        {
            string[] rpta = null;
            try
            {
                if (cupones != null && cupones.Length > 0 )
                {
                    string[] strCupones = Array.ConvertAll(cupones, x => x.ToString());
                    rpta = new DA.Pangui().CuponesSeriados(bonus, nroDoc, strCupones, hostname, tienda);
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
