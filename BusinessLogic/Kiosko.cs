﻿using System;
using DA = Cupones.DataAccess;
using BE = Cupones.BusinessEntity;
using System.Collections.Generic;

namespace Cupones.BusinessLogic
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Kiosko 
    {

        /// <summary>
        /// Listar el ultimo monitoreo del kioskos
        /// </summary>
        /// <returns></returns>
        public List<BE.Kiosko> ListarMonitoreo()
        {
            try
            {
                var lstBeKioskos = new List<BE.Kiosko>();

                var lstKioskos = new DA.BaseDatos().ListarMonitoreo();

                foreach (var kiosko in lstKioskos)
                {
                    var beKiosko = new BE.Kiosko();
                    beKiosko.Empresa = kiosko.Empresa;
                    beKiosko.Tienda = kiosko.Tienda;
                    beKiosko.Hostname = kiosko.Hostname;
                    beKiosko.Ip = kiosko.Ip;
                    beKiosko.Conexion = kiosko.Conexion == true ? "SI" : "NO";
                    beKiosko.Encendido = kiosko.Encendido == true ? "SI" : "NO";
                    beKiosko.Version = kiosko.Version;
                    beKiosko.Sesion = kiosko.Sesion;
                    beKiosko.FechaHora = kiosko.FechaHora;
                    lstBeKioskos.Add(beKiosko);
                }

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reinicio del kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public bool Reiniciar(string hostname)
        {
            try
            {
                string msgTrue = "RESTORING";
                string msg = new DA.Network().Restart(hostname);
                return msg.ToUpper().Equals(msgTrue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }

}
