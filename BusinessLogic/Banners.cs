﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DA = Cupones.DataAccess;
using DE = Cupones.DataEntity;
using BE = Cupones.BusinessEntity;

namespace Cupones.BusinessLogic
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Banners : Base
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tienda"></param>
        /// <param name="hostname"></param>
        /// <param name="nroDoc"></param>
        /// <returns></returns>
        public List<BE.Banner> ListarBanners(string tienda, string hostname, string nroDoc)
        {
            var lstBeCupon = new List<BE.Banner>();
            try
            {
                var lstDeCupon = fuente.MostrarBanners(tienda, hostname, nroDoc);

                foreach (var deBanner in lstDeCupon)
                {
                    var bebanner = new BE.Banner();

                    bebanner.Id = deBanner.Id;
                    bebanner.Nombre = deBanner.Nombre;
                    bebanner.ImagenPath = deBanner.Imagen;
                    bebanner.Orden = deBanner.Orden;
                    bebanner.Prioridad = deBanner.Prioridad;
                    bebanner.Tiempo = deBanner.Tiempo;

                    lstBeCupon.Add(bebanner);
                }

                return lstBeCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tienda"></param>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public List<BE.Banner> ListarBanners(string tienda, string hostname)
        {
            var lstBeCupon = new List<BE.Banner>();
            try
            {
                var lstDeCupon = fuente.MostrarBanners(tienda, hostname, "");

                foreach (var deBanner in lstDeCupon)
                {
                    var bebanner = new BE.Banner();

                    bebanner.Nombre = deBanner.Nombre;
                    bebanner.ImagenPath = deBanner.Imagen;
                    bebanner.Orden = deBanner.Orden;
                    bebanner.Prioridad = deBanner.Prioridad;
                    bebanner.Tiempo = deBanner.Tiempo;

                    lstBeCupon.Add(bebanner);
                }

                return lstBeCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Registra los banners visualizados 
        /// </summary>
        /// <param name="banners">Codigo de Banners separados por comas</param>
        /// <returns></returns>
        public bool RegistrarVisualizacion(string banners)
        {
            try
            {
                return fuente.ContarBanners(banners);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
