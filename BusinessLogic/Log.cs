﻿using System;
using DA = Cupones.DataAccess;

namespace Cupones.BusinessLogic
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Log : Base
    {

        private string tienda = "";
        private string hostname = "";

        public Log(string tienda, string hostname)
        {
            this.tienda = tienda;
            this.hostname = hostname;
        }

        public bool RegistrarError(int tipo, string mensaje, string descripcion)
        {
            bool rpta = false;
            try
            {
                rpta = fuente.RegistrarLog(this.tienda, this.hostname, tipo, mensaje, descripcion);
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
    
}
