﻿namespace Cupones.Winform
{
    partial class FrmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.btnNumCero = new System.Windows.Forms.Button();
            this.btnNumNueve = new System.Windows.Forms.Button();
            this.btnNumOcho = new System.Windows.Forms.Button();
            this.btnNumSiete = new System.Windows.Forms.Button();
            this.btnNumSeis = new System.Windows.Forms.Button();
            this.btnNumCinco = new System.Windows.Forms.Button();
            this.btnNumCuatro = new System.Windows.Forms.Button();
            this.btnNumTres = new System.Windows.Forms.Button();
            this.btnNumDos = new System.Windows.Forms.Button();
            this.btnNumUno = new System.Windows.Forms.Button();
            this.btnDNI = new System.Windows.Forms.Button();
            this.btnOtroDoc = new System.Windows.Forms.Button();
            this.pbAlertBonus = new System.Windows.Forms.PictureBox();
            this.LLTerminos = new System.Windows.Forms.LinkLabel();
            this.LLPoliticas = new System.Windows.Forms.LinkLabel();
            this.pnHabilitar = new System.Windows.Forms.Panel();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.pbBanner = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlertBonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBanner)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBorrar
            // 
            this.btnBorrar.BackColor = System.Drawing.Color.Transparent;
            this.btnBorrar.FlatAppearance.BorderSize = 0;
            this.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBorrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.Image = global::Cupones.Winform.Properties.Resources._01_tecla_borrar;
            this.btnBorrar.Location = new System.Drawing.Point(323, 1029);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(0);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(264, 86);
            this.btnBorrar.TabIndex = 24;
            this.btnBorrar.UseVisualStyleBackColor = false;
            this.btnBorrar.Click += new System.EventHandler(this.btnTecladoBorrar_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.BackColor = System.Drawing.Color.Transparent;
            this.btnDisplay.Enabled = false;
            this.btnDisplay.FlatAppearance.BorderSize = 0;
            this.btnDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplay.Image = global::Cupones.Winform.Properties.Resources._01_input_doc;
            this.btnDisplay.Location = new System.Drawing.Point(185, 635);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(402, 80);
            this.btnDisplay.TabIndex = 23;
            this.btnDisplay.TabStop = false;
            this.btnDisplay.UseVisualStyleBackColor = false;
            // 
            // btnNumCero
            // 
            this.btnNumCero.BackColor = System.Drawing.Color.Transparent;
            this.btnNumCero.FlatAppearance.BorderSize = 0;
            this.btnNumCero.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumCero.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumCero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumCero.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCero.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumCero.Image = global::Cupones.Winform.Properties.Resources._01_tecla_0;
            this.btnNumCero.Location = new System.Drawing.Point(183, 1026);
            this.btnNumCero.Name = "btnNumCero";
            this.btnNumCero.Size = new System.Drawing.Size(138, 92);
            this.btnNumCero.TabIndex = 22;
            this.btnNumCero.TabStop = false;
            this.btnNumCero.Tag = "0";
            this.btnNumCero.UseMnemonic = false;
            this.btnNumCero.UseVisualStyleBackColor = false;
            this.btnNumCero.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumNueve
            // 
            this.btnNumNueve.BackColor = System.Drawing.Color.Transparent;
            this.btnNumNueve.FlatAppearance.BorderSize = 0;
            this.btnNumNueve.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumNueve.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumNueve.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumNueve.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumNueve.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumNueve.Image = global::Cupones.Winform.Properties.Resources._01_tecla_9;
            this.btnNumNueve.Location = new System.Drawing.Point(455, 934);
            this.btnNumNueve.Name = "btnNumNueve";
            this.btnNumNueve.Size = new System.Drawing.Size(138, 92);
            this.btnNumNueve.TabIndex = 21;
            this.btnNumNueve.TabStop = false;
            this.btnNumNueve.Tag = "9";
            this.btnNumNueve.UseMnemonic = false;
            this.btnNumNueve.UseVisualStyleBackColor = false;
            this.btnNumNueve.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumOcho
            // 
            this.btnNumOcho.BackColor = System.Drawing.Color.Transparent;
            this.btnNumOcho.FlatAppearance.BorderSize = 0;
            this.btnNumOcho.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumOcho.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumOcho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumOcho.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumOcho.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumOcho.Image = global::Cupones.Winform.Properties.Resources._01_tecla_8;
            this.btnNumOcho.Location = new System.Drawing.Point(321, 934);
            this.btnNumOcho.Name = "btnNumOcho";
            this.btnNumOcho.Size = new System.Drawing.Size(138, 92);
            this.btnNumOcho.TabIndex = 20;
            this.btnNumOcho.TabStop = false;
            this.btnNumOcho.Tag = "8";
            this.btnNumOcho.UseVisualStyleBackColor = false;
            this.btnNumOcho.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumSiete
            // 
            this.btnNumSiete.BackColor = System.Drawing.Color.Transparent;
            this.btnNumSiete.FlatAppearance.BorderSize = 0;
            this.btnNumSiete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumSiete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumSiete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumSiete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumSiete.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumSiete.Image = global::Cupones.Winform.Properties.Resources._01_tecla_7;
            this.btnNumSiete.Location = new System.Drawing.Point(183, 934);
            this.btnNumSiete.Name = "btnNumSiete";
            this.btnNumSiete.Size = new System.Drawing.Size(138, 92);
            this.btnNumSiete.TabIndex = 19;
            this.btnNumSiete.TabStop = false;
            this.btnNumSiete.Tag = "7";
            this.btnNumSiete.UseMnemonic = false;
            this.btnNumSiete.UseVisualStyleBackColor = false;
            this.btnNumSiete.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumSeis
            // 
            this.btnNumSeis.BackColor = System.Drawing.Color.Transparent;
            this.btnNumSeis.FlatAppearance.BorderSize = 0;
            this.btnNumSeis.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumSeis.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumSeis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumSeis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumSeis.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumSeis.Image = global::Cupones.Winform.Properties.Resources._01_tecla_6;
            this.btnNumSeis.Location = new System.Drawing.Point(455, 842);
            this.btnNumSeis.Name = "btnNumSeis";
            this.btnNumSeis.Size = new System.Drawing.Size(138, 92);
            this.btnNumSeis.TabIndex = 18;
            this.btnNumSeis.TabStop = false;
            this.btnNumSeis.Tag = "6";
            this.btnNumSeis.UseMnemonic = false;
            this.btnNumSeis.UseVisualStyleBackColor = false;
            this.btnNumSeis.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumCinco
            // 
            this.btnNumCinco.BackColor = System.Drawing.Color.Transparent;
            this.btnNumCinco.FlatAppearance.BorderSize = 0;
            this.btnNumCinco.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumCinco.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumCinco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumCinco.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCinco.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumCinco.Image = global::Cupones.Winform.Properties.Resources._01_tecla_5;
            this.btnNumCinco.Location = new System.Drawing.Point(322, 842);
            this.btnNumCinco.Name = "btnNumCinco";
            this.btnNumCinco.Size = new System.Drawing.Size(138, 92);
            this.btnNumCinco.TabIndex = 17;
            this.btnNumCinco.TabStop = false;
            this.btnNumCinco.Tag = "5";
            this.btnNumCinco.UseMnemonic = false;
            this.btnNumCinco.UseVisualStyleBackColor = false;
            this.btnNumCinco.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumCuatro
            // 
            this.btnNumCuatro.BackColor = System.Drawing.Color.Transparent;
            this.btnNumCuatro.FlatAppearance.BorderSize = 0;
            this.btnNumCuatro.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumCuatro.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumCuatro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumCuatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCuatro.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumCuatro.Image = global::Cupones.Winform.Properties.Resources._01_tecla_4;
            this.btnNumCuatro.Location = new System.Drawing.Point(183, 842);
            this.btnNumCuatro.Name = "btnNumCuatro";
            this.btnNumCuatro.Size = new System.Drawing.Size(138, 92);
            this.btnNumCuatro.TabIndex = 16;
            this.btnNumCuatro.TabStop = false;
            this.btnNumCuatro.Tag = "4";
            this.btnNumCuatro.UseMnemonic = false;
            this.btnNumCuatro.UseVisualStyleBackColor = false;
            this.btnNumCuatro.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumTres
            // 
            this.btnNumTres.BackColor = System.Drawing.Color.Transparent;
            this.btnNumTres.FlatAppearance.BorderSize = 0;
            this.btnNumTres.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumTres.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumTres.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumTres.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumTres.Image = global::Cupones.Winform.Properties.Resources._01_tecla_3;
            this.btnNumTres.Location = new System.Drawing.Point(455, 748);
            this.btnNumTres.Name = "btnNumTres";
            this.btnNumTres.Size = new System.Drawing.Size(138, 92);
            this.btnNumTres.TabIndex = 15;
            this.btnNumTres.TabStop = false;
            this.btnNumTres.Tag = "3";
            this.btnNumTres.UseMnemonic = false;
            this.btnNumTres.UseVisualStyleBackColor = false;
            this.btnNumTres.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumDos
            // 
            this.btnNumDos.BackColor = System.Drawing.Color.Transparent;
            this.btnNumDos.FlatAppearance.BorderSize = 0;
            this.btnNumDos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumDos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumDos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumDos.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumDos.Image = global::Cupones.Winform.Properties.Resources._01_tecla_2;
            this.btnNumDos.Location = new System.Drawing.Point(321, 747);
            this.btnNumDos.Name = "btnNumDos";
            this.btnNumDos.Size = new System.Drawing.Size(138, 92);
            this.btnNumDos.TabIndex = 14;
            this.btnNumDos.TabStop = false;
            this.btnNumDos.Tag = "2";
            this.btnNumDos.UseMnemonic = false;
            this.btnNumDos.UseVisualStyleBackColor = false;
            this.btnNumDos.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnNumUno
            // 
            this.btnNumUno.BackColor = System.Drawing.Color.Transparent;
            this.btnNumUno.FlatAppearance.BorderSize = 0;
            this.btnNumUno.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnNumUno.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnNumUno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumUno.ForeColor = System.Drawing.Color.Transparent;
            this.btnNumUno.Image = global::Cupones.Winform.Properties.Resources._01_tecla_1;
            this.btnNumUno.Location = new System.Drawing.Point(182, 747);
            this.btnNumUno.Name = "btnNumUno";
            this.btnNumUno.Size = new System.Drawing.Size(138, 92);
            this.btnNumUno.TabIndex = 13;
            this.btnNumUno.TabStop = false;
            this.btnNumUno.Tag = "1";
            this.btnNumUno.UseMnemonic = false;
            this.btnNumUno.UseVisualStyleBackColor = false;
            this.btnNumUno.Click += new System.EventHandler(this.btnTecladoNumerico_Click);
            // 
            // btnDNI
            // 
            this.btnDNI.FlatAppearance.BorderSize = 0;
            this.btnDNI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDNI.Image = global::Cupones.Winform.Properties.Resources._01_btn_dni;
            this.btnDNI.Location = new System.Drawing.Point(243, 472);
            this.btnDNI.Name = "btnDNI";
            this.btnDNI.Size = new System.Drawing.Size(140, 80);
            this.btnDNI.TabIndex = 36;
            this.btnDNI.TabStop = false;
            this.btnDNI.UseVisualStyleBackColor = true;
            this.btnDNI.Click += new System.EventHandler(this.btnDNI_Click);
            // 
            // btnOtroDoc
            // 
            this.btnOtroDoc.FlatAppearance.BorderSize = 0;
            this.btnOtroDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOtroDoc.Image = global::Cupones.Winform.Properties.Resources._01_btn_otros;
            this.btnOtroDoc.Location = new System.Drawing.Point(389, 472);
            this.btnOtroDoc.Name = "btnOtroDoc";
            this.btnOtroDoc.Size = new System.Drawing.Size(140, 80);
            this.btnOtroDoc.TabIndex = 41;
            this.btnOtroDoc.TabStop = false;
            this.btnOtroDoc.UseVisualStyleBackColor = true;
            this.btnOtroDoc.Click += new System.EventHandler(this.btnOtroDoc_Click);
            // 
            // pbAlertBonus
            // 
            this.pbAlertBonus.Image = global::Cupones.Winform.Properties.Resources._01_btn_alert_bonus;
            this.pbAlertBonus.Location = new System.Drawing.Point(182, 558);
            this.pbAlertBonus.Name = "pbAlertBonus";
            this.pbAlertBonus.Size = new System.Drawing.Size(405, 71);
            this.pbAlertBonus.TabIndex = 42;
            this.pbAlertBonus.TabStop = false;
            this.pbAlertBonus.Visible = false;
            // 
            // LLTerminos
            // 
            this.LLTerminos.AutoSize = true;
            this.LLTerminos.BackColor = System.Drawing.Color.Transparent;
            this.LLTerminos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LLTerminos.LinkColor = System.Drawing.Color.Red;
            this.LLTerminos.Location = new System.Drawing.Point(182, 1125);
            this.LLTerminos.Name = "LLTerminos";
            this.LLTerminos.Size = new System.Drawing.Size(198, 20);
            this.LLTerminos.TabIndex = 46;
            this.LLTerminos.TabStop = true;
            this.LLTerminos.Text = "Ver términos y condiciones";
            this.LLTerminos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LLTerminos_LinkClicked);
            // 
            // LLPoliticas
            // 
            this.LLPoliticas.AutoSize = true;
            this.LLPoliticas.BackColor = System.Drawing.Color.Transparent;
            this.LLPoliticas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LLPoliticas.LinkColor = System.Drawing.Color.Red;
            this.LLPoliticas.Location = new System.Drawing.Point(401, 1125);
            this.LLPoliticas.Name = "LLPoliticas";
            this.LLPoliticas.Size = new System.Drawing.Size(181, 20);
            this.LLPoliticas.TabIndex = 47;
            this.LLPoliticas.TabStop = true;
            this.LLPoliticas.Text = "Ver políticas y privacidad";
            this.LLPoliticas.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LLPoliticas_LinkClicked);
            // 
            // pnHabilitar
            // 
            this.pnHabilitar.BackColor = System.Drawing.Color.Transparent;
            this.pnHabilitar.Location = new System.Drawing.Point(0, 0);
            this.pnHabilitar.Name = "pnHabilitar";
            this.pnHabilitar.Size = new System.Drawing.Size(220, 221);
            this.pnHabilitar.TabIndex = 48;
            this.pnHabilitar.Click += new System.EventHandler(this.pnHabilitar_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.BackColor = System.Drawing.SystemColors.Window;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(29, 647);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(132, 56);
            this.lblCodigo.TabIndex = 36;
            this.lblCodigo.Tag = "IC";
            this.lblCodigo.Text = "Ingresa Codigo";
            this.lblCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCodigo.Visible = false;
            // 
            // pbBanner
            // 
            this.pbBanner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbBanner.Location = new System.Drawing.Point(0, 1202);
            this.pbBanner.Name = "pbBanner";
            this.pbBanner.Size = new System.Drawing.Size(768, 162);
            this.pbBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBanner.TabIndex = 50;
            this.pbBanner.TabStop = false;
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::Cupones.Winform.Properties.Resources._01_background;
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.ControlBox = false;
            this.Controls.Add(this.pbBanner);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.pnHabilitar);
            this.Controls.Add(this.LLPoliticas);
            this.Controls.Add(this.LLTerminos);
            this.Controls.Add(this.pbAlertBonus);
            this.Controls.Add(this.btnOtroDoc);
            this.Controls.Add(this.btnDNI);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.btnNumCero);
            this.Controls.Add(this.btnNumNueve);
            this.Controls.Add(this.btnNumOcho);
            this.Controls.Add(this.btnNumSiete);
            this.Controls.Add(this.btnNumSeis);
            this.Controls.Add(this.btnNumCinco);
            this.Controls.Add(this.btnNumCuatro);
            this.Controls.Add(this.btnNumTres);
            this.Controls.Add(this.btnNumDos);
            this.Controls.Add(this.btnNumUno);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CENCOSUD";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmInicio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbAlertBonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBanner)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnDisplay;
        internal System.Windows.Forms.Button btnNumCero;
        internal System.Windows.Forms.Button btnNumNueve;
        internal System.Windows.Forms.Button btnNumOcho;
        internal System.Windows.Forms.Button btnNumSiete;
        internal System.Windows.Forms.Button btnNumSeis;
        internal System.Windows.Forms.Button btnNumCinco;
        internal System.Windows.Forms.Button btnNumCuatro;
        internal System.Windows.Forms.Button btnNumTres;
        internal System.Windows.Forms.Button btnNumDos;
        internal System.Windows.Forms.Button btnNumUno;
        internal System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnDNI;
        private System.Windows.Forms.Button btnOtroDoc;
        private System.Windows.Forms.PictureBox pbAlertBonus;
        private System.Windows.Forms.LinkLabel LLTerminos;
        private System.Windows.Forms.LinkLabel LLPoliticas;
        private System.Windows.Forms.Panel pnHabilitar;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.PictureBox pbBanner;
    }
}