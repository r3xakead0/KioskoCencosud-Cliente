﻿namespace Cupones.Winform
{
    partial class FrmMenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlCabecera = new System.Windows.Forms.Panel();
            this.lblNombleCliente = new System.Windows.Forms.Label();
            this.lblTiempoSession = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlBotones = new System.Windows.Forms.Panel();
            this.btnGrupo8S = new System.Windows.Forms.Button();
            this.btnGrupo6S = new System.Windows.Forms.Button();
            this.btnGrupo5S = new System.Windows.Forms.Button();
            this.btnGrupo7S = new System.Windows.Forms.Button();
            this.btnGrupo1S = new System.Windows.Forms.Button();
            this.btnGrupo2S = new System.Windows.Forms.Button();
            this.btnGrupo3S = new System.Windows.Forms.Button();
            this.btnGrupo4S = new System.Windows.Forms.Button();
            this.btnGrupo5S = new System.Windows.Forms.Button();
            this.btnGrupo6S = new System.Windows.Forms.Button();
            this.btnGrupo7S = new System.Windows.Forms.Button();
            this.btnGrupo8S = new System.Windows.Forms.Button();
            this.btnGrupo9S = new System.Windows.Forms.Button();
            this.pnlSuperCupon = new System.Windows.Forms.Panel();
            this.btnRegalo = new System.Windows.Forms.Button();
            this.pbSuperCupon = new System.Windows.Forms.PictureBox();
            this.pnlRuleta = new System.Windows.Forms.Panel();
            this.pbRuleta = new System.Windows.Forms.PictureBox();
            this.btnJugar = new System.Windows.Forms.Button();
            this.pnlCupones = new System.Windows.Forms.Panel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.btnMultiCupon = new System.Windows.Forms.Button();
            this.btnNextImages = new System.Windows.Forms.Button();
            this.btnPreviousImages = new System.Windows.Forms.Button();
            this.dataViewImages = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.pbMensajeNoCupones = new System.Windows.Forms.PictureBox();
            this.pbBanner = new System.Windows.Forms.PictureBox();
            this.pnlBanner = new System.Windows.Forms.Panel();
            this.btnCupon11 = new System.Windows.Forms.Button();
            this.pbCupon0 = new System.Windows.Forms.PictureBox();
            this.pbCupon1 = new System.Windows.Forms.PictureBox();
            this.pbCupon2 = new System.Windows.Forms.PictureBox();
            this.pbCupon3 = new System.Windows.Forms.PictureBox();
            this.pbCupon4 = new System.Windows.Forms.PictureBox();
            this.pbCupon5 = new System.Windows.Forms.PictureBox();
            this.pbCupon6 = new System.Windows.Forms.PictureBox();
            this.pbCupon7 = new System.Windows.Forms.PictureBox();
            this.pbCupon8 = new System.Windows.Forms.PictureBox();
            this.pbCupon9 = new System.Windows.Forms.PictureBox();
            this.pbCupon10 = new System.Windows.Forms.PictureBox();
            this.pnlArbol = new System.Windows.Forms.Panel();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape12 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape11 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.pnlPuntosBonus = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblTiempoCupon = new Cupones.Winform.Controls.TransparentLabel();
            this.pnlCabecera.SuspendLayout();
            this.pnlBotones.SuspendLayout();
            this.pnlSuperCupon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSuperCupon)).BeginInit();
            this.pnlRuleta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRuleta)).BeginInit();
            this.pnlCupones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataViewImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMensajeNoCupones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBanner)).BeginInit();
            this.pnlBanner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon10)).BeginInit();
            this.pnlArbol.SuspendLayout();
            this.pnlPuntosBonus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCabecera
            // 
            this.pnlCabecera.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCabecera.BackColor = System.Drawing.Color.Transparent;
            this.pnlCabecera.Controls.Add(this.lblNombleCliente);
            this.pnlCabecera.Controls.Add(this.lblTiempoSession);
            this.pnlCabecera.Controls.Add(this.btnSalir);
            this.pnlCabecera.Location = new System.Drawing.Point(12, 6);
            this.pnlCabecera.Name = "pnlCabecera";
            this.pnlCabecera.Size = new System.Drawing.Size(744, 152);
            this.pnlCabecera.TabIndex = 0;
            // 
            // lblNombleCliente
            // 
            this.lblNombleCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombleCliente.AutoSize = true;
            this.lblNombleCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblNombleCliente.Font = new System.Drawing.Font("Arial", 38.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombleCliente.ForeColor = System.Drawing.Color.White;
            this.lblNombleCliente.Location = new System.Drawing.Point(256, 89);
            this.lblNombleCliente.Name = "lblNombleCliente";
            this.lblNombleCliente.Size = new System.Drawing.Size(0, 60);
            this.lblNombleCliente.TabIndex = 10;
            // 
            // lblTiempoSession
            // 
            this.lblTiempoSession.AutoSize = true;
            this.lblTiempoSession.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblTiempoSession.Location = new System.Drawing.Point(394, 4);
            this.lblTiempoSession.Name = "lblTiempoSession";
            this.lblTiempoSession.Size = new System.Drawing.Size(32, 22);
            this.lblTiempoSession.TabIndex = 11;
            this.lblTiempoSession.Text = "45";
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.Transparent;
            this.btnSalir.Image = global::Cupones.Winform.Properties.Resources._07_btn_salir;
            this.btnSalir.Location = new System.Drawing.Point(619, 14);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(120, 49);
            this.btnSalir.TabIndex = 9;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pnlBotones
            // 
            this.pnlBotones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBotones.BackColor = System.Drawing.Color.Transparent;
            this.pnlBotones.Controls.Add(this.btnGrupo8S);
            this.pnlBotones.Controls.Add(this.btnGrupo6S);
            this.pnlBotones.Controls.Add(this.btnGrupo5S);
            this.pnlBotones.Controls.Add(this.btnGrupo7S);
            this.pnlBotones.Controls.Add(this.btnGrupo1S);
            this.pnlBotones.Controls.Add(this.btnGrupo2S);
            this.pnlBotones.Controls.Add(this.btnGrupo3S);
            this.pnlBotones.Controls.Add(this.btnGrupo4S);
            this.pnlBotones.Controls.Add(this.btnGrupo9S);
            this.pnlBotones.Location = new System.Drawing.Point(-3, 229);
            this.pnlBotones.Name = "pnlBotones";
            this.pnlBotones.Size = new System.Drawing.Size(769, 126);
            this.pnlBotones.TabIndex = 12;
            // 
            // btnGrupo8S
            // 
            this.btnGrupo8S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo8S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo8S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo8S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo8S.Location = new System.Drawing.Point(774, 6);
            this.btnGrupo8S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo8S.Name = "btnGrupo8S";
            this.btnGrupo8S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo8S.TabIndex = 11;
            this.btnGrupo8S.Tag = "8";
            this.btnGrupo8S.UseVisualStyleBackColor = false;
            this.btnGrupo8S.Visible = false;
            this.btnGrupo8S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo8S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo8S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo6S
            // 
            this.btnGrupo6S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo6S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo6S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo6S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo6S.Location = new System.Drawing.Point(334, 6);
            this.btnGrupo6S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo6S.Name = "btnGrupo6S";
            this.btnGrupo6S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo6S.TabIndex = 10;
            this.btnGrupo6S.Tag = "6";
            this.btnGrupo6S.UseVisualStyleBackColor = false;
            this.btnGrupo6S.Visible = false;
            this.btnGrupo6S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo6S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo6S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo5S
            // 
            this.btnGrupo5S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo5S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo5S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo5S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo5S.Location = new System.Drawing.Point(554, 6);
            this.btnGrupo5S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo5S.Name = "btnGrupo5S";
            this.btnGrupo5S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo5S.TabIndex = 9;
            this.btnGrupo5S.Tag = "5";
            this.btnGrupo5S.UseVisualStyleBackColor = false;
            this.btnGrupo5S.Visible = false;
            this.btnGrupo5S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo5S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo5S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo7S
            // 
            this.btnGrupo7S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo7S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo7S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo7S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo7S.Location = new System.Drawing.Point(664, 6);
            this.btnGrupo7S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo7S.Name = "btnGrupo7S";
            this.btnGrupo7S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo7S.TabIndex = 8;
            this.btnGrupo7S.Tag = "7";
            this.btnGrupo7S.UseVisualStyleBackColor = false;
            this.btnGrupo7S.Visible = false;
            this.btnGrupo7S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo7S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo7S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo1S
            // 
            this.btnGrupo1S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo1S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo1S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo1S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo1S.Location = new System.Drawing.Point(4, 6);
            this.btnGrupo1S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo1S.Name = "btnGrupo1S";
            this.btnGrupo1S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo1S.TabIndex = 2;
            this.btnGrupo1S.Tag = "1";
            this.btnGrupo1S.UseVisualStyleBackColor = false;
            this.btnGrupo1S.Visible = false;
            this.btnGrupo1S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo1S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo1S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo2S
            // 
            this.btnGrupo2S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo2S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo2S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo2S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo2S.Location = new System.Drawing.Point(114, 6);
            this.btnGrupo2S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo2S.Name = "btnGrupo2S";
            this.btnGrupo2S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo2S.TabIndex = 3;
            this.btnGrupo2S.Tag = "2";
            this.btnGrupo2S.UseVisualStyleBackColor = false;
            this.btnGrupo2S.Visible = false;
            this.btnGrupo2S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo2S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo2S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo3S
            // 
            this.btnGrupo3S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo3S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo3S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo3S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo3S.Location = new System.Drawing.Point(224, 6);
            this.btnGrupo3S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo3S.Name = "btnGrupo3S";
            this.btnGrupo3S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo3S.TabIndex = 4;
            this.btnGrupo3S.Tag = "3";
            this.btnGrupo3S.UseVisualStyleBackColor = false;
            this.btnGrupo3S.Visible = false;
            this.btnGrupo3S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo3S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo3S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo4S
            // 
            this.btnGrupo4S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo4S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo4S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo4S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo4S.Location = new System.Drawing.Point(444, 6);
            this.btnGrupo4S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo4S.Name = "btnGrupo4S";
            this.btnGrupo4S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo4S.TabIndex = 5;
            this.btnGrupo4S.Tag = "4";
            this.btnGrupo4S.UseVisualStyleBackColor = false;
            this.btnGrupo4S.Visible = false;
            this.btnGrupo4S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo4S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo4S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // btnGrupo9S
            // 
            this.btnGrupo9S.BackColor = System.Drawing.Color.Transparent;
            this.btnGrupo9S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrupo9S.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo9S.ForeColor = System.Drawing.Color.Transparent;
            this.btnGrupo9S.Location = new System.Drawing.Point(884, 6);
            this.btnGrupo9S.Margin = new System.Windows.Forms.Padding(0);
            this.btnGrupo9S.Name = "btnGrupo9S";
            this.btnGrupo9S.Size = new System.Drawing.Size(110, 119);
            this.btnGrupo9S.TabIndex = 27;
            this.btnGrupo9S.Tag = "9";
            this.btnGrupo9S.UseVisualStyleBackColor = false;
            this.btnGrupo9S.Visible = false;
            this.btnGrupo9S.Click += new System.EventHandler(this.btnGrupos_Click);
            this.btnGrupo9S.MouseEnter += new System.EventHandler(this.btnGrupos_MouseEnter);
            this.btnGrupo9S.MouseLeave += new System.EventHandler(this.btnGrupos_MouseLeave);
            // 
            // pnlSuperCupon
            // 
            this.pnlSuperCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSuperCupon.BackColor = System.Drawing.Color.White;
            this.pnlSuperCupon.Controls.Add(this.lblTiempoCupon);
            this.pnlSuperCupon.Controls.Add(this.btnRegalo);
            this.pnlSuperCupon.Controls.Add(this.pbSuperCupon);
            this.pnlSuperCupon.Location = new System.Drawing.Point(1, 380);
            this.pnlSuperCupon.Name = "pnlSuperCupon";
            this.pnlSuperCupon.Size = new System.Drawing.Size(767, 790);
            this.pnlSuperCupon.TabIndex = 1;
            this.pnlSuperCupon.Visible = false;
            // 
            // btnRegalo
            // 
            this.btnRegalo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRegalo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegalo.ForeColor = System.Drawing.Color.Transparent;
            this.btnRegalo.Image = global::Cupones.Winform.Properties.Resources._04_btn_regalo;
            this.btnRegalo.Location = new System.Drawing.Point(247, 29);
            this.btnRegalo.Margin = new System.Windows.Forms.Padding(0);
            this.btnRegalo.Name = "btnRegalo";
            this.btnRegalo.Size = new System.Drawing.Size(249, 254);
            this.btnRegalo.TabIndex = 0;
            this.btnRegalo.UseVisualStyleBackColor = false;
            this.btnRegalo.Click += new System.EventHandler(this.btnRegalo_Click);
            // 
            // pbSuperCupon
            // 
            this.pbSuperCupon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbSuperCupon.Location = new System.Drawing.Point(210, -5);
            this.pbSuperCupon.Name = "pbSuperCupon";
            this.pbSuperCupon.Size = new System.Drawing.Size(320, 780);
            this.pbSuperCupon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbSuperCupon.TabIndex = 2;
            this.pbSuperCupon.TabStop = false;
            this.pbSuperCupon.Click += new System.EventHandler(this.pbSuperCupon_Click);
            // 
            // pnlRuleta
            // 
            this.pnlRuleta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRuleta.BackColor = System.Drawing.Color.White;
            this.pnlRuleta.Controls.Add(this.pbRuleta);
            this.pnlRuleta.Controls.Add(this.btnJugar);
            this.pnlRuleta.Location = new System.Drawing.Point(1, 380);
            this.pnlRuleta.Name = "pnlRuleta";
            this.pnlRuleta.Size = new System.Drawing.Size(767, 790);
            this.pnlRuleta.TabIndex = 17;
            this.pnlRuleta.Visible = false;
            // 
            // pbRuleta
            // 
            this.pbRuleta.Image = global::Cupones.Winform.Properties.Resources._09_ruleta;
            this.pbRuleta.Location = new System.Drawing.Point(186, 40);
            this.pbRuleta.Name = "pbRuleta";
            this.pbRuleta.Size = new System.Drawing.Size(500, 500);
            this.pbRuleta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRuleta.TabIndex = 1;
            this.pbRuleta.TabStop = false;
            // 
            // btnJugar
            // 
            this.btnJugar.Image = global::Cupones.Winform.Properties.Resources._03_btn_prev;
            this.btnJugar.Location = new System.Drawing.Point(71, 25);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(75, 41);
            this.btnJugar.TabIndex = 0;
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // pnlCupones
            // 
            this.pnlCupones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCupones.BackColor = System.Drawing.Color.White;
            this.pnlCupones.Controls.Add(this.btnImprimir);
            this.pnlCupones.Controls.Add(this.btnSeleccionar);
            this.pnlCupones.Controls.Add(this.btnMultiCupon);
            this.pnlCupones.Controls.Add(this.btnNextImages);
            this.pnlCupones.Controls.Add(this.btnPreviousImages);
            this.pnlCupones.Controls.Add(this.dataViewImages);
            this.pnlCupones.Location = new System.Drawing.Point(1, 380);
            this.pnlCupones.Name = "pnlCupones";
            this.pnlCupones.Size = new System.Drawing.Size(767, 790);
            this.pnlCupones.TabIndex = 5;
            this.pnlCupones.Visible = false;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.ForeColor = System.Drawing.Color.Transparent;
            this.btnImprimir.Image = global::Cupones.Winform.Properties.Resources._03_btn_imprimir;
            this.btnImprimir.Location = new System.Drawing.Point(16, 710);
            this.btnImprimir.Margin = new System.Windows.Forms.Padding(0);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(130, 55);
            this.btnImprimir.TabIndex = 13;
            this.btnImprimir.UseVisualStyleBackColor = false;
            this.btnImprimir.Visible = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSeleccionar.FlatAppearance.BorderSize = 0;
            this.btnSeleccionar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnSeleccionar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSeleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeleccionar.ForeColor = System.Drawing.Color.Transparent;
            this.btnSeleccionar.Image = global::Cupones.Winform.Properties.Resources._03_btn_select_all;
            this.btnSeleccionar.Location = new System.Drawing.Point(318, 710);
            this.btnSeleccionar.Margin = new System.Windows.Forms.Padding(0);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(135, 55);
            this.btnSeleccionar.TabIndex = 22;
            this.btnSeleccionar.UseVisualStyleBackColor = false;
            this.btnSeleccionar.Visible = false;
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // btnMultiCupon
            // 
            this.btnMultiCupon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMultiCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMultiCupon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiCupon.ForeColor = System.Drawing.Color.Transparent;
            this.btnMultiCupon.Image = global::Cupones.Winform.Properties.Resources._03_btn_multi;
            this.btnMultiCupon.Location = new System.Drawing.Point(620, 710);
            this.btnMultiCupon.Name = "btnMultiCupon";
            this.btnMultiCupon.Size = new System.Drawing.Size(135, 55);
            this.btnMultiCupon.TabIndex = 15;
            this.btnMultiCupon.UseVisualStyleBackColor = true;
            this.btnMultiCupon.Click += new System.EventHandler(this.btnMultiCupon_Click);
            // 
            // btnNextImages
            // 
            this.btnNextImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextImages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextImages.ForeColor = System.Drawing.Color.Transparent;
            this.btnNextImages.Image = global::Cupones.Winform.Properties.Resources._03_btn_prev;
            this.btnNextImages.Location = new System.Drawing.Point(537, 710);
            this.btnNextImages.Name = "btnNextImages";
            this.btnNextImages.Size = new System.Drawing.Size(85, 55);
            this.btnNextImages.TabIndex = 11;
            this.btnNextImages.UseVisualStyleBackColor = true;
            this.btnNextImages.Click += new System.EventHandler(this.btnNextImages_Click);
            // 
            // btnPreviousImages
            // 
            this.btnPreviousImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreviousImages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreviousImages.ForeColor = System.Drawing.Color.Transparent;
            this.btnPreviousImages.Image = global::Cupones.Winform.Properties.Resources._03_btn_atras;
            this.btnPreviousImages.Location = new System.Drawing.Point(149, 710);
            this.btnPreviousImages.Name = "btnPreviousImages";
            this.btnPreviousImages.Size = new System.Drawing.Size(85, 55);
            this.btnPreviousImages.TabIndex = 10;
            this.btnPreviousImages.UseVisualStyleBackColor = true;
            this.btnPreviousImages.Click += new System.EventHandler(this.btnPreviousImages_Click);
            // 
            // dataViewImages
            // 
            this.dataViewImages.AllowUserToAddRows = false;
            this.dataViewImages.AllowUserToDeleteRows = false;
            this.dataViewImages.AllowUserToResizeColumns = false;
            this.dataViewImages.AllowUserToResizeRows = false;
            this.dataViewImages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataViewImages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dataViewImages.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataViewImages.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataViewImages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataViewImages.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataViewImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataViewImages.ColumnHeadersVisible = false;
            this.dataViewImages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataViewImages.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataViewImages.GridColor = System.Drawing.SystemColors.Control;
            this.dataViewImages.Location = new System.Drawing.Point(16, 17);
            this.dataViewImages.MultiSelect = false;
            this.dataViewImages.Name = "dataViewImages";
            this.dataViewImages.RowHeadersVisible = false;
            this.dataViewImages.ShowCellErrors = false;
            this.dataViewImages.ShowEditingIcon = false;
            this.dataViewImages.ShowRowErrors = false;
            this.dataViewImages.Size = new System.Drawing.Size(732, 690);
            this.dataViewImages.TabIndex = 9;
            this.dataViewImages.Visible = false;
            this.dataViewImages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataViewImages_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 5;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Width = 5;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Width = 5;
            // 
            // pbMensajeNoCupones
            // 
            this.pbMensajeNoCupones.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbMensajeNoCupones.Image = global::Cupones.Winform.Properties.Resources._03_sin_cupon_alert;
            this.pbMensajeNoCupones.Location = new System.Drawing.Point(20, 600);
            this.pbMensajeNoCupones.Name = "pbMensajeNoCupones";
            this.pbMensajeNoCupones.Size = new System.Drawing.Size(727, 290);
            this.pbMensajeNoCupones.TabIndex = 21;
            this.pbMensajeNoCupones.TabStop = false;
            this.pbMensajeNoCupones.Visible = false;
            // 
            // pbBanner
            // 
            this.pbBanner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbBanner.Location = new System.Drawing.Point(0, 0);
            this.pbBanner.Name = "pbBanner";
            this.pbBanner.Size = new System.Drawing.Size(767, 189);
            this.pbBanner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBanner.TabIndex = 15;
            this.pbBanner.TabStop = false;
            // 
            // pnlBanner
            // 
            this.pnlBanner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBanner.AutoSize = true;
            this.pnlBanner.BackColor = System.Drawing.Color.Transparent;
            this.pnlBanner.Controls.Add(this.pbBanner);
            this.pnlBanner.Location = new System.Drawing.Point(1, 1176);
            this.pnlBanner.Name = "pnlBanner";
            this.pnlBanner.Size = new System.Drawing.Size(767, 189);
            this.pnlBanner.TabIndex = 1;
            // 
            // btnCupon11
            // 
            this.btnCupon11.BackColor = System.Drawing.Color.White;
            this.btnCupon11.FlatAppearance.BorderSize = 0;
            this.btnCupon11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCupon11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCupon11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCupon11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCupon11.ForeColor = System.Drawing.Color.Black;
            this.btnCupon11.Image = global::Cupones.Winform.Properties.Resources._05_cupon_regalo;
            this.btnCupon11.Location = new System.Drawing.Point(299, 76);
            this.btnCupon11.Margin = new System.Windows.Forms.Padding(0);
            this.btnCupon11.Name = "btnCupon11";
            this.btnCupon11.Size = new System.Drawing.Size(188, 187);
            this.btnCupon11.TabIndex = 14;
            this.btnCupon11.UseVisualStyleBackColor = false;
            this.btnCupon11.Click += new System.EventHandler(this.btnCupon11_Click);
            // 
            // pbCupon0
            // 
            this.pbCupon0.Location = new System.Drawing.Point(2, 636);
            this.pbCupon0.Name = "pbCupon0";
            this.pbCupon0.Size = new System.Drawing.Size(126, 141);
            this.pbCupon0.TabIndex = 16;
            this.pbCupon0.TabStop = false;
            this.pbCupon0.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon1
            // 
            this.pbCupon1.Location = new System.Drawing.Point(128, 636);
            this.pbCupon1.Name = "pbCupon1";
            this.pbCupon1.Size = new System.Drawing.Size(126, 141);
            this.pbCupon1.TabIndex = 17;
            this.pbCupon1.TabStop = false;
            this.pbCupon1.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon2
            // 
            this.pbCupon2.Location = new System.Drawing.Point(257, 635);
            this.pbCupon2.Name = "pbCupon2";
            this.pbCupon2.Size = new System.Drawing.Size(126, 141);
            this.pbCupon2.TabIndex = 18;
            this.pbCupon2.TabStop = false;
            this.pbCupon2.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon3
            // 
            this.pbCupon3.Location = new System.Drawing.Point(383, 635);
            this.pbCupon3.Name = "pbCupon3";
            this.pbCupon3.Size = new System.Drawing.Size(126, 141);
            this.pbCupon3.TabIndex = 19;
            this.pbCupon3.TabStop = false;
            this.pbCupon3.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon4
            // 
            this.pbCupon4.Location = new System.Drawing.Point(512, 635);
            this.pbCupon4.Name = "pbCupon4";
            this.pbCupon4.Size = new System.Drawing.Size(126, 141);
            this.pbCupon4.TabIndex = 20;
            this.pbCupon4.TabStop = false;
            this.pbCupon4.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon5
            // 
            this.pbCupon5.Location = new System.Drawing.Point(638, 635);
            this.pbCupon5.Name = "pbCupon5";
            this.pbCupon5.Size = new System.Drawing.Size(126, 141);
            this.pbCupon5.TabIndex = 21;
            this.pbCupon5.TabStop = false;
            this.pbCupon5.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon6
            // 
            this.pbCupon6.Location = new System.Drawing.Point(66, 467);
            this.pbCupon6.Name = "pbCupon6";
            this.pbCupon6.Size = new System.Drawing.Size(126, 141);
            this.pbCupon6.TabIndex = 22;
            this.pbCupon6.TabStop = false;
            this.pbCupon6.Tag = 5;
            this.pbCupon6.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon7
            // 
            this.pbCupon7.Location = new System.Drawing.Point(321, 467);
            this.pbCupon7.Name = "pbCupon7";
            this.pbCupon7.Size = new System.Drawing.Size(126, 141);
            this.pbCupon7.TabIndex = 23;
            this.pbCupon7.TabStop = false;
            this.pbCupon7.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon8
            // 
            this.pbCupon8.Location = new System.Drawing.Point(566, 467);
            this.pbCupon8.Name = "pbCupon8";
            this.pbCupon8.Size = new System.Drawing.Size(126, 141);
            this.pbCupon8.TabIndex = 24;
            this.pbCupon8.TabStop = false;
            this.pbCupon8.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon9
            // 
            this.pbCupon9.Location = new System.Drawing.Point(198, 296);
            this.pbCupon9.Name = "pbCupon9";
            this.pbCupon9.Size = new System.Drawing.Size(126, 141);
            this.pbCupon9.TabIndex = 25;
            this.pbCupon9.TabStop = false;
            this.pbCupon9.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pbCupon10
            // 
            this.pbCupon10.Location = new System.Drawing.Point(448, 296);
            this.pbCupon10.Name = "pbCupon10";
            this.pbCupon10.Size = new System.Drawing.Size(126, 141);
            this.pbCupon10.TabIndex = 26;
            this.pbCupon10.TabStop = false;
            this.pbCupon10.Click += new System.EventHandler(this.pbCupones_Click);
            // 
            // pnlArbol
            // 
            this.pnlArbol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlArbol.BackColor = System.Drawing.Color.White;
            this.pnlArbol.Controls.Add(this.pbCupon10);
            this.pnlArbol.Controls.Add(this.pbCupon9);
            this.pnlArbol.Controls.Add(this.pbCupon8);
            this.pnlArbol.Controls.Add(this.pbCupon7);
            this.pnlArbol.Controls.Add(this.pbCupon6);
            this.pnlArbol.Controls.Add(this.pbCupon5);
            this.pnlArbol.Controls.Add(this.pbCupon4);
            this.pnlArbol.Controls.Add(this.pbCupon3);
            this.pnlArbol.Controls.Add(this.pbCupon2);
            this.pnlArbol.Controls.Add(this.pbCupon1);
            this.pnlArbol.Controls.Add(this.pbCupon0);
            this.pnlArbol.Controls.Add(this.btnCupon11);
            this.pnlArbol.Controls.Add(this.shapeContainer1);
            this.pnlArbol.Location = new System.Drawing.Point(1, 380);
            this.pnlArbol.Name = "pnlArbol";
            this.pnlArbol.Size = new System.Drawing.Size(767, 790);
            this.pnlArbol.TabIndex = 16;
            this.pnlArbol.Visible = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape12,
            this.lineShape11,
            this.lineShape10,
            this.lineShape9,
            this.lineShape8,
            this.lineShape7,
            this.lineShape6,
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(767, 790);
            this.shapeContainer1.TabIndex = 15;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape12
            // 
            this.lineShape12.Name = "lineShape12";
            this.lineShape12.X1 = 139;
            this.lineShape12.X2 = 184;
            this.lineShape12.Y1 = 578;
            this.lineShape12.Y2 = 640;
            // 
            // lineShape11
            // 
            this.lineShape11.Name = "lineShape11";
            this.lineShape11.X1 = 395;
            this.lineShape11.X2 = 432;
            this.lineShape11.Y1 = 579;
            this.lineShape11.Y2 = 635;
            // 
            // lineShape10
            // 
            this.lineShape10.Name = "lineShape10";
            this.lineShape10.X1 = 643;
            this.lineShape10.X2 = 680;
            this.lineShape10.Y1 = 580;
            this.lineShape10.Y2 = 636;
            // 
            // lineShape9
            // 
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.X1 = 618;
            this.lineShape9.X2 = 580;
            this.lineShape9.Y1 = 579;
            this.lineShape9.Y2 = 637;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 373;
            this.lineShape8.X2 = 322;
            this.lineShape8.Y1 = 577;
            this.lineShape8.Y2 = 639;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 120;
            this.lineShape7.X2 = 69;
            this.lineShape7.Y1 = 577;
            this.lineShape7.Y2 = 639;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 464;
            this.lineShape6.X2 = 398;
            this.lineShape6.Y1 = 420;
            this.lineShape6.Y2 = 492;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 305;
            this.lineShape5.X2 = 354;
            this.lineShape5.Y1 = 411;
            this.lineShape5.Y2 = 487;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 217;
            this.lineShape4.X2 = 151;
            this.lineShape4.Y1 = 417;
            this.lineShape4.Y2 = 489;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 564;
            this.lineShape3.X2 = 613;
            this.lineShape3.Y1 = 424;
            this.lineShape3.Y2 = 500;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 333;
            this.lineShape2.X2 = 278;
            this.lineShape2.Y1 = 259;
            this.lineShape2.Y2 = 316;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 456;
            this.lineShape1.X2 = 494;
            this.lineShape1.Y1 = 258;
            this.lineShape1.Y2 = 310;
            // 
            // pnlPuntosBonus
            // 
            this.pnlPuntosBonus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPuntosBonus.BackColor = System.Drawing.Color.White;
            this.pnlPuntosBonus.Controls.Add(this.dataGridView1);
            this.pnlPuntosBonus.Controls.Add(this.button1);
            this.pnlPuntosBonus.Controls.Add(this.button2);
            this.pnlPuntosBonus.Controls.Add(this.button3);
            this.pnlPuntosBonus.Controls.Add(this.button4);
            this.pnlPuntosBonus.Controls.Add(this.button5);
            this.pnlPuntosBonus.Location = new System.Drawing.Point(1, 380);
            this.pnlPuntosBonus.Name = "pnlPuntosBonus";
            this.pnlPuntosBonus.Size = new System.Drawing.Size(767, 790);
            this.pnlPuntosBonus.TabIndex = 22;
            this.pnlPuntosBonus.Visible = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Image = global::Cupones.Winform.Properties.Resources._03_btn_imprimir;
            this.button1.Location = new System.Drawing.Point(16, 710);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 55);
            this.button1.TabIndex = 13;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Image = global::Cupones.Winform.Properties.Resources._03_btn_select_all;
            this.button2.Location = new System.Drawing.Point(318, 710);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 55);
            this.button2.TabIndex = 22;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Transparent;
            this.button3.Image = global::Cupones.Winform.Properties.Resources._03_btn_multi;
            this.button3.Location = new System.Drawing.Point(620, 710);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 55);
            this.button3.TabIndex = 15;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.Transparent;
            this.button4.Image = global::Cupones.Winform.Properties.Resources._03_btn_prev;
            this.button4.Location = new System.Drawing.Point(537, 710);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 55);
            this.button4.TabIndex = 11;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.Transparent;
            this.button5.Image = global::Cupones.Winform.Properties.Resources._03_btn_atras;
            this.button5.Location = new System.Drawing.Point(149, 710);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(85, 55);
            this.button5.TabIndex = 10;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(16, 17);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.ShowRowErrors = false;
            this.dataGridView1.Size = new System.Drawing.Size(732, 690);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Column2";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 5;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Column3";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 5;
            // 
            // lblTiempoCupon
            // 
            this.lblTiempoCupon.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoCupon.ForeColor = System.Drawing.SystemColors.Window;
            this.lblTiempoCupon.Location = new System.Drawing.Point(272, 220);
            this.lblTiempoCupon.Name = "lblTiempoCupon";
            this.lblTiempoCupon.Size = new System.Drawing.Size(198, 45);
            this.lblTiempoCupon.TabIndex = 23;
            this.lblTiempoCupon.TabStop = false;
            this.lblTiempoCupon.Text = "00:00:00";
            this.lblTiempoCupon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmMenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = global::Cupones.Winform.Properties.Resources._03_background;
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.ControlBox = false;
            this.Controls.Add(this.pnlPuntosBonus);
            this.Controls.Add(this.pnlCabecera);
            this.Controls.Add(this.pnlBotones);
            this.Controls.Add(this.pnlCupones);
            this.Controls.Add(this.pnlSuperCupon);
            this.Controls.Add(this.pnlArbol);
            this.Controls.Add(this.pnlBanner);
            this.Controls.Add(this.pnlRuleta);
            this.Controls.Add(this.pbMensajeNoCupones);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMenuPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CENCOSUD";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlCabecera.ResumeLayout(false);
            this.pnlCabecera.PerformLayout();
            this.pnlBotones.ResumeLayout(false);
            this.pnlSuperCupon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbSuperCupon)).EndInit();
            this.pnlRuleta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRuleta)).EndInit();
            this.pnlCupones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataViewImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMensajeNoCupones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBanner)).EndInit();
            this.pnlBanner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon10)).EndInit();
            this.pnlArbol.ResumeLayout(false);
            this.pnlPuntosBonus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlCabecera;
        private System.Windows.Forms.Button btnGrupo1S;
        private System.Windows.Forms.Button btnGrupo2S;
        private System.Windows.Forms.Button btnGrupo3S;
        private System.Windows.Forms.Button btnGrupo4S;
        private System.Windows.Forms.Button btnGrupo5S;
        private System.Windows.Forms.Button btnGrupo6S;
        private System.Windows.Forms.Button btnGrupo7S;
        private System.Windows.Forms.Button btnGrupo8S;
        private System.Windows.Forms.Button btnGrupo9S;
        private System.Windows.Forms.Panel pnlBotones;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblTiempoSession;
        private System.Windows.Forms.Label lblNombleCliente;
        private System.Windows.Forms.Panel pnlSuperCupon;
        private System.Windows.Forms.Button btnRegalo;
        private System.Windows.Forms.PictureBox pbSuperCupon;
        private System.Windows.Forms.Panel pnlRuleta;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.PictureBox pbRuleta;
        private System.Windows.Forms.Panel pnlCupones;
        private System.Windows.Forms.PictureBox pbMensajeNoCupones;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnSeleccionar;
        private System.Windows.Forms.Button btnMultiCupon;
        private System.Windows.Forms.Button btnNextImages;
        private System.Windows.Forms.Button btnPreviousImages;
        private System.Windows.Forms.DataGridView dataViewImages;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
        private System.Windows.Forms.PictureBox pbBanner;
        private System.Windows.Forms.Panel pnlBanner;
        private System.Windows.Forms.Button btnCupon11;
        private System.Windows.Forms.PictureBox pbCupon0;
        private System.Windows.Forms.PictureBox pbCupon1;
        private System.Windows.Forms.PictureBox pbCupon2;
        private System.Windows.Forms.PictureBox pbCupon3;
        private System.Windows.Forms.PictureBox pbCupon5;
        private System.Windows.Forms.PictureBox pbCupon6;
        private System.Windows.Forms.PictureBox pbCupon7;
        private System.Windows.Forms.PictureBox pbCupon8;
        private System.Windows.Forms.PictureBox pbCupon9;
        private System.Windows.Forms.PictureBox pbCupon10;
        private System.Windows.Forms.PictureBox pbCupon4;
        private System.Windows.Forms.Panel pnlArbol;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape12;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Controls.TransparentLabel lblTiempoCupon;
        private System.Windows.Forms.Panel pnlPuntosBonus;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
    }
}