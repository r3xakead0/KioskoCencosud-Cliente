﻿namespace Cupones.Winform
{
    partial class FrmCupon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCupon = new System.Windows.Forms.Panel();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.pbCupon = new System.Windows.Forms.PictureBox();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.pnlCupon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCupon
            // 
            this.pnlCupon.BackColor = System.Drawing.Color.Black;
            this.pnlCupon.Controls.Add(this.btnCerrar);
            this.pnlCupon.Controls.Add(this.pbCupon);
            this.pnlCupon.Controls.Add(this.btnImprimir);
            this.pnlCupon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCupon.Location = new System.Drawing.Point(0, 0);
            this.pnlCupon.Name = "pnlCupon";
            this.pnlCupon.Size = new System.Drawing.Size(768, 1366);
            this.pnlCupon.TabIndex = 0;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Image = global::Cupones.Winform.Properties.Resources._08_btn_cerrar;
            this.btnCerrar.Location = new System.Drawing.Point(676, 19);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(64, 64);
            this.btnCerrar.TabIndex = 5;
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // pbCupon
            // 
            this.pbCupon.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbCupon.BackColor = System.Drawing.Color.Transparent;
            this.pbCupon.Location = new System.Drawing.Point(115, 33);
            this.pbCupon.Name = "pbCupon";
            this.pbCupon.Size = new System.Drawing.Size(534, 1230);
            this.pbCupon.TabIndex = 4;
            this.pbCupon.TabStop = false;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnImprimir.FlatAppearance.BorderSize = 0;
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Image = global::Cupones.Winform.Properties.Resources._08_btn_imprimir;
            this.btnImprimir.Location = new System.Drawing.Point(195, 1269);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(375, 93);
            this.btnImprimir.TabIndex = 3;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // FrmCupon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.ControlBox = false;
            this.Controls.Add(this.pnlCupon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCupon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cupon";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCupon_Load);
            this.pnlCupon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCupon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCupon;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.PictureBox pbCupon;
        private System.Windows.Forms.Button btnImprimir;

    }
}