﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using BL = Cupones.BusinessLogic;

namespace Cupones.Winform
{
    public partial class FrmExperiencia : Form
    {

        private int tiempoSesion = 45; //Tiempo maximo de sesion en segundos
        private Timer tmrSesion = new Timer(); //Temporizador de sesion

        private string nroDocumento = "";
        private int tipoDocumento = 0;

        public FrmExperiencia()
        {
            InitializeComponent();

            try
            {
                this.lblTiempoSession.Text = this.tiempoSesion.ToString();

                //Configurar tiempo en sesion
                
                this.tmrSesion.Interval = 1000;
                this.tmrSesion.Tick += new EventHandler(tmrSesion_Tick);
                this.tmrSesion.Enabled = false;
                this.tmrSesion.Stop();

                //Agregar evento MouseClick a todos los controles
                this.MouseClick += new MouseEventHandler(this.All_MouseClick);

                foreach (Control crtl in this.Controls)
                {
                    if (crtl is Panel)
                    {
                        Panel pnlChild = (crtl as Panel);
                        this.AddMouseClick(pnlChild);
                    }
                    else
                        crtl.MouseClick += new MouseEventHandler(this.All_MouseClick);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporalizador de sesion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrSesion_Tick(object sender, EventArgs e)
        {
            try
            {
                int segundos = int.Parse(this.lblTiempoSession.Text);

                if (segundos > 0)
                {
                    this.lblTiempoSession.Text = (segundos - 1).ToString();
                }
                else
                {
                    this.ActivarSesion(false);

                    var inicio = FrmContenedor.Instance.Inicio();
                    inicio.Limpiar();

                    FrmContenedor.Instance.CerrarSesion();
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Agregar evento MouseClick a todos los controles del panel
        /// </summary>
        /// <param name="pnl">Control del tipo panel</param>
        private void AddMouseClick(Panel pnl)
        {
            try
            {
                pnl.MouseClick += new MouseEventHandler(this.All_MouseClick);

                foreach (Control crtl in pnl.Controls)
                {
                    if (crtl is Panel)
                    {
                        Panel pnlChild = (crtl as Panel);
                        this.AddMouseClick(pnlChild);
                    }
                    else
                        crtl.MouseClick += new MouseEventHandler(this.All_MouseClick);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Evento Mouse Click del control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void All_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                this.lblTiempoSession.Text = this.tiempoSesion.ToString();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Activa o desactiva el tiempo de sesion
        /// </summary>
        /// <param name="si"></param>
        public void ActivarSesion(bool si = false)
        {
            try
            {
                this.lblTiempoSession.Text = this.tiempoSesion.ToString();
                this.tmrSesion.Enabled = si;
                if (si)
                    this.tmrSesion.Start();
                else
                    this.tmrSesion.Stop();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Carga de formulario 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmExperiencia_Load(object sender, EventArgs e)
        {
            try
            {

                this.lblMensaje.Visible = false;

                this.btnNo.Enabled = true;
                this.btnAlgo.Enabled = true;
                this.btnMegusto.Enabled = true;
                this.btnMeencanto.Enabled = true;

                this.btnMeencanto.Select();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }


        /// <summary>
        /// Evento click del boton Salir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivarSesion(false);

                var inicio = FrmContenedor.Instance.Inicio();
                inicio.Limpiar();

                FrmContenedor.Instance.CerrarSesion();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton Inicio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInicio_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivarSesion(false);

                FrmContenedor.Instance.MenuPrincipal();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Guarda la Experiencia del cliente
        /// </summary>
        /// <param name="ambito"></param>
        /// <param name="codTienda"></param>
        /// <returns></returns>
        public bool GuardaExperiencia(int tipoDoc, string nroDoc, int idExperiencia)
        {
            bool rpta = false;
            try
            {
                string tienda = General.Configuracion.Tienda;
                string hostname = General.Configuracion.Hostname;

                rpta = new BL.Experiencias().GuardaExperiencia(this.tipoDocumento, this.nroDocumento, idExperiencia, tienda, hostname);
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bloquea todos los botones
        /// </summary>
        /// <param name="si"></param>
        private void BloquearControles(bool si = true)
        {
            try
            {
                this.btnSalir.Enabled = !si;
                this.btnInicio.Enabled = !si;

                this.lblMensaje.Visible = si;

                this.btnNo.Enabled = !si;
                this.btnAlgo.Enabled = !si;
                this.btnMegusto.Enabled = !si;
                this.btnMeencanto.Enabled = !si;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Evento click de los botones de Experiencia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExperiencia_Click(object sender, EventArgs e)
        {
            try
            {
                this.nroDocumento = FrmContenedor.Instance.NroDocumento;
                this.tipoDocumento = FrmContenedor.Instance.TipoDocumento;

                this.BloquearControles(true);

                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += (s, args) =>
                {
                    var btn = (sender as Button);
                    int idExperiencia = int.Parse(btn.Tag.ToString());

                    args.Result = this.GuardaExperiencia(this.tipoDocumento, this.nroDocumento, idExperiencia);
                };
                bw.RunWorkerCompleted += (s, args) =>
                {
                    this.BloquearControles(false);

                    bool rpta = (bool)args.Result;
                    if (rpta == true)
                    {
                        this.ActivarSesion(false);

                        var inicio = FrmContenedor.Instance.Inicio();
                        inicio.Limpiar();

                        FrmContenedor.Instance.CerrarSesion();
                    }
                };
                bw.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        

    }
}
