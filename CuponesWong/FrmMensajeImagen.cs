using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Cupones.Winform
{
    public partial class FrmMensajeImagen : Form
    {

        #region Singleton pattern

        private static FrmMensajeImagen instance = null;

        public static FrmMensajeImagen Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmMensajeImagen();

                return instance;
            }
        }

        #endregion

        private Timer tmrMensaje = null;

        public FrmMensajeImagen()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Mostrar imagen de mensaje
        /// </summary>
        /// <param name="mensaje">Imagen del mensaje</param>
        public void MostrarMensaje(Image mensaje)
        {
            try
            {
                if (mensaje != null)
                {
                    this.pbMensaje.Image = mensaje;
                    this.pbMensaje.Visible = true;
                }
                else
                {
                    this.pbMensaje.Image = null;
                    this.pbMensaje.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FrmPanel2_Load(object sender, EventArgs e)
        {
            try
            {
                this.tmrMensaje = new Timer();
                this.tmrMensaje.Interval = 10 * 1000;
                this.tmrMensaje.Tick += new System.EventHandler(this.tmrMensaje_tick);
                this.tmrMensaje.Enabled = true;
                this.tmrMensaje.Start();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void FrmPanel2_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics mGraphics = e.Graphics;
                Pen pen1 = new Pen(Color.FromArgb(255, 0, 0), 1);

                Rectangle Area1 = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                LinearGradientBrush LGB = new LinearGradientBrush(Area1, Color.FromArgb(255, 0, 0), Color.FromArgb(255, 251, 251), LinearGradientMode.Vertical);
                mGraphics.FillRectangle(LGB, Area1);
                mGraphics.DrawRectangle(pen1, Area1);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void tmrMensaje_tick(object sender, EventArgs e)
        {
            try
            {
                this.tmrMensaje.Enabled = false;
                this.tmrMensaje.Stop();

                this.pbMensaje.Image = null;
                this.pbMensaje.Visible = false;

                this.Close();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }
    }
}