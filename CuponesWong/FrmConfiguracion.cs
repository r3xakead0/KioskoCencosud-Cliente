﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.Net;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using System.Diagnostics;

namespace Cupones.Winform
{
    public partial class FrmConfiguracion : Form
    {

        private string hostname = ""; //Nombre de la PC
        private string appClave = ""; //Clave de la Aplicacion

        private TextBox crtlTexto = null;

        public FrmConfiguracion()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                General.RegistrarError(args.Exception);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                General.RegistrarError((args.ExceptionObject as Exception));
            };
        }

        private void FrmConfiguracion_Load(object sender, EventArgs e)
        {
            try
            {
                this.AgregarClickTeclado();
                this.AgregarEnterTextos();

                this.CargarCompanias();
                this.CargarTiendas();

                this.CargarVersion();

                this.CargarConfiguracion();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener version del aplicativo
        /// </summary>
        private void CargarVersion()
        {
            try
            {
                string version = General.GetVersion();
                this.lblVersion.Text = string.Format("VERSIÓN {0}", version);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cargar lista de compañias
        /// </summary>
        private void CargarCompanias()
        {
            try
            {
                
                var lstCompanhia = new BL.Configuraciones().ListarCompanhias();
                if (lstCompanhia.Count > 0)
                {
                    this.cbCompañia.DataSource = lstCompanhia;
                    this.cbCompañia.DisplayMember = "Nombre";
                    this.cbCompañia.ValueMember = "Codigo";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cargar lista de tiendas
        /// </summary>
        private void CargarTiendas()
        {
            try
            {
                var lstTienda = new BL.Configuraciones().ListarTiendas();
                if (lstTienda.Count > 0)
                {
                    this.cbTienda.DataSource = lstTienda;
                    this.cbTienda.DisplayMember = "Nombre";
                    this.cbTienda.ValueMember = "Codigo";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Agregar evento click a los botones del formulario
        /// </summary>
        private void AgregarClickTeclado()
        {
            try
            {
                foreach (Control ctrl in this.Controls)
                {
                    if (ctrl is Button)
                    {
                        string nombre = ctrl.Name;
                        if (nombre.Equals("btnCancelar") == true)
                        {
                            ctrl.Click += new System.EventHandler(this.btnCancelar_Click);
                        }
                        else if (nombre.Equals("btnGuardar") == true)
                        {
                            ctrl.Click += new System.EventHandler(this.btnGuardar_Click);
                        }
                        else if (nombre.Equals("btnReiniciar") == true)
                        {
                            ctrl.Click += new System.EventHandler(this.btnReiniciar_Click);
                        }
                        else //Teclado
                        {
                            ctrl.Click += new System.EventHandler(this.btnTeclado_Click);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Agregar evento enter a los textbox del formulario
        /// </summary>
        private void AgregarEnterTextos()
        {
            try
            {
                //Agregar evento enter a los textbox del panel de configuracion
                foreach (var ctrlConfig in this.GBConfig.Controls)
                {
                    if (ctrlConfig is TextBox)
                    {
                        (ctrlConfig as TextBox).Enter += new System.EventHandler(this.txtTexto_Enter);
                    }
                }

                //Agregar evento enter a los textbox del panel de soporte
                foreach (var ctrlSoporte in this.GBST.Controls)
                {
                    if (ctrlSoporte is TextBox)
                    {
                        (ctrlSoporte as TextBox).Enter += new System.EventHandler(this.txtTexto_Enter);
                    }
                }

                //Agregar evento enter a los textbox del panel de correo
                foreach (var ctrlCorreo in this.GBCC.Controls)
                {
                    if (ctrlCorreo is TextBox)
                    {
                        (ctrlCorreo as TextBox).Enter += new System.EventHandler(this.txtTexto_Enter);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cargar la configuracion de la tienda
        /// </summary>
        public void CargarConfiguracion()
        {
            try
            {

                BE.Configuracion configTienda = null;

                if (General.Configuracion == null)
                {
                    string tienda = ConfigurationManager.AppSettings["Tienda"];
                    string hostname = Dns.GetHostName();
                    configTienda = new BL.Configuraciones().Obtener(tienda, hostname);
                }
                else
                    configTienda = General.Configuracion;

                if (configTienda != null)
                {
                    this.cbCompañia.Text = configTienda.Compania;
                    this.cbTienda.Text = configTienda.Tienda;
                    this.hostname = configTienda.Hostname;
                    this.TBUbiTienda.Text = configTienda.Ubicacion;

                    this.TBTelfST.Text = configTienda.TecnicoTelefono;
                    this.TBEmailST.Text = configTienda.TecnicoEmail;

                    this.TBServidor.Text = configTienda.EmailServidor;
                    this.TBPuerto.Text = configTienda.EmailPuerto.ToString();
                    this.TBEmail.Text = configTienda.EmailCuenta;
                    this.TBPassword.Text = configTienda.EmailClave;

                    this.appClave = configTienda.AplicacionClave.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Evento click del boton reiniciar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("shutdown", "/r /t 0");
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton cancelar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                var inicio = FrmContenedor.Instance.Inicio();
                inicio.Limpiar();

                FrmContenedor.Instance.ValidacionesAutomaticas(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {

                #region Validar Email
                bool validateEmail = false;
                var strEmails = this.TBEmailST.Text.Trim();
                if (strEmails.Length > 0)
                {
                    var arrEmails = strEmails.Split(',');
                    for (int i = 0; i < arrEmails.Length; i++)
                    {
                        string email = arrEmails[i];
                        if (General.IsValidEmail(email) == false)
                        {
                            validateEmail = false;
                            break;
                        }
                    }
                }

                if (validateEmail == false)
                {
                    string msg = "El formato del email es invalido";
                    FrmContenedor.Instance.Alerta(msg);
                    return;
                }

                #endregion

                var beConfiguracion = new BE.Configuracion();

                beConfiguracion.EmailServidor = this.TBServidor.Text;
                beConfiguracion.EmailPuerto = int.Parse(this.TBPuerto.Text);
                beConfiguracion.EmailCuenta = this.TBEmail.Text;
                beConfiguracion.EmailClave = this.TBPassword.Text;
                beConfiguracion.Compania = this.cbCompañia.Text;
                beConfiguracion.Tienda = this.cbTienda.Text;
                beConfiguracion.Hostname = this.hostname;
                beConfiguracion.Ubicacion = this.TBUbiTienda.Text;
                beConfiguracion.AplicacionClave = this.appClave;
                beConfiguracion.TecnicoEmail = this.TBEmailST.Text;
                beConfiguracion.TecnicoTelefono = this.TBTelfST.Text;

                bool rpta = new BL.Configuraciones().Actualizar(beConfiguracion);

                if (rpta == true)
                {
                    General.Configuracion = beConfiguracion;

                    var inicio = FrmContenedor.Instance.Inicio();
                    inicio.Limpiar();
                }

                FrmContenedor.Instance.ValidacionesAutomaticas(true);

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento de ingreso al control de texto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTexto_Enter(object sender, EventArgs e)
        {
            try
            {
                this.crtlTexto = (sender as TextBox);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click de los botones del teclado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeclado_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.crtlTexto == null) //No selecciono un textbox
                    return;

                if (this.crtlTexto.ReadOnly == true) //Selecciono un textbox solo de lectura
                    return;

                string texto = this.crtlTexto.Text;

                var boton = sender as Button;
                string valor = boton.Text;

                if (valor == "<=") //Borrar
                {
                    texto = texto.Remove(texto.Length - 1);
                }
                else if (crtlTexto.Name.Equals("TBLongPapel") 
                    || crtlTexto.Name.Equals("TBLongCupon")
                    || crtlTexto.Name.Equals("TBTotImp")
                    || crtlTexto.Name.Equals("TBPuerto"))
                {
                    int numero = 0;
                    if (int.TryParse(valor, out numero) == true) //Solo Numeros
                        texto += valor;
                }
                else
                {
                    texto += valor;
                }

                this.crtlTexto.Text = texto;

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void tbImpresion_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        
    }
}
