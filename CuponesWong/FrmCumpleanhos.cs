﻿using System;
using System.Windows.Forms;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using System.Threading.Tasks;

namespace Cupones.Winform
{
    public partial class FrmCumpleanhos : Form
    {

        int tiempoVisible = 0;
        Timer tmrVisible = null;

        public FrmCumpleanhos()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Reproducir canción de cumpleaños
        /// </summary>
        private void Playaudio()
        {
            try
            {
                ((System.ComponentModel.ISupportInitialize)(this.wmpCumpleanhos)).BeginInit();
                ((System.ComponentModel.ISupportInitialize)(this.wmpCumpleanhos)).EndInit();

                this.wmpCumpleanhos.URL = @"Songs\cumpleanhos.wav";
                this.wmpCumpleanhos.Ctlcontrols.play();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Parar canción de cumpleaños
        /// </summary>
        private void StopAudio()
        {
            try
            {
                this.wmpCumpleanhos.Ctlcontrols.stop();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Inicia()
        {
            try
            {
                this.tiempoVisible = 5;
                this.lblTiempo.Text = tiempoVisible.ToString();

                string nroDoc = FrmContenedor.Instance.NroDocumento;
                var tskObtenerCliente = Task.Factory.StartNew(() => this.ObtenerCliente(nroDoc));

                try
                {
                    tskObtenerCliente.Wait();
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }

                if (tskObtenerCliente.Result != null)
                {
                    BE.Cliente beCliente = tskObtenerCliente.Result;
                    this.lblCliente.Text = beCliente.NompleCompleto;
                }

                this.Playaudio();

                this.tmrVisible = new Timer();
                this.tmrVisible.Tick += new EventHandler(tmrVisible_tick);
                this.tmrVisible.Interval = 1000;
                this.tmrVisible.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void tmrVisible_tick(object sender, EventArgs e) 
        {
            try
            {
                if (this.tiempoVisible == 0)
                {
                    this.tmrVisible.Stop();
                    this.StopAudio();
                    var menu = FrmContenedor.Instance.MenuPrincipal();
                    menu.Inicia(true);
                }
                else
                {
                    this.tiempoVisible--;
                    this.lblTiempo.Text = this.tiempoVisible.ToString();
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                this.tmrVisible.Stop();
                this.StopAudio();
                var menu = FrmContenedor.Instance.MenuPrincipal();
                menu.Inicia(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener los datos del cliente
        /// </summary>
        /// <param name="nroDoc">Numero de Documento</param>
        /// <returns></returns>
        private BE.Cliente ObtenerCliente(string nroDoc)
        {
            BE.Cliente cliente = null;

            try
            {
                cliente = new BL.Clientes().ObtenerCliente(nroDoc);
                return cliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
