﻿using System;
using System.Windows.Forms;
using COMMON = Cupones.Common;

namespace Cupones.Winform
{
    public partial class FrmPoliticas : Form
    {

        #region Singleton pattern

        private static FrmPoliticas instance = null;

        public static FrmPoliticas Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmPoliticas();
                return instance;
            }
        }

        #endregion

        public FrmPoliticas()
        {
            InitializeComponent();
        }

        private void FrmPoliticas_Load(object sender, EventArgs e)
        {
            try
            {
                string compania = General.Ambito;
                string politicas = "";

                politicas = new COMMON.Legal(compania).GetPoliciesAndPrivacy();

                this.txtPoliticas.Text = politicas;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }
    }
}
