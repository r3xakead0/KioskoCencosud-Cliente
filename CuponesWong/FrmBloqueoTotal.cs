using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Text;
using System.Reflection;

namespace Cupones.Winform
{
    public partial class FrmBloqueoTotal : Form
    {

        #region Singleton pattern

        private static FrmBloqueoTotal instance = null;

        public static FrmBloqueoTotal Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmBloqueoTotal();

                return instance;
            }
        }

        #endregion

        public FrmBloqueoTotal()
        {
            InitializeComponent();
        }

        private void FrmPanel2_Load(object sender, EventArgs e)
        {
            try
            {
                General.RegistrarAdvertencia(this.lblMensajeDescripcion.Text, MethodBase.GetCurrentMethod());
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void FrmPanel2_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics mGraphics = e.Graphics;
                Pen pen1 = new Pen(Color.FromArgb(255, 0, 0), 1);

                Rectangle Area1 = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                LinearGradientBrush LGB = new LinearGradientBrush(Area1, Color.FromArgb(255, 0, 0), Color.FromArgb(255, 251, 251), LinearGradientMode.Vertical);
                mGraphics.FillRectangle(LGB, Area1);
                mGraphics.DrawRectangle(pen1, Area1);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

    }
}