﻿using System;
using System.Text;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using log4net;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;

namespace Cupones.Winform
{

    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class General
    {
        public static string Ambito = "WONG"; //Ambito

        public static BE.Configuracion Configuracion = null; //Configuraciones de Kiosko

        /// <summary>
        /// Registrar el error
        /// </summary>
        /// <param name="ex">Objeto de error</param>
        public static void RegistrarError(Exception ex)
        {
            if (Configuracion == null)
                return;

            string origen = ex.TargetSite == null ? "" : ex.TargetSite.Name + ":";

            StringBuilder error = new StringBuilder();
            error.AppendLine("Message   : " + ex.Message.ToString());
            error.AppendLine("Trace     : " + origen + ex.StackTrace.ToString());

            var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            log.Error(error);

            int tecnico = 1;
            string tienda = Configuracion.Tienda;
            string hostname = Configuracion.Hostname;
            var taskError = Task.Factory.StartNew(() => new BL.Log(tienda, hostname).RegistrarError(tecnico, ex.Message, origen + ex.StackTrace));
        }

        /// <summary>
        /// Registrar la advertencia
        /// </summary>
        /// <param name="mensaje">Mensaje de advertencia</param>
        /// <param name="info">Objeto de origen</param>
        public static void RegistrarAdvertencia(string mensaje, MethodBase info)
        {
            if (Configuracion == null)
                return;

            var trace = string.Format("{0}.{1}.{2}()", info.ReflectedType.Namespace, info.ReflectedType.Name, info.Name);

            StringBuilder error = new StringBuilder();
            error.AppendLine("Message   : " + mensaje);
            error.AppendLine("Trace     : " + trace);

            var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            log.Error(error);

            int usuario = 2;
            string tienda = Configuracion.Tienda;
            string hostname = Configuracion.Hostname;
            var taskAdvertencia = Task.Factory.StartNew(() => new BL.Log(tienda, hostname).RegistrarError(usuario, mensaje, trace));
        }
        
        
        
        /// <summary>
        /// Convertir los segundos en formato de tiempo
        /// </summary>
        /// <param name="seconds">Número de segundos</param>
        /// <returns></returns>
        public static string ParseSecondsToTime(int seconds)
        {
            var span = TimeSpan.FromSeconds(seconds);

            string strValor = string.Format("{0:00}:{1:00}:{2:00}", (int)span.TotalHours, span.Minutes, span.Seconds);

            return strValor;
        }

        /// <summary>
        /// Validar el formato del Correo electronico
        /// </summary>
        /// <param name="email">Correo electronico (EMAIL)</param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, @"\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}\z")
                && Regex.IsMatch(email, @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*");
        }

        /// <summary>
        /// Obtener el numero IP del Kiosko
        /// </summary>
        /// <returns></returns>
        public static string GetLocalIPAddress()
        {
            string localIpAddress = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIpAddress = ip.ToString();
                    break;
                }
            }
            return localIpAddress;
        }

        /// <summary>
        /// Obtener la versión del software del Kiosko
        /// </summary>
        /// <returns></returns>
        public static string GetVersion()
        {
            try
            {
                string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                return version;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
