namespace Cupones.Winform
{
    partial class FrmBloqueoParcial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMensajeTitulo = new System.Windows.Forms.Label();
            this.lblMensajeDescripcion = new System.Windows.Forms.Label();
            this.pnHabilitar = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblMensajeTitulo
            // 
            this.lblMensajeTitulo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMensajeTitulo.AutoSize = true;
            this.lblMensajeTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblMensajeTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblMensajeTitulo.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensajeTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblMensajeTitulo.Location = new System.Drawing.Point(39, 625);
            this.lblMensajeTitulo.Name = "lblMensajeTitulo";
            this.lblMensajeTitulo.Size = new System.Drawing.Size(230, 34);
            this.lblMensajeTitulo.TabIndex = 9;
            this.lblMensajeTitulo.Text = "LO SENTIMOS. ";
            // 
            // lblMensajeDescripcion
            // 
            this.lblMensajeDescripcion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMensajeDescripcion.AutoSize = true;
            this.lblMensajeDescripcion.BackColor = System.Drawing.Color.Transparent;
            this.lblMensajeDescripcion.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold);
            this.lblMensajeDescripcion.Location = new System.Drawing.Point(39, 703);
            this.lblMensajeDescripcion.Name = "lblMensajeDescripcion";
            this.lblMensajeDescripcion.Size = new System.Drawing.Size(543, 102);
            this.lblMensajeDescripcion.TabIndex = 10;
            this.lblMensajeDescripcion.Text = "EL EQUIPO SE QUED� SIN PAPEL. \nPOR FAVOR, COMUNICARSE CON UN \nPERSONAL DE TIENDA." +
    "";
            // 
            // pnHabilitar
            // 
            this.pnHabilitar.BackColor = System.Drawing.Color.Transparent;
            this.pnHabilitar.Location = new System.Drawing.Point(4, 3);
            this.pnHabilitar.Name = "pnHabilitar";
            this.pnHabilitar.Size = new System.Drawing.Size(220, 221);
            this.pnHabilitar.TabIndex = 44;
            this.pnHabilitar.Click += new System.EventHandler(this.pnHabilitar_Click);
            // 
            // FrmBloqueoParcial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.ControlBox = false;
            this.Controls.Add(this.pnHabilitar);
            this.Controls.Add(this.lblMensajeDescripcion);
            this.Controls.Add(this.lblMensajeTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmBloqueoParcial";
            this.Opacity = 0.8D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel";
            this.Load += new System.EventHandler(this.FrmPanel_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmPanel_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMensajeTitulo;
        public System.Windows.Forms.Label lblMensajeDescripcion;
        public System.Windows.Forms.Panel pnHabilitar;
    }
}