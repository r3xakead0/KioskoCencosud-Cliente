﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;
using System.Management;
using BE = Cupones.BusinessEntity;
using BL = Cupones.BusinessLogic;
using COMMON = Cupones.Common;

namespace Cupones.Winform
{
    public partial class FrmMenuPrincipal : Form
    {

        //private const int MISCUPONES = 1;
        //private const int SUPERCUPONES = 2;
        //private const int CUPONESMARCA = 3;
        //private const int CUPONESLUGAR = 4;
        //private const int RULETA = 5;
        //private const int CUPONESARBOL = 6;
        //private const int CUPONESBONUS = 7;
        //private const int CUPONESINVITACIONES = 8;
        //private const int CUPONESCOMERCIALES = 9;

        private int tiempoSesion = 45; //Tiempo maximo de sesion en segundos
        private Timer tmrSesion = new Timer(); //Temporizador de sesion

        private BE.Perfil perfilAplicacion = new BE.Perfil();

        private string nroDocumento = ""; //Numero de Documento del Cliente
        private int tipoDocumento = 1; //Tipo del documento del Cliente (1 = DNI | 2 = OTRO)
        private BE.Cliente cliente = new BE.Cliente(); //Datos del Cliente

        private string ambito = "";  //Ambito
        private string tienda = ""; //Codigo de Tienda
        private string hostname = ""; //Nombre de Host

        private List<BE.Cupon> lstCupones = null; //Mis Cupones, Marcas y Lugares
        private List<BE.Cupon> lstCuponesSeleccionados = null; //Seleccionados Mis Cupones, Marcas y Lugares
        private BE.Cupon superCupon = null; //Super Cupon
        private List<BE.Cupon> lstArbolCupon = null; //Arbol de Cupones

        private int idGrupo = 0; //Id del boton seleccionado
        private bool multiCupon = false; //Flag de boton MultiCupon
        private bool seleccionTodos = false; //Flag de boton Seleccion

        private int idCupon = 0; //Id del Cupon para imprimir
        private int tipoCupon = 0; //Tipo de Cupon solo para Super Cupon (1 = Cumpleanños | 2 = Supercupon | 3 = Horario)
        private string codigoBarraCupon = ""; //Codigo de Barra del Cupon para imprimir
        private Image impresionCupon = null; //Imagen temporal del cupon para impresion

        private int segundosTiempoCupon = 0;
        private Timer tmrTiempoCupon = new Timer(); //Temporizador de tiempo para super cupon

        #region Variables de la Grilla de Cupones
        private int _imageSize = 300;
        private int _numberPreviewImages = 8;
        private int _currentStartImageIndex = 0;
        private int _currentEndImageIndex = 8;
        #endregion

        private Dictionary<int, Image> BotonesActivos = new Dictionary<int, Image>(); //Imagenes de botones activos
        private Dictionary<int, Image> BotonesInactivos = new Dictionary<int, Image>(); //Imagenes de botones inactivos
        private Dictionary<int, Image> BotonesAlternativos = new Dictionary<int, Image>(); //Imagenes de botones alternos

        private bool animado = false;
        private Timer tmrAnimacion = new Timer(); //Temporizador de animacion de botones nuevos

        private int rotacion = 0;
        private Timer tmrRuleta = new Timer(); //Temporizador de rotacion de ruleta

        #region Variables de Banners
        private int nroBanner = 0; // Nro del Banner
        private int tiempoBanner = 0; // Tiempo de visualizacion del Banner
        private List<BE.Banner> lstBeBanners; // Listado de Banners
        private BE.Banner beBanner; // Banner visible
        private Timer tmrBanners = new Timer(); //Temporizador de carusel de banners
        #endregion

        /// <summary>
        /// Carga del Formulario
        /// </summary>
        public FrmMenuPrincipal()
        {
            InitializeComponent();

            try
            {

                this.lblTiempoSession.Text = this.tiempoSesion.ToString();

                #region Configurar tiempo de super cupon
                this.tmrTiempoCupon.Interval = 1000;
                this.tmrTiempoCupon.Tick += new EventHandler(tmrTiempoCupon_Tick);
                this.tmrTiempoCupon.Enabled = false;
                this.tmrTiempoCupon.Stop();
                #endregion

                #region Configurar tiempo en sesion
                this.tmrSesion.Interval = 1000;
                this.tmrSesion.Tick += new EventHandler(tmrSesion_Tick);
                this.tmrSesion.Enabled = false;
                this.tmrSesion.Stop();
                #endregion

                #region Configurar tiempo en animacion de botones
                this.tmrAnimacion.Interval = 250;
                this.tmrAnimacion.Tick += new EventHandler(tmrAnimacion_Tick);
                this.tmrAnimacion.Enabled = false;
                this.tmrAnimacion.Stop();
                #endregion

                #region Configurar tiempo en rotacion de ruleta
                this.tmrRuleta.Interval = 10;
                this.tmrRuleta.Tick += new EventHandler(tmrRuleta_Tick);
                this.tmrRuleta.Enabled = false;
                this.tmrRuleta.Stop();
                #endregion

                #region Agregar evento MouseClick a todos los controles
                this.MouseClick += new MouseEventHandler(this.All_MouseClick);

                foreach (Control crtl in this.Controls)
                {
                    if (crtl is Panel)
                    {
                        Panel pnlChild = (crtl as Panel);
                        this.AddMouseClick(pnlChild);
                    }
                    else
                        crtl.MouseClick += new MouseEventHandler(this.All_MouseClick);
                }
                #endregion

                #region Configurar tiempo en animacion de banners
                this.tmrBanners.Interval = 1000; // Intervalos de 1 Segundo
                this.tmrBanners.Tick += new EventHandler(tmrBanners_Tick);
                this.tmrBanners.Enabled = false;
                this.tmrBanners.Stop();
                #endregion
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporalizador de carusel de banners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrBanners_Tick(object sender, EventArgs e)
        {
            try
            {
                this.tiempoBanner++;

                if (this.tiempoBanner >= this.beBanner.Tiempo)
                {
                    if ((this.lstBeBanners.Count - 1) == this.nroBanner)
                        this.nroBanner = 0;
                    else
                        this.nroBanner++;

                    this.lstBeBanners[this.nroBanner].Contador = this.lstBeBanners[this.nroBanner].Contador + 1;
                    this.beBanner = this.lstBeBanners[this.nroBanner];

                    if (this.beBanner.Id > 0 
                        && FrmContenedor.Instance.Sesion.BannersVistos.Contains(this.beBanner.Id) == false)
                        FrmContenedor.Instance.Sesion.BannersVistos.Add(this.beBanner.Id);

                    this.pbBanner.Image = this.beBanner.Imagen;

                    this.tiempoBanner = 0;
                }

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporalizador de animacion de botones de grupos nuevos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrAnimacion_Tick(object sender, EventArgs e)
        {
            try
            {
                var idGruposNuevos = this.BotonesAlternativos.Where(i => i.Value != null);
                if (idGruposNuevos != null)
                {
                    foreach (var item in idGruposNuevos)
                    {

                        if (item.Key != this.idGrupo)
                        {
                            string nomGrupoBoton = "btnGrupo" + item.Key.ToString() + "S";
                            var btnGrupoBoton = this.Controls.Find(nomGrupoBoton, true).FirstOrDefault() as Button;

                            if (btnGrupoBoton != null)
                            {
                                var imgAlterno = item.Value;
                                var imgNormal = this.BotonesActivos.FirstOrDefault(f => f.Key == item.Key).Value;

                                animado = !animado;

                                if (animado)
                                    btnGrupoBoton.Image = imgAlterno;
                                else
                                    btnGrupoBoton.Image = imgNormal;
                            }
                            btnGrupoBoton.Refresh();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporalizador de sesion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrTiempoCupon_Tick(object sender, EventArgs e)
        {
            try
            {
                this.segundosTiempoCupon -= 1;
                this.lblTiempoCupon.Text = General.ParseSecondsToTime(this.segundosTiempoCupon);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener los banners del Cliente
        /// </summary>
        /// <returns></returns>
        private List<BE.Banner> ListarBanners()
        {
            var lstBeBanners = new List<BE.Banner>();

            try
            {
                #region Obtener los Banners
                lstBeBanners = new BL.Banners().ListarBanners(this.tienda, this.hostname, this.nroDocumento);
                #endregion

                string path = "";
                Image imgBanner = null;
                var common = new COMMON.Banner();

                if (lstBeBanners.Count > 0)
                {
                    for (int i = 0; i < lstBeBanners.Count; i++)
                    {
                        path = lstBeBanners[i].ImagenPath;
                        imgBanner = common.GetImage(path);

                        lstBeBanners[i].Imagen = imgBanner;
                    }
                }
                else
                {
                    string rutaBannerDefault = FrmContenedor.Instance.RutaBannerKiosko;

                    if (rutaBannerDefault.Trim().Length > 0)
                        path = rutaBannerDefault;

                    imgBanner = common.GetImage(path);

                    var beBanner = new BE.Banner();
                    beBanner.Id = 0;
                    beBanner.Nombre = "Default";
                    beBanner.ImagenPath = path;
                    beBanner.Imagen = imgBanner;
                    beBanner.Orden = 1;
                    beBanner.Prioridad = 1;
                    beBanner.Tiempo = 1;
                    lstBeBanners.Add(beBanner);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }

            return lstBeBanners;
        }

        /// <summary>
        /// Asigna visibilidad e imagen al boton del menu
        /// </summary>
        /// <param name="btnMenu">Boton de menu de grupos</param>
        /// <param name="visible">¿Es visible?</param>
        /// <param name="nuevo">¿Es Nuevo?</param>
        private void AsignarOpcionMenu(ref Button btnMenu, bool visible, bool nuevo)
        {
            try
            {

                if (btnMenu == null)
                    return;

                int idBoton = 0;
                if (int.TryParse(btnMenu.Tag.ToString(), out idBoton) == false)
                    return;

                #region Imagenes de Botones de Menu
                Image[] botonesActivos = {
                            Cupones.Winform.Properties.Resources._02_menu_01_hover,
                            Cupones.Winform.Properties.Resources._02_menu_02_super_hover,
                            Cupones.Winform.Properties.Resources._02_menu_03_hover,
                            Cupones.Winform.Properties.Resources._02_menu_04_hover,
                            Cupones.Winform.Properties.Resources._02_menu_05_hover,
                            Cupones.Winform.Properties.Resources._02_menu_06_hover,
                            Cupones.Winform.Properties.Resources._02_menu_07_hover,
                            Cupones.Winform.Properties.Resources._02_menu_08_hover,
                            Cupones.Winform.Properties.Resources._02_menu_09_hover
                        };
                Image[] botonesInactivos = {
                            Cupones.Winform.Properties.Resources._02_menu_01,
                            Cupones.Winform.Properties.Resources._02_menu_02_super,
                            Cupones.Winform.Properties.Resources._02_menu_03,
                            Cupones.Winform.Properties.Resources._02_menu_04,
                            Cupones.Winform.Properties.Resources._02_menu_05,
                            Cupones.Winform.Properties.Resources._02_menu_06,
                            Cupones.Winform.Properties.Resources._02_menu_07,
                            Cupones.Winform.Properties.Resources._02_menu_08,
                            Cupones.Winform.Properties.Resources._02_menu_09
                        };
                Image[] botonesActivosNuevos = {
                            Cupones.Winform.Properties.Resources._02_menu_01_hover,
                            Cupones.Winform.Properties.Resources._02_menu_02_super_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_03_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_04_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_05_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_06_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_07_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_08_new_activo,
                            Cupones.Winform.Properties.Resources._02_menu_09_new_activo
                        };
                Image[] botonesAlternosNuevos = {
                            Cupones.Winform.Properties.Resources._02_menu_01_hover,
                            Cupones.Winform.Properties.Resources._02_menu_02_super_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_03_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_04_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_05_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_06_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_07_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_08_new_alterno,
                            Cupones.Winform.Properties.Resources._02_menu_09_new_alterno
                        };
                Image[] botonesInactivosNuevos = {
                            Cupones.Winform.Properties.Resources._02_menu_01,
                            Cupones.Winform.Properties.Resources._02_menu_02_super_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_03_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_04_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_05_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_06_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_07_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_08_new_normal,
                            Cupones.Winform.Properties.Resources._02_menu_09_new_normal
                        };

                #endregion

                int indiceBoton = idBoton - 1;
                Image imagenBotonActivo = null;
                Image imagenBotonInactivo = null;
                Image imagenBotonAlterno = null;

                if (nuevo)
                {
                    if (idBoton == 2) //Super Cupon
                    {
                        if (TieneSuperCuponCumpleanhos(this.nroDocumento, this.ambito, this.tienda) == true)
                        {
                            imagenBotonActivo = Cupones.Winform.Properties.Resources._02_menu_02_regalo_new_activo;
                            imagenBotonInactivo = Cupones.Winform.Properties.Resources._02_menu_02_regalo_new_alterno;
                            imagenBotonAlterno = Cupones.Winform.Properties.Resources._02_menu_02_regalo_new_normal;
                        }
                        else if (TieneSuperCuponHorario(this.nroDocumento, this.ambito, this.tienda) == true)
                        {
                            imagenBotonActivo = Cupones.Winform.Properties.Resources._02_menu_02_reloj_new_activo;
                            imagenBotonInactivo = Cupones.Winform.Properties.Resources._02_menu_02_reloj_new_alterno;
                            imagenBotonAlterno = Cupones.Winform.Properties.Resources._02_menu_02_reloj_new_normal;
                        }
                        else
                        {
                            imagenBotonActivo = botonesActivosNuevos[indiceBoton];
                            imagenBotonInactivo = botonesInactivosNuevos[indiceBoton];
                            imagenBotonAlterno = botonesAlternosNuevos[indiceBoton];
                        }
                    }
                    else
                    {
                        imagenBotonActivo = botonesActivosNuevos[indiceBoton];
                        imagenBotonInactivo = botonesInactivosNuevos[indiceBoton];
                        imagenBotonAlterno = botonesAlternosNuevos[indiceBoton];
                    }
                }
                else
                {
                    if (idBoton == 2) //Super Cupon
                    {
                        if (TieneSuperCuponCumpleanhos(this.nroDocumento, this.ambito, this.tienda) == true)
                        {
                            imagenBotonActivo = Cupones.Winform.Properties.Resources._02_menu_02_regalo_hover;
                            imagenBotonInactivo = Cupones.Winform.Properties.Resources._02_menu_02_regalo;
                            imagenBotonAlterno = null;
                        }
                        else if (TieneSuperCuponHorario(this.nroDocumento, this.ambito, this.tienda) == true)
                        {
                            imagenBotonActivo = Cupones.Winform.Properties.Resources._02_menu_02_reloj_hover;
                            imagenBotonInactivo = Cupones.Winform.Properties.Resources._02_menu_02_reloj;
                            imagenBotonAlterno = null;
                        }
                        else
                        {
                            imagenBotonActivo = botonesActivos[indiceBoton];
                            imagenBotonInactivo = botonesInactivos[indiceBoton];
                            imagenBotonAlterno = null;
                        }
                    }
                    else
                    {
                        imagenBotonActivo = botonesActivos[indiceBoton];
                        imagenBotonInactivo = botonesInactivos[indiceBoton];
                        imagenBotonAlterno = null;
                    }
                }

                btnMenu.Visible = visible;

                if (visible)
                    btnMenu.Image = imagenBotonActivo;
                else
                    btnMenu.Image = imagenBotonInactivo;

                this.BotonesActivos.Add(idBoton, imagenBotonActivo);
                this.BotonesInactivos.Add(idBoton, imagenBotonInactivo);
                this.BotonesAlternativos.Add(idBoton, imagenBotonAlterno);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Verifica si el boton de opción es visible
        /// </summary>
        /// <param name="opcion">Tipo de Opcion ( 0 : Inactivo | 1 : Activo | 2 : Nuevo) </param>
        /// <returns></returns>
        private bool EsOpcionVisible(BE.Opcion opcion)
        {
            try
            {
                return (opcion == BE.Opcion.Activo || opcion == BE.Opcion.Nuevo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Aplica visibilidad de botones del menu principal dependiendo del Perfil General
        /// </summary>
        private void MostrarOpcionesMenu()
        {
            try
            {

                if (this.perfilAplicacion == null)
                    return;

                int cntOpcionesMenu = 9;

                var visibleGrupo = new bool[cntOpcionesMenu];
                visibleGrupo[0] = this.perfilAplicacion.OpcionMisCupones == BE.Opcion.Activo || this.perfilAplicacion.OpcionMisCupones == BE.Opcion.ActivoNuevo;
                visibleGrupo[1] = this.perfilAplicacion.OpcionMiSuperCupon == BE.Opcion.Activo || this.perfilAplicacion.OpcionMiSuperCupon == BE.Opcion.ActivoNuevo;
                visibleGrupo[2] = this.perfilAplicacion.OpcionMarcasSeleccionadas == BE.Opcion.Activo || this.perfilAplicacion.OpcionMarcasSeleccionadas == BE.Opcion.ActivoNuevo;
                visibleGrupo[3] = this.perfilAplicacion.OpcionCuponesDelLugar == BE.Opcion.Activo || this.perfilAplicacion.OpcionCuponesDelLugar == BE.Opcion.ActivoNuevo;
                visibleGrupo[4] = this.perfilAplicacion.OpcionRuletaDeCupones == BE.Opcion.Activo || this.perfilAplicacion.OpcionRuletaDeCupones == BE.Opcion.ActivoNuevo;
                visibleGrupo[5] = this.perfilAplicacion.OpcionArbolDeCupones == BE.Opcion.Activo || this.perfilAplicacion.OpcionArbolDeCupones == BE.Opcion.ActivoNuevo;
                visibleGrupo[6] = this.perfilAplicacion.OpcionMisPuntosBonus == BE.Opcion.Activo || this.perfilAplicacion.OpcionMisPuntosBonus == BE.Opcion.ActivoNuevo;
                visibleGrupo[7] = this.perfilAplicacion.OpcionMisInvitaciones == BE.Opcion.Activo || this.perfilAplicacion.OpcionMisInvitaciones == BE.Opcion.ActivoNuevo;
                visibleGrupo[8] = this.perfilAplicacion.OpcionCuponesComerciales == BE.Opcion.Activo || this.perfilAplicacion.OpcionCuponesComerciales == BE.Opcion.ActivoNuevo;

                var nuevoGrupo = new bool[cntOpcionesMenu];
                nuevoGrupo[0] = this.perfilAplicacion.OpcionMisCupones == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionMisCupones == BE.Opcion.ActivoNuevo;
                nuevoGrupo[1] = this.perfilAplicacion.OpcionMiSuperCupon == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionMiSuperCupon == BE.Opcion.ActivoNuevo;
                nuevoGrupo[2] = this.perfilAplicacion.OpcionMarcasSeleccionadas == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionMarcasSeleccionadas == BE.Opcion.ActivoNuevo;
                nuevoGrupo[3] = this.perfilAplicacion.OpcionCuponesDelLugar == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionCuponesDelLugar == BE.Opcion.ActivoNuevo;
                nuevoGrupo[4] = this.perfilAplicacion.OpcionRuletaDeCupones == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionRuletaDeCupones == BE.Opcion.ActivoNuevo;
                nuevoGrupo[5] = this.perfilAplicacion.OpcionArbolDeCupones == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionArbolDeCupones == BE.Opcion.ActivoNuevo;
                nuevoGrupo[6] = this.perfilAplicacion.OpcionMisPuntosBonus == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionMisPuntosBonus == BE.Opcion.ActivoNuevo;
                nuevoGrupo[7] = this.perfilAplicacion.OpcionMisInvitaciones == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionMisInvitaciones == BE.Opcion.ActivoNuevo;
                nuevoGrupo[8] = this.perfilAplicacion.OpcionCuponesComerciales == BE.Opcion.Nuevo || this.perfilAplicacion.OpcionCuponesComerciales == BE.Opcion.ActivoNuevo;

                #region Activar tiempo en animacion si existe un boton nuevo

                this.tmrAnimacion.Enabled = (nuevoGrupo.Contains(true));
                if (this.tmrAnimacion.Enabled == true)
                    this.tmrAnimacion.Start();
                else
                    this.tmrAnimacion.Stop();

                #endregion

                #region Agregar los botones visibles y su posicion

                this.BotonesActivos.Clear();
                this.BotonesInactivos.Clear();
                this.BotonesAlternativos.Clear();

                int posicionBoton = 0;
                for (int i = 0; i < cntOpcionesMenu; i++)
                {
                    int numeroBoton = i + 1;
                    string nomGrupoBoton = "btnGrupo" + numeroBoton.ToString() + "S";

                    var btnGrupoBoton = this.Controls.Find(nomGrupoBoton, true).FirstOrDefault() as Button;
                    if (btnGrupoBoton != null)
                    {
                        this.AsignarOpcionMenu(ref btnGrupoBoton, visibleGrupo[i], nuevoGrupo[i]);

                        if (btnGrupoBoton.Visible)
                        {
                            int posX = (posicionBoton * 110) + 4;
                            int posY = 6;
                            btnGrupoBoton.Location = new Point(posX, posY);
                            posicionBoton++;
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        /// <summary>
        /// Mostrar Ruleta
        /// </summary>
        private void MostrarRuleta()
        {
            try
            {
                this.pnlCupones.Visible = false;

                #region Panel Super Cupon
                this.pnlSuperCupon.Visible = false;
                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = false;
                this.lblTiempoCupon.Visible = false;
                this.tmrTiempoCupon.Enabled = false;
                this.tmrTiempoCupon.Stop();
                #endregion

                this.pnlRuleta.Visible = true;
                this.pnlArbol.Visible = false;
                this.pnlPuntosBonus.Visible = false;

                this.pbMensajeNoCupones.Visible = false;

                this.pnlRuleta.BringToFront();

                this.pbRuleta.Visible = true;
                this.btnJugar.Visible = true;

                if (this.EsOpcionVisible(this.perfilAplicacion.OpcionRuletaDeCupones) == false)
                    return;

                this.lstCupones = new BL.Cupones().ListarCupones(this.nroDocumento, 
                                                                this.idGrupo,
                                                                this.ambito,
                                                                this.tienda, 
                                                                this.tipoCupon);

                pbRuleta.Image = this.ObtenerImagenRuleta(this.lstCupones);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar Bonus
        /// </summary>
        private void MostrarBonus()
        {
            try
            {
                this.pnlCupones.Visible = false;

                #region Panel Super Cupon
                this.pnlSuperCupon.Visible = false;
                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = false;
                this.lblTiempoCupon.Visible = false;
                this.tmrTiempoCupon.Enabled = false;
                this.tmrTiempoCupon.Stop();
                #endregion

                this.pnlRuleta.Visible = false;
                this.pnlArbol.Visible = false;
                this.pnlPuntosBonus.Visible = true;

                this.pbMensajeNoCupones.Visible = false;

                this.pnlPuntosBonus.BringToFront();

                if (this.EsOpcionVisible(this.perfilAplicacion.OpcionMisPuntosBonus) == false)
                    return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Existe super cupon por horario
        /// </summary>
        /// <param name="nroDoc">Nro Documento</param>
        /// <param name="ambito">WONG o METRO</param>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <returns></returns>
        private bool TieneSuperCuponHorario(string nroDoc, string ambito, string tienda)
        {
            bool rpta = false;
            try
            {

                int grupo = 2; //Super cupon
                int tipoCupon = 3; //Cupon horario

                var lstHorarioCupones = new BL.Cupones().ListarCupones(nroDoc, grupo, ambito, tienda, tipoCupon);
                if (lstHorarioCupones.Count == 1)
                {
                    var beCupon = lstHorarioCupones[0];

                    if (beCupon.FechaHoraRedencion != null)
                    {
                        var fechaHoraActual = DateTime.Now;
                        var fechaHoraCupon = (DateTime)beCupon.FechaHoraRedencion;
                        var tiempo = (int)(fechaHoraCupon - fechaHoraActual).TotalSeconds;

                        rpta = (tiempo > 0);
                    }
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Existe super cupon por cumpleaños
        /// </summary>
        /// <param name="nroDoc">Nro Documento</param>
        /// <param name="ambito">WONG o METRO</param>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <returns></returns>
        private bool TieneSuperCuponCumpleanhos(string nroDoc, string ambito, string tienda)
        {
            bool rpta = false;
            try
            {
                bool cumpleHoy = false;
                bool cuponCumple = false;
                DateTime fechaHora;
                bool valido = new BL.Clientes().ValidarCliente(nroDoc, 
                                                            ambito, 
                                                            out cumpleHoy, 
                                                            out cuponCumple,
                                                            out fechaHora);

                if (valido == true && cumpleHoy == true && cuponCumple == true)
                {

                    int grupo = 2; //Super cupon
                    int tipoCupon = 1; //Cupon Cumpleaños

                    var lstCumpleanhosCupones = new BL.Cupones().ListarCupones(nroDoc, grupo, ambito, tienda, tipoCupon);

                    rpta = (lstCumpleanhosCupones.Count == 1);

                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar Super Cupones
        /// </summary>
        private void MostrarSuperCupones()
        {
            try
            {
                this.pnlCupones.Visible = false;
                this.pnlSuperCupon.Visible = true;
                this.pnlRuleta.Visible = false;
                this.pnlArbol.Visible = false;
                this.pnlPuntosBonus.Visible = false;

                this.pbMensajeNoCupones.Visible = false;

                this.pnlSuperCupon.BringToFront();

                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = false;
                this.lblTiempoCupon.Visible = false;

                if (this.EsOpcionVisible(this.perfilAplicacion.OpcionMiSuperCupon) == false)
                    return;

                this.superCupon = null;
                this.idCupon = 0;
                this.tipoCupon = 0;
                this.codigoBarraCupon = "";
                this.segundosTiempoCupon = 0;

                bool cumpleHoy = false;
                bool cuponCumple = false;
                DateTime fechaHora;
                bool valido = new BL.Clientes().ValidarCliente(this.nroDocumento, 
                                                            this.ambito, 
                                                            out cumpleHoy, 
                                                            out cuponCumple,
                                                            out fechaHora);

                var blCupones = new BL.Cupones();
                if (valido == true && cumpleHoy == true && cuponCumple == true)
                {
                    #region Cupon Cumpleaños
                    this.tipoCupon = 1; //Cupon Cumpleaños

                    var lstCumpleanhosCupones = blCupones.ListarCupones(this.nroDocumento, 2, this.ambito, this.tienda, this.tipoCupon);
                    if (lstCumpleanhosCupones.Count == 1)
                    {
                        this.superCupon = lstCumpleanhosCupones[0];
                        this.idCupon = this.superCupon.CodCampania;

                        this.btnRegalo.Visible = true;
                        this.pbSuperCupon.Visible = false;
                    }
                    else
                        this.superCupon = null;
                    #endregion
                }
                else
                {
                    #region Cupon Horario

                    this.tipoCupon = 3; //Cupon Horario

                    var lstHorarioCupones = blCupones.ListarCupones(this.nroDocumento, 2, this.ambito, this.tienda, this.tipoCupon);
                    if (lstHorarioCupones.Count == 1)
                    {
                        if (lstHorarioCupones[0].FechaHoraRedencion != null)
                        {
                            var fechaHoraActual = DateTime.Now;
                            var fechaHoraCupon = (DateTime)lstHorarioCupones[0].FechaHoraRedencion;
                            var tiempo = (int)(fechaHoraCupon - fechaHoraActual).TotalSeconds;

                            if (tiempo > 0)
                            {
                                this.superCupon = lstHorarioCupones[0];
                                this.idCupon = this.superCupon.CodCampania;
                                this.segundosTiempoCupon = tiempo;

                                this.lblTiempoCupon.Visible = true;
                                this.lblTiempoCupon.Text = General.ParseSecondsToTime(this.segundosTiempoCupon);
                                this.tmrTiempoCupon.Enabled = true;
                                this.tmrTiempoCupon.Start();

                                this.MostrarImagenSuperCuponTiempo();
                            }
                        }
                    }

                    #endregion
                }

                #region Super Cupon

                if (this.superCupon == null)
                {
                    this.tipoCupon = 2; //Super Cupon

                    var lstSuperCupones = blCupones.ListarCupones(this.nroDocumento, 2, this.ambito, this.tienda, this.tipoCupon);
                    if (lstSuperCupones.Count == 1)
                    {
                        this.superCupon = lstSuperCupones[0];
                        this.idCupon = this.superCupon.CodCampania;

                        this.MostrarImagenSuperCuponNormal();
                    }
                    else
                        this.superCupon = null;
                }

                #endregion

                if (this.superCupon == null)
                {

                    this.idCupon = 0;
                    this.tipoCupon = 0;
                    this.codigoBarraCupon = "";
                    this.segundosTiempoCupon = 0;

                    foreach (Control ctrl in this.pnlCupones.Controls)
                    {
                        ctrl.Visible = false;
                    }

                    this.pbMensajeNoCupones.BringToFront();
                    this.pbMensajeNoCupones.Visible = true;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen del nodo del arbol
        /// </summary>
        /// <param name="cupon">Objeto Cupon</param>
        /// <param name="activo">Opcional : ¿estado activo?</param>
        /// <returns></returns>
        private Image ObtenerImagenRama(BE.Cupon cupon, bool activo = false)
        {
            Image nodoArbol = null;
            try
            {
                Brush dBrcolor = null;

                if (activo == true)
                {
                    nodoArbol = Cupones.Winform.Properties.Resources._05_cupon_activo;
                    dBrcolor = Brushes.WhiteSmoke;
                }
                else
                {
                    nodoArbol = Cupones.Winform.Properties.Resources._05_cupon_desactivo;
                    dBrcolor = Brushes.Gray;
                }

                StringFormat drawFormat = new StringFormat();
                drawFormat.Alignment = StringAlignment.Center;

                using (Graphics graphics = Graphics.FromImage(nodoArbol))
                {
                    using (Font arialFont = new Font("Arial Black", 12))
                    {
                        graphics.DrawString(cupon.Descuento, arialFont, dBrcolor, 65f, 19f, drawFormat);

                    }
                    using (Font arialFont = new Font("Arial Black", 10))
                    {
                        graphics.DrawString(cupon.Mensaje1, arialFont, dBrcolor, 64f, 60f, drawFormat);
                        graphics.DrawString(cupon.Mensaje2, arialFont, dBrcolor, 64f, 80f, drawFormat);
                        graphics.DrawString(cupon.Mensaje3, arialFont, dBrcolor, 64f, 100f, drawFormat);
                    }
                }

                return nodoArbol;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar Arbol de Cúpones
        /// </summary>
        private void MostrarArbol()
        {
            try
            {
                this.pnlCupones.Visible = false;

                #region Panel Super Cupon
                this.pnlSuperCupon.Visible = false;
                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = false;
                this.lblTiempoCupon.Visible = false;
                this.tmrTiempoCupon.Enabled = false;
                this.tmrTiempoCupon.Stop();
                #endregion

                this.pnlRuleta.Visible = false;
                this.pnlArbol.Visible = true;
                this.pnlPuntosBonus.Visible = false;

                this.pnlArbol.BringToFront();

                this.pbMensajeNoCupones.Visible = false;

                if (this.EsOpcionVisible(this.perfilAplicacion.OpcionArbolDeCupones) == false)
                    return;

                this.tipoCupon = 0;
                this.codigoBarraCupon = "";

                string codGrupo = this.idGrupo.ToString();
                this.lstArbolCupon = new BL.Cupones().ListarCupones(this.nroDocumento, 6, this.ambito, this.tienda, this.tipoCupon);
                if (this.lstArbolCupon.Count > 0)
                {

                    foreach (Control ctrl in this.pnlArbol.Controls)
                    {
                        ctrl.Visible = true;
                    }

                    this.lstArbolCupon = this.lstArbolCupon.OrderBy(x => x.Prioridad).ToList();

                    bool nodoActivo = false;
                    foreach (BE.Cupon cupon in this.lstArbolCupon)
                    {

                        switch (cupon.Prioridad)
                        {
                            case 0:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon0.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon0.Enabled = nodoActivo;
                                break;

                            case 1:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon1.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon1.Enabled = nodoActivo;
                                break;

                            case 2:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon2.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon2.Enabled = nodoActivo;
                                break;

                            case 3:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon3.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon3.Enabled = nodoActivo;
                                break;

                            case 4:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon4.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon4.Enabled = nodoActivo;
                                break;

                            case 5:
                                nodoActivo = (cupon.Redimidos == 0 && cupon.CantidadCuponesDisponibles >= 1);
                                this.pbCupon5.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon5.Enabled = nodoActivo;
                                break;
                            case 6:
                                nodoActivo = (cupon.Redimidos == 0)
                                    && (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[0].Redimidos == 1)
                                    && (this.lstArbolCupon[1].Redimidos == 1);
                                this.pbCupon6.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon6.Enabled = nodoActivo;
                                break;

                            case 7:
                                nodoActivo = (cupon.Redimidos == 0)
                                    && (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[2].Redimidos == 1)
                                    && (this.lstArbolCupon[3].Redimidos == 1);
                                this.pbCupon7.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon7.Enabled = nodoActivo;
                                break;

                            case 8:
                                nodoActivo = (cupon.Redimidos == 0)
                                    && (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[4].Redimidos == 1)
                                    && (this.lstArbolCupon[5].Redimidos == 1);
                                this.pbCupon8.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon8.Enabled = nodoActivo;
                                break;

                            case 9:
                                nodoActivo = (cupon.Redimidos == 0)
                                    && (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[6].Redimidos == 1)
                                    && (this.lstArbolCupon[7].Redimidos == 1);
                                this.pbCupon9.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon9.Enabled = nodoActivo;
                                break;

                            case 10:
                                nodoActivo = (cupon.Redimidos == 0)
                                    && (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[7].Redimidos == 1)
                                    && (this.lstArbolCupon[8].Redimidos == 1);
                                this.pbCupon10.Image = this.ObtenerImagenRama(cupon, nodoActivo);
                                this.pbCupon10.Enabled = nodoActivo;
                                break;

                            default:
                                nodoActivo = (cupon.CantidadCuponesDisponibles >= 1)
                                    && (this.lstArbolCupon[9].Redimidos == 1)
                                    && (this.lstArbolCupon[10].Redimidos == 1);

                                this.btnCupon11.Enabled = nodoActivo;
                                break;

                        }
                    }

                }
                else
                {
                    foreach (Control ctrl in this.pnlArbol.Controls)
                    {
                        ctrl.Visible = false;
                    }

                    this.pbMensajeNoCupones.BringToFront();
                    this.pbMensajeNoCupones.Visible = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar Mis Cupones, Cupones Premium (Marcas) o Cupones de Lugar
        /// </summary>
        private void MostrarCupones()
        {

            try
            {
                this.pnlCupones.Visible = true;

                #region Panel Super Cupon
                this.pnlSuperCupon.Visible = false;
                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = false;
                this.lblTiempoCupon.Visible = false;
                this.tmrTiempoCupon.Enabled = false;
                #endregion

                #region Panel Ruleta
                this.pnlRuleta.Visible = false;
                #endregion

                #region Panel Arbol
                this.pnlArbol.Visible = false;
                #endregion

                #region Panel Bonus
                this.pnlPuntosBonus.Visible = false;
                #endregion

                this.pnlCupones.BringToFront();

                this.pbMensajeNoCupones.Visible = false;

                this.btnImprimir.Visible = false;
                this.btnSeleccionar.Visible = false;
                this.btnMultiCupon.Visible = false;
                this.btnNextImages.Visible = false;
                this.btnPreviousImages.Visible = false;
                this.dataViewImages.Visible = false;

                this.btnImprimir.Enabled = true;
                this.btnSeleccionar.Enabled = true;
                this.btnMultiCupon.Enabled = true;
                this.btnNextImages.Enabled = true;
                this.btnPreviousImages.Enabled = true;

                this.btnSeleccionar.Image = Cupones.Winform.Properties.Resources._03_btn_select_all;

                this.tipoCupon = 0;
                this.codigoBarraCupon = "";
                this.multiCupon = false;

                this.lstCupones = new BL.Cupones().ListarCupones(this.nroDocumento, this.idGrupo, this.ambito, this.tienda, this.tipoCupon);

                int cntCupones = this.lstCupones.Count;
                if (cntCupones > 0)
                {

                    this.lstCuponesSeleccionados = new List<BE.Cupon>();

                    _numberPreviewImages = 8;
                    _currentStartImageIndex = 0;
                    _currentEndImageIndex = 8;

                    this.MostrarImagenesCupones();

                    //Habilitar paginacion
                    if (lstCupones.Count > 8)
                    {
                        this.btnNextImages.Visible = true;
                        this.btnPreviousImages.Visible = true;
                    }
                    else
                    {
                        this.btnNextImages.Visible = false;
                        this.btnPreviousImages.Visible = false;
                    }

                    this.btnMultiCupon.Visible = true;
                    this.dataViewImages.Visible = true;
                }
                else
                {
                    foreach (Control ctrl in this.pnlCupones.Controls)
                    {
                        ctrl.Visible = false;
                    }

                    this.pbMensajeNoCupones.BringToFront();
                    this.pbMensajeNoCupones.Visible = true;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Mostrar las imagenes de cupones en grilla
        /// </summary>
        private void MostrarImagenesCupones()
        {
            try
            {


                //Clear rows and columns
                this.dataViewImages.Rows.Clear();
                this.dataViewImages.Columns.Clear();

                int numColumnsForWidth = (this.dataViewImages.Width - 10) / (_imageSize);
                int numRows = 0;

                int numImagesRequired = 0;

                if (_currentEndImageIndex > this.lstCupones.Count)
                {
                    // Are we requesting to display more images than we actually have? If so then reduce
                    if (_currentStartImageIndex == 0)
                    {
                        numImagesRequired = this.lstCupones.Count;
                    }
                    else
                    {
                        numImagesRequired = (_currentEndImageIndex - _currentStartImageIndex) - (_currentEndImageIndex - this.lstCupones.Count);
                    }
                }
                else
                {
                    // Calculated the number of rows we will need for normal use
                    numImagesRequired = _currentEndImageIndex - _currentStartImageIndex;
                }

                numRows = numImagesRequired / numColumnsForWidth;

                // Do we have a an overfill for a row
                if (numImagesRequired % numColumnsForWidth > 0)
                {
                    numRows += 1;
                }

                // Catch when we have less images than the maximum number of columns for the DataGridView width
                if (numImagesRequired < numColumnsForWidth)
                {
                    numColumnsForWidth = numImagesRequired;
                }

                int numGeneratedCells = numRows * numColumnsForWidth;

                // Dynamically create the columns
                for (int index = 0; index < numColumnsForWidth; index++)
                {
                    var imagenColumn = new DataGridViewImageColumn();
                    imagenColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    this.dataViewImages.Columns.Add(imagenColumn);
                    this.dataViewImages.Columns[index].Width = _imageSize;// +45;
                }

                // Create the rows
                for (int index = 0; index < numRows; index++)
                {
                    this.dataViewImages.Rows.Add();
                    this.dataViewImages.Rows[index].Height = _imageSize;// + 40;
                }

                int columnIndex = 0;
                int rowIndex = 0;
                bool check = false;

                for (int index = _currentStartImageIndex; index < _currentStartImageIndex + numImagesRequired; index++)
                {

                    var beCupon = this.lstCupones[index];

                    var commonCupon = new COMMON.Coupon(General.Ambito);

                    if (this.multiCupon)
                    {
                        var cuponSeleccionado = this.lstCuponesSeleccionados
                                .SingleOrDefault(x => x.CodCampania == beCupon.CodCampania);

                        check = (cuponSeleccionado != null);

                        impresionCupon = commonCupon.ImagePreviewMulti(beCupon, check);
                    }
                    else
                    {
                        impresionCupon = commonCupon.ImagePreview(beCupon);
                    }
                    beCupon.Imagen = impresionCupon;

                    commonCupon = null;

                    string resumen = beCupon.Mensaje1 + "*" + beCupon.Id;
                    string tag = beCupon.CodCampania.ToString() + "*" + (check ? "SI" : "NO") + "*" + beCupon.ExclusivoApp;

                    this.dataViewImages.Rows[rowIndex].Cells[columnIndex].Value = impresionCupon;
                    this.dataViewImages.Rows[rowIndex].Cells[columnIndex].Tag = tag;
                    this.dataViewImages.Rows[rowIndex].Cells[columnIndex].ToolTipText = resumen;
                    this.dataViewImages.Rows[rowIndex].Cells[columnIndex].Style.BackColor = Color.White;

                    // Have we reached the end column? if so then start on the next row
                    if (columnIndex == numColumnsForWidth - 1)
                    {
                        rowIndex++;
                        columnIndex = 0;
                    }
                    else
                    {
                        columnIndex++;
                    }

                }

                if (numGeneratedCells > numImagesRequired)
                {
                    for (int index = 0; index < numGeneratedCells - numImagesRequired; index++)
                    {
                        DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
                        dataGridViewCellStyle.NullValue = null;
                        dataGridViewCellStyle.Tag = "";
                        this.dataViewImages.Rows[rowIndex].Cells[columnIndex + index].Style = dataGridViewCellStyle;
                    }
                }

                if (_currentStartImageIndex == 0)
                {
                    this.btnPreviousImages.Enabled = false;
                }
                else
                {
                    this.btnPreviousImages.Enabled = true;
                }

                if (_currentEndImageIndex < this.lstCupones.Count)
                {
                    this.btnNextImages.Enabled = true;
                }
                else
                {
                    this.btnNextImages.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar la imagen del cupon por tiempo u horario
        /// </summary>
        private void MostrarImagenSuperCuponTiempo()
        {
            try
            {

                var commonCupon = new COMMON.Coupon(General.Ambito);
                Image imgSuperCupon = commonCupon.ImagePreviewSuperTime(this.superCupon);
                this.superCupon.Imagen = imgSuperCupon;
                commonCupon = null;

                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = true;
                this.pbSuperCupon.Image = imgSuperCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Mostrar la imagen del super cupon
        /// </summary>
        private void MostrarImagenSuperCuponNormal()
        {
            try
            {

                var commonCupon = new COMMON.Coupon(General.Ambito);
                Image imgSuperCupon = commonCupon.ImagePreviewSuperNormal(this.superCupon);
                this.superCupon.Imagen = imgSuperCupon;
                commonCupon = null;

                this.btnRegalo.Visible = false;
                this.pbSuperCupon.Visible = true;
                this.pbSuperCupon.Image = imgSuperCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Retorna la opcion
        /// </summary>
        /// <param name="visible"></param>
        /// <param name="nuevo"></param>
        /// <returns></returns>
        private BE.Opcion ObtenerOpcion(bool visible, bool nuevo)
        {
            BE.Opcion opcion = BE.Opcion.Inactivo;
            try
            {

                if (nuevo && visible)
		            opcion = BE.Opcion.ActivoNuevo;
                else
	            {
                    if (nuevo)
		                opcion = BE.Opcion.Nuevo;
                    else if (visible)
                        opcion = BE.Opcion.Activo;
	            }
                return opcion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener los datos del perfil general
        /// </summary>
        /// <param name="hostName">Nombre de Host</param>
        /// <param name="codigoTienda">Codigo de Tienda</param>
        /// <param name="ambito">Ambito</param>
        /// <param name="tipoDocumento">Id del Tipo Documento</param>
        /// <param name="nroDocumento">Numero de Documento</param>
        /// <returns></returns>
        private BE.Perfil ObtenerPerfil(string hostName,
                                    string codigoTienda,
                                    string ambito,
                                    int tipoDocumento,
                                    string nroDocumento,
                                    out string mensaje)
        {
            BE.Perfil perfil = null;

            try
            {
                string msg = "";

                BE.PerfilTienda perfilTienda = null;
                BE.PerfilKiosko perfilKiosko = null;
                BE.PerfilCliente perfilCliente = null;

                #region Validar u Obtener perfiles de Tienda, Kiosko y Cliente

                var blPerfiles = new BL.Perfiles();

                perfilTienda = blPerfiles.ObtenerPerfilTienda(codigoTienda);
                if (perfilTienda == null)
                {
                    msg = "No existe configuracion de perfil de Tienda.";
                }
                else
                {
                    perfilKiosko = blPerfiles.ObtenerPerfilKiosko(codigoTienda, hostName);
                    if (perfilKiosko == null)
                    {
                        msg = "No existe configuracion de perfil del Kiosko.";
                    }
                    else if (perfilKiosko.Impresora == null)
                    {
                        msg = "No existe configuracion de perfil de Impresora.";
                    }
                    else
                    {
                        perfilCliente = blPerfiles.ObtenerPerfilCliente(tipoDocumento, nroDocumento, ambito);
                        if (perfilCliente == null)
                        {
                            msg = "No existe configuracion de perfil de Cliente.";
                        }
                        else
                        {
                            string[] tiendaAdmitidas = perfilCliente.TiendaAdmitidas.Split(',');
                            var resultado = Array.Find(tiendaAdmitidas, s => s.Equals(codigoTienda));
                            if (resultado == null)
                            {
                                msg = "No existe configuracion de perfil de Cliente.";
                            }
                        }
                    }
                }

                #endregion

                if (msg.Length == 0)
                {
         
                    int opcionActivo = 1;   // 1 = Activo 
                    int opcionNuevo = 2;    // 2 = Nuevo y Activo

                    bool visibleMisCupones = (perfilTienda.OpcionMisCupones == opcionActivo || perfilTienda.OpcionMisCupones == opcionNuevo);
                    if (visibleMisCupones)
	                    visibleMisCupones = perfilCliente.OpcionMisCupones;

                    bool visibleMiSuperCupon = (perfilTienda.OpcionMiSuperCupon == opcionActivo || perfilTienda.OpcionMiSuperCupon == opcionNuevo);
                    if (visibleMiSuperCupon)
                        visibleMiSuperCupon = perfilCliente.OpcionMiSuperCupon;

                    bool visibleMarcasSeleccionadas = (perfilTienda.OpcionMarcasSeleccionadas == opcionActivo || perfilTienda.OpcionMarcasSeleccionadas == opcionNuevo);
                    if (visibleMarcasSeleccionadas)
                        visibleMarcasSeleccionadas = perfilCliente.OpcionMarcasSeleccionadas;

                    bool visibleArbolDeCupones = (perfilTienda.OpcionArbolDeCupones == opcionActivo || perfilTienda.OpcionArbolDeCupones == opcionNuevo);
                    if (visibleArbolDeCupones)
                        visibleArbolDeCupones = perfilCliente.OpcionArbolDeCupones;

                    bool visibleCuponesDelLugar = (perfilTienda.OpcionCuponesDelLugar == opcionActivo || perfilTienda.OpcionCuponesDelLugar == opcionNuevo);
                    if (visibleCuponesDelLugar)
                        visibleCuponesDelLugar = perfilCliente.OpcionCuponesDelLugar;

                    bool visibleRuletaDeCupones = (perfilTienda.OpcionRuletaDeCupones == opcionActivo || perfilTienda.OpcionRuletaDeCupones == opcionNuevo);
                    if (visibleRuletaDeCupones)
                        visibleRuletaDeCupones = perfilCliente.OpcionRuletaDeCupones;

                    bool visibleMisPuntosBonus = (perfilTienda.OpcionMisPuntosBonus == opcionActivo || perfilTienda.OpcionMisPuntosBonus == opcionNuevo);
                    if (visibleMisPuntosBonus)
                        visibleMisPuntosBonus = perfilCliente.OpcionMisPuntosBonus;

                    bool visibleMisInvitaciones = (perfilTienda.OpcionMisInvitaciones == opcionActivo || perfilTienda.OpcionMisInvitaciones == opcionNuevo);
                    if (visibleMisInvitaciones)
                        visibleMisInvitaciones = perfilCliente.OpcionMisInvitaciones;

                    bool visibleCuponesComerciales = (perfilTienda.OpcionCuponesComerciales == opcionActivo || perfilTienda.OpcionCuponesComerciales == opcionNuevo);
                    if (visibleCuponesComerciales)
                        visibleCuponesComerciales = perfilCliente.OpcionCuponesComerciales;


                    bool nuevoMisCupones = (perfilTienda.OpcionMisCupones == opcionNuevo);
                    bool nuevoMiSuperCupon = (perfilTienda.OpcionMiSuperCupon == opcionNuevo);
                    bool nuevoMarcasSeleccionadas = (perfilTienda.OpcionMarcasSeleccionadas == opcionNuevo);
                    bool nuevoArbolDeCupones = (perfilTienda.OpcionArbolDeCupones == opcionNuevo);
                    bool nuevoCuponesDelLugar = (perfilTienda.OpcionCuponesDelLugar == opcionNuevo);
                    bool nuevoRuletaDeCupones = (perfilTienda.OpcionRuletaDeCupones == opcionNuevo);
                    bool nuevoMisPuntosBonus = (perfilTienda.OpcionMisPuntosBonus == opcionNuevo);
                    bool nuevoMisInvitaciones = (perfilTienda.OpcionMisInvitaciones == opcionNuevo);
                    bool nuevoCuponesComerciales = (perfilTienda.OpcionCuponesComerciales == opcionNuevo);

                    BE.Opcion opcionMisCupones = ObtenerOpcion(visibleMisCupones, nuevoMisCupones);
                    BE.Opcion opcionMiSuperCupon = ObtenerOpcion(visibleMiSuperCupon, nuevoMiSuperCupon);
                    BE.Opcion opcionMarcasSeleccionadas = ObtenerOpcion(visibleMarcasSeleccionadas, nuevoMarcasSeleccionadas);
                    BE.Opcion opcionArbolDeCupones = ObtenerOpcion(visibleArbolDeCupones, nuevoArbolDeCupones);
                    BE.Opcion opcionCuponesDelLugar = ObtenerOpcion(visibleCuponesDelLugar, nuevoCuponesDelLugar);
                    BE.Opcion opcionRuletaDeCupones = ObtenerOpcion(visibleRuletaDeCupones, nuevoRuletaDeCupones);
                    BE.Opcion opcionMisPuntosBonus = ObtenerOpcion(visibleMisPuntosBonus, nuevoMisPuntosBonus);
                    BE.Opcion opcionMisInvitaciones = ObtenerOpcion(visibleMisInvitaciones, nuevoMisInvitaciones);
                    BE.Opcion opcionCuponesComerciales = ObtenerOpcion(visibleCuponesComerciales, nuevoCuponesComerciales);

                    perfil = new BE.Perfil();

                    perfil.OpcionMisCupones = opcionMisCupones;
                    perfil.OpcionMiSuperCupon = opcionMiSuperCupon;
                    perfil.OpcionMarcasSeleccionadas = opcionMarcasSeleccionadas;
                    perfil.OpcionArbolDeCupones = opcionArbolDeCupones;
                    perfil.OpcionCuponesDelLugar = opcionCuponesDelLugar;
                    perfil.OpcionRuletaDeCupones = opcionRuletaDeCupones;
                    perfil.OpcionMisPuntosBonus = opcionMisPuntosBonus;
                    perfil.OpcionMisInvitaciones = opcionMisInvitaciones;
                    perfil.OpcionCuponesComerciales = opcionCuponesComerciales;

                    perfil.FechaIniBanner = perfilKiosko.FechaIniBanner;
                    perfil.FechaFinBanner = perfilKiosko.FechaFinBanner;
                    perfil.RutaImagenBannerTienda = perfilKiosko.RutaImagenBannerTienda;

                    perfil.MargenIzquierdoX = perfilKiosko.Impresora.MargenIzquierdoX;
                    perfil.MargenSuperiorY = perfilKiosko.Impresora.MargenSuperiorY;
                    perfil.Ancho = perfilKiosko.Impresora.Ancho;
                    perfil.Alto = perfilKiosko.Impresora.Alto;

                }

                mensaje = msg;
                return perfil;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Agregar evento MouseClick a todos los controles del panel
        /// </summary>
        /// <param name="pnl">Control del tipo panel</param>
        private void AddMouseClick(Panel pnl)
        {
            try
            {
                pnl.MouseClick += new MouseEventHandler(this.All_MouseClick);

                foreach (Control crtl in pnl.Controls)
                {
                    if (crtl is Panel)
                    {
                        Panel pnlChild = (crtl as Panel);
                        this.AddMouseClick(pnlChild);
                    }
                    else
                        crtl.MouseClick += new MouseEventHandler(this.All_MouseClick);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporalizador de sesion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrSesion_Tick(object sender, EventArgs e)
        {
            try
            {
                int segundos = int.Parse(this.lblTiempoSession.Text);

                if (segundos > 0)
                {
                    this.lblTiempoSession.Text = (segundos - 1).ToString();
                }
                else
                {
                    this.ActivarSesion(false);

                    var inicio = FrmContenedor.Instance.Inicio();
                    inicio.Limpiar();

                    FrmContenedor.Instance.CerrarSesion();
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento Mouse Click del control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void All_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                this.lblTiempoSession.Text = this.tiempoSesion.ToString();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Inicializa los valores del formulario
        /// </summary>
        /// <param name="cumpleanho">Es el cumpleaños del cliente</param>
        public void Inicia(bool cumpleanho = false)
        {
            try
            {
                FrmContenedor.Instance.Cargar();

                #region Asignar variables
                this.nroDocumento = FrmContenedor.Instance.NroDocumento; //Numero de Documento del Cliente
                this.tipoDocumento = FrmContenedor.Instance.TipoDocumento; //Tipo del documento del Cliente

                this.ambito = General.Ambito;  //Ambito
                this.tienda = General.Configuracion.Tienda; //Codigo de Tienda
                this.hostname = General.Configuracion.Hostname; //Nombre de PC
                #endregion

                #region Limpiar Variables
                this.lstCupones = null;
                this.lstCuponesSeleccionados = null;
                this.superCupon = null;
                this.lstArbolCupon = null;

                this.idGrupo = 0;
                this.multiCupon = false;
                this.seleccionTodos = false;

                this.idGrupo = 1;
                this.tipoCupon = 0;
                this.codigoBarraCupon = "";
                this.impresionCupon = null;

                this._imageSize = 300;
                this._numberPreviewImages = 8;
                this._currentStartImageIndex = 0;
                this._currentEndImageIndex = 8;
                #endregion

                #region Cargar Perfil y Cliente (Procesos paralelos)
                var tasks = new[]
                {
                        Task.Factory.StartNew(() => this.CargarPerfil()),
                        Task.Factory.StartNew(() => this.CargarCliente())
                    };

                try
                {
                    Task.WaitAll(tasks);
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }
                #endregion

                #region Mostrar Opciones del Perfil de la Aplicacion
                this.MostrarOpcionesMenu();
                #endregion

                #region Mostrar el Nombre del cliente
                if (this.cliente != null)
                    this.lblNombleCliente.Text = this.cliente.NompleCompleto;
                else
                    this.lblNombleCliente.Text = string.Empty;
                #endregion

                this.idGrupo = 1; //Mostrar la primera opcion del menu
                if (cumpleanho)
                {
                    if (this.EsOpcionVisible(this.perfilAplicacion.OpcionMiSuperCupon))
                        this.idGrupo = 2; //Mostrar el super cupon de cumpleaños
                }

                this.BotonPresionado();

                switch (this.idGrupo)
                {
                    case 1://Cupones estandar
                        FrmContenedor.Instance.Sesion.OpcionMisCupones = true;
                        this.MostrarCupones();
                        break;

                    case 2://Super Cupon
                        FrmContenedor.Instance.Sesion.OpcionMiSuperCupon = true;
                        this.MostrarSuperCupones();
                        break;
                    default:
                        break;
                }

                FrmContenedor.Instance.MenuPrincipal();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene el Perfil del kiosko
        /// </summary>
        private void CargarPerfil()
        {
            try
            {
                string hostname = General.Configuracion.Hostname;
                string mensaje = "";

                this.perfilAplicacion = this.ObtenerPerfil(hostname,
                                                            this.tienda,
                                                            this.ambito,
                                                            this.tipoDocumento,
                                                            this.nroDocumento,
                                                            out mensaje);

                if (mensaje.Length > 0)
                    throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Obtiene datos del cliente
        /// </summary>
        private void CargarCliente()
        {
            try
            {
                this.cliente = new BL.Clientes().ObtenerCliente(this.nroDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Boton de Super cupon por cumpleaños
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRegalo_Click(object sender, EventArgs e)
        {
            try
            {
                int idCupon = this.superCupon.CodCampania;

                var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, 2, this.tipoCupon);
                detalleCupon.TipoDocumento = this.tipoDocumento;
                detalleCupon.Tienda = this.tienda;

                this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);
                
                if (this.impresionCupon != null)
                {
                    var cupon = FrmContenedor.Instance.Cupon();
                    cupon.VistaPrevia(this.impresionCupon, this.idCupon, this.idGrupo);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Pagina siguiente
        /// </summary>
        private void PaginacionSiguiente()
        {
            try
            {

                _numberPreviewImages = 8;
                _currentStartImageIndex += _numberPreviewImages;
                _currentEndImageIndex = _currentStartImageIndex + _numberPreviewImages;

                this.MostrarImagenesCupones();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Pagina Anterior
        /// </summary>
        private void PaginacionAnterior()
        {
            try
            {
                _currentStartImageIndex -= _numberPreviewImages;
                _currentEndImageIndex -= _numberPreviewImages;

                this.MostrarImagenesCupones();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Evento click del boton Pagina Anterior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreviousImages_Click(object sender, EventArgs e)
        {
            try
            {
                this.PaginacionAnterior();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton Pagina Posterior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextImages_Click(object sender, EventArgs e)
        {
            try
            {
                this.PaginacionSiguiente();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Seleccion de cupon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataViewImages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0 || e.ColumnIndex < 0)
                    return;

                int row = e.RowIndex;
                int col = e.ColumnIndex;

                if (this.dataViewImages.Rows[row].Cells[col].Tag == null)
                    return;

                int codCampania = 0;
                bool check = false;
                bool exclusivoApp = false;

                string tag = this.dataViewImages.Rows[row].Cells[col].Tag.ToString();
                string[] cell = tag.Split('*');

                if (cell.Length == 3)
                {
                    codCampania = int.Parse(cell[0]);
                    check = cell[1].Equals("SI") ? true : false;
                    exclusivoApp = bool.Parse(cell[2]);
                }
                
                var cuponSeleccionado = this.lstCupones.SingleOrDefault(x => x.CodCampania == codCampania);

                if (this.multiCupon) //Selección de multiples cupones
                {
                    if (exclusivoApp == false) //Solo cupones normales
                    {
                        if (check == false)
                            this.lstCuponesSeleccionados.Add(cuponSeleccionado);
                        else
                            this.lstCuponesSeleccionados.Remove(cuponSeleccionado);

                        this.MostrarImagenesCupones();

                        if (this.lstCupones.Count == this.lstCuponesSeleccionados.Count)
                        {
                            this.btnSeleccionar.Image = Cupones.Winform.Properties.Resources._03_btn_deselect_all;
                            this.seleccionTodos = true;
                        }
                        else
                        {
                            this.btnSeleccionar.Image = Cupones.Winform.Properties.Resources._03_btn_select_all;
                            this.seleccionTodos = false;
                        }
                    }
                }
                else 
                {
                    if (exclusivoApp == true) //Cupon exclusivo en APP (No se imprime cupon)
                    {
                        Image imgMensaje = Cupones.Winform.Properties.Resources._03_popup_exclusivo;
                        FrmContenedor.Instance.MensajeImagen(imgMensaje);
                    }
                    else //Vista previa de impresion de tickets
                    {
                        int idCupon = codCampania;

                        var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, this.idGrupo);
                        detalleCupon.TipoDocumento = this.tipoDocumento;
                        detalleCupon.Tienda = this.tienda;

                        this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);

                        if (this.impresionCupon != null)
                        {
                            this.idCupon = idCupon;

                            var cupon = FrmContenedor.Instance.Cupon();
                            cupon.VistaPrevia(this.impresionCupon, this.idCupon, this.idGrupo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento Click del boton de Multi Cupon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMultiCupon_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnMultiCupon.Enabled = false;

                this.btnSeleccionar.Enabled = true;
                //this.btnSeleccionar.Visible = true;

                this.btnImprimir.Enabled = true;
                this.btnImprimir.Visible = true;

                this.multiCupon = true;
                this.MostrarImagenesCupones();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento Click del boton se Seleccion Multiple
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                this.lstCuponesSeleccionados.Clear();

                if (this.seleccionTodos)
                {
                    this.btnSeleccionar.Image = Cupones.Winform.Properties.Resources._03_btn_select_all;
                }
                else
                {
                    this.lstCuponesSeleccionados = this.lstCupones.ToList();
                
                    this.btnSeleccionar.Image = Cupones.Winform.Properties.Resources._03_btn_deselect_all;
                }

                this.MostrarImagenesCupones();

                this.seleccionTodos = !this.seleccionTodos;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Quitar la disponibilidad y registrar la interacción del cupon
        /// </summary>
        /// <param name="idCupon">ID de Codigo de Campaña</param>
        /// <param name="tipoCupon">Tipo de Super Cupon ( 0 : NA | 1 : Cupon Cumpleaños | 2 : Super Cupon )</param>
        /// <param name="codigoBarra">Codigo de Barra</param>
        /// <returns></returns>
        private bool RegistraImpresionCupon(int idCupon, int tipoCupon, string codigoBarra)
        {
            try
            {
                if (tipoCupon < 0 || tipoCupon > 3) //Solo si el Id del tipo cupon es 0, 1, 2 y 3
                    return false;

                int idInteraccion = 2; //Imprimir

                bool disponibilidad = false; //Quito disponibilidad de cupon
                bool interaccion = false; //Registrar la interaccion con el cupon

                var blCupones = new BL.Cupones();

                disponibilidad = blCupones.QuitarDisponibilidad(this.nroDocumento,
                                                                idCupon,
                                                                this.idGrupo,
                                                                tipoCupon,
                                                                codigoBarra);

                if (disponibilidad)
                {
                    interaccion = blCupones.RegistrarInteraccion(this.nroDocumento,
                                                                idCupon,
                                                                this.idGrupo,
                                                                idInteraccion,
                                                                this.tienda);
                }

                return (disponibilidad && interaccion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Limpiar todas las tareas de la cola de impresión
        /// </summary>
        private void LimpiarImpresora()
        {
            try
            {
                var sql = @"SELECT * FROM Win32_PrintJob";

                var searchPrintJobs = new ManagementObjectSearcher(sql);

                var prntJobCollection = searchPrintJobs.Get();

                foreach (ManagementObject prntJob in prntJobCollection)
                {
                    prntJob.Delete();
                    prntJob.InvokeMethod("CancelAllJobs", null);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Verifica que la impresora se encuentre lista
        /// </summary>
        /// <param name="intentos">Opcional: Número de intentos</param>
        /// <returns></returns>
        private bool ImpresoraLista(int intentos = 10)
        {
            bool estado = false;
            try
            {

                while (!estado)
                {

                    if (intentos == 0)
                        break;

                    intentos -= 1;

                    //Esperar 1 segundo
                    System.Threading.Thread.Sleep(1000);

                    //Obtener Impresora principal y con estado listo
                    var sql = @"SELECT * FROM Win32_Printer WHERE Default = 1 AND PrinterStatus = 3";
                    var oSelectQuery = new SelectQuery();
                    oSelectQuery.QueryString = sql;

                    var oManagementScope = new ManagementScope(ManagementPath.DefaultPath);
                    oManagementScope.Connect();

                    var oObjectSearcher = new ManagementObjectSearcher(oManagementScope, @oSelectQuery);

                    estado = (oObjectSearcher.Get().Count == 1);
                }

                return estado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Impresion de un solo cupon
        /// </summary>
        public void ImprimirSimple()
        {
            try
            {
                if (this.impresionCupon == null)
                    return;

                FrmContenedor.Instance.Cargar(true);

                string codBarEmpty = new string('0', 13);

                if (this.ImpresoraLista(3))
                {

                    #region Obtener detalle de Cupon
                    var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, this.idCupon, this.idGrupo, this.tipoCupon);
                    detalleCupon.TipoDocumento = this.tipoDocumento;
                    detalleCupon.Tienda = this.tienda;
                    #endregion

                    #region Actualizar el codigo de barra del cupon
                    if (detalleCupon.FlagSeriado == 1)
                    {
                        string numeroBonusTitular = detalleCupon.BonusTitularSeriado;
                        string tipocupon = detalleCupon.CodEmision.ToString();

                        detalleCupon.CodigoBarra = new BL.Cupones().ObtenerCodigoBarra(numeroBonusTitular,
                                                                                    this.nroDocumento,
                                                                                    tipocupon,
                                                                                    this.hostname,
                                                                                    this.tienda);

                    }

                    this.codigoBarraCupon = detalleCupon.CodigoBarra;
                    #endregion

                    if (this.codigoBarraCupon.Equals(codBarEmpty) == false)  //Si existe codigo de barras
                    {
                        #region Obtener imagen del cupon a imprimir
                        this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);
                        #endregion

                        #region Mandar tarea de impresion 
                        using (var pdCupon = new PrintDocument())
                        {
                            pdCupon.PrintPage += new PrintPageEventHandler(pdCupon_PrintPage);
                            pdCupon.Print();
                        }
                        #endregion

                        #region Registrar la impresion
                        var taskImpresion = Task<bool>.Factory.StartNew(() => this.RegistraImpresionCupon(this.idCupon, this.tipoCupon, this.codigoBarraCupon));
                        try
                        {
                            taskImpresion.Wait();

                            if (taskImpresion.Result)
                            {
                                if (FrmContenedor.Instance.Sesion.CuponesImpresos == null)
                                    FrmContenedor.Instance.Sesion.CuponesImpresos = new List<int>();

                                FrmContenedor.Instance.Sesion.CuponesImpresos.Add(this.idCupon);
                            }

                        }
                        catch (AggregateException taskEx)
                        {
                            throw taskEx.Flatten().InnerException;
                        }
                        #endregion

                        //Validar impresion
                        if (this.ImpresoraLista(10))
                        {

                            #region Mostrar Menu
                            this.CargarPerfil();
                            if (this.perfilAplicacion != null)
                            {
                                this.MostrarOpcionesMenu();
                            }
                            else //Cargar solo si tiene cupones, caso contrario llevar todo a Mis Cupones
                            {
                                if (this.idGrupo == 2 && this.EsOpcionVisible(this.perfilAplicacion.OpcionMiSuperCupon) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 3 && this.EsOpcionVisible(this.perfilAplicacion.OpcionMarcasSeleccionadas) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 4 && this.EsOpcionVisible(this.perfilAplicacion.OpcionCuponesDelLugar) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 5 && this.EsOpcionVisible(this.perfilAplicacion.OpcionRuletaDeCupones) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 6 && this.EsOpcionVisible(this.perfilAplicacion.OpcionArbolDeCupones) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 7 && this.EsOpcionVisible(this.perfilAplicacion.OpcionMisPuntosBonus) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 8 && this.EsOpcionVisible(this.perfilAplicacion.OpcionMisInvitaciones) == false)
                                    this.idGrupo = 1;
                                else if (this.idGrupo == 9 && this.EsOpcionVisible(this.perfilAplicacion.OpcionCuponesComerciales) == false)
                                    this.idGrupo = 1;
                            }
                            #endregion

                            #region Mostrar Cupones
                            this.BotonPresionado();

                            switch (this.idGrupo)
                            {
                                case 1://Cupones estandar
                                    FrmContenedor.Instance.Sesion.OpcionMisCupones = true;
                                    this.MostrarCupones();
                                    break;

                                case 2://Super Cupon
                                    FrmContenedor.Instance.Sesion.OpcionMiSuperCupon = true;
                                    this.MostrarSuperCupones();
                                    break;

                                case 3://Cupones Premiun
                                    FrmContenedor.Instance.Sesion.OpcionMarcasSeleccionadas = true;
                                    this.MostrarCupones();
                                    break;

                                case 4://Cupones del Lugar
                                    FrmContenedor.Instance.Sesion.OpcionCuponesDelLugar = true;
                                    this.MostrarCupones();
                                    break;

                                case 5://Ruleta de Cupones
                                    FrmContenedor.Instance.Sesion.OpcionRuletaDeCupones = true;
                                    this.MostrarRuleta();
                                    break;

                                case 6://Arbol de Cupones
                                    FrmContenedor.Instance.Sesion.OpcionArbolDeCupones = true;
                                    this.MostrarArbol();
                                    break;

                                case 7://Bonus
                                    FrmContenedor.Instance.Sesion.OpcionMisPuntosBonus = true;
                                    this.MostrarBonus();
                                    break;

                                case 8://Cupones invitaciones
                                    FrmContenedor.Instance.Sesion.OpcionMisInvitaciones = true;
                                    this.MostrarCupones();
                                    break;

                                case 9://Cupones Comerciales
                                    FrmContenedor.Instance.Sesion.OpcionCuponesComerciales = true;
                                    this.MostrarCupones();
                                    break;

                                default:
                                    break;
                            }
                            #endregion

                        }
                        else
                        {
                            FrmContenedor.Instance.impresoraOperativa = false;
                        }
                    }

                }
                else
                {
                    FrmContenedor.Instance.impresoraOperativa = false;
                }

                FrmContenedor.Instance.MenuPrincipal();
                this.LimpiarImpresora();

                if (this.codigoBarraCupon.Equals(codBarEmpty) == true)
                    FrmContenedor.Instance.Alerta("CUPON INVALIDO");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.codigoBarraCupon = "";
                this.impresionCupon = null;
            }
        }

        /// <summary>
        /// Impresion de multiples cupones
        /// </summary>
        public void ImprimirMultiple()
        {
            try
            {
                #region Validar cupones seleccionados
                if (this.lstCuponesSeleccionados == null
                    || this.lstCuponesSeleccionados.Count == 0)
                {
                    FrmContenedor.Instance.Alerta("SELECCIONE AL MENOS UN CUPON.");
                    return;
                }
                #endregion

                FrmContenedor.Instance.Cargar(true);

                if (this.ImpresoraLista(3) == true)
                {

                    //Obtener imagenes de cupones para imprimir
                    int idCupon = 0;
                    int tipoCupon = 0;
                    string codigoBarra = "";
                    string codBarEmpty = new string('0', 13);

                    var lstImpresionCupon = new List<Image>();
                    foreach (BE.Cupon cuponSeleccionado in this.lstCuponesSeleccionados)
                    {
                        idCupon = cuponSeleccionado.CodCampania;

                        var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, this.idGrupo);
                        detalleCupon.TipoDocumento = this.tipoDocumento;
                        detalleCupon.Tienda = this.tienda;

                        #region Actualizar el codigo de barra del cupon

                        if (detalleCupon.FlagSeriado == 1)
                        {
                            string numeroBonusTitular = detalleCupon.BonusTitularSeriado;
                            string tipocupon = detalleCupon.CodEmision.ToString();

                            detalleCupon.CodigoBarra = new BL.Cupones().ObtenerCodigoBarra(numeroBonusTitular,
                                                                                        this.nroDocumento,
                                                                                        tipocupon,
                                                                                        this.hostname,
                                                                                        this.tienda);
                        }

                        codigoBarra = detalleCupon.CodigoBarra;

                        #endregion

                        if (codigoBarra.Equals(codBarEmpty) == false)  //Si existe codigo de barras
                        {

                            //Obtener imagen del cupon a imprimir
                            var imagenCupon = this.ObtenerImagenImpresionCupon(detalleCupon);

                            if (imagenCupon != null)
                            {

                                this.impresionCupon = imagenCupon;

                                #region Mandar a Imprimir
                                using (var pdCupon = new PrintDocument())
                                {
                                    pdCupon.PrintPage += new PrintPageEventHandler(pdCupon_PrintPage);
                                    pdCupon.Print();
                                }
                                #endregion

                                #region Registra la impresion
                                var taskImpresion = Task<bool>.Factory.StartNew(() => this.RegistraImpresionCupon(idCupon, tipoCupon, codigoBarra));
                                try
                                {
                                    taskImpresion.Wait();

                                    if (taskImpresion.Result)
                                    {
                                        if (FrmContenedor.Instance.Sesion.CuponesImpresos == null)
                                            FrmContenedor.Instance.Sesion.CuponesImpresos = new List<int>();

                                        FrmContenedor.Instance.Sesion.CuponesImpresos.Add(idCupon);
                                    }
                                }
                                catch (AggregateException taskEx)
                                {
                                    throw taskEx.Flatten().InnerException;
                                }
                                #endregion

                                //Valida si imprimio
                                if (this.ImpresoraLista(10) == false)
                                {
                                    FrmContenedor.Instance.impresoraOperativa = false;
                                    break;
                                }

                            }

                        }
                        
                    }

                }
                else
                {
                    FrmContenedor.Instance.impresoraOperativa = false;
                }

                //Mostrar Menu
                this.CargarPerfil();
                if (this.perfilAplicacion != null)
                    this.MostrarOpcionesMenu();

                FrmContenedor.Instance.MenuPrincipal();
                this.LimpiarImpresora();

                //Mostrar cupones 
                this.BotonPresionado();
                this.MostrarCupones();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.impresionCupon = null;
            }
        }

        /// <summary>
        /// Evento click del boton imprimir multiple
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.ImprimirMultiple();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Impresion de cupon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void pdCupon_PrintPage(object sender, PrintPageEventArgs ev)
        {
            try
            {
                Rectangle pRectangle = new System.Drawing.Rectangle(
                       perfilAplicacion.MargenIzquierdoX,
                       perfilAplicacion.MargenSuperiorY,
                       (int)ev.Graphics.VisibleClipBounds.Width + perfilAplicacion.Ancho,
                       (int)ev.Graphics.VisibleClipBounds.Height + perfilAplicacion.Alto);

                // Draw a picture.
                ev.Graphics.DrawImage(this.impresionCupon, pRectangle);

                // Indicate that this is the last page to print.
                ev.HasMorePages = false;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener la imagen de la ruleta de cupones
        /// </summary>
        /// <param name="lstCupones">Lista de cupones de ruleta</param>
        /// <returns></returns>
        private Image ObtenerImagenRuleta(List<BE.Cupon> lstCupones)
        {
            Image ruleta = null;
            try
            {

                //TEMPORAL
                ruleta = Cupones.Winform.Properties.Resources._09_ruleta;

                return ruleta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen de impresion del cupon
        /// </summary>
        /// <param name="beDetalleCupon">Objeto del detalle de cupon</param>
        /// <returns></returns>
        private Image ObtenerImagenImpresionCupon(BE.DetalleCupon beDetalleCupon)
        {
            Image impresionCupon = null;
            try
            {
                var commonCupon = new COMMON.Coupon(General.Ambito);
                impresionCupon = commonCupon.ImageForPrint(beDetalleCupon);
                commonCupon = null;

                return impresionCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }    
        }

        /// <summary>
        /// Evento de imagen de super cupon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbSuperCupon_Click(object sender, EventArgs e)
        {
            try
            {
                int idCupon = this.superCupon.CodCampania;

                var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, 2, this.tipoCupon);
                detalleCupon.TipoDocumento = this.tipoDocumento;
                detalleCupon.Tienda = this.tienda;

                this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);
                if (this.impresionCupon != null)
                {
                    var cupon = FrmContenedor.Instance.Cupon();
                    cupon.VistaPrevia(this.impresionCupon, this.idCupon, this.idGrupo);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click de los botones del arbol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbCupones_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstArbolCupon == null || this.lstArbolCupon.Count == 0)
                    return;

                string nameOrigen = "pbCupon";
                string nameTemp = (sender as PictureBox).Name;
                int pos = nameTemp.Length - nameOrigen.Length;
                int iNodo = int.Parse(nameTemp.Substring(nameTemp.Length - pos));

                var cuponArbol = this.lstArbolCupon[iNodo];

                int idCupon = cuponArbol.CodCampania;

                var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, this.idGrupo);
                detalleCupon.TipoDocumento = this.tipoDocumento;
                detalleCupon.Tienda = this.tienda;

                this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);
                if (this.impresionCupon != null)
                {
                    this.idCupon = idCupon;

                    var cupon = FrmContenedor.Instance.Cupon();
                    cupon.VistaPrevia(this.impresionCupon, this.idCupon, this.idGrupo);
                }

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton Copa del Arbol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCupon11_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstArbolCupon == null || this.lstArbolCupon.Count == 0)
                    return;

                int copaNodo = this.lstArbolCupon.Count - 1;

                var cuponArbol = this.lstArbolCupon[copaNodo];

                int idCupon = cuponArbol.CodCampania;

                var detalleCupon = new BL.Cupones().ObtenerDetalleCupon(this.nroDocumento, idCupon, this.idGrupo);
                detalleCupon.TipoDocumento = this.tipoDocumento;
                detalleCupon.Tienda = this.tienda;

                this.impresionCupon = this.ObtenerImagenImpresionCupon(detalleCupon);

                if (this.impresionCupon != null)
                {
                    this.idCupon = idCupon;

                    var cupon = FrmContenedor.Instance.Cupon();
                    cupon.VistaPrevia(this.impresionCupon, this.idCupon, this.idGrupo);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton jugar ruleta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnJugar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.idGrupo != 5)
                    return;

                //Obdener cupones

                //Dibujar ruleta

                //Girar ruleta
                this.tmrRuleta.Enabled = !this.tmrRuleta.Enabled;
                if (this.tmrRuleta.Enabled == true)
                    this.tmrRuleta.Start();
                else
                    this.tmrRuleta.Stop();
 
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Temporizador de la ruleta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrRuleta_Tick(object sender, EventArgs e)
        {
            try
            {

                rotacion += 15;

                Image Newimage;
                Bitmap bitmap = (Bitmap)Cupones.Winform.Properties.Resources._09_ruleta;
                Newimage = bitmap;

                using (Graphics graphics = Graphics.FromImage(Newimage))
                {
                    graphics.TranslateTransform((float)bitmap.Width / 2, (float)bitmap.Height / 2);
                    graphics.RotateTransform(rotacion);
                    graphics.TranslateTransform(-(float)bitmap.Width / 2, -(float)bitmap.Height / 2);
                    graphics.DrawImage(bitmap, new Point(0, 0));
                }

                pbRuleta.Image = Newimage;
                if (rotacion >= 360)
                {
                    rotacion = 0;
                }

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }


        /// <summary>
        /// Vista del menu de botones segun selección
        /// </summary>
        /// <param name="idGrupo">Id del Grupo</param>
        private void BotonPresionado()
        {
            try
            {

                int cntGruposBotones = 8;

                for (int idGrupoBoton = 1; idGrupoBoton <= cntGruposBotones; idGrupoBoton++)
                {
                    string nomGrupoBoton = "btnGrupo" + idGrupoBoton.ToString() +  "S";
                    Button btnGrupoBoton = null;

                    var findGrupoBoton = this.Controls.Find(nomGrupoBoton, true);
                    if (findGrupoBoton.Count() == 1)
                    {
                        btnGrupoBoton = (Button)findGrupoBoton[0];

                        if (idGrupoBoton == this.idGrupo)
                        {
                            if (this.BotonesActivos.ContainsKey(idGrupoBoton))
                                btnGrupoBoton.Image = this.BotonesActivos.FirstOrDefault(f => f.Key == idGrupoBoton).Value;
                        }
                        else
                        {
                            if (this.BotonesInactivos.ContainsKey(idGrupoBoton))
                                btnGrupoBoton.Image = this.BotonesInactivos.FirstOrDefault(f => f.Key == idGrupoBoton).Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Evento click de los botones del menu de opciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGrupos_Click(object sender, EventArgs e)
        {
            try
            {
                var boton = (sender as Button);
                int idGrupoNuevo = int.Parse(boton.Tag.ToString());

                #region Validar que no se seleccione el mismo boton de grupo
                if (this.idGrupo == idGrupoNuevo)
                {
                    if (this.idGrupo == 1
                        || this.idGrupo == 3
                        || this.idGrupo == 4
                        || this.idGrupo == 8
                        || this.idGrupo == 9)
                    {
                        if (this.multiCupon == false)
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                #endregion

                this.idGrupo = idGrupoNuevo;
                this.BotonPresionado();

                switch (this.idGrupo)
                {
                    case 1://Cupones estandar
                        FrmContenedor.Instance.Sesion.OpcionMisCupones = true;
                        this.MostrarCupones();
                        break;

                    case 2://Super Cupon
                        FrmContenedor.Instance.Sesion.OpcionMiSuperCupon = true;
                        this.MostrarSuperCupones();
                        break;

                    case 3://Cupones Premiun
                        FrmContenedor.Instance.Sesion.OpcionMarcasSeleccionadas = true;
                        this.MostrarCupones();
                        break;

                    case 4://Cupones del Lugar
                        FrmContenedor.Instance.Sesion.OpcionCuponesDelLugar = true;
                        this.MostrarCupones();
                        break;

                    case 5://Ruleta de Cupones
                        FrmContenedor.Instance.Sesion.OpcionRuletaDeCupones = true;
                        this.MostrarRuleta();
                        break;

                    case 6://Arbol de Cupones
                        FrmContenedor.Instance.Sesion.OpcionArbolDeCupones = true;
                        this.MostrarArbol();
                        break;

                    case 7://Bonus
                        FrmContenedor.Instance.Sesion.OpcionMisPuntosBonus = true;
                        this.MostrarBonus();
                        break;

                    case 8://Cupones invitaciones
                        FrmContenedor.Instance.Sesion.OpcionMisInvitaciones = true;
                        this.MostrarCupones();
                        break;

                    case 9://Cupones Comerciales
                        FrmContenedor.Instance.Sesion.OpcionCuponesComerciales = true;
                        this.MostrarCupones();
                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento Hover de los botones del menu de opciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGrupos_MouseEnter(object sender, EventArgs e)
        {
            var boton = (sender as Button);
            boton.UseVisualStyleBackColor = false;
            boton.BackColor = Color.White;
        }

        /// <summary>
        /// Evento Leave de los botones del menu de opciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGrupos_MouseLeave(object sender, EventArgs e)
        {
            var boton = (sender as Button);
            boton.UseVisualStyleBackColor = true;
        }

        /// <summary>
        /// Evento click del boton salir del menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                this.ActivarSesion(false);

                FrmContenedor.Instance.Experiencia();
                
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Activa o desactiva el tiempo de sesion
        /// </summary>
        /// <param name="si">Opcional : ¿Sesion activa?</param>
        public void ActivarSesion(bool si = false)
        {
            try
            {
                this.lblTiempoSession.Text = this.tiempoSesion.ToString();

                #region Activar o Desactivar Sesion
                this.tmrSesion.Enabled = si;

                if (si)
                    this.tmrSesion.Start();
                else
                    this.tmrSesion.Stop();
                #endregion

                #region Mostrar Banners
                if (si)
                {
                    this.lstBeBanners = new List<BE.Banner>();

                    try
                    {
                        var tsk = Task<List<BE.Banner>>.Factory.StartNew(() => ListarBanners());
                        tsk.Wait();
                        this.lstBeBanners = tsk.Result;
                    }
                    catch (AggregateException taskEx)
                    {
                        throw taskEx.Flatten().InnerException;
                    }

                    this.lstBeBanners[0].Contador = 1;
                    this.beBanner = this.lstBeBanners[0];

                    if (this.beBanner.Id > 0 
                        && FrmContenedor.Instance.Sesion.BannersVistos.Contains(this.beBanner.Id) == false)
                        FrmContenedor.Instance.Sesion.BannersVistos.Add(this.beBanner.Id);

                    this.pbBanner.Image = this.beBanner.Imagen;

                    if (this.lstBeBanners.Count > 1)
                    {
                        this.tmrBanners.Enabled = true;
                        this.tmrBanners.Start();
                    }   
                    else
                    {
                        this.tmrBanners.Enabled = false;
                        this.tmrBanners.Stop();
                    }
                        
                }
                else
                {
                    this.nroBanner = 0;
                    this.tiempoBanner = 0;

                    this.lblTiempoCupon.Text = "1";
                    this.lblTiempoSession.Text = "1";

                    this.tmrBanners.Enabled = false;
                    this.tmrBanners.Stop();
                }
                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
