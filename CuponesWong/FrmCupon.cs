﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using BL = Cupones.BusinessLogic;

namespace Cupones.Winform
{
    public partial class FrmCupon : Form
    {

        private int idCupon = 0;
        private int idGrupo = 0; 

        private Image cupon = null;

        public FrmCupon()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Carga Inicial del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCupon_Load(object sender, EventArgs e)
        {
            try
            {
                if (this.cupon == null)
                    return;

                this.pbCupon.Image = this.cupon;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }


        /// <summary>
        /// Registra la interaccion del cupon
        /// </summary>
        /// <param name="nroDocumento"></param>
        /// <param name="idInteraccion"></param>
        /// <param name="tienda"></param>
        private void RegistrarInteraccion(string nroDocumento, int idInteraccion, string tienda)
        {
            try
            {
                bool rpta = new BL.Cupones().RegistrarInteraccion(nroDocumento, idCupon, idGrupo, idInteraccion, tienda);

                if (rpta == false)
                    throw new Exception("No se registro la interacción");
                else
                {
                    if (FrmContenedor.Instance.Sesion.CuponesVistos == null)
                        FrmContenedor.Instance.Sesion.CuponesVistos = new List<int>();

                    FrmContenedor.Instance.Sesion.CuponesVistos.Add(idCupon);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }


        /// <summary>
        /// Evento click del boton Cerrar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {

                //Registra Interaccion
                string nroDocumento = FrmContenedor.Instance.NroDocumento;
                string ambito = General.Ambito;
                string tienda = General.Configuracion.Tienda; 
                int idInteraccion = 1;

                var taskInteraccion = Task.Factory.StartNew(() => RegistrarInteraccion(nroDocumento, idInteraccion, tienda));

                var menu = FrmContenedor.Instance.MenuPrincipal();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click del boton Imprimir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cupon == null)
                    return;

                var menu = FrmContenedor.Instance.MenuPrincipal();
                menu.ImprimirSimple();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Vista previa del cupon impreso
        /// </summary>
        /// <param name="cupon"></param>
        public void VistaPrevia(Image cupon, int idCupon, int idGrupo)
        {
            try
            {
                this.cupon = cupon;

                this.idCupon = idCupon;
                this.idGrupo = idGrupo;

                this.pbCupon.Image = this.cupon;
                this.pbCupon.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
