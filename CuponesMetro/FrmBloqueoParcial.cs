using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Text;
using System.Reflection;

namespace Cupones.Winform
{
    public partial class FrmBloqueoParcial : Form
    {

        #region Singleton pattern

        private static FrmBloqueoParcial instance = null;

        public static FrmBloqueoParcial Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmBloqueoParcial();

                return instance;
            }
        }

        #endregion

        private int clickConfiguracion = 0; //Cuenta la cantidad de clic para ingresar a la configuracion

        public FrmBloqueoParcial()
        {
            InitializeComponent();
        }

        private void FrmPanel_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics mGraphics = e.Graphics;
                Pen pen1 = new Pen(Color.FromArgb(255, 0, 0), 1);

                Rectangle Area1 = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                LinearGradientBrush LGB = new LinearGradientBrush(Area1, Color.FromArgb(255, 0, 0), Color.FromArgb(255, 251, 251), LinearGradientMode.Vertical);
                mGraphics.FillRectangle(LGB, Area1);
                mGraphics.DrawRectangle(pen1, Area1);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

       
        private void pnHabilitar_Click(object sender, EventArgs e)
        {
            try
            {
                this.clickConfiguracion++;
                if (this.clickConfiguracion >= 5) //Clicks 5 veces 
                {
                    this.clickConfiguracion = 0;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void lblMensajeDescripcion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                General.RegistrarAdvertencia(this.lblMensajeDescripcion.Text, MethodBase.GetCurrentMethod());
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void FrmPanel_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblMensajeDescripcion.TextChanged += new EventHandler(lblMensajeDescripcion_TextChanged);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

    }
}