﻿using System;
using System.Windows.Forms;
using COMMON = Cupones.Common;

namespace Cupones.Winform
{
    public partial class FrmTerminos : Form
    {

        #region Singleton pattern

        private static FrmTerminos instance = null;

        public static FrmTerminos Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmTerminos();
                return instance;
            }
        }

        #endregion

        public FrmTerminos()
        {
            InitializeComponent();
        }

        private void FrmTerminos_Load(object sender, EventArgs e)
        {
            try
            {
                string compania = General.Ambito;
                string terminos = "";

                terminos = new COMMON.Legal(compania).GetTermsAndConditions();

                this.txtTerminos.Text = terminos;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }
       
    }
}
