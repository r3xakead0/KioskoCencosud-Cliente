﻿namespace Cupones.Winform
{
    partial class FrmCumpleanhos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCumpleanhos));
            this.lblTiempo = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.wmpCumpleanhos = new AxWMPLib.AxWindowsMediaPlayer();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.wmpCumpleanhos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTiempo
            // 
            this.lblTiempo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempo.ForeColor = System.Drawing.Color.Red;
            this.lblTiempo.Location = new System.Drawing.Point(354, 966);
            this.lblTiempo.Name = "lblTiempo";
            this.lblTiempo.Size = new System.Drawing.Size(63, 25);
            this.lblTiempo.TabIndex = 2;
            this.lblTiempo.Text = "5";
            this.lblTiempo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.Color.Black;
            this.lblCliente.Location = new System.Drawing.Point(0, 840);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(762, 57);
            this.lblCliente.TabIndex = 4;
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wmpCumpleanhos
            // 
            this.wmpCumpleanhos.Enabled = true;
            this.wmpCumpleanhos.Location = new System.Drawing.Point(12, 12);
            this.wmpCumpleanhos.Name = "wmpCumpleanhos";
            this.wmpCumpleanhos.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmpCumpleanhos.OcxState")));
            this.wmpCumpleanhos.Size = new System.Drawing.Size(744, 69);
            this.wmpCumpleanhos.TabIndex = 1;
            this.wmpCumpleanhos.Visible = false;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.Transparent;
            this.btnSalir.Image = global::Cupones.Winform.Properties.Resources._07_btn_salir;
            this.btnSalir.Location = new System.Drawing.Point(619, 14);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(120, 49);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmCumpleanhos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Cupones.Winform.Properties.Resources._06_background;
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblTiempo);
            this.Controls.Add(this.wmpCumpleanhos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCumpleanhos";
            this.Text = "CENCOSUD";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //((System.ComponentModel.ISupportInitialize)(this.wmpCumpleanhos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTiempo;
        private System.Windows.Forms.Label lblCliente;
        private AxWMPLib.AxWindowsMediaPlayer wmpCumpleanhos;
        private System.Windows.Forms.Button btnSalir;
    }
}