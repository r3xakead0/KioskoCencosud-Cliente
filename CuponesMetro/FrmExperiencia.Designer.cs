﻿namespace Cupones.Winform
{
    partial class FrmExperiencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNo = new System.Windows.Forms.Button();
            this.btnAlgo = new System.Windows.Forms.Button();
            this.btnMegusto = new System.Windows.Forms.Button();
            this.btnMeencanto = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.btnInicio = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblTiempoSession = new System.Windows.Forms.Label();
            this.lblSession = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnNo
            // 
            this.btnNo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNo.Image = global::Cupones.Winform.Properties.Resources._07_encuesta_01;
            this.btnNo.Location = new System.Drawing.Point(65, 913);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(137, 175);
            this.btnNo.TabIndex = 0;
            this.btnNo.Tag = "1";
            this.btnNo.UseVisualStyleBackColor = true;
            this.btnNo.Click += new System.EventHandler(this.btnExperiencia_Click);
            // 
            // btnAlgo
            // 
            this.btnAlgo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAlgo.Image = global::Cupones.Winform.Properties.Resources._07_encuesta_02;
            this.btnAlgo.Location = new System.Drawing.Point(232, 913);
            this.btnAlgo.Name = "btnAlgo";
            this.btnAlgo.Size = new System.Drawing.Size(137, 175);
            this.btnAlgo.TabIndex = 1;
            this.btnAlgo.Tag = "2";
            this.btnAlgo.UseVisualStyleBackColor = true;
            this.btnAlgo.Click += new System.EventHandler(this.btnExperiencia_Click);
            // 
            // btnMegusto
            // 
            this.btnMegusto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnMegusto.Image = global::Cupones.Winform.Properties.Resources._07_encuesta_03;
            this.btnMegusto.Location = new System.Drawing.Point(399, 913);
            this.btnMegusto.Name = "btnMegusto";
            this.btnMegusto.Size = new System.Drawing.Size(137, 175);
            this.btnMegusto.TabIndex = 2;
            this.btnMegusto.Tag = "3";
            this.btnMegusto.UseVisualStyleBackColor = true;
            this.btnMegusto.Click += new System.EventHandler(this.btnExperiencia_Click);
            // 
            // btnMeencanto
            // 
            this.btnMeencanto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnMeencanto.Image = global::Cupones.Winform.Properties.Resources._07_encuesta_04;
            this.btnMeencanto.Location = new System.Drawing.Point(566, 913);
            this.btnMeencanto.Name = "btnMeencanto";
            this.btnMeencanto.Size = new System.Drawing.Size(137, 175);
            this.btnMeencanto.TabIndex = 3;
            this.btnMeencanto.Tag = "4";
            this.btnMeencanto.UseVisualStyleBackColor = true;
            this.btnMeencanto.Click += new System.EventHandler(this.btnExperiencia_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Location = new System.Drawing.Point(155, 1099);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(463, 24);
            this.lblMensaje.TabIndex = 7;
            this.lblMensaje.Text = "GRACIAS POR SU OPINION. VUELVA PRONTO";
            this.lblMensaje.Visible = false;
            // 
            // btnInicio
            // 
            this.btnInicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInicio.BackgroundImage = global::Cupones.Winform.Properties.Resources._07_btn_inicio;
            this.btnInicio.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInicio.FlatAppearance.BorderSize = 0;
            this.btnInicio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicio.ForeColor = System.Drawing.Color.Transparent;
            this.btnInicio.Location = new System.Drawing.Point(498, 21);
            this.btnInicio.Margin = new System.Windows.Forms.Padding(0);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(120, 49);
            this.btnInicio.TabIndex = 5;
            this.btnInicio.UseVisualStyleBackColor = false;
            this.btnInicio.Visible = false;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.BackgroundImage = global::Cupones.Winform.Properties.Resources._07_btn_salir;
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.Transparent;
            this.btnSalir.Location = new System.Drawing.Point(632, 21);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(120, 49);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Visible = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTiempoSession
            // 
            this.lblTiempoSession.AutoSize = true;
            this.lblTiempoSession.BackColor = System.Drawing.Color.Transparent;
            this.lblTiempoSession.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoSession.Location = new System.Drawing.Point(415, 13);
            this.lblTiempoSession.Name = "lblTiempoSession";
            this.lblTiempoSession.Size = new System.Drawing.Size(32, 22);
            this.lblTiempoSession.TabIndex = 6;
            this.lblTiempoSession.Text = "45";
            // 
            // lblSession
            // 
            this.lblSession.AutoSize = true;
            this.lblSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSession.Location = new System.Drawing.Point(273, 14);
            this.lblSession.Name = "lblSession";
            this.lblSession.Size = new System.Drawing.Size(144, 18);
            this.lblSession.TabIndex = 8;
            this.lblSession.Text = "Tu sesión cerrará en";
            this.lblSession.Visible = true;
            // 
            // FrmExperiencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Cupones.Winform.Properties.Resources._07_background;
            this.ClientSize = new System.Drawing.Size(768, 1366);
            this.ControlBox = false;
            this.Controls.Add(this.lblSession);
            this.Controls.Add(this.lblTiempoSession);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnInicio);
            this.Controls.Add(this.btnMeencanto);
            this.Controls.Add(this.btnMegusto);
            this.Controls.Add(this.btnAlgo);
            this.Controls.Add(this.btnNo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmExperiencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Experiencia";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmExperiencia_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNo;
        private System.Windows.Forms.Button btnAlgo;
        private System.Windows.Forms.Button btnMegusto;
        private System.Windows.Forms.Button btnMeencanto;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label lblTiempoSession;
        private System.Windows.Forms.Label lblSession;
    }
}