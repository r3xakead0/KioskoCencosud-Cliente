using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Cupones.Winform
{
    public partial class FrmMensaje : Form
    {

        #region Singleton pattern

        private static FrmMensaje instance = null;

        public static FrmMensaje Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmMensaje();

                return instance;
            }
        }

        #endregion

        private Timer tmrMensaje = null;
        private int tiempoMensaje = 30; //Tiempo de duracion en segundos

        public FrmMensaje()
        {
            InitializeComponent();
        }

        private void FrmMensaje_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblTiempo.Text = tiempoMensaje.ToString();

                this.tmrMensaje = new Timer();
                this.tmrMensaje.Interval = 1000;
                this.tmrMensaje.Tick += new System.EventHandler(this.tmrMensaje_tick);
                this.tmrMensaje.Enabled = true; 
                this.tmrMensaje.Start();

                General.RegistrarAdvertencia(this.lblMensaje.Text, MethodBase.GetCurrentMethod());

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
            
        }

        private void FrmMensaje_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics mGraphics = e.Graphics;
                Pen pen1 = new Pen(Color.FromArgb(255, 0, 0), 1);

                Rectangle Area1 = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                LinearGradientBrush LGB = new LinearGradientBrush(Area1, Color.FromArgb(255, 0, 0), Color.FromArgb(255, 251, 251), LinearGradientMode.Vertical);
                mGraphics.FillRectangle(LGB, Area1);
                mGraphics.DrawRectangle(pen1, Area1);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
           
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblTiempo.Text = this.tiempoMensaje.ToString();
                this.tmrMensaje.Enabled = false;
                this.tmrMensaje.Stop();
                this.Close();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void tmrMensaje_tick(object sender, EventArgs e)
        {
            try
            {
                int tiempo = int.Parse(this.lblTiempo.Text);
                tiempo--;

                if (tiempo >= 0)
                {
                    this.lblTiempo.Text = tiempo.ToString();
                }
                else
                {
                    this.lblTiempo.Text = this.tiempoMensaje.ToString();
                    this.tmrMensaje.Enabled = false;
                    this.tmrMensaje.Stop();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        } 
    }
}