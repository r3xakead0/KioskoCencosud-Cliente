﻿namespace Cupones.Winform
{
    partial class FrmConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GBConfig = new System.Windows.Forms.GroupBox();
            this.TBUbiTienda = new System.Windows.Forms.TextBox();
            this.cbTienda = new System.Windows.Forms.ComboBox();
            this.cbCompañia = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GBST = new System.Windows.Forms.GroupBox();
            this.TBEmailST = new System.Windows.Forms.TextBox();
            this.TBTelfST = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.GBCC = new System.Windows.Forms.GroupBox();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.TBPuerto = new System.Windows.Forms.TextBox();
            this.TBEmail = new System.Windows.Forms.TextBox();
            this.TBServidor = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnNumCero = new System.Windows.Forms.Button();
            this.btnNumNueve = new System.Windows.Forms.Button();
            this.btnNumOcho = new System.Windows.Forms.Button();
            this.btnNumSiete = new System.Windows.Forms.Button();
            this.btnNumSeis = new System.Windows.Forms.Button();
            this.btnNumCinco = new System.Windows.Forms.Button();
            this.btnNumCuatro = new System.Windows.Forms.Button();
            this.btnNumTres = new System.Windows.Forms.Button();
            this.btnNumDos = new System.Windows.Forms.Button();
            this.btnNumUno = new System.Windows.Forms.Button();
            this.btnLetraP = new System.Windows.Forms.Button();
            this.btnLetraO = new System.Windows.Forms.Button();
            this.btnLetraI = new System.Windows.Forms.Button();
            this.btnLetraU = new System.Windows.Forms.Button();
            this.btnLetraY = new System.Windows.Forms.Button();
            this.btnLetraT = new System.Windows.Forms.Button();
            this.btnLetraR = new System.Windows.Forms.Button();
            this.btnLetraE = new System.Windows.Forms.Button();
            this.btnLetraW = new System.Windows.Forms.Button();
            this.btnLetraQ = new System.Windows.Forms.Button();
            this.btnLetraÑ = new System.Windows.Forms.Button();
            this.btnLetraL = new System.Windows.Forms.Button();
            this.btnLetraK = new System.Windows.Forms.Button();
            this.btnLetraJ = new System.Windows.Forms.Button();
            this.btnLetraH = new System.Windows.Forms.Button();
            this.btnLetraG = new System.Windows.Forms.Button();
            this.btnLetraF = new System.Windows.Forms.Button();
            this.btnLetraD = new System.Windows.Forms.Button();
            this.btnLetraS = new System.Windows.Forms.Button();
            this.btnLetraA = new System.Windows.Forms.Button();
            this.btnLetraPunto = new System.Windows.Forms.Button();
            this.btnLetraM = new System.Windows.Forms.Button();
            this.btnLetraN = new System.Windows.Forms.Button();
            this.btnLetraB = new System.Windows.Forms.Button();
            this.btnLetraV = new System.Windows.Forms.Button();
            this.btnLetraC = new System.Windows.Forms.Button();
            this.btnLetraX = new System.Windows.Forms.Button();
            this.btnLetraZ = new System.Windows.Forms.Button();
            this.btnLetraArroba = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.GBConfig.SuspendLayout();
            this.GBST.SuspendLayout();
            this.GBCC.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBConfig
            // 
            this.GBConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GBConfig.Controls.Add(this.TBUbiTienda);
            this.GBConfig.Controls.Add(this.cbTienda);
            this.GBConfig.Controls.Add(this.cbCompañia);
            this.GBConfig.Controls.Add(this.label3);
            this.GBConfig.Controls.Add(this.label2);
            this.GBConfig.Controls.Add(this.label1);
            this.GBConfig.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBConfig.Location = new System.Drawing.Point(12, 12);
            this.GBConfig.Name = "GBConfig";
            this.GBConfig.Size = new System.Drawing.Size(740, 160);
            this.GBConfig.TabIndex = 0;
            this.GBConfig.TabStop = false;
            this.GBConfig.Text = "Configuración";
            // 
            // TBUbiTienda
            // 
            this.TBUbiTienda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBUbiTienda.Font = new System.Drawing.Font("Arial", 15F);
            this.TBUbiTienda.Location = new System.Drawing.Point(194, 105);
            this.TBUbiTienda.Name = "TBUbiTienda";
            this.TBUbiTienda.Size = new System.Drawing.Size(527, 30);
            this.TBUbiTienda.TabIndex = 3;
            // 
            // cbTienda
            // 
            this.cbTienda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTienda.Font = new System.Drawing.Font("Arial", 14F);
            this.cbTienda.FormattingEnabled = true;
            this.cbTienda.Location = new System.Drawing.Point(194, 69);
            this.cbTienda.Name = "cbTienda";
            this.cbTienda.Size = new System.Drawing.Size(527, 30);
            this.cbTienda.TabIndex = 2;
            // 
            // cbCompañia
            // 
            this.cbCompañia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCompañia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCompañia.Font = new System.Drawing.Font("Arial", 14F);
            this.cbCompañia.FormattingEnabled = true;
            this.cbCompañia.Location = new System.Drawing.Point(194, 33);
            this.cbCompañia.Name = "cbCompañia";
            this.cbCompañia.Size = new System.Drawing.Size(527, 30);
            this.cbCompañia.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ubicación en la tienda:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tienda:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Compañia:";
            // 
            // GBST
            // 
            this.GBST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GBST.Controls.Add(this.TBEmailST);
            this.GBST.Controls.Add(this.TBTelfST);
            this.GBST.Controls.Add(this.label9);
            this.GBST.Controls.Add(this.label8);
            this.GBST.Font = new System.Drawing.Font("Arial", 12F);
            this.GBST.Location = new System.Drawing.Point(12, 178);
            this.GBST.Name = "GBST";
            this.GBST.Size = new System.Drawing.Size(741, 119);
            this.GBST.TabIndex = 0;
            this.GBST.TabStop = false;
            this.GBST.Text = "Datos soporte técnico";
            // 
            // TBEmailST
            // 
            this.TBEmailST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBEmailST.Font = new System.Drawing.Font("Arial", 15F);
            this.TBEmailST.Location = new System.Drawing.Point(217, 66);
            this.TBEmailST.Name = "TBEmailST";
            this.TBEmailST.Size = new System.Drawing.Size(505, 30);
            this.TBEmailST.TabIndex = 10;
            // 
            // TBTelfST
            // 
            this.TBTelfST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBTelfST.Font = new System.Drawing.Font("Arial", 15F);
            this.TBTelfST.Location = new System.Drawing.Point(218, 30);
            this.TBTelfST.Name = "TBTelfST";
            this.TBTelfST.Size = new System.Drawing.Size(504, 30);
            this.TBTelfST.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "Email Soporte Técnico: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "Teléfono Soporte Técnico:";
            // 
            // GBCC
            // 
            this.GBCC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GBCC.Controls.Add(this.TBPassword);
            this.GBCC.Controls.Add(this.TBPuerto);
            this.GBCC.Controls.Add(this.TBEmail);
            this.GBCC.Controls.Add(this.TBServidor);
            this.GBCC.Controls.Add(this.label13);
            this.GBCC.Controls.Add(this.label12);
            this.GBCC.Controls.Add(this.label11);
            this.GBCC.Controls.Add(this.label10);
            this.GBCC.Font = new System.Drawing.Font("Arial", 12F);
            this.GBCC.Location = new System.Drawing.Point(12, 303);
            this.GBCC.Name = "GBCC";
            this.GBCC.Size = new System.Drawing.Size(741, 124);
            this.GBCC.TabIndex = 0;
            this.GBCC.TabStop = false;
            this.GBCC.Text = "Configuración de Correo";
            // 
            // TBPassword
            // 
            this.TBPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TBPassword.Font = new System.Drawing.Font("Arial", 15F);
            this.TBPassword.Location = new System.Drawing.Point(625, 71);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.PasswordChar = '*';
            this.TBPassword.Size = new System.Drawing.Size(97, 30);
            this.TBPassword.TabIndex = 14;
            // 
            // TBPuerto
            // 
            this.TBPuerto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TBPuerto.Font = new System.Drawing.Font("Arial", 15F);
            this.TBPuerto.Location = new System.Drawing.Point(625, 35);
            this.TBPuerto.Name = "TBPuerto";
            this.TBPuerto.Size = new System.Drawing.Size(97, 30);
            this.TBPuerto.TabIndex = 12;
            this.TBPuerto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TBEmail
            // 
            this.TBEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBEmail.Font = new System.Drawing.Font("Arial", 15F);
            this.TBEmail.Location = new System.Drawing.Point(104, 71);
            this.TBEmail.Name = "TBEmail";
            this.TBEmail.Size = new System.Drawing.Size(441, 30);
            this.TBEmail.TabIndex = 13;
            // 
            // TBServidor
            // 
            this.TBServidor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBServidor.Font = new System.Drawing.Font("Arial", 15F);
            this.TBServidor.Location = new System.Drawing.Point(104, 35);
            this.TBServidor.Name = "TBServidor";
            this.TBServidor.Size = new System.Drawing.Size(441, 30);
            this.TBServidor.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(551, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 18);
            this.label13.TabIndex = 5;
            this.label13.Text = "Clave : ";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(551, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 18);
            this.label12.TabIndex = 4;
            this.label12.Text = "Puerto : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "Email : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 18);
            this.label10.TabIndex = 2;
            this.label10.Text = "Servidor: ";
            // 
            // btnBorrar
            // 
            this.btnBorrar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBorrar.BackColor = System.Drawing.Color.Transparent;
            this.btnBorrar.FlatAppearance.BorderSize = 0;
            this.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnBorrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.Location = new System.Drawing.Point(623, 637);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(57, 57);
            this.btnBorrar.TabIndex = 35;
            this.btnBorrar.TabStop = false;
            this.btnBorrar.Text = "<=";
            this.btnBorrar.UseVisualStyleBackColor = false;
            // 
            // btnNumCero
            // 
            this.btnNumCero.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumCero.FlatAppearance.BorderSize = 0;
            this.btnNumCero.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumCero.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCero.Location = new System.Drawing.Point(622, 456);
            this.btnNumCero.Name = "btnNumCero";
            this.btnNumCero.Size = new System.Drawing.Size(57, 57);
            this.btnNumCero.TabIndex = 34;
            this.btnNumCero.TabStop = false;
            this.btnNumCero.Text = "0";
            this.btnNumCero.UseMnemonic = false;
            this.btnNumCero.UseVisualStyleBackColor = false;
            // 
            // btnNumNueve
            // 
            this.btnNumNueve.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumNueve.FlatAppearance.BorderSize = 0;
            this.btnNumNueve.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumNueve.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumNueve.Location = new System.Drawing.Point(562, 456);
            this.btnNumNueve.Name = "btnNumNueve";
            this.btnNumNueve.Size = new System.Drawing.Size(57, 57);
            this.btnNumNueve.TabIndex = 33;
            this.btnNumNueve.TabStop = false;
            this.btnNumNueve.Text = "9";
            this.btnNumNueve.UseMnemonic = false;
            this.btnNumNueve.UseVisualStyleBackColor = false;
            // 
            // btnNumOcho
            // 
            this.btnNumOcho.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumOcho.FlatAppearance.BorderSize = 0;
            this.btnNumOcho.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumOcho.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumOcho.Location = new System.Drawing.Point(501, 456);
            this.btnNumOcho.Name = "btnNumOcho";
            this.btnNumOcho.Size = new System.Drawing.Size(57, 57);
            this.btnNumOcho.TabIndex = 32;
            this.btnNumOcho.TabStop = false;
            this.btnNumOcho.Text = "8";
            this.btnNumOcho.UseMnemonic = false;
            this.btnNumOcho.UseVisualStyleBackColor = true;
            // 
            // btnNumSiete
            // 
            this.btnNumSiete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumSiete.FlatAppearance.BorderSize = 0;
            this.btnNumSiete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumSiete.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumSiete.Location = new System.Drawing.Point(440, 456);
            this.btnNumSiete.Name = "btnNumSiete";
            this.btnNumSiete.Size = new System.Drawing.Size(57, 57);
            this.btnNumSiete.TabIndex = 31;
            this.btnNumSiete.TabStop = false;
            this.btnNumSiete.Text = "7";
            this.btnNumSiete.UseMnemonic = false;
            this.btnNumSiete.UseVisualStyleBackColor = false;
            // 
            // btnNumSeis
            // 
            this.btnNumSeis.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumSeis.FlatAppearance.BorderSize = 0;
            this.btnNumSeis.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumSeis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumSeis.Location = new System.Drawing.Point(379, 456);
            this.btnNumSeis.Name = "btnNumSeis";
            this.btnNumSeis.Size = new System.Drawing.Size(57, 57);
            this.btnNumSeis.TabIndex = 30;
            this.btnNumSeis.TabStop = false;
            this.btnNumSeis.Text = "6";
            this.btnNumSeis.UseMnemonic = false;
            this.btnNumSeis.UseVisualStyleBackColor = true;
            // 
            // btnNumCinco
            // 
            this.btnNumCinco.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumCinco.BackColor = System.Drawing.Color.Transparent;
            this.btnNumCinco.FlatAppearance.BorderSize = 0;
            this.btnNumCinco.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumCinco.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCinco.Location = new System.Drawing.Point(319, 456);
            this.btnNumCinco.Name = "btnNumCinco";
            this.btnNumCinco.Size = new System.Drawing.Size(57, 57);
            this.btnNumCinco.TabIndex = 29;
            this.btnNumCinco.TabStop = false;
            this.btnNumCinco.Text = "5";
            this.btnNumCinco.UseMnemonic = false;
            this.btnNumCinco.UseVisualStyleBackColor = false;
            // 
            // btnNumCuatro
            // 
            this.btnNumCuatro.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumCuatro.FlatAppearance.BorderSize = 0;
            this.btnNumCuatro.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumCuatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumCuatro.Location = new System.Drawing.Point(258, 456);
            this.btnNumCuatro.Name = "btnNumCuatro";
            this.btnNumCuatro.Size = new System.Drawing.Size(57, 57);
            this.btnNumCuatro.TabIndex = 28;
            this.btnNumCuatro.TabStop = false;
            this.btnNumCuatro.Text = "4";
            this.btnNumCuatro.UseMnemonic = false;
            this.btnNumCuatro.UseVisualStyleBackColor = true;
            // 
            // btnNumTres
            // 
            this.btnNumTres.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumTres.BackColor = System.Drawing.Color.Transparent;
            this.btnNumTres.FlatAppearance.BorderSize = 0;
            this.btnNumTres.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumTres.Location = new System.Drawing.Point(197, 456);
            this.btnNumTres.Name = "btnNumTres";
            this.btnNumTres.Size = new System.Drawing.Size(57, 57);
            this.btnNumTres.TabIndex = 27;
            this.btnNumTres.TabStop = false;
            this.btnNumTres.Text = "3";
            this.btnNumTres.UseMnemonic = false;
            this.btnNumTres.UseVisualStyleBackColor = false;
            // 
            // btnNumDos
            // 
            this.btnNumDos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumDos.BackColor = System.Drawing.SystemColors.Control;
            this.btnNumDos.FlatAppearance.BorderSize = 0;
            this.btnNumDos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumDos.Location = new System.Drawing.Point(137, 456);
            this.btnNumDos.Name = "btnNumDos";
            this.btnNumDos.Size = new System.Drawing.Size(57, 57);
            this.btnNumDos.TabIndex = 26;
            this.btnNumDos.TabStop = false;
            this.btnNumDos.Text = "2";
            this.btnNumDos.UseMnemonic = false;
            this.btnNumDos.UseVisualStyleBackColor = false;
            // 
            // btnNumUno
            // 
            this.btnNumUno.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNumUno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnNumUno.FlatAppearance.BorderSize = 0;
            this.btnNumUno.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNumUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumUno.Location = new System.Drawing.Point(78, 456);
            this.btnNumUno.Name = "btnNumUno";
            this.btnNumUno.Size = new System.Drawing.Size(57, 57);
            this.btnNumUno.TabIndex = 25;
            this.btnNumUno.TabStop = false;
            this.btnNumUno.Text = "1";
            this.btnNumUno.UseMnemonic = false;
            this.btnNumUno.UseVisualStyleBackColor = false;
            // 
            // btnLetraP
            // 
            this.btnLetraP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraP.FlatAppearance.BorderSize = 0;
            this.btnLetraP.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraP.Location = new System.Drawing.Point(622, 516);
            this.btnLetraP.Name = "btnLetraP";
            this.btnLetraP.Size = new System.Drawing.Size(57, 57);
            this.btnLetraP.TabIndex = 45;
            this.btnLetraP.TabStop = false;
            this.btnLetraP.Text = "P";
            this.btnLetraP.UseMnemonic = false;
            this.btnLetraP.UseVisualStyleBackColor = false;
            // 
            // btnLetraO
            // 
            this.btnLetraO.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraO.FlatAppearance.BorderSize = 0;
            this.btnLetraO.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraO.Location = new System.Drawing.Point(563, 516);
            this.btnLetraO.Name = "btnLetraO";
            this.btnLetraO.Size = new System.Drawing.Size(57, 57);
            this.btnLetraO.TabIndex = 44;
            this.btnLetraO.TabStop = false;
            this.btnLetraO.Text = "O";
            this.btnLetraO.UseMnemonic = false;
            this.btnLetraO.UseVisualStyleBackColor = false;
            // 
            // btnLetraI
            // 
            this.btnLetraI.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraI.FlatAppearance.BorderSize = 0;
            this.btnLetraI.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraI.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraI.Location = new System.Drawing.Point(502, 516);
            this.btnLetraI.Name = "btnLetraI";
            this.btnLetraI.Size = new System.Drawing.Size(57, 57);
            this.btnLetraI.TabIndex = 43;
            this.btnLetraI.TabStop = false;
            this.btnLetraI.Text = "I";
            this.btnLetraI.UseMnemonic = false;
            this.btnLetraI.UseVisualStyleBackColor = true;
            // 
            // btnLetraU
            // 
            this.btnLetraU.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraU.FlatAppearance.BorderSize = 0;
            this.btnLetraU.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraU.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraU.Location = new System.Drawing.Point(441, 516);
            this.btnLetraU.Name = "btnLetraU";
            this.btnLetraU.Size = new System.Drawing.Size(57, 57);
            this.btnLetraU.TabIndex = 42;
            this.btnLetraU.TabStop = false;
            this.btnLetraU.Text = "U";
            this.btnLetraU.UseMnemonic = false;
            this.btnLetraU.UseVisualStyleBackColor = false;
            // 
            // btnLetraY
            // 
            this.btnLetraY.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraY.FlatAppearance.BorderSize = 0;
            this.btnLetraY.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraY.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraY.Location = new System.Drawing.Point(380, 516);
            this.btnLetraY.Name = "btnLetraY";
            this.btnLetraY.Size = new System.Drawing.Size(57, 57);
            this.btnLetraY.TabIndex = 41;
            this.btnLetraY.TabStop = false;
            this.btnLetraY.Text = "Y";
            this.btnLetraY.UseMnemonic = false;
            this.btnLetraY.UseVisualStyleBackColor = true;
            // 
            // btnLetraT
            // 
            this.btnLetraT.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraT.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraT.FlatAppearance.BorderSize = 0;
            this.btnLetraT.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraT.Location = new System.Drawing.Point(320, 516);
            this.btnLetraT.Name = "btnLetraT";
            this.btnLetraT.Size = new System.Drawing.Size(57, 57);
            this.btnLetraT.TabIndex = 40;
            this.btnLetraT.TabStop = false;
            this.btnLetraT.Text = "T";
            this.btnLetraT.UseMnemonic = false;
            this.btnLetraT.UseVisualStyleBackColor = false;
            // 
            // btnLetraR
            // 
            this.btnLetraR.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraR.FlatAppearance.BorderSize = 0;
            this.btnLetraR.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraR.Location = new System.Drawing.Point(259, 516);
            this.btnLetraR.Name = "btnLetraR";
            this.btnLetraR.Size = new System.Drawing.Size(57, 57);
            this.btnLetraR.TabIndex = 39;
            this.btnLetraR.TabStop = false;
            this.btnLetraR.Text = "R";
            this.btnLetraR.UseMnemonic = false;
            this.btnLetraR.UseVisualStyleBackColor = true;
            // 
            // btnLetraE
            // 
            this.btnLetraE.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraE.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraE.FlatAppearance.BorderSize = 0;
            this.btnLetraE.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraE.Location = new System.Drawing.Point(198, 516);
            this.btnLetraE.Name = "btnLetraE";
            this.btnLetraE.Size = new System.Drawing.Size(57, 57);
            this.btnLetraE.TabIndex = 38;
            this.btnLetraE.TabStop = false;
            this.btnLetraE.Text = "E";
            this.btnLetraE.UseMnemonic = false;
            this.btnLetraE.UseVisualStyleBackColor = false;
            // 
            // btnLetraW
            // 
            this.btnLetraW.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraW.BackColor = System.Drawing.SystemColors.Control;
            this.btnLetraW.FlatAppearance.BorderSize = 0;
            this.btnLetraW.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraW.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraW.Location = new System.Drawing.Point(138, 516);
            this.btnLetraW.Name = "btnLetraW";
            this.btnLetraW.Size = new System.Drawing.Size(57, 57);
            this.btnLetraW.TabIndex = 37;
            this.btnLetraW.TabStop = false;
            this.btnLetraW.Text = "W";
            this.btnLetraW.UseMnemonic = false;
            this.btnLetraW.UseVisualStyleBackColor = true;
            // 
            // btnLetraQ
            // 
            this.btnLetraQ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraQ.BackColor = System.Drawing.Color.MintCream;
            this.btnLetraQ.FlatAppearance.BorderSize = 0;
            this.btnLetraQ.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraQ.Location = new System.Drawing.Point(79, 516);
            this.btnLetraQ.Name = "btnLetraQ";
            this.btnLetraQ.Size = new System.Drawing.Size(57, 57);
            this.btnLetraQ.TabIndex = 36;
            this.btnLetraQ.TabStop = false;
            this.btnLetraQ.Text = "Q";
            this.btnLetraQ.UseMnemonic = false;
            this.btnLetraQ.UseVisualStyleBackColor = false;
            // 
            // btnLetraÑ
            // 
            this.btnLetraÑ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraÑ.FlatAppearance.BorderSize = 0;
            this.btnLetraÑ.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraÑ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraÑ.Location = new System.Drawing.Point(623, 577);
            this.btnLetraÑ.Name = "btnLetraÑ";
            this.btnLetraÑ.Size = new System.Drawing.Size(57, 57);
            this.btnLetraÑ.TabIndex = 55;
            this.btnLetraÑ.TabStop = false;
            this.btnLetraÑ.Text = "Ñ";
            this.btnLetraÑ.UseMnemonic = false;
            this.btnLetraÑ.UseVisualStyleBackColor = false;
            // 
            // btnLetraL
            // 
            this.btnLetraL.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraL.FlatAppearance.BorderSize = 0;
            this.btnLetraL.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraL.Location = new System.Drawing.Point(564, 577);
            this.btnLetraL.Name = "btnLetraL";
            this.btnLetraL.Size = new System.Drawing.Size(57, 57);
            this.btnLetraL.TabIndex = 54;
            this.btnLetraL.TabStop = false;
            this.btnLetraL.Text = "L";
            this.btnLetraL.UseMnemonic = false;
            this.btnLetraL.UseVisualStyleBackColor = false;
            // 
            // btnLetraK
            // 
            this.btnLetraK.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraK.FlatAppearance.BorderSize = 0;
            this.btnLetraK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraK.Location = new System.Drawing.Point(503, 577);
            this.btnLetraK.Name = "btnLetraK";
            this.btnLetraK.Size = new System.Drawing.Size(57, 57);
            this.btnLetraK.TabIndex = 53;
            this.btnLetraK.TabStop = false;
            this.btnLetraK.Text = "K";
            this.btnLetraK.UseMnemonic = false;
            this.btnLetraK.UseVisualStyleBackColor = true;
            // 
            // btnLetraJ
            // 
            this.btnLetraJ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraJ.FlatAppearance.BorderSize = 0;
            this.btnLetraJ.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraJ.Location = new System.Drawing.Point(442, 577);
            this.btnLetraJ.Name = "btnLetraJ";
            this.btnLetraJ.Size = new System.Drawing.Size(57, 57);
            this.btnLetraJ.TabIndex = 52;
            this.btnLetraJ.TabStop = false;
            this.btnLetraJ.Text = "J";
            this.btnLetraJ.UseMnemonic = false;
            this.btnLetraJ.UseVisualStyleBackColor = false;
            // 
            // btnLetraH
            // 
            this.btnLetraH.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraH.FlatAppearance.BorderSize = 0;
            this.btnLetraH.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraH.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraH.Location = new System.Drawing.Point(381, 577);
            this.btnLetraH.Name = "btnLetraH";
            this.btnLetraH.Size = new System.Drawing.Size(57, 57);
            this.btnLetraH.TabIndex = 51;
            this.btnLetraH.TabStop = false;
            this.btnLetraH.Text = "H";
            this.btnLetraH.UseMnemonic = false;
            this.btnLetraH.UseVisualStyleBackColor = true;
            // 
            // btnLetraG
            // 
            this.btnLetraG.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraG.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraG.FlatAppearance.BorderSize = 0;
            this.btnLetraG.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraG.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraG.Location = new System.Drawing.Point(321, 577);
            this.btnLetraG.Name = "btnLetraG";
            this.btnLetraG.Size = new System.Drawing.Size(57, 57);
            this.btnLetraG.TabIndex = 50;
            this.btnLetraG.TabStop = false;
            this.btnLetraG.Text = "G";
            this.btnLetraG.UseMnemonic = false;
            this.btnLetraG.UseVisualStyleBackColor = false;
            // 
            // btnLetraF
            // 
            this.btnLetraF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraF.FlatAppearance.BorderSize = 0;
            this.btnLetraF.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraF.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraF.Location = new System.Drawing.Point(260, 577);
            this.btnLetraF.Name = "btnLetraF";
            this.btnLetraF.Size = new System.Drawing.Size(57, 57);
            this.btnLetraF.TabIndex = 49;
            this.btnLetraF.TabStop = false;
            this.btnLetraF.Text = "F";
            this.btnLetraF.UseMnemonic = false;
            this.btnLetraF.UseVisualStyleBackColor = true;
            // 
            // btnLetraD
            // 
            this.btnLetraD.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraD.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraD.FlatAppearance.BorderSize = 0;
            this.btnLetraD.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraD.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraD.Location = new System.Drawing.Point(199, 577);
            this.btnLetraD.Name = "btnLetraD";
            this.btnLetraD.Size = new System.Drawing.Size(57, 57);
            this.btnLetraD.TabIndex = 48;
            this.btnLetraD.TabStop = false;
            this.btnLetraD.Text = "D";
            this.btnLetraD.UseMnemonic = false;
            this.btnLetraD.UseVisualStyleBackColor = false;
            // 
            // btnLetraS
            // 
            this.btnLetraS.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraS.BackColor = System.Drawing.SystemColors.Control;
            this.btnLetraS.FlatAppearance.BorderSize = 0;
            this.btnLetraS.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraS.Location = new System.Drawing.Point(139, 577);
            this.btnLetraS.Name = "btnLetraS";
            this.btnLetraS.Size = new System.Drawing.Size(57, 57);
            this.btnLetraS.TabIndex = 47;
            this.btnLetraS.TabStop = false;
            this.btnLetraS.Text = "S";
            this.btnLetraS.UseMnemonic = false;
            this.btnLetraS.UseVisualStyleBackColor = true;
            // 
            // btnLetraA
            // 
            this.btnLetraA.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraA.BackColor = System.Drawing.Color.MintCream;
            this.btnLetraA.FlatAppearance.BorderSize = 0;
            this.btnLetraA.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraA.Location = new System.Drawing.Point(80, 577);
            this.btnLetraA.Name = "btnLetraA";
            this.btnLetraA.Size = new System.Drawing.Size(57, 57);
            this.btnLetraA.TabIndex = 46;
            this.btnLetraA.TabStop = false;
            this.btnLetraA.Text = "A";
            this.btnLetraA.UseMnemonic = false;
            this.btnLetraA.UseVisualStyleBackColor = false;
            // 
            // btnLetraPunto
            // 
            this.btnLetraPunto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraPunto.FlatAppearance.BorderSize = 0;
            this.btnLetraPunto.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraPunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraPunto.Location = new System.Drawing.Point(565, 637);
            this.btnLetraPunto.Name = "btnLetraPunto";
            this.btnLetraPunto.Size = new System.Drawing.Size(57, 57);
            this.btnLetraPunto.TabIndex = 64;
            this.btnLetraPunto.TabStop = false;
            this.btnLetraPunto.Text = ".";
            this.btnLetraPunto.UseMnemonic = false;
            this.btnLetraPunto.UseVisualStyleBackColor = false;
            // 
            // btnLetraM
            // 
            this.btnLetraM.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraM.FlatAppearance.BorderSize = 0;
            this.btnLetraM.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraM.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraM.Location = new System.Drawing.Point(504, 637);
            this.btnLetraM.Name = "btnLetraM";
            this.btnLetraM.Size = new System.Drawing.Size(57, 57);
            this.btnLetraM.TabIndex = 63;
            this.btnLetraM.TabStop = false;
            this.btnLetraM.Text = "M";
            this.btnLetraM.UseMnemonic = false;
            this.btnLetraM.UseVisualStyleBackColor = true;
            // 
            // btnLetraN
            // 
            this.btnLetraN.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraN.FlatAppearance.BorderSize = 0;
            this.btnLetraN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraN.Location = new System.Drawing.Point(443, 637);
            this.btnLetraN.Name = "btnLetraN";
            this.btnLetraN.Size = new System.Drawing.Size(57, 57);
            this.btnLetraN.TabIndex = 62;
            this.btnLetraN.TabStop = false;
            this.btnLetraN.Text = "N";
            this.btnLetraN.UseMnemonic = false;
            this.btnLetraN.UseVisualStyleBackColor = false;
            // 
            // btnLetraB
            // 
            this.btnLetraB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraB.FlatAppearance.BorderSize = 0;
            this.btnLetraB.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraB.Location = new System.Drawing.Point(382, 637);
            this.btnLetraB.Name = "btnLetraB";
            this.btnLetraB.Size = new System.Drawing.Size(57, 57);
            this.btnLetraB.TabIndex = 61;
            this.btnLetraB.TabStop = false;
            this.btnLetraB.Text = "B";
            this.btnLetraB.UseMnemonic = false;
            this.btnLetraB.UseVisualStyleBackColor = true;
            // 
            // btnLetraV
            // 
            this.btnLetraV.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraV.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraV.FlatAppearance.BorderSize = 0;
            this.btnLetraV.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraV.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraV.Location = new System.Drawing.Point(322, 637);
            this.btnLetraV.Name = "btnLetraV";
            this.btnLetraV.Size = new System.Drawing.Size(57, 57);
            this.btnLetraV.TabIndex = 60;
            this.btnLetraV.TabStop = false;
            this.btnLetraV.Text = "V";
            this.btnLetraV.UseMnemonic = false;
            this.btnLetraV.UseVisualStyleBackColor = false;
            // 
            // btnLetraC
            // 
            this.btnLetraC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraC.FlatAppearance.BorderSize = 0;
            this.btnLetraC.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraC.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraC.Location = new System.Drawing.Point(261, 637);
            this.btnLetraC.Name = "btnLetraC";
            this.btnLetraC.Size = new System.Drawing.Size(57, 57);
            this.btnLetraC.TabIndex = 59;
            this.btnLetraC.TabStop = false;
            this.btnLetraC.Text = "C";
            this.btnLetraC.UseMnemonic = false;
            this.btnLetraC.UseVisualStyleBackColor = true;
            // 
            // btnLetraX
            // 
            this.btnLetraX.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraX.BackColor = System.Drawing.Color.Transparent;
            this.btnLetraX.FlatAppearance.BorderSize = 0;
            this.btnLetraX.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraX.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraX.Location = new System.Drawing.Point(200, 637);
            this.btnLetraX.Name = "btnLetraX";
            this.btnLetraX.Size = new System.Drawing.Size(57, 57);
            this.btnLetraX.TabIndex = 58;
            this.btnLetraX.TabStop = false;
            this.btnLetraX.Text = "X";
            this.btnLetraX.UseMnemonic = false;
            this.btnLetraX.UseVisualStyleBackColor = false;
            // 
            // btnLetraZ
            // 
            this.btnLetraZ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraZ.BackColor = System.Drawing.SystemColors.Control;
            this.btnLetraZ.FlatAppearance.BorderSize = 0;
            this.btnLetraZ.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraZ.Location = new System.Drawing.Point(140, 637);
            this.btnLetraZ.Name = "btnLetraZ";
            this.btnLetraZ.Size = new System.Drawing.Size(57, 57);
            this.btnLetraZ.TabIndex = 57;
            this.btnLetraZ.TabStop = false;
            this.btnLetraZ.Text = "Z";
            this.btnLetraZ.UseMnemonic = false;
            this.btnLetraZ.UseVisualStyleBackColor = true;
            // 
            // btnLetraArroba
            // 
            this.btnLetraArroba.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLetraArroba.BackColor = System.Drawing.Color.MintCream;
            this.btnLetraArroba.FlatAppearance.BorderSize = 0;
            this.btnLetraArroba.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLetraArroba.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLetraArroba.Location = new System.Drawing.Point(81, 637);
            this.btnLetraArroba.Name = "btnLetraArroba";
            this.btnLetraArroba.Size = new System.Drawing.Size(57, 57);
            this.btnLetraArroba.TabIndex = 56;
            this.btnLetraArroba.TabStop = false;
            this.btnLetraArroba.Text = "@";
            this.btnLetraArroba.UseMnemonic = false;
            this.btnLetraArroba.UseVisualStyleBackColor = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCancelar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelar.Location = new System.Drawing.Point(289, 731);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(182, 51);
            this.btnCancelar.TabIndex = 16;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGuardar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardar.Location = new System.Drawing.Point(476, 731);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(182, 51);
            this.btnGuardar.TabIndex = 15;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(532, 798);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(224, 24);
            this.lblVersion.TabIndex = 15;
            this.lblVersion.Text = "VERSIÓN 1.0.0.0";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReiniciar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnReiniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReiniciar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReiniciar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReiniciar.Location = new System.Drawing.Point(101, 731);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(182, 51);
            this.btnReiniciar.TabIndex = 65;
            this.btnReiniciar.Text = "Reiniciar";
            this.btnReiniciar.UseVisualStyleBackColor = false;
            // 
            // FrmConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(768, 831);
            this.ControlBox = false;
            this.Controls.Add(this.btnReiniciar);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnLetraPunto);
            this.Controls.Add(this.btnLetraM);
            this.Controls.Add(this.btnLetraN);
            this.Controls.Add(this.btnLetraB);
            this.Controls.Add(this.btnLetraV);
            this.Controls.Add(this.btnLetraC);
            this.Controls.Add(this.btnLetraX);
            this.Controls.Add(this.btnLetraZ);
            this.Controls.Add(this.btnLetraArroba);
            this.Controls.Add(this.btnLetraÑ);
            this.Controls.Add(this.btnLetraL);
            this.Controls.Add(this.btnLetraK);
            this.Controls.Add(this.btnLetraJ);
            this.Controls.Add(this.btnLetraH);
            this.Controls.Add(this.btnLetraG);
            this.Controls.Add(this.btnLetraF);
            this.Controls.Add(this.btnLetraD);
            this.Controls.Add(this.btnLetraS);
            this.Controls.Add(this.btnLetraA);
            this.Controls.Add(this.btnLetraP);
            this.Controls.Add(this.btnLetraO);
            this.Controls.Add(this.btnLetraI);
            this.Controls.Add(this.btnLetraU);
            this.Controls.Add(this.btnLetraY);
            this.Controls.Add(this.btnLetraT);
            this.Controls.Add(this.btnLetraR);
            this.Controls.Add(this.btnLetraE);
            this.Controls.Add(this.btnLetraW);
            this.Controls.Add(this.btnLetraQ);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnNumCero);
            this.Controls.Add(this.btnNumNueve);
            this.Controls.Add(this.btnNumOcho);
            this.Controls.Add(this.btnNumSiete);
            this.Controls.Add(this.btnNumSeis);
            this.Controls.Add(this.btnNumCinco);
            this.Controls.Add(this.btnNumCuatro);
            this.Controls.Add(this.btnNumTres);
            this.Controls.Add(this.btnNumDos);
            this.Controls.Add(this.btnNumUno);
            this.Controls.Add(this.GBCC);
            this.Controls.Add(this.GBST);
            this.Controls.Add(this.GBConfig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmConfiguracion";
            this.Text = "FrmConfiguracion";
            this.Load += new System.EventHandler(this.FrmConfiguracion_Load);
            this.GBConfig.ResumeLayout(false);
            this.GBConfig.PerformLayout();
            this.GBST.ResumeLayout(false);
            this.GBST.PerformLayout();
            this.GBCC.ResumeLayout(false);
            this.GBCC.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBConfig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GBST;
        private System.Windows.Forms.GroupBox GBCC;
        private System.Windows.Forms.ComboBox cbTienda;
        private System.Windows.Forms.ComboBox cbCompañia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Button btnBorrar;
        internal System.Windows.Forms.Button btnNumCero;
        internal System.Windows.Forms.Button btnNumNueve;
        internal System.Windows.Forms.Button btnNumOcho;
        internal System.Windows.Forms.Button btnNumSiete;
        internal System.Windows.Forms.Button btnNumSeis;
        internal System.Windows.Forms.Button btnNumCinco;
        internal System.Windows.Forms.Button btnNumCuatro;
        internal System.Windows.Forms.Button btnNumTres;
        internal System.Windows.Forms.Button btnNumDos;
        internal System.Windows.Forms.Button btnNumUno;
        internal System.Windows.Forms.Button btnLetraP;
        internal System.Windows.Forms.Button btnLetraO;
        internal System.Windows.Forms.Button btnLetraI;
        internal System.Windows.Forms.Button btnLetraU;
        internal System.Windows.Forms.Button btnLetraY;
        internal System.Windows.Forms.Button btnLetraT;
        internal System.Windows.Forms.Button btnLetraR;
        internal System.Windows.Forms.Button btnLetraE;
        internal System.Windows.Forms.Button btnLetraW;
        internal System.Windows.Forms.Button btnLetraQ;
        internal System.Windows.Forms.Button btnLetraÑ;
        internal System.Windows.Forms.Button btnLetraL;
        internal System.Windows.Forms.Button btnLetraK;
        internal System.Windows.Forms.Button btnLetraJ;
        internal System.Windows.Forms.Button btnLetraH;
        internal System.Windows.Forms.Button btnLetraG;
        internal System.Windows.Forms.Button btnLetraF;
        internal System.Windows.Forms.Button btnLetraD;
        internal System.Windows.Forms.Button btnLetraS;
        internal System.Windows.Forms.Button btnLetraA;
        internal System.Windows.Forms.Button btnLetraPunto;
        internal System.Windows.Forms.Button btnLetraM;
        internal System.Windows.Forms.Button btnLetraN;
        internal System.Windows.Forms.Button btnLetraB;
        internal System.Windows.Forms.Button btnLetraV;
        internal System.Windows.Forms.Button btnLetraC;
        internal System.Windows.Forms.Button btnLetraX;
        internal System.Windows.Forms.Button btnLetraZ;
        internal System.Windows.Forms.Button btnLetraArroba;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox TBUbiTienda;
        private System.Windows.Forms.TextBox TBEmailST;
        private System.Windows.Forms.TextBox TBTelfST;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.TextBox TBPuerto;
        private System.Windows.Forms.TextBox TBEmail;
        private System.Windows.Forms.TextBox TBServidor;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button btnReiniciar;
    }
}