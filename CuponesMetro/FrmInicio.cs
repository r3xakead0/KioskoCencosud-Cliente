﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using COMMON = Cupones.Common;

namespace Cupones.Winform
{

    public partial class FrmInicio : Form
    {

        private enum DocumentoIdentidad
        {
            NA = 0,
            DNI = 1,
            OTRO = 2
        }

        private DocumentoIdentidad tipoDocumento = DocumentoIdentidad.NA;

        private int clickConfiguracion = 0; //Cuenta la cantidad de clic para ingresar a la configuracion

        #region Variables de Banners
        private int nroBanner = 0; // Nro del Banner
        private int tiempoBanner = 0; // Tiempo de visualizacion del Banner
        private List<BE.Banner> lstBeBanners; // Listado de Banners
        private BE.Banner beBanner; // Banner visible
        private System.Windows.Forms.Timer tmrBanners; //Temporizador de carusel de banners
        #endregion

        public FrmInicio()
        {
            InitializeComponent();

            try
            {
                this.lstBeBanners = new List<BE.Banner>();
                this.tmrBanners = new System.Windows.Forms.Timer();

                #region Obtener banners
                try
                {
                    var tsk = Task<List<BE.Banner>>.Factory.StartNew(() => ListarBanners());
                    tsk.Wait();
                    this.lstBeBanners = tsk.Result;
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }
                #endregion

                #region Configurar tiempo en animacion de banners
                this.tmrBanners.Interval = 1000; // Intervalos de 1 Segundo
                this.tmrBanners.Tick += new EventHandler(tmrBanners_Tick);
                this.tmrBanners.Enabled = false;
                this.tmrBanners.Stop();
                #endregion
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }

        }

        /// <summary>
        /// Temporalizador de carusel de banners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrBanners_Tick(object sender, EventArgs e)
        {
            try
            {
                this.tiempoBanner++;

                if (this.tiempoBanner >= this.beBanner.Tiempo)
                {
                    if (this.lstBeBanners.Count == this.nroBanner)
                        this.nroBanner = 0;
                    else
                        this.nroBanner++;

                    this.lstBeBanners[this.nroBanner].Contador = this.lstBeBanners[this.nroBanner].Contador + 1;
                    this.beBanner = this.lstBeBanners[this.nroBanner];

                    this.pbBanner.Image = this.beBanner.Imagen;

                    this.tiempoBanner = 0;
                }

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener los banners del kiosko
        /// </summary>
        /// <returns></returns>
        private List<BE.Banner> ListarBanners()
        {
            var lstBeBanners = new List<BE.Banner>();

            try
            {
                #region Obtener los Banners
                string tienda = General.Configuracion.Tienda;
                string hostname = General.Configuracion.Hostname;

                lstBeBanners = new BL.Banners().ListarBanners(tienda, hostname);
                #endregion

                string path = "";
                Image imgBanner = null;
                var common = new COMMON.Banner();

                if (lstBeBanners.Count > 0)
                {
                    for (int i = 0; i < lstBeBanners.Count; i++)
                    {
                        path = lstBeBanners[i].ImagenPath;
                        imgBanner = common.GetImage(path);

                        lstBeBanners[i].Imagen = imgBanner;
                    }
                }
                else
                {
                    string rutaBannerDefault = FrmContenedor.Instance.RutaBannerKiosko;

                    if (rutaBannerDefault.Trim().Length > 0)
                        path = rutaBannerDefault;

                    imgBanner = common.GetImage(path);

                    var beBanner = new BE.Banner();
                    beBanner.Nombre = "Default";
                    beBanner.ImagenPath = path;
                    beBanner.Imagen = imgBanner;
                    beBanner.Orden = 1;
                    beBanner.Prioridad = 1;
                    beBanner.Tiempo = 1;
                    lstBeBanners.Add(beBanner);
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }

            return lstBeBanners;
        }


        private void FrmInicio_Load(object sender, EventArgs e)
        {
            try
            {

                this.lstBeBanners[0].Contador = 1;
                this.beBanner = this.lstBeBanners[0];
                this.pbBanner.Image = this.beBanner.Imagen;

                this.Limpiar();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Inicia los controles del formulario
        /// </summary>
        public void Limpiar()
        {
            try
            {
                this.tipoDocumento = DocumentoIdentidad.DNI;

                this.btnDNI.Image = Cupones.Winform.Properties.Resources._01_btn_dni_hover;
                this.btnOtroDoc.Image = Cupones.Winform.Properties.Resources._01_btn_otros;

                this.lblCodigo.Visible = false;
                this.btnDisplay.Text = string.Empty;

                this.pbAlertBonus.Visible = false;

                if (this.lstBeBanners.Count > 1)
                    this.tmrBanners.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private delegate bool CallValidarDocumento(string nroDoc, out bool esCumpleanhos, out DateTime fecha);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nroDoc"></param>
        /// <param name="esCumpleanhos"></param>
        /// <returns></returns>
        private bool ValidarDocumento(string nroDoc, out bool esCumpleanhos, out DateTime fechaHoraConsulta)
        {
            bool valido = false;
            bool cumpleanhos = false;
            bool cupon = false;
            DateTime fechahora;
            try
            {
                string ambito = General.Ambito;

                valido = new BL.Clientes().ValidarCliente(nroDoc, ambito, out cumpleanhos, out cupon, out fechahora);

                esCumpleanhos = cumpleanhos;
                fechaHoraConsulta = fechahora;

                return valido;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
 

        /// <summary>
        /// Evento del teclado digital numerico (0-9)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTecladoNumerico_Click(object sender, EventArgs e)
        {
            try 
	        {

                //Longitud del Documento
                int lngNumDoc = 0;
                int tipDoc = 0;
                if (this.tipoDocumento == DocumentoIdentidad.DNI)
                {
                    lngNumDoc = 8;
                    tipDoc = 1;
                }
                else if (this.tipoDocumento == DocumentoIdentidad.OTRO)
                {
                    lngNumDoc = 9;
                    tipDoc = 2;
                }
                else if (this.tipoDocumento == DocumentoIdentidad.NA)
                {
                    lngNumDoc = General.Configuracion.AplicacionClave.Length;
                    tipDoc = 0;
                }
                else
                {
                    return;
                }

                //Valor del boton numerico presionado
                Button boton = (Button)sender;
                string numero = boton.Tag.ToString();
                if (this.btnDisplay.Text.Length < lngNumDoc)
                    this.btnDisplay.Text += numero;
                
                //Valida el numero de documento
                if (this.btnDisplay.Text.Length == lngNumDoc)
                {
                    //Muestra el ultimo digito ingresado por medio segundo
                    this.btnDisplay.Refresh();
                    Thread.Sleep(500);

                    if (tipDoc == 1 || tipDoc == 2) //Solo clientes
                    {
                        FrmContenedor.Instance.Cargar();

                        //Asincrono
                        string nroDoc = this.btnDisplay.Text;
                        bool cumple = false;
                        DateTime fecha;

                        CallValidarDocumento method = ValidarDocumento;
                        IAsyncResult cookie = method.BeginInvoke(nroDoc, out cumple, out fecha, null, null);
                        bool valido = method.EndInvoke(out cumple, out fecha, cookie);

                        if (valido)
                        {
                            FrmContenedor.Instance.NroDocumento = nroDoc;
                            FrmContenedor.Instance.TipoDocumento = tipDoc;

                            FrmContenedor.Instance.IniciarSesion(nroDoc, tipDoc, fecha);

                            if (cumple)
                            {
                                var cumpleanhos = FrmContenedor.Instance.Cumpleanho();
                                cumpleanhos.Inicia();
                            }
                            else
                            {
                                var menu = FrmContenedor.Instance.MenuPrincipal();
                                menu.Inicia();
                            }

                            this.tmrBanners.Stop();
                        }
                        else
                        {
                            var principal = FrmContenedor.Instance;
                            principal.Inicio();
                            
                            this.btnDisplay.Text = string.Empty;

                            Image imgMensaje = Cupones.Winform.Properties.Resources._03_popup_usuario;
                            FrmContenedor.Instance.MensajeImagen(imgMensaje);
                        }
                    }
                    else //Clave de Aplicativo
                    {
                        string claveIngresada = this.btnDisplay.Text;
                        string claveAplicacion = General.Configuracion.AplicacionClave;

                        if (claveAplicacion.Equals(claveIngresada) == true)
                        {
                            var config = FrmContenedor.Instance.Configuracion();
                            config.CargarConfiguracion();

                            this.tmrBanners.Stop();
                        }
                        else
                        {
                            this.tipoDocumento = DocumentoIdentidad.DNI;

                            this.btnDNI.Image = Cupones.Winform.Properties.Resources._01_btn_dni_hover;
                            this.btnOtroDoc.Image = Cupones.Winform.Properties.Resources._01_btn_otros;

                            this.btnDisplay.Text = string.Empty;
                            this.lblCodigo.Visible = false;

                            this.pbAlertBonus.Visible = false;
                        }
                    }
                                        
                }

	        }
	        catch (Exception ex)
	        {
                General.RegistrarError(ex);
	        }
        }

        /// <summary>
        /// Evento del teclado digital para borrar última posición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTecladoBorrar_Click(object sender, EventArgs e)
        {
            try 
	        {
                string numero = this.btnDisplay.Text.Trim();
                if (numero.Length > 0)
                {
                    numero = numero.Remove(numero.Length - 1);
                    this.btnDisplay.Text = numero;
                }
	        }
	        catch (Exception ex)
	        {
                General.RegistrarError(ex);
	        }
        }


        /// <summary>
        /// Evento click para abrir terminos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LLTerminos_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                var frmTerminos = FrmTerminos.Instance;
                frmTerminos.ShowDialog();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento click para abrir politicas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LLPoliticas_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                var frmPolitica = FrmPoliticas.Instance;
                frmPolitica.ShowDialog();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
            
        }


        /// <summary>
        /// Evento clic para seleccion de tipo de documento DNI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDNI_Click(object sender, EventArgs e)
        {
            try
            {
                this.tipoDocumento = DocumentoIdentidad.DNI;

                this.btnDNI.Image = Cupones.Winform.Properties.Resources._01_btn_dni_hover;
                this.btnOtroDoc.Image = Cupones.Winform.Properties.Resources._01_btn_otros;

                this.btnDisplay.Text = string.Empty;
                this.lblCodigo.Visible = false;

                this.pbAlertBonus.Visible = false;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Evento clic para seleccion de tipo de documento OTRO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOtroDoc_Click(object sender, EventArgs e)
        {
            try
            {
                this.tipoDocumento = DocumentoIdentidad.OTRO;

                this.btnDNI.Image = Cupones.Winform.Properties.Resources._01_btn_dni;
                this.btnOtroDoc.Image = Cupones.Winform.Properties.Resources._01_btn_otros_hover;

                this.btnDisplay.Text = string.Empty;
                this.lblCodigo.Visible = false;

                this.pbAlertBonus.Visible = true;
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void pnHabilitar_Click(object sender, EventArgs e)
        {
            try
            {
                this.clickConfiguracion++;
                if (this.clickConfiguracion == 5) //Clicks 5 veces 
                {
                    this.clickConfiguracion = 0;

                    this.tipoDocumento = DocumentoIdentidad.NA;

                    this.btnDNI.Image = Cupones.Winform.Properties.Resources._01_btn_dni;
                    this.btnOtroDoc.Image = Cupones.Winform.Properties.Resources._01_btn_otros;

                    this.btnDisplay.Text = string.Empty;
                    this.lblCodigo.Visible = true;

                    this.pbAlertBonus.Visible = false;
                }
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        
    }
}
