﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Diagnostics;
using System.Net.Sockets;
using Newtonsoft.Json;
using BE = Cupones.BusinessEntity;
using BL = Cupones.BusinessLogic;
using System.Drawing;

namespace Cupones.Winform
{
    public partial class FrmContenedor : Form
    {

        #region Singleton pattern

        private static FrmContenedor instance = null;

        public static FrmContenedor Instance
        {
            get
            {
                if (instance == null)
                    instance = new FrmContenedor();
                return instance;
            }
        }

        #endregion

        public BE.Sesion Sesion = null;

        private Timer tmrValidacion = null;
        private int tiempoValidacion = 5; //Segundo

        public string NroDocumento = ""; //Numero de Documento del Cliente
        public int TipoDocumento = 1;  //Tipo del documento del Cliente (1 = DNI | 2 = OTRO)

        public string RutaBannerKiosko = ""; //Ruta de la imagen del banner por defecto del kiosko

        public FrmContenedor()
        {
            InitializeComponent();

            try
            {

                #region Configurar Validaciones Periodicas

                this.tmrValidacion = new Timer();
                this.tmrValidacion.Interval = tiempoValidacion * 1000;
                this.tmrValidacion.Tick += new EventHandler(this.tmrValidacion_tick);
                this.tmrValidacion.Enabled = false;
                this.tmrValidacion.Stop();

                #endregion

                #region Servidor de Solicitudes

                var tskServidor = new Task(Servidor);
                tskServidor.Start();

                #endregion

                #region Control de Errores

                Application.ThreadException += (sender, args) =>
                {
                    General.RegistrarError(args.Exception);
                };
                AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                {
                    General.RegistrarError((args.ExceptionObject as Exception));
                };

                #endregion

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        private void FrmContenedor_Load(object sender, EventArgs e)
        {
            try
            {

                //Muestra la carga
                this.Cargar();

                string hostname = Dns.GetHostName().ToUpper();
                string tienda = this.ObtenerTienda(hostname).ToUpper();

#if DEBUG
                tienda = "H014";
                hostname = "M214901KS001";
                //tienda = "H003";
                //hostname = "M203901KS001";
                //tienda = "OFIM";
                //hostname = "R200603RR001";
                //tienda = "T104";
                //hostname = "W104901KS001";
                //tienda = "H005";
                //hostname = "M205901KS001";
#endif

                #region Validar la Conexion de Red

                while (this.ValidarConexion() == false)
                {
                    this.Alerta("NO EXISTE CONEXION A LA RED. POR FAVOR, COMUNICARSE CON EL PERSONAL DE TIENDA.");
                }

                #endregion

                #region Descargar Archivos al Cache

#if !DEBUG
                var tasksImagenes = new[]
                    {
                        Task.Factory.StartNew(() => this.DescargarImagenesMisCupones()),
                        Task.Factory.StartNew(() => this.DescargarImagenesSuperCupones()),
                        Task.Factory.StartNew(() => this.DescargarImagenesCuponesMarcas()),
                        Task.Factory.StartNew(() => this.DescargarImagenesCuponesLugar()),
                        Task.Factory.StartNew(() => this.DescargarImagenesCuponesBonus()),
                        Task.Factory.StartNew(() => this.DescargarImagenesCuponesClub()),
                        Task.Factory.StartNew(() => this.DescargarImagenesCuponesComerciales()),
                        Task.Factory.StartNew(() => this.DescargarImagenesBanners()),
                        Task.Factory.StartNew(() => this.DescargarTextosLegales())
                    };

                try
                {
                    Task.WaitAll(tasksImagenes, 300000); //Espera un maximo de 5 minutos
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }
#endif

                #endregion

                #region Validar Perfil de Tienda

                var tskObtenerPerfilTienda = Task.Factory.StartNew(() => this.ObtenerPerfilTienda(tienda));
                try
                {
                    tskObtenerPerfilTienda.Wait();
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }

                BE.PerfilTienda bePerfilTienda = tskObtenerPerfilTienda.Result;

                while (bePerfilTienda == null)
                {
                    this.BloqueoTotal("LA TIENDA NO ESTA CONFIGURADA. \n"
                                + "POR FAVOR, COMUNICARSE CON EL \n"
                                + "PERSONAL DE TIENDA.");

                    tskObtenerPerfilTienda = Task.Factory.StartNew(() => this.ObtenerPerfilTienda(tienda));
                    try
                    {
                        tskObtenerPerfilTienda.Wait();
                    }
                    catch (AggregateException taskEx)
                    {
                        throw taskEx.Flatten().InnerException;
                    }

                    bePerfilTienda = tskObtenerPerfilTienda.Result;
                }

                #endregion

                #region Validar Perfil de Kiosko

                var tskObtenerPerfilKiosko = Task.Factory.StartNew(() => this.ObtenerPerfilKiosko(tienda, hostname));
                try
                {
                    tskObtenerPerfilKiosko.Wait();
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }

                BE.PerfilKiosko bePerfilKiosko = tskObtenerPerfilKiosko.Result;

                while (bePerfilKiosko == null)
                {
                    this.BloqueoSimple("EL KIOSKO NO ESTA CONFIGURADO. \n"
                                + "POR FAVOR, COMUNICARSE CON EL \n"
                                + "PERSONAL DE TIENDA.");

                    tskObtenerPerfilKiosko = Task.Factory.StartNew(() => this.ObtenerPerfilKiosko(tienda, hostname));
                    try
                    {
                        tskObtenerPerfilKiosko.Wait();
                    }
                    catch (AggregateException taskEx)
                    {
                        throw taskEx.Flatten().InnerException;
                    }

                    bePerfilKiosko = tskObtenerPerfilKiosko.Result;
                }

                this.RutaBannerKiosko = bePerfilKiosko.RutaImagenBannerTienda;

                #endregion

                #region Validar Configuracion de Kiosko

                var tskObtenerConfiguracion = Task.Factory.StartNew(() => this.ObtenerConfiguracion(tienda, hostname));
                try
                {
                    tskObtenerConfiguracion.Wait();
                }
                catch (AggregateException taskEx)
                {
                    throw taskEx.Flatten().InnerException;
                }

                BE.Configuracion beConfiguracion = tskObtenerConfiguracion.Result;

                while (beConfiguracion == null)
                {
                    this.BloqueoSimple("EL EQUIPO NO ESTA CONFIGURADO. \n"
                                + "POR FAVOR, COMUNICARSE CON EL \n"
                                + "PERSONAL DE TIENDA.");

                    tskObtenerConfiguracion = Task.Factory.StartNew(() => this.ObtenerConfiguracion(tienda, hostname));
                    try
                    {
                        tskObtenerConfiguracion.Wait();
                    }
                    catch (AggregateException taskEx)
                    {
                        throw taskEx.Flatten().InnerException;
                    }

                    beConfiguracion = tskObtenerConfiguracion.Result;
                }

                General.Configuracion = beConfiguracion;

                #endregion

                //Muestra el inicio
                this.tmrValidacion.Enabled = true;
                this.tmrValidacion.Start();
                this.Inicio();

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Servidor de Solicitudes remotas
        /// </summary>
        private void Servidor()
        {
            try
            {

                string ip = General.GetLocalIPAddress();
                int portNumber = 9876;

                IPAddress localAdd = IPAddress.Parse(ip);
                var listener = new TcpListener(localAdd, portNumber);
                listener.Start();

                bool quit = false;
                while (!quit)
                {
                    using (TcpClient client = listener.AcceptTcpClient())
                    {
                        NetworkStream nwStream = null;

                        nwStream = client.GetStream();
                        byte[] buffer = new byte[client.ReceiveBufferSize];
                        int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
                        string textReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);

                        string textToSend = "";
                        byte[] bytesToSend;
                        switch (textReceived.ToUpper())
                        {

                            case "STATUS":

                                string version = General.GetVersion();
                                string tipdoc = "";
                                string numdoc = "";
                                if (this.Sesion != null)
                                {
                                    tipdoc = (this.Sesion.TipoDocumento == 1 ? "DNI" : "OTRO");
                                    numdoc = this.Sesion.NroDocumento;
                                }

                                var jsonObject = new
                                {
                                    Version = version,
                                    Type = tipdoc,
                                    Number = numdoc
                                };

                                string jsonText = JsonConvert.SerializeObject(jsonObject);

                                bytesToSend = Encoding.ASCII.GetBytes(jsonText);
                                nwStream = client.GetStream();
                                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                                break;

                            case "RESTART":

                                if (this.Sesion == null)
                                {
                                    textToSend = "Restoring";
                                    bytesToSend = Encoding.ASCII.GetBytes(textToSend);
                                    nwStream = client.GetStream();
                                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                                    Process.Start("shutdown", "/r /t 0");
                                }
                                else
                                {
                                    textToSend = "Busy";
                                    bytesToSend = Encoding.ASCII.GetBytes(textToSend);
                                    nwStream = client.GetStream();
                                    nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                                }
                                break;

                        }

                        client.Close();
                    }
                }

                listener.Stop();
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Registra el inicio de sesion del cliente
        /// </summary>
        /// <param name="nroDocumento">Nro del Documento del cliente</param>
        /// <param name="tipoDocumento">Id del tipo de documento (1=DNI | 2=BONUS)</param>
        /// <param name="nombreCompleto">Nombre completo del cliente</param>
        public void IniciarSesion(string nroDocumento, int tipoDocumento, DateTime fecha)
        {
            try
            {
                this.Sesion = new BE.Sesion(nroDocumento, tipoDocumento, fecha);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Registra el fin de sesion del cliente
        /// </summary>
        public void CerrarSesion()
        {
            try
            {
                if (this.Sesion != null)
                {
                    this.Sesion.Fin = DateTime.Now;

                    //Registrar la sesion
                    string tienda = General.Configuracion.Tienda;
                    string hostname = General.Configuracion.Hostname;
                    var inicio = this.Sesion.Inicio;
                    var fin = (DateTime)this.Sesion.Fin;
                    string cuponesImpresos = String.Join(",", this.Sesion.CuponesImpresos.ToArray());
                    string cuponesVistos = String.Join(",", this.Sesion.CuponesVistos.ToArray());
                    string bannersVistos = String.Join(",", this.Sesion.BannersVistos.ToArray());

                    try
                    {
                        var rpta = new BL.Sesion(tienda, hostname).RegistrarSesion(this.Sesion.Id,
                            inicio, fin,
                            this.Sesion.NroDocumento, this.Sesion.TipoDocumento,
                            this.Sesion.OpcionMisCupones, this.Sesion.OpcionMiSuperCupon, this.Sesion.OpcionMarcasSeleccionadas, 
                            this.Sesion.OpcionArbolDeCupones, this.Sesion.OpcionCuponesDelLugar, this.Sesion.OpcionRuletaDeCupones,
                            this.Sesion.OpcionMisPuntosBonus, this.Sesion.OpcionMisInvitaciones, this.Sesion.OpcionCuponesComerciales,
                            cuponesImpresos, cuponesVistos, bannersVistos);

                        if (rpta == true)
                        {
                            rpta = new BL.Banners().RegistrarVisualizacion(bannersVistos);
                        }
                    }
                    catch (AggregateException exTask)
                    {
                        throw exTask.Flatten();
                    }
                }

                this.Sesion = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Activar o Desactivar validaciones periodicas
        /// </summary>
        /// <param name="activar"></param>
        public void ValidacionesAutomaticas(bool activar)
        {
            try
            {
                this.tmrValidacion.Enabled = activar;
                if (activar)
                    this.tmrValidacion.Start();
                else
                    this.tmrValidacion.Stop();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void tmrValidacion_tick(object sender, EventArgs e)
        {
            try
            {

                #region Validar conexion a red
                while (this.ValidarConexion() == false)
                {
                    this.Alerta("NO EXISTE CONEXION A LA RED. POR FAVOR, COMUNICARSE CON EL PERSONAL DE TIENDA.");
                }
                #endregion

                #region Validar de operatividad de impresora
                if (this.impresoraOperativa == false)
                {

                    this.BloqueoSimple("LA IMPRESORA ESTA INOPERATIVA. \n"
                                    + "POR FAVOR, COMUNICARSE CON EL \n"
                                    + "PERSONAL DE TIENDA.");

                    impresoraOperativa = true;

                    #region Limpiar tareas de la Impresora

                    var sql = @"SELECT * FROM Win32_PrintJob";

                    var searchPrintJobs = new ManagementObjectSearcher(sql);

                    var prntJobCollection = searchPrintJobs.Get();

                    foreach (ManagementObject prntJob in prntJobCollection)
                    {
                        prntJob.Delete();
                        prntJob.InvokeMethod("CancelAllJobs", null);
                    }

                    #endregion

                }
                #endregion

            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener y Actualizar el archivo config XML
        /// </summary>
        /// <param name="hostname">Nombre de Hostname</param>
        /// <returns></returns>
        private string ObtenerTienda(string hostname)
        {
            string tienda = "";

            try
            {
                tienda = new BL.Configuraciones().ObtenerTienda(hostname);
                tienda = tienda.Trim();

                return tienda;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar imagenes de Mis Cupones del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesMisCupones()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderMisCupones(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Super Cupones del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesSuperCupones()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderSuperCupones(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Cupones de Marcas del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesCuponesMarcas()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderCuponesMarcas(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Cupones de Lugar del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesCuponesLugar()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderCuponesLugar(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Cupones Comerciales del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesCuponesComerciales()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderCuponesComerciales(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Cupones Bonus del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesCuponesBonus()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderCuponesBonus(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Cupones de Lugar del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesCuponesClub()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Coupon(compania).DownloadFolderCuponesClub(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar imagenes de Banners del Ftp a Cache 
        /// </summary>
        private void DescargarImagenesBanners()
        {
            try
            {
                new Common.Banner().DownloadFolder(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Descargar archivos legales del Ftp a Cache
        /// </summary>
        private void DescargarTextosLegales()
        {
            try
            {
                string compania = General.Ambito;
                new Common.Legal(compania).DownloadFolder(true);
            }
            catch (Exception ex)
            {
                General.RegistrarError(ex);
            }
        }

        /// <summary>
        /// Obtener el perfil de la tienda
        /// </summary>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <returns></returns>
        private BE.PerfilTienda ObtenerPerfilTienda(string tienda)
        {
            var perfilTienda = new BL.Perfiles().ObtenerPerfilTienda(tienda);
            return perfilTienda;
        }

        /// <summary>
        /// Obtener el perfil del kiosko
        /// </summary>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <param name="hostname">Nombre del kiosko</param>
        /// <returns></returns>
        private BE.PerfilKiosko ObtenerPerfilKiosko(string tienda, string hostname)
        {
            var bePerfilKiosko = new BL.Perfiles().ObtenerPerfilKiosko(tienda, hostname);
            return bePerfilKiosko;
        }

        /// <summary>
        /// Obtener la configuracion del kiosko
        /// </summary>
        /// <param name="tienda">Codigo de Tienda</param>
        /// <param name="hostname">Nombre del kiosko</param>
        /// <returns></returns>
        private BE.Configuracion ObtenerConfiguracion(string tienda, string hostname)
        {
            var beConfiguracion = new BL.Configuraciones().Obtener(tienda, hostname);
            return beConfiguracion;
        }

        /// <summary>
        /// Verifica si existe conexión de red
        /// </summary>
        /// <returns></returns>
        private bool ValidarConexion()
        {
            bool redDisponible = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            return redDisponible;
        }

        public bool impresoraOperativa = true;

        #region Control de Formularios

        private void PrepararFormulario(Form frm)
        {
            frm.TopLevel = false;
            frm.AutoScroll = false;
            frm.WindowState = FormWindowState.Maximized;
            frm.FormBorderStyle = FormBorderStyle.None;
        }

        public void Alerta(string mensaje)
        {
            var alerta = FrmMensaje.Instance;
            alerta.lblTitulo.Text = General.Ambito;
            alerta.lblMensaje.Text = mensaje;
            alerta.BringToFront();
            alerta.ShowDialog();
        }

        /// <summary>
        /// Bloquea la pantalla sin posibilidad de salir
        /// </summary>
        /// <param name="mensaje"></param>
        public void BloqueoTotal(string mensaje)
        {
            this.tmrValidacion.Enabled = false;
            this.tmrValidacion.Stop();

            var bloqueo = FrmBloqueoTotal.Instance;
            bloqueo.lblMensajeDescripcion.Text = mensaje;
            bloqueo.BringToFront();
            bloqueo.ShowDialog();

            this.tmrValidacion.Enabled = true;
            this.tmrValidacion.Start();
        }

        /// <summary>
        /// Bloquea la pantalla con posibilidad de salir 
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public void BloqueoSimple(string mensaje)
        {
            this.tmrValidacion.Enabled = false;
            this.tmrValidacion.Stop();

            var bloqueo = FrmBloqueoParcial.Instance;
            bloqueo.lblMensajeDescripcion.Text = mensaje;
            bloqueo.pnHabilitar.Enabled = true;
            bloqueo.BringToFront();
            bloqueo.ShowDialog();

            this.tmrValidacion.Enabled = true;
            this.tmrValidacion.Start();
        }

        /// <summary>
        /// Mensaje en la pantalla con posibilidad de salir 
        /// </summary>
        /// <param name="imagen"></param>
        /// <returns></returns>
        public void MensajeImagen(Image imagen)
        {
            this.tmrValidacion.Enabled = false;
            this.tmrValidacion.Stop();

            var mensaje = FrmMensajeImagen.Instance;
            mensaje.MostrarMensaje(imagen);
            mensaje.BringToFront();
            mensaje.ShowDialog();

            this.tmrValidacion.Enabled = true;
            this.tmrValidacion.Start();
        }

        public void Cargar(bool mensaje = false)
        {
            FrmCargar cargar = this.pnlContenedor.Controls.Find("FrmCargaFinal", true).FirstOrDefault() as FrmCargar;
            if (cargar == null)
            {
                cargar = new FrmCargar();
                this.PrepararFormulario(cargar);
                this.pnlContenedor.Controls.Add(cargar);

                cargar.Show();
            }

            cargar.lblMensaje.Visible = mensaje;

            cargar.Refresh();
            cargar.BringToFront();
        }

        public FrmInicio Inicio()
        {
            FrmInicio inicio = this.pnlContenedor.Controls.Find("FrmInicio", true).FirstOrDefault() as FrmInicio;
            if (inicio == null)
            {
                inicio = new FrmInicio();
                this.PrepararFormulario(inicio);
                this.pnlContenedor.Controls.Add(inicio);
                inicio.Show();
            }

            inicio.BringToFront();

            return inicio;
        }


        public FrmMenuPrincipal MenuPrincipal()
        {
            FrmMenuPrincipal menuPrincipal = this.pnlContenedor.Controls.Find("FrmMenuPrincipal", true).FirstOrDefault() as FrmMenuPrincipal;

            if (menuPrincipal == null)
            {
                menuPrincipal = new FrmMenuPrincipal();
                this.PrepararFormulario(menuPrincipal);
                this.pnlContenedor.Controls.Add(menuPrincipal);
                menuPrincipal.Show();
            }

            menuPrincipal.BringToFront();
            menuPrincipal.ActivarSesion(true);

            return menuPrincipal;
        }

        public FrmCumpleanhos Cumpleanho()
        {
            FrmCumpleanhos cumple = this.pnlContenedor.Controls.Find("FrmCumpleanhos", true).FirstOrDefault() as FrmCumpleanhos;
            if (cumple == null)
            {
                cumple = new FrmCumpleanhos();
                this.PrepararFormulario(cumple);
                this.pnlContenedor.Controls.Add(cumple);
                cumple.Show();
            }

            cumple.BringToFront();

            return cumple;
        }

        public FrmCupon Cupon()
        {
            FrmCupon cupon = this.pnlContenedor.Controls.Find("FrmCupon", true).FirstOrDefault() as FrmCupon;
            if (cupon == null)
            {
                cupon = new FrmCupon();
                this.PrepararFormulario(cupon);
                this.pnlContenedor.Controls.Add(cupon);
                cupon.Show();
            }

            cupon.BringToFront();

            return cupon;
        }

        public FrmExperiencia Experiencia()
        {
            FrmExperiencia expe = this.pnlContenedor.Controls.Find("FrmExperiencia", true).FirstOrDefault() as FrmExperiencia;

            if (expe == null)
            {
                expe = new FrmExperiencia();
                this.PrepararFormulario(expe);
                this.pnlContenedor.Controls.Add(expe);
                expe.Show();
            }

            expe.BringToFront();
            expe.ActivarSesion(true);

            return expe;
        }

        public FrmConfiguracion Configuracion()
        {

            FrmConfiguracion config = this.pnlContenedor.Controls.Find("FrmConfiguracion", true).FirstOrDefault() as FrmConfiguracion;

            if (config == null)
            {
                config = new FrmConfiguracion();
                this.PrepararFormulario(config);
                this.pnlContenedor.Controls.Add(config);
                config.Show();
            }

            config.BringToFront();

            return config;
        }

        #endregion

    }
}
