﻿namespace Cupones.Winform
{
    partial class FrmCargar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picCarga = new System.Windows.Forms.PictureBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picCarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // picCarga
            // 
            this.picCarga.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picCarga.BackColor = System.Drawing.Color.Transparent;
            this.picCarga.Image = global::Cupones.Winform.Properties.Resources._00_carga;
            this.picCarga.Location = new System.Drawing.Point(332, 466);
            this.picCarga.Name = "picCarga";
            this.picCarga.Size = new System.Drawing.Size(104, 115);
            this.picCarga.TabIndex = 14;
            this.picCarga.TabStop = false;
            // 
            // picLogo
            // 
            this.picLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.Image = global::Cupones.Winform.Properties.Resources._00_carga_texto;
            this.picLogo.Location = new System.Drawing.Point(189, 94);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(390, 366);
            this.picLogo.TabIndex = 13;
            this.picLogo.TabStop = false;
            this.picLogo.WaitOnLoad = true;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.SystemColors.Window;
            this.lblMensaje.Location = new System.Drawing.Point(32, 652);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(706, 87);
            this.lblMensaje.TabIndex = 15;
            this.lblMensaje.Text = "EVITE JALAR EL TICKET HASTA \r\nQUE TERMINE DE IMPRIMIR";
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMensaje.Visible = false;
            // 
            // FrmCargar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(768, 768);
            this.ControlBox = false;
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.picCarga);
            this.Controls.Add(this.picLogo);
            this.Cursor = System.Windows.Forms.Cursors.No;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCargar";
            this.Opacity = 0.8D;
            this.Text = "FrmCargaFinal";
            this.Load += new System.EventHandler(this.FrmCargar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picCarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picCarga;
        private System.Windows.Forms.PictureBox picLogo;
        public System.Windows.Forms.Label lblMensaje;
    }
}