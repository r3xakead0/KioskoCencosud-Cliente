﻿namespace Cupones.Winform
{
    partial class FrmPoliticas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCerrar = new System.Windows.Forms.Button();
            this.txtPoliticas = new Cupones.Winform.Controls.AdvRichTextBox();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Image = global::Cupones.Winform.Properties.Resources._08_btn_cerrar;
            this.btnCerrar.Location = new System.Drawing.Point(545, 12);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(69, 72);
            this.btnCerrar.TabIndex = 6;
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // txtPoliticas
            // 
            this.txtPoliticas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPoliticas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPoliticas.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPoliticas.ForeColor = System.Drawing.Color.Black;
            this.txtPoliticas.Location = new System.Drawing.Point(15, 96);
            this.txtPoliticas.Name = "txtPoliticas";
            this.txtPoliticas.SelectionAlignment = Cupones.Winform.Controls.TextAlign.Justify;
            this.txtPoliticas.Size = new System.Drawing.Size(600, 750);
            this.txtPoliticas.TabIndex = 10;
            this.txtPoliticas.Text = "";
            // 
            // FrmPoliticas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(626, 950);
            this.ControlBox = false;
            this.Controls.Add(this.txtPoliticas);
            this.Controls.Add(this.btnCerrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPoliticas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Politicas";
            this.Load += new System.EventHandler(this.FrmPoliticas_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCerrar;
        private Controls.AdvRichTextBox txtPoliticas;
    }
}