﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Text;
using System.Linq;
using log4net;
using System.Configuration;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;

namespace Cupones.Winform
{
    public static class Program
    {

        #region Log4net
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                var frmContenedor = FrmContenedor.Instance;
                EjecutarMotitorSecundario(frmContenedor);

                Application.Run(frmContenedor);
            }
            catch (Exception ex)
            {
                StringBuilder error = new StringBuilder();

                error.AppendLine("Message   : " + ex.Message.ToString());
                error.AppendLine("Trace     : " + ex.StackTrace.ToString());

                log.Error(error);
            }
        }

        /// <summary>
        /// Abre el aplicativo en una pantalla secundaria
        /// </summary>
        /// <param name="frm"></param>
        static void EjecutarMotitorSecundario(Form frm)
        {
            try
            {
                if (Screen.AllScreens.Length == 2) //Existe segundo monitor
                {
                    foreach (Screen screen in Screen.AllScreens)
                    {
                        if (screen.Primary == false)
                        {
                            frm.StartPosition = FormStartPosition.Manual;
                            frm.Location = screen.WorkingArea.Location;
                            frm.Size = new Size(screen.WorkingArea.Width, screen.WorkingArea.Height);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
