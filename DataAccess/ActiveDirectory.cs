﻿using System;
using System.DirectoryServices;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class ActiveDirectory : IAcceso
    {

        #region Construtores

        public ActiveDirectory()
        {

        }

        #endregion

        public bool Login(string domain, string user, string password)
        {
            bool authenticated = false;

            try
            {
                DirectoryEntry entry = new DirectoryEntry(domain, user, password);
                object nativeObject = entry.NativeObject;
                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                authenticated = false;
            }

            return authenticated;
        }


    }
}
