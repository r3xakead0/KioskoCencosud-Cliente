﻿using System;
using System.Collections.Generic;
using System.Configuration;
using RestSharp;
using Newtonsoft.Json;
using DE = Cupones.DataEntity;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Vridge : IDatos
    {

        private static string token = "";

        private string baseCredential = "";
        private string baseEndpoint = "";

        #region Construtores

        public Vridge()
        {
            try
            {
                if (token == "")
                {
                    string urlCredential = ConfigurationManager.AppSettings["UrlCredential"];
                    if (urlCredential == null || urlCredential.Trim().Length == 0)
                        this.baseCredential = @"http://cencosud.vridge.io:4000/oauth2/token"; //Produccion
                    else
                        this.baseCredential = urlCredential.Trim() + @"/oauth2/token";

                    var tokenJson = this.ObtenerToken();
                    token = tokenJson.token_type + " " + tokenJson.access_token;
                }

                string urlVridge = ConfigurationManager.AppSettings["UrlVridge"];
                if (urlVridge == null || urlVridge.Trim().Length == 0)
                    this.baseEndpoint = @"https://cencosud.vridge.io:86/?r=site/"; //Produccion
                else
                    this.baseEndpoint = urlVridge.Trim() + @"/?r=site/"; 


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        /// <summary>
        /// Generar y obtener el token de acceso
        /// </summary>
        /// <returns>Devuelve el Token</returns>
        private DE.TokenJson ObtenerToken()
        {
            DE.TokenJson token = null;
            try
            {
                string credentials = "client_credentials";
                string id = "QUIGYWTGGYSHIEI4QB8M";
                string secret = "ECF7M5XXXQRFU6M0R1F3";

                RestClient restClient = new RestClient(this.baseCredential);

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
                restRequest.AddParameter("application/x-www-form-urlencoded",
                                            string.Format("grant_type={0}&client_id={1}&client_secret={2}&",
                                            (object)credentials, (object)id, (object)secret), ParameterType.RequestBody);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                    token = JsonConvert.DeserializeObject<DE.TokenJson>(json);

                return token;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarConfiguracion(string tienda, string hostname, string telefono, string email)
        {
            bool rpta = false;
            try
            {
                string endPoint = this.baseEndpoint + "actualizar-config-tienda-v2&strTienda={0}&strHostname={1}&intLongPapel={2}&intLongCupon={3}&intTotImpresiones={4}&intRestImpresiones={5}&strTelefono={6}&strEmail={7}";
                RestClient restClient = new RestClient(string.Format(endPoint, tienda, hostname, 0, 14, 0, 0, telefono, email));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content; //CAMBIAR

                rpta = true;

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.CompanhiaEntity> MostrarCompanias()
        {
            var lstCompanhias = new List<DE.CompanhiaEntity>();
            try
            {
                string endPoint = this.baseEndpoint + "mostrar-companias-v2";

                RestClient restClient = new RestClient(endPoint);

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstCompanhiasJson = JsonConvert.DeserializeObject<List<DE.CompanhiaJson>>(json);

                    foreach (var companhiaJson in lstCompanhiasJson)
                    {
                        var companhia = new DE.CompanhiaEntity();
                        companhia.Compania = companhiaJson.Compania;
                        lstCompanhias.Add(companhia);
                    }
                }

                return lstCompanhias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.CuponEntity> MostrarCupones(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            var lstCupones = new List<DE.CuponEntity>();
            try
            {
                string endPoint = this.baseEndpoint + "mostrar-cupones-v2&strNroDoc={0}&intGrupo={1}&strAmbito={2}&strTienda={3}&intTipo={4}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento, idGrupo, ambito, tienda, tipoCupon));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstCuponesJson = JsonConvert.DeserializeObject<List<DE.CuponJson>>(json);

                    foreach (var cuponJson in lstCuponesJson)
                    {
                        var cupon = new DE.CuponEntity();

                        cupon.IdKiosko = int.Parse(cuponJson.IdKiosko);
                        cupon.CodCampania = int.Parse(cuponJson.CodCampania);
                        cupon.Descuento = cuponJson.Descuento;
                        cupon.Mensaje1 = cuponJson.Mensaje1;
                        cupon.Mensaje2 = cuponJson.Mensaje2;
                        cupon.Mensaje3 = cuponJson.Mensaje3;
                        cupon.Imagen = cuponJson.Imagen;
                        cupon.Descripcion = cuponJson.Descripcion;
                        cupon.Prioridad = int.Parse(cuponJson.Prioridad);
                        cupon.CantidadDisponibles = int.Parse(cuponJson.Disponibles);
                        cupon.CantidadRedimidos = int.Parse(cuponJson.Redimidos);
                        cupon.FechaRedencion = cuponJson.FechaRedencion;
                        cupon.HoraRedencion = cuponJson.HoraRedencion;
                        cupon.ExclusivoApp = Convert.ToBoolean(Convert.ToInt32(cuponJson.ExclusivoApp));

                        lstCupones.Add(cupon);
                    }
                }

                return lstCupones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.TiendaEntity> MostrarTiendas()
        {
            var lstTiendas = new List<DE.TiendaEntity>();
            try
            {
                string endPoint = this.baseEndpoint + "mostrar-tiendas-v2";

                RestClient restClient = new RestClient(endPoint);

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstTiendasJson = JsonConvert.DeserializeObject<List<DE.TiendaJson>>(json);

                    foreach (var tiendaJson in lstTiendasJson)
                    {
                        var tienda = new DE.TiendaEntity();
                        tienda.Compania = tiendaJson.Compania;
                        tienda.Tienda = tiendaJson.Tienda;
                        tienda.Hostname = tiendaJson.Hostname;
                        lstTiendas.Add(tienda);
                    }
                }

                return lstTiendas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ClienteEntity ObtenerCliente(string nroDocumento)
        {
            DE.ClienteEntity cliente = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-cliente-v2&strNroDoc={0}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstClienteJson = JsonConvert.DeserializeObject<List<DE.ClienteJson>>(json);
                    if (lstClienteJson.Count == 1)
                    {
                        var clienteJson = lstClienteJson[0];

                        cliente = new DE.ClienteEntity();
                        cliente.TipoDocumento = clienteJson.TipoDocumento;
                        cliente.NumeroDocumento = clienteJson.NumeroDocumento;
                        cliente.Nombres = clienteJson.Nombres;
                    }
                }

                return cliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ConfiguracionEntity ObtenerConfiguracion(string tienda, string hostname)
        {
            DE.ConfiguracionEntity configuracion = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-config-tienda-v2&strTienda={0}&strHostname={1}";

                RestClient restClient = new RestClient(string.Format(endPoint, tienda, hostname));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstConfiguracionesJson = JsonConvert.DeserializeObject<List<DE.ConfiguracionJson>>(json);
                    if (lstConfiguracionesJson.Count == 1)
                    {
                        var configuracionesJson = lstConfiguracionesJson[0];

                        configuracion = new DE.ConfiguracionEntity();

                        configuracion.Compania = configuracionesJson.Compania;
                        configuracion.Tienda = configuracionesJson.Tienda;
                        configuracion.Hostname = configuracionesJson.Hostname;
                        configuracion.Ubicacion = configuracionesJson.Ubicacion;
                        configuracion.LongPapel = configuracionesJson.LongPapel;
                        configuracion.LongCupon = configuracionesJson.LongCupon;
                        configuracion.TotImpresiones = configuracionesJson.TotImpresiones;
                        configuracion.RestImpresiones = configuracionesJson.RestImpresiones;
                        configuracion.TecnicoTelefono = configuracionesJson.Telefono;
                        configuracion.TecnicoEmail = configuracionesJson.Email;
                        configuracion.EmailServidor = configuracionesJson.Servidor;
                        configuracion.EmailPuerto = configuracionesJson.Puerto;
                        configuracion.EmailCuenta = configuracionesJson.EmailSalida;
                        configuracion.EmailClave = configuracionesJson.Pass;
                        configuracion.AplicacionClave = configuracionesJson.PassAplicacion;

                    }
                }

                return configuracion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.DetalleCuponEntity ObtenerCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon)
        {
            DE.DetalleCuponEntity detalleCupon = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-cupon-v2&strNroDoc={0}&intCupon={1}&intGrupo={2}&intTipo={3}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento, nroCupon, idGrupo, tipoCupon));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstDetallesCuponesJson = JsonConvert.DeserializeObject<List<DE.DetalleCuponJson>>(json);
                    if (lstDetallesCuponesJson.Count == 1)
                    {
                        var detalleCuponJson = lstDetallesCuponesJson[0];

                        detalleCupon = new DE.DetalleCuponEntity();

                        detalleCupon.Descuento = detalleCuponJson.Descuento;
                        detalleCupon.Mensaje1 = detalleCuponJson.Mensaje1;
                        detalleCupon.Mensaje2 = detalleCuponJson.Mensaje2;
                        detalleCupon.Mensaje3 = detalleCuponJson.Mensaje3;
                        detalleCupon.Ambito = detalleCuponJson.Ambito;
                        detalleCupon.TipoLegal = int.Parse(detalleCuponJson.TipoLegal);
                        detalleCupon.MensajeLegal = detalleCuponJson.MensajeLegal;
                        detalleCupon.Imagen = detalleCuponJson.Imagen;
                        detalleCupon.CodigoBarra = detalleCuponJson.CodigoBarra;
                        detalleCupon.CodigoCanal = int.Parse(detalleCuponJson.CodigoCanal);
                        detalleCupon.FlagSeriado = int.Parse(detalleCuponJson.FlagSeriado);
                        detalleCupon.BonusPrincipalSeriado = detalleCuponJson.BonusSeriado; //detalleCuponJson.BonusPrincipalSeriado;
                        detalleCupon.BonusTitularSeriado = detalleCuponJson.BonusTitularSeriado;
                        detalleCupon.CodigoOnline = detalleCuponJson.CodigoOnline;
                        detalleCupon.Vigencia = detalleCuponJson.Vigencia;
                        detalleCupon.Tipo = int.Parse(detalleCuponJson.Tipo);
                        detalleCupon.CodEmision = detalleCuponJson.CodEmision;

                    }
                }

                return detalleCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilClienteEntity ObtenerPerfilCliente(int tipoDocumento, string nroDocumento, string ambito)
        {
            DE.PerfilClienteEntity perfil = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-perfil-cliente-v2&intTipDoc={0}&strNroDoc={1}&strAmbito={2}";

                RestClient restClient = new RestClient(string.Format(endPoint, tipoDocumento, nroDocumento, ambito));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstPerfilesJson = JsonConvert.DeserializeObject<List<DE.PerfilClienteJson>>(json);
                    if (lstPerfilesJson.Count == 1)
                    {
                        var perfilJson = lstPerfilesJson[0];

                        perfil = new DE.PerfilClienteEntity();

                        perfil.TipoDocumento = perfilJson.TipoDocumento;
                        perfil.NumeroDocumento = perfilJson.NumeroDocumento;
                        perfil.TiendaAdmitidas = perfilJson.TiendaAdmitidas;
                        perfil.Ambito = perfilJson.Ambito;
                        perfil.OpcionMisCupones = perfilJson.OpcionMisCupones.Equals("1");
                        perfil.OpcionMiSuperCupon = perfilJson.OpcionMiSuperCupon.Equals("1");
                        perfil.OpcionMarcasSeleccionadas = perfilJson.OpcionMarcasSeleccionadas.Equals("1");
                        perfil.OpcionArbolDeCupones = perfilJson.OpcionArbolDeCupones.Equals("1");
                        perfil.OpcionCuponesDelLugar = perfilJson.OpcionCuponesDelLugar.Equals("1");
                        perfil.OpcionRuletaDeCupones = perfilJson.OpcionRuletaDeCupones.Equals("1");
                        perfil.OpcionMisPuntosBonus = perfilJson.OpcionMisPuntosBonus.Equals("1");
                        perfil.OpcionMisInvitaciones = perfilJson.OpcionMisInvitaciones.Equals("1");
                        perfil.OpcionCuponesComerciales = perfilJson.OpcionCuponesComerciales.Equals("1");

                    }
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilKioskoEntity ObtenerPerfilKiosko(string tienda, string hostname)
        {
            DE.PerfilKioskoEntity perfil = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-perfil-kiosko-v2&strTienda={0}&strHostname={1}";

                RestClient restClient = new RestClient(string.Format(endPoint, tienda, hostname));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstPerfilesJson = JsonConvert.DeserializeObject<List<DE.PerfilKioskoJson>>(json);
                    if (lstPerfilesJson.Count == 1)
                    {
                        var perfilJson = lstPerfilesJson[0];

                        perfil = new DE.PerfilKioskoEntity();

                        perfil.TiendaKioskoId = int.Parse(perfilJson.TiendaKioskoId);
                        perfil.TiendaId = perfilJson.TiendaId;
                        perfil.Ambito = perfilJson.Ambito;
                        perfil.HostName = perfilJson.HostName;
                        perfil.DireccionIP = perfilJson.DireccionIP;
                        perfil.Zona = perfilJson.Zona;
                        perfil.ImagenBanner = perfilJson.ImagenBanner;
                        perfil.FechaIniBanner = perfilJson.FechaIniBanner;
                        perfil.FechaFinBanner = perfilJson.FechaFinBanner;
                        perfil.ImpresoraId = int.Parse(perfilJson.ImpresoraId);
                        perfil.Marca = perfilJson.Marca;
                        perfil.Modelo = perfilJson.Modelo;
                        perfil.MargenIzquierdoX = int.Parse(perfilJson.MargenIzquierdoX);
                        perfil.MargenSuperiorY = int.Parse(perfilJson.MargenSuperiorY);
                        perfil.Ancho = int.Parse(perfilJson.Ancho);
                        perfil.Alto = int.Parse(perfilJson.Alto);
                    }
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilTiendaEntity ObtenerPerfilTienda(string tienda)
        {
            DE.PerfilTiendaEntity perfil = null;
            try
            {
                string endPoint = this.baseEndpoint + "obtener-perfil-tienda-v2&strTienda={0}";

                RestClient restClient = new RestClient(string.Format(endPoint, tienda));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstPerfilesJson = JsonConvert.DeserializeObject<List<DE.PerfilTiendaJson>>(json);
                    if (lstPerfilesJson.Count == 1)
                    {
                        var perfilJson = lstPerfilesJson[0];

                        perfil = new DE.PerfilTiendaEntity();

                        perfil.TiendaId = perfilJson.TiendaId;
                        perfil.Ambito = perfilJson.Ambito;
                        perfil.OpcionMisCupones = int.Parse(perfilJson.OpcionMisCupones);
                        perfil.OpcionMiSuperCupon = int.Parse(perfilJson.OpcionMiSuperCupon);
                        perfil.OpcionMarcasSeleccionadas = int.Parse(perfilJson.OpcionMarcasSeleccionadas);
                        perfil.OpcionArbolDeCupones = int.Parse(perfilJson.OpcionArbolDeCupones);
                        perfil.OpcionCuponesDelLugar = int.Parse(perfilJson.OpcionCuponesDelLugar);
                        perfil.OpcionRuletaDeCupones = int.Parse(perfilJson.OpcionRuletaDeCupones);
                        perfil.OpcionMisPuntosBonus = int.Parse(perfilJson.OpcionMisPuntosBonus);
                        perfil.OpcionMisInvitaciones = int.Parse(perfilJson.OpcionMisInvitaciones);
                        perfil.OpcionCuponesComerciales = int.Parse(perfilJson.OpcionCuponesComerciales);

                    }
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool QuitarDisponibilidadCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon, string codigoBarra)
        {
            bool rpta = false;
            string json = "";
            try
            {
                string endPoint = this.baseEndpoint + "quitar-disponibilidad-cupon-v2&strNroDoc={0}&intCupon={1}&intGrupo={2}&intTipo={3}&strCodBarra={4}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento, nroCupon, idGrupo, tipoCupon, codigoBarra));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarExperiencia(int tipoDocumento, string nroDocumento, int experiencia, string tienda, string hostname)
        {
            bool rpta = false;
            try
            {
                string endPoint = this.baseEndpoint + "registrar-experiencia-v2&intTipDoc={0}&strNroDoc={1}&intExperiencia={2}&strTienda={3}&strHostname={4}";

                RestClient restClient = new RestClient(string.Format(endPoint, tipoDocumento, nroDocumento, experiencia, tienda, hostname));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarInteraccionCupon(string nroDocumento, int nroCupon, int idGrupo, int idInteraccion, string tienda)
        {
            bool rpta = false;
            string json = "";
            try
            {
                string endPoint = this.baseEndpoint + "registrar-interaccion-cupon-v2&strNroDoc={0}&intCupon={1}&intGrupo={2}&intFlag={3}&strTienda={4}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento, nroCupon, idGrupo, idInteraccion, tienda));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ValidarEntity ValidarCliente(string nroDocumento, string ambito)
        {
            DE.ValidarEntity validar = null;
            try
            {

                string endPoint = this.baseEndpoint + "validar-cliente-v2&strNroDoc={0}&strAmbito={1}";

                RestClient restClient = new RestClient(string.Format(endPoint, nroDocumento, ambito));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstValidarJson = JsonConvert.DeserializeObject<List<DE.ValidarJson>>(json);
                    if (lstValidarJson.Count == 1)
                    {
                        var validarJson = lstValidarJson[0];

                        validar = new DE.ValidarEntity();

                        validar.Valido = validarJson.Valido.Equals("1");
                        validar.EsCumpleanhos = validarJson.EsCumpleanhos.Equals("1");
                        validar.CuponCumpleanhos = validarJson.CuponCumpleanhos.Equals("1");

                        string formatoFechaHora = "yyyy-MM-dd HH:mm:ss";
                        DateTime fechaHora = DateTime.ParseExact(validarJson.FechaHoraConsulta, formatoFechaHora, System.Globalization.CultureInfo.InvariantCulture);
                        validar.FechaHoraConsulta = fechaHora;
                    }
                }

                return validar;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarLog(string tienda, string hostname, int tipo, string mensaje, string descripcion)
        {
            bool rpta = false;
            try
            {
                string endPoint = this.baseEndpoint + "registrar-log-v2&strTienda={0}&strHostname={1}&intTipo={2}&strMensaje={3}&strDescripcion={4}";

                RestClient restClient = new RestClient(string.Format(endPoint, tienda, hostname, tipo, mensaje, descripcion));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool RegistrarSesion(string tienda, string hostname, string id, int tipoDocumento, string nroDocumento, string inicio, string fin,
                                    bool OpcionMisCupones, bool OpcionMiSuperCupon,
                                    bool OpcionMarcasSeleccionadas, bool OpcionArbolDeCupones,
                                    bool OpcionCuponesDelLugar, bool OpcionRuletaDeCupones,
                                    bool OpcionMisPuntosBonus, bool OpcionMisInvitaciones, 
                                    bool OpcionCuponesComerciales,
                                    string CuponesImpresos, string CuponesVistos,
                                    string BannersVistos)
        {
            bool rpta = false;
            try
            {

                int bitMisCupones = Convert.ToInt32(OpcionMisCupones);
                int bitMiSuperCupon = Convert.ToInt32(OpcionMiSuperCupon);
                int bitMarcasSeleccionadas = Convert.ToInt32(OpcionMarcasSeleccionadas);
                int bitArbolDeCupones = Convert.ToInt32(OpcionArbolDeCupones);
                int bitCuponesDelLugar = Convert.ToInt32(OpcionCuponesDelLugar);
                int bitRuletaDeCupones = Convert.ToInt32(OpcionRuletaDeCupones);
                int bitMisPuntosBonus = Convert.ToInt32(OpcionMisPuntosBonus);
                int bitMisInvitaciones = Convert.ToInt32(OpcionMisInvitaciones);
                int bitCuponesComerciales = Convert.ToInt32(OpcionCuponesComerciales);

                string endPoint = this.baseEndpoint;
                endPoint += "registrar-sesion-v2&strTienda={0}&strHostname={1}&strId={2}&intTipDoc={3}&strNroDoc={4}&strInicio={5}&strFin={6}";
                endPoint += "&bitMisCupones={7}&bitMiSuperCupon={8}&bitMarcasSeleccionadas={9}&bitArbolDeCupones={10}";
                endPoint += "&bitCuponesDelLugar={11}&bitRuletaDeCupones={12}&bitMisPuntosBonus={13}&bitMisInvitaciones={14}";

                //endPoint += "&strCuponesImpresos={15}&strCuponesVistos={16}&strBannersVistos={17}";

                //string rest = String.Format(endPoint, tienda, hostname, id, tipoDocumento, nroDocumento, inicio, fin,
                //                                        bitMisCupones, bitMiSuperCupon, bitMarcasSeleccionadas, bitArbolDeCupones,
                //                                        bitCuponesDelLugar, bitRuletaDeCupones, bitMisPuntosBonus, bitMisInvitaciones,
                //                                        CuponesImpresos, CuponesVistos, BannersVistos);

                endPoint += "&bitCuponesComerciales={15}";
                endPoint += "&strCuponesImpresos={16}&strCuponesVistos={17}&strBannersVistos={18}";

                string rest = String.Format(endPoint, tienda, hostname, id, tipoDocumento, nroDocumento, inicio, fin,
                                                        bitMisCupones, bitMiSuperCupon, bitMarcasSeleccionadas, bitArbolDeCupones,
                                                        bitCuponesDelLugar, bitRuletaDeCupones, bitMisPuntosBonus, bitMisInvitaciones,
                                                        bitCuponesComerciales,
                                                        CuponesImpresos, CuponesVistos, BannersVistos);

                RestClient restClient = new RestClient(rest);

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.BannerEntity> MostrarBanners(string tienda, string hostname, string nroDocumento)
        {
            var lstBanners = new List<DE.BannerEntity>();
            try
            {
                string endPoint = this.baseEndpoint + "mostrar-banners-v2&strTienda={0}&strHostname={1}&strNroDoc={2}";

                RestClient restClient = new RestClient(string.Format(endPoint, tienda, hostname, nroDocumento));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstBannersJson = JsonConvert.DeserializeObject<List<DE.BannerJson>>(json);

                    foreach (var bannerJson in lstBannersJson)
                    {
                        var banner = new DE.BannerEntity();

                        banner.Id = bannerJson.IdBanner;
                        banner.Nombre = bannerJson.Nombre;
                        banner.Imagen = bannerJson.Imagen;
                        banner.Orden = bannerJson.Orden;
                        banner.Prioridad = bannerJson.Prioridad;
                        banner.Tiempo = bannerJson.Tiempo;

                        lstBanners.Add(banner);
                    }
                }

                return lstBanners;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ContarBanners(string banners)
        {
            bool rpta = false;
            try
            {
                string endPoint = this.baseEndpoint + "contar-banners-v2&strBannersVistos={0}";

                RestClient restClient = new RestClient(string.Format(endPoint, banners));

                RestRequest restRequest = new RestRequest(Method.POST);
                restRequest.AddHeader("cache-control", "no-cache");
                restRequest.AddHeader("authorization", token);

                string json = restClient.Execute(restRequest).Content;

                if (json.Length > 0)
                {
                    var lstRegistradosJson = JsonConvert.DeserializeObject<List<DE.RegistradoJson>>(json);
                    if (lstRegistradosJson.Count == 1)
                        rpta = lstRegistradosJson[0].Registrado.Equals("1");
                }
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
