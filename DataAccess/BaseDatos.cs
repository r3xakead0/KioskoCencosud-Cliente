﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DE = Cupones.DataEntity;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class BaseDatos : IDatos 
    {

        private static string stringConnection = "";

        #region Construtores

        public BaseDatos()
        {
            try
            {
                if (stringConnection == "")
                {
                    //stringConnection = "Data Source=172.29.73.2;Initial Catalog=PruebaTechnology02;User id=KioskoUserTest;Password=1a2b3c4d;";
                    //stringConnection = "Data Source=172.29.73.2;Initial Catalog=Technology02;User id=usrKiosko;Password=usrKiosko;";
                    //stringConnection = "Data Source=G200603SVHC2;Initial Catalog=PruebaKiosko;User id=u_pruebakiosko;Password=u_pru3b4k10sk0;";
                    stringConnection = "Data Source=172.29.73.1;Initial Catalog=PruebaKiosko;User id=egonzava;Password=erickelme1318;";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public List<DE.CompanhiaEntity> MostrarCompanias()
        {
            var lstCompanias = new List<DE.CompanhiaEntity>();
            try
            {
                string sp = "SpMostrarCompaniasV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var compania = new DE.CompanhiaEntity();
                        compania.Compania = reader["Compania"].ToString();
                        lstCompanias.Add(compania);
                    }

                    cnn.Close();
                }

                return lstCompanias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.TiendaEntity> MostrarTiendas()
        {
            var lstTiendas = new List<DE.TiendaEntity>();
            try
            {
                string sp = "SpMostrarTiendasV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var tienda = new DE.TiendaEntity();
                        tienda.Compania = reader["Compania"].ToString();
                        tienda.Tienda = reader["Tienda"].ToString();
                        tienda.Hostname = reader["Hostname"].ToString();
                        lstTiendas.Add(tienda);
                    }

                    cnn.Close();
                }

                return lstTiendas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ClienteEntity ObtenerCliente(string nroDocumento)
        {
            DE.ClienteEntity cliente = null;
            try
            {
                string sp = "SpObtenerClienteV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cliente = new DE.ClienteEntity();

                        cliente.TipoDocumento = reader["TipoDocumento"].ToString();
                        cliente.NumeroDocumento = reader["NumeroDocumento"].ToString();
                        cliente.Nombres = reader["Nombres"].ToString(); ;
                        cliente.CuponesAsignadosMetro = int.Parse(reader["CuponesAsignadosMetro"].ToString());
                        cliente.CuponesAsignadosWong = int.Parse(reader["CuponesAsignadosWong"].ToString());
                    }

                    cnn.Close();
                }

                return cliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ValidarEntity ValidarCliente(string nroDocumento, string ambito)
        {
            DE.ValidarEntity validar = null;
            try
            {
                string sp = "SpValidarClienteV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strAmbito", ambito));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        validar = new DE.ValidarEntity();

                        validar.Valido = bool.Parse(reader["Valido"].ToString());
                        validar.EsCumpleanhos = bool.Parse(reader["EsCumpleanhos"].ToString());
                        validar.CuponCumpleanhos = bool.Parse(reader["CuponCumpleanhos"].ToString());
                        validar.FechaHoraConsulta = DateTime.Parse(reader["FechaHoraConsulta"].ToString());
                    }

                    cnn.Close();
                }

                return validar;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.ConfiguracionEntity ObtenerConfiguracion(string tienda, string hostname)
        {
            DE.ConfiguracionEntity config = null;
            try
            {
                string sp = "SpObtenerConfigTiendaV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        config = new DE.ConfiguracionEntity();

                        config.Compania = reader["Compania"].ToString();
                        config.Tienda = reader["Tienda"].ToString();
                        config.Hostname = reader["Hostname"].ToString();
                        config.Ubicacion = reader["Ubicacion"].ToString();
                        config.LongPapel = int.Parse(reader["LongPapel"].ToString());
                        config.LongCupon = int.Parse(reader["LongCupon"].ToString());
                        config.TotImpresiones = int.Parse(reader["TotImpresiones"].ToString());
                        config.RestImpresiones = int.Parse(reader["RestImpresiones"].ToString());
                        config.TecnicoTelefono = reader["Telefono"].ToString();
                        config.TecnicoEmail = reader["Email"].ToString();
                        config.EmailServidor = reader["Servidor"].ToString();
                        config.EmailPuerto = int.Parse(reader["Puerto"].ToString());
                        config.EmailCuenta = reader["EmailSalida"].ToString();
                        config.EmailClave = reader["Pass"].ToString();
                        config.AplicacionClave = reader["PassAplicacion"].ToString();
                    }

                    cnn.Close();
                }

                return config;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarConfiguracion(string tienda, string hostname, string telefono, string email)
        {
            bool rpta = false;
            try
            {
                string sp = "SpActualizarConfigTiendaV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));
                    cmd.Parameters.Add(new SqlParameter("@intLongPapel", 0));
                    cmd.Parameters.Add(new SqlParameter("@intLongCupon", 14));
                    cmd.Parameters.Add(new SqlParameter("@intTotImpresiones", 0));
                    cmd.Parameters.Add(new SqlParameter("@intRestImpresiones", 0));
                    cmd.Parameters.Add(new SqlParameter("@strTelefono", telefono));
                    cmd.Parameters.Add(new SqlParameter("@strEmail", email));

                    rpta = (cmd.ExecuteNonQuery() > 0);

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilClienteEntity ObtenerPerfilCliente(int tipoDocumento, string nroDocumento, string ambito)
        {
            DE.PerfilClienteEntity perfil = null;
            try
            {
                string sp = "SpObtenerPerfilClienteV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@intTipDoc", tipoDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strAmbito", ambito));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        perfil = new DE.PerfilClienteEntity();

                        perfil.TipoDocumento = reader["TipoDocumento"].ToString();
                        perfil.NumeroDocumento = reader["NumeroDocumento"].ToString();
                        perfil.TiendaAdmitidas = reader["TiendaAdmitidas"].ToString();
                        perfil.Ambito = reader["Ambito"].ToString();
                        perfil.OpcionMisCupones = bool.Parse(reader["OpcionMisCupones"].ToString());
                        perfil.OpcionMiSuperCupon = bool.Parse(reader["OpcionMiSuperCupon"].ToString());
                        perfil.OpcionMarcasSeleccionadas = bool.Parse(reader["OpcionMarcasSeleccionadas"].ToString());
                        perfil.OpcionArbolDeCupones = bool.Parse(reader["OpcionArbolDeCupones"].ToString());
                        perfil.OpcionCuponesDelLugar = bool.Parse(reader["OpcionCuponesDelLugar"].ToString());
                        perfil.OpcionRuletaDeCupones = bool.Parse(reader["OpcionRuletaDeCupones"].ToString());
                        perfil.OpcionMisPuntosBonus = bool.Parse(reader["OpcionMisPuntosBonus"].ToString());
                        perfil.OpcionMisInvitaciones = bool.Parse(reader["OpcionMisInvitaciones"].ToString());
                        perfil.OpcionCuponesComerciales = bool.Parse(reader["OpcionCuponesComerciales"].ToString());
                    }

                    cnn.Close();
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilTiendaEntity ObtenerPerfilTienda(string tienda)
        {
            DE.PerfilTiendaEntity perfil = null;
            try
            {
                string sp = "SpObtenerPerfilTiendaV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        perfil = new DE.PerfilTiendaEntity();

                        perfil.TiendaId = reader["TiendaId"].ToString();
                        perfil.Ambito = reader["Ambito"].ToString();
                        perfil.OpcionMisCupones = int.Parse(reader["OpcionMisCupones"].ToString());
                        perfil.OpcionMiSuperCupon = int.Parse(reader["OpcionMiSuperCupon"].ToString());
                        perfil.OpcionMarcasSeleccionadas = int.Parse(reader["OpcionMarcasSeleccionadas"].ToString());
                        perfil.OpcionArbolDeCupones = int.Parse(reader["OpcionArbolDeCupones"].ToString());
                        perfil.OpcionCuponesDelLugar = int.Parse(reader["OpcionCuponesDelLugar"].ToString());
                        perfil.OpcionRuletaDeCupones = int.Parse(reader["OpcionRuletaDeCupones"].ToString());
                        perfil.OpcionMisPuntosBonus = int.Parse(reader["OpcionMisPuntosBonus"].ToString());
                        perfil.OpcionMisInvitaciones = int.Parse(reader["OpcionMisInvitaciones"].ToString());
                        perfil.OpcionCuponesComerciales = int.Parse(reader["OpcionCuponesComerciales"].ToString());
                    }

                    cnn.Close();
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.PerfilKioskoEntity ObtenerPerfilKiosko(string tienda, string hostname)
        {
            DE.PerfilKioskoEntity perfil = null;
            try
            {
                string sp = "SpObtenerPerfilKioskoV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        perfil = new DE.PerfilKioskoEntity();

                        perfil.TiendaKioskoId = int.Parse(reader["TiendaKioskoId"].ToString());
                        perfil.TiendaId = reader["TiendaId"].ToString();
                        perfil.Ambito = reader["Ambito"].ToString();
                        perfil.HostName = reader["HostName"].ToString();
                        perfil.DireccionIP = reader["DireccionIP"].ToString();
                        perfil.Zona = reader["Zona"].ToString();
                        perfil.ImagenBanner = reader["ImagenBanner"].ToString();
                        perfil.FechaIniBanner = DateTime.Parse(reader["FechaIniBanner"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        perfil.FechaFinBanner = DateTime.Parse(reader["FechaFinBanner"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        perfil.ImpresoraId = int.Parse(reader["ImpresoraId"].ToString());
                        perfil.Marca = reader["Marca"].ToString();
                        perfil.Modelo = reader["Modelo"].ToString();
                        perfil.MargenIzquierdoX = int.Parse(reader["MargenIzquierdoX"].ToString());
                        perfil.MargenSuperiorY = int.Parse(reader["MargenSuperiorY"].ToString());
                        perfil.Ancho = int.Parse(reader["Ancho"].ToString());
                        perfil.Alto = int.Parse(reader["Alto"].ToString());
                    }

                    cnn.Close();
                }

                return perfil;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.CuponEntity> MostrarCupones(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            var lstCupones = new List<DE.CuponEntity>();
            try
            {
                string sp = "SpMostrarCuponesV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@intGrupo", idGrupo));
                    cmd.Parameters.Add(new SqlParameter("@strAmbito", ambito));
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@intTipo", tipoCupon));

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var cupon = new DE.CuponEntity();

                        cupon.IdKiosko = int.Parse(reader["IdKiosko"].ToString());
                        cupon.CodCampania = int.Parse(reader["CodCampania"].ToString());
                        cupon.Descuento = reader["Descuento"].ToString();
                        cupon.Mensaje1 = reader["Mensaje1"].ToString();
                        cupon.Mensaje2 = reader["Mensaje2"].ToString();
                        cupon.Mensaje3 = reader["mensaje3"].ToString();
                        cupon.Imagen = reader["Imagen"].ToString();
                        cupon.Descripcion = reader["Descripcion"].ToString();
                        cupon.Prioridad = reader["Prioridad"] as int? ?? default(int);
                        cupon.CantidadDisponibles = reader["Disponibles"] as int? ?? default(int);
                        cupon.CantidadRedimidos = reader["Redimidos"] as int? ?? default(int);
                        cupon.FechaRedencion = reader["FechaRedencion"].ToString();
                        cupon.HoraRedencion = reader["HoraRedencion"].ToString();
                        cupon.ExclusivoApp = reader["ExclusivoApp"] as bool? ?? default(bool);

                        lstCupones.Add(cupon);
                    }

                    cnn.Close();
                }

                return lstCupones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DE.DetalleCuponEntity ObtenerCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon)
        {
            DE.DetalleCuponEntity detalleCupon = null;
            try
            {
                string sp = "SpObtenerCuponV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@intCupon", nroCupon));
                    cmd.Parameters.Add(new SqlParameter("@intGrupo", idGrupo));
                    cmd.Parameters.Add(new SqlParameter("@intTipo", tipoCupon));

                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        detalleCupon = new DE.DetalleCuponEntity();

                        detalleCupon.Descuento = reader["Descuento"].ToString();
                        detalleCupon.Mensaje1 = reader["Mensaje1"].ToString();
                        detalleCupon.Mensaje2 = reader["Mensaje2"].ToString();
                        detalleCupon.Mensaje3 = reader["mensaje3"].ToString();
                        detalleCupon.Ambito = reader["Ambito"].ToString();
                        detalleCupon.TipoLegal = int.Parse(reader["TipoLegal"].ToString());
                        detalleCupon.MensajeLegal = reader["MensajeLegal"].ToString();
                        detalleCupon.Imagen = reader["Imagen"].ToString();
                        detalleCupon.CodigoBarra = reader["CodigoBarra"].ToString();
                        detalleCupon.CodigoCanal = int.Parse(reader["CodigoCanal"].ToString());
                        detalleCupon.FlagSeriado = int.Parse(reader["FlagSeriado"].ToString());
                        detalleCupon.BonusPrincipalSeriado = reader["BonusSeriado"].ToString();
                        detalleCupon.BonusTitularSeriado = reader["BonusTitularSeriado"].ToString();
                        detalleCupon.CodigoOnline = reader["CodigoOnline"].ToString();
                        detalleCupon.Vigencia = reader["Vigencia"].ToString();
                        detalleCupon.Tipo = int.Parse(reader["Tipo"].ToString());
                        detalleCupon.CodEmision = int.Parse(reader["CodEmision"].ToString() ?? "0");

                    }

                    cnn.Close();
                }

                return detalleCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool QuitarDisponibilidadCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon, string codigoBarra)
        {
            bool rpta = false;
            try
            {
                string sp = "SpQuitarDisponibilidadCuponV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@intCupon", nroCupon));
                    cmd.Parameters.Add(new SqlParameter("@intGrupo", idGrupo));
                    cmd.Parameters.Add(new SqlParameter("@intTipo", tipoCupon));
                    cmd.Parameters.Add(new SqlParameter("@strCodBarra", codigoBarra));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarInteraccionCupon(string nroDocumento, int nroCupon, int idGrupo, int idInteraccion, string tienda)
        {
            bool rpta = false;
            try
            {
                string sp = "SpRegistrarInteraccionCuponV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@intCupon", nroCupon));
                    cmd.Parameters.Add(new SqlParameter("@intGrupo", idGrupo));
                    cmd.Parameters.Add(new SqlParameter("@intFlag", idInteraccion));
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarExperiencia(int tipoDocumento, string nroDocumento, int experiencia, string tienda, string hostname)
        {
            bool rpta = false;
            try
            {
                string sp = "SpRegistrarExperienciaV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@intTipDoc", tipoDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@intExperiencia", experiencia));
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarLog(string tienda, string hostname, int tipo, string mensaje, string descripcion)
        {
            bool rpta = false;
            try
            {
                string sp = "SpRegistrarLogV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));
                    cmd.Parameters.Add(new SqlParameter("@intTipo", tipo));
                    cmd.Parameters.Add(new SqlParameter("@strMensaje", mensaje));
                    cmd.Parameters.Add(new SqlParameter("@strDescripcion", descripcion));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool RegistrarSesion(string tienda, string hostname, string id, int tipoDocumento, string nroDocumento, string inicio, string fin,
                                    bool OpcionMisCupones, bool OpcionMiSuperCupon,
                                    bool OpcionMarcasSeleccionadas, bool OpcionArbolDeCupones,
                                    bool OpcionCuponesDelLugar, bool OpcionRuletaDeCupones,
                                    bool OpcionMisPuntosBonus, bool OpcionMisInvitaciones,
                                    bool OpcionCuponesComerciales,
                                    string CuponesImpresos, string CuponesVistos,
                                    string BannersVistos)
        {
            bool rpta = false;
            try
            {
                string sp = "SpRegistrarSesionV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strId", id));
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));
                    cmd.Parameters.Add(new SqlParameter("@intTipDoc", tipoDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));
                    cmd.Parameters.Add(new SqlParameter("@strInicio", inicio));
                    cmd.Parameters.Add(new SqlParameter("@strFin", fin));
                    cmd.Parameters.Add(new SqlParameter("@bitMisCupones", OpcionMisCupones));
                    cmd.Parameters.Add(new SqlParameter("@bitMiSuperCupon", OpcionMiSuperCupon));
                    cmd.Parameters.Add(new SqlParameter("@bitMarcasSeleccionadas", OpcionMarcasSeleccionadas));
                    cmd.Parameters.Add(new SqlParameter("@bitArbolDeCupones", OpcionArbolDeCupones));
                    cmd.Parameters.Add(new SqlParameter("@bitCuponesDelLugar", OpcionCuponesDelLugar));
                    cmd.Parameters.Add(new SqlParameter("@bitRuletaDeCupones", OpcionRuletaDeCupones));
                    cmd.Parameters.Add(new SqlParameter("@bitMisPuntosBonus", OpcionMisPuntosBonus));
                    cmd.Parameters.Add(new SqlParameter("@bitMisInvitaciones", OpcionMisInvitaciones));
                    cmd.Parameters.Add(new SqlParameter("@bitCuponesComerciales", OpcionCuponesComerciales));
                    cmd.Parameters.Add(new SqlParameter("@strCuponesImpresos", CuponesImpresos));
                    cmd.Parameters.Add(new SqlParameter("@strCuponesVistos", CuponesVistos));
                    cmd.Parameters.Add(new SqlParameter("@strBannersVistos", BannersVistos));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.BannerEntity> MostrarBanners(string tienda, string hostname, string nroDocumento)
        {
            var lstBanners = new List<DE.BannerEntity>();
            try
            {
                string sp = "SpMostrarBannersV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strTienda", tienda));
                    cmd.Parameters.Add(new SqlParameter("@strHostname", hostname));
                    cmd.Parameters.Add(new SqlParameter("@strNroDoc", nroDocumento));

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var banner = new DE.BannerEntity();

                        banner.Id = int.Parse(reader["IdBanner"].ToString());
                        banner.Nombre = reader["Nombre"].ToString();
                        banner.Imagen = reader["Imagen"].ToString();
                        banner.Orden = int.Parse(reader["Orden"].ToString());
                        banner.Prioridad = int.Parse(reader["Prioridad"].ToString());
                        banner.Tiempo = int.Parse(reader["Tiempo"].ToString());

                        lstBanners.Add(banner);
                    }

                    cnn.Close();
                }

                return lstBanners;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ContarBanners(string banners)
        {
            bool rpta = false;
            try
            {
                string sp = "SpContarBannersV2";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sp, cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strBannersVistos", banners));

                    rpta = bool.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DE.KioskoEntity> ListarMonitoreo()
        {
            try
            {
                var lstKioskos = new List<DE.KioskoEntity>();

                string sql = @"SELECT	T1.Empresa, 
		                                T1.Tienda, 
		                                T1.Hostname, 
		                                T1.Ip, 
		                                CASE WHEN ISNULL(T1.Ip,'') = '' THEN 0 ELSE 1 END AS Conectado, 
		                                CASE WHEN ISNULL(T1.[Version],'') = '' THEN 0 ELSE 1 END AS Encendido, 
		                                T1.[Version], 
		                                T1.Sesion, 
		                                T1.FechaHora  
                                FROM	Auditoria.Kiosko_Monitoreo T1 WITH(NOLOCK)
                                INNER JOIN (
                                SELECT	T0.Hostname, 
		                                MAX(T0.Id) AS Id,
		                                MAX(T0.FechaHora) AS FechaHora
                                FROM	Auditoria.Kiosko_Monitoreo T0 WITH(NOLOCK)
                                WHERE	T0.Hostname IN (SELECT T0.Hostname FROM Perfil.Config T0 WITH(NOLOCK)) 
                                AND     CAST(T0.FechaHora AS DATE) = CAST(GETDATE() AS DATE)
                                GROUP BY T0.Empresa, 
		                                T0.Tienda, 
		                                T0.Hostname
                                ) T2 ON T2.Id = T1.Id 
                                ORDER BY T1.Empresa,
		                                T1.Tienda, 
		                                T1.Hostname";

                using (var cnn = new SqlConnection(stringConnection))
                {
                    cnn.Open();

                    var cmd = new SqlCommand(sql, cnn);
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var kiosko = new DE.KioskoEntity();

                        kiosko.Empresa = reader["Empresa"].ToString();
                        kiosko.Tienda = reader["Tienda"].ToString();
                        kiosko.Hostname = reader["Hostname"].ToString();
                        kiosko.Ip = reader["Ip"].ToString();
                        kiosko.Conexion = bool.Parse(reader["Conectado"].ToString());
                        kiosko.Encendido = bool.Parse(reader["Encendido"].ToString());
                        kiosko.Version = reader["Version"].ToString();
                        kiosko.Sesion = reader["Sesion"].ToString();
                        kiosko.FechaHora = DateTime.Parse(reader["FechaHora"].ToString());

                        lstKioskos.Add(kiosko);
                    }

                    cnn.Close();
                }

                return lstKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
