﻿using System;
using System.Collections.Generic;
using DE = Cupones.DataEntity;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public interface IDatos
    {
        bool ActualizarConfiguracion(string tienda, string hostname, string telefono, string email);
        List<DE.CompanhiaEntity> MostrarCompanias();
        List<DE.CuponEntity> MostrarCupones(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon);
        List<DE.TiendaEntity> MostrarTiendas();
        DE.ClienteEntity ObtenerCliente(string nroDocumento);
        DE.ConfiguracionEntity ObtenerConfiguracion(string tienda, string hostname);
        DE.DetalleCuponEntity ObtenerCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon);
        DE.PerfilClienteEntity ObtenerPerfilCliente(int tipoDocumento, string nroDocumento, string ambito);
        DE.PerfilKioskoEntity ObtenerPerfilKiosko(string tienda, string hostname);
        DE.PerfilTiendaEntity ObtenerPerfilTienda(string tienda);
        bool QuitarDisponibilidadCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon, string codBarra);
        bool RegistrarExperiencia(int tipoDocumento, string nroDocumento, int experiencia, string tienda, string hostname);
        bool RegistrarInteraccionCupon(string nroDocumento, int nroCupon, int idGrupo, int idInteraccion, string tienda);
        DE.ValidarEntity ValidarCliente(string nroDocumento, string ambito);
        bool RegistrarLog(string tienda, string hostname, int tipo, string mensaje, string descripcion);
        bool RegistrarSesion(string tienda, string hostname, string id, int tipoDocumento, string nroDocumento, string inicio, string fin,
                                    bool OpcionMisCupones, bool OpcionMiSuperCupon,
                                    bool OpcionMarcasSeleccionadas, bool OpcionArbolDeCupones,
                                    bool OpcionCuponesDelLugar, bool OpcionRuletaDeCupones,
                                    bool OpcionMisPuntosBonus, bool OpcionMisInvitaciones,
                                    bool OpcionCuponesComerciales,
                                    string CuponesImpresos, string CuponesVistos,
                                    string BannersVistos);
        List<DE.BannerEntity> MostrarBanners(string tienda, string hostname, string nroDocumento);
        bool ContarBanners(string banners);
    }
}
