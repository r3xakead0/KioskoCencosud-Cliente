﻿using System;
using System.Collections.Generic;
using DE = Cupones.DataEntity;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public interface IAcceso
    {
        bool Login(string dominio, string usuario, string clave);
       
    }
}
