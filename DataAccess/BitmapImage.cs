﻿using MS.Internal;
//using MS.Internal.PresentationCore;
using System.ComponentModel;
using System.IO;
using System.Net.Cache;
using System.Security;
//using System.Windows.Markup;
//using System.Windows.Media.Animation;

namespace CapaLogicaNegocio
{
    public sealed class BitmapImage : BitmapSource, ISupportInitialize, IUriContext
    {
        internal static RequestCachePolicy s_UriCachePolicy = (RequestCachePolicy)null;
        internal static Uri s_UriSource = (Uri)null;
        internal static Stream s_StreamSource = (Stream)null;
        internal static Int32Rect s_SourceRect = Int32Rect.Empty;
        internal static BitmapCreateOptions s_CreateOptions = BitmapCreateOptions.None;
        internal static BitmapCacheOption s_CacheOption = BitmapCacheOption.Default;
        private Uri _baseUri;
        private bool _isDownloading;
        private BitmapDecoder _decoder;
        private RequestCachePolicy _uriCachePolicy;
        private Uri _uriSource;
        private Stream _streamSource;
        private int _decodePixelWidth;
        private int _decodePixelHeight;
        private Rotation _rotation;
        private Int32Rect _sourceRect;
        private BitmapCreateOptions _createOptions;
        private BitmapCacheOption _cacheOption;
        private BitmapSource _finalSource;
        private BitmapImage _cachedBitmapImage;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.UriCachePolicy" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.UriCachePolicy" />.</returns>
        public static readonly DependencyProperty UriCachePolicyProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.UriSource" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.UriSource" />.</returns>
        public static readonly DependencyProperty UriSourceProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.StreamSource" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.StreamSource" />.</returns>
        public static readonly DependencyProperty StreamSourceProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.DecodePixelWidth" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.DecodePixelWidth" />.</returns>
        public static readonly DependencyProperty DecodePixelWidthProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.DecodePixelHeight" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.DecodePixelHeight" />.</returns>
        public static readonly DependencyProperty DecodePixelHeightProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.Rotation" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.Rotation" />.</returns>
        public static readonly DependencyProperty RotationProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.SourceRect" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.SourceRect" />.</returns>
        public static readonly DependencyProperty SourceRectProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.CreateOptions" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.CreateOptions" />.</returns>
        public static readonly DependencyProperty CreateOptionsProperty;
        /// <summary>Identifica la propiedad de dependencia<see cref="P:System.Windows.Media.Imaging.BitmapImage.CacheOption" />.</summary>
        /// <returns>Identificador de la propiedad de dependencia <see cref="P:System.Windows.Media.Imaging.BitmapImage.CacheOption" />.</returns>
        public static readonly DependencyProperty CacheOptionProperty;
        internal const int c_DecodePixelWidth = 0;
        internal const int c_DecodePixelHeight = 0;
        internal const Rotation c_Rotation = Rotation.Rotate0;

        /// <summary>Inicializa una nueva instancia de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        public BitmapImage()
            : base(true)
        {
        }

        /// <summary>Inicializa una nueva instancia de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> con el <see cref="T:System.Uri" /> especificado.</summary>
        /// <param name="uriSource">El <see cref="T:System.Uri" /> que se va a usar como el origen de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</param>
        /// <exception cref="T:System.ArgumentNullException">El valor del parámetro <paramref name="uriSource" /> es null.</exception>
        /// <exception cref="T:System.IO.FileNotFoundException">No se encuentra el archivo especificado por el parámetro <paramref name="uriSource" />.</exception>
        public BitmapImage(Uri uriSource)
            : this(uriSource, (RequestCachePolicy)null)
        {
        }

        /// <summary>Inicializa una nueva instancia de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> con una imagen cuyo origen es un <see cref="T:System.Uri" />, y se almacena en caché conforme a la <see cref="T:System.Net.Cache.RequestCachePolicy" /> especificada.</summary>
        /// <param name="uriSource">El <see cref="T:System.Uri" /> que se va a usar como el origen de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</param>
        /// <param name="uriCachePolicy">
        /// <see cref="T:System.Net.Cache.RequestCachePolicy" /> que especifica los requisitos de almacenamiento en caché de las imágenes obtenidas mediante HTTP.</param>
        /// <exception cref="T:System.ArgumentNullException">El valor del parámetro <paramref name="uriSource" /> es null.</exception>
        /// <exception cref="T:System.IO.FileNotFoundException">No se encuentra el archivo especificado por el parámetro <paramref name="uriSource" />.</exception>
        public BitmapImage(Uri uriSource, RequestCachePolicy uriCachePolicy)
            : base(true)
        {
            if (uriSource == (Uri)null)
                throw new ArgumentNullException(nameof(uriSource));
            this.BeginInit();
            this.UriSource = uriSource;
            this.UriCachePolicy = uriCachePolicy;
            this.EndInit();
        }

        /// <summary>Señala el inicio de la inicialización de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <exception cref="T:System.InvalidOperationException">
        /// <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> se está inicializando en la actualidad. Ya se ha llamado a <see cref="M:System.Windows.Media.Imaging.BitmapImage.BeginInit" />. O bien El objeto <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> ya se ha inicializado.</exception>
        public void BeginInit()
        {
            this.WritePreamble();
            this._bitmapInit.BeginInit();
        }

        /// <summary>Señala el fin de la inicialización de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <exception cref="T:System.InvalidOperationException">El valor de las propiedades <see cref="P:System.Windows.Media.Imaging.BitmapImage.UriSource" /> o <see cref="P:System.Windows.Media.Imaging.BitmapImage.StreamSource" /> es null. O bien Se llamó al método <see cref="M:System.Windows.Media.Imaging.BitmapImage.EndInit" /> sin llamar primero a <see cref="M:System.Windows.Media.Imaging.BitmapImage.BeginInit" />.</exception>
        public void EndInit()
        {
            this.WritePreamble();
            this._bitmapInit.EndInit();
            if (this.UriSource == (Uri)null && this.StreamSource == null)
                throw new InvalidOperationException(MS.Internal.PresentationCore.SR.Get("Image_NeitherArgument", (object)"UriSource", (object)"StreamSource"));
            if (this.UriSource != (Uri)null && !this.UriSource.IsAbsoluteUri && this.CacheOption != BitmapCacheOption.OnLoad)
                this.DelayCreation = true;
            if (this.DelayCreation || this.CreationCompleted)
                return;
            this.FinalizeCreation();
        }

        /// <summary>Obtiene o establece un valor que representa la clase <see cref="T:System.Uri" /> base del contexto de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> actual.</summary>
        /// <returns>Clase <see cref="T:System.Uri" /> base del contexto actual.</returns>
        public Uri BaseUri
        {
            get
            {
                this.ReadPreamble();
                return this._baseUri;
            }
            set
            {
                this.WritePreamble();
                if (this.CreationCompleted || !(this._baseUri != value))
                    return;
                this._baseUri = value;
                this.WritePostscript();
            }
        }

        /// <summary>Obtiene un valor que indica si <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> está descargando contenido actualmente.</summary>
        /// <returns>Es true si la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> está descargando contenido; de lo contrario, es false.</returns>
        public override bool IsDownloading
        {
            get
            {
                this.ReadPreamble();
                return this._isDownloading;
            }
        }

        /// <summary>No se admite. <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> no admite la propiedad <see cref="P:System.Windows.Media.Imaging.BitmapImage.Metadata" /> y provocará una excepción <see cref="T:System.NotSupportedException" />.</summary>
        /// <returns>No se admite.</returns>
        /// <exception cref="T:System.NotSupportedException">Se produce un intento de leer la propiedad <see cref="P:System.Windows.Media.Imaging.BitmapImage.Metadata" />.</exception>
        public override ImageMetadata Metadata
        {
            get
            {
                throw new NotSupportedException(MS.Internal.PresentationCore.SR.Get("Image_MetadataNotSupported"));
            }
        }

        internal override bool CanSerializeToString()
        {
            this.ReadPreamble();
            if (this.UriSource != (Uri)null && this.StreamSource == null && (this.SourceRect.IsEmpty && this.DecodePixelWidth == 0 && (this.DecodePixelHeight == 0 && this.Rotation == Rotation.Rotate0) && (this.CreateOptions == BitmapCreateOptions.None && this.CacheOption == BitmapCacheOption.Default)))
                return this.UriCachePolicy == null;
            return false;
        }

        internal override string ConvertToString(string format, IFormatProvider provider)
        {
            this.ReadPreamble();
            if (!(this.UriSource != (Uri)null))
                return base.ConvertToString(format, provider);
            if (this._baseUri != (Uri)null)
                return BindUriHelper.UriToString(new Uri(this._baseUri, this.UriSource));
            return BindUriHelper.UriToString(this.UriSource);
        }

        private void ClonePrequel(BitmapImage otherBitmapImage)
        {
            this.BeginInit();
            this._isDownloading = otherBitmapImage._isDownloading;
            this._decoder = otherBitmapImage._decoder;
            this._baseUri = otherBitmapImage._baseUri;
        }

        private void ClonePostscript(BitmapImage otherBitmapImage)
        {
            if (this._isDownloading)
            {
                this._decoder.DownloadProgress += new EventHandler<DownloadProgressEventArgs>(this.OnDownloadProgress);
                this._decoder.DownloadCompleted += new EventHandler(this.OnDownloadCompleted);
                this._decoder.DownloadFailed += new EventHandler<ExceptionEventArgs>(this.OnDownloadFailed);
            }
            this.EndInit();
        }

        private BitmapImage CheckCache(Uri uri)
        {
            if (uri != (Uri)null)
            {
                WeakReference weakReference = ImagingCache.CheckImageCache(uri) as WeakReference;
                if (weakReference != null)
                {
                    BitmapImage target = weakReference.Target as BitmapImage;
                    if (target != null)
                        return target;
                    ImagingCache.RemoveFromImageCache(uri);
                }
            }
            return (BitmapImage)null;
        }

        private void InsertInCache(Uri uri)
        {
            if (!(uri != (Uri)null))
                return;
            ImagingCache.AddToImageCache(uri, (object)new WeakReference((object)this));
        }

        [SecurityTreatAsSafe]
        [SecurityCritical]
        internal override void FinalizeCreation()
        {
            this._bitmapInit.EnsureInitializedComplete();
            Uri uri = this.UriSource;
            if (this._baseUri != (Uri)null)
                uri = new Uri(this._baseUri, this.UriSource);
            if ((this.CreateOptions & BitmapCreateOptions.IgnoreImageCache) != BitmapCreateOptions.None)
                ImagingCache.RemoveFromImageCache(uri);
            BitmapImage bitmapImage = this.CheckCache(uri);
            if (bitmapImage != null && bitmapImage.CheckAccess() && (bitmapImage.SourceRect.Equals(this.SourceRect) && bitmapImage.DecodePixelWidth == this.DecodePixelWidth && (bitmapImage.DecodePixelHeight == this.DecodePixelHeight && bitmapImage.Rotation == this.Rotation) && (bitmapImage.CreateOptions & BitmapCreateOptions.IgnoreColorProfile) == (this.CreateOptions & BitmapCreateOptions.IgnoreColorProfile)))
            {
                this._syncObject = bitmapImage.SyncObject;
                lock (this._syncObject)
                {
                    this.WicSourceHandle = bitmapImage.WicSourceHandle;
                    this.IsSourceCached = bitmapImage.IsSourceCached;
                    this._convertedDUCEPtr = bitmapImage._convertedDUCEPtr;
                    this._cachedBitmapImage = bitmapImage;
                }
                this.UpdateCachedSettings();
            }
            else
            {
                BitmapDecoder bitmapDecoder;
                if (this._decoder == null)
                {
                    bitmapDecoder = BitmapDecoder.CreateFromUriOrStream(this._baseUri, this.UriSource, this.StreamSource, this.CreateOptions & ~BitmapCreateOptions.DelayCreation, BitmapCacheOption.None, this._uriCachePolicy, false);
                    if (bitmapDecoder.IsDownloading)
                    {
                        this._isDownloading = true;
                        this._decoder = bitmapDecoder;
                        bitmapDecoder.DownloadProgress += new EventHandler<DownloadProgressEventArgs>(this.OnDownloadProgress);
                        bitmapDecoder.DownloadCompleted += new EventHandler(this.OnDownloadCompleted);
                        bitmapDecoder.DownloadFailed += new EventHandler<ExceptionEventArgs>(this.OnDownloadFailed);
                    }
                }
                else
                {
                    bitmapDecoder = this._decoder;
                    this._decoder = (BitmapDecoder)null;
                }
                if (bitmapDecoder.Frames.Count == 0)
                    throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_NoDecodeFrames"));
                BitmapFrame frame = bitmapDecoder.Frames[0];
                BitmapSource source = (BitmapSource)frame;
                Int32Rect int32Rect1 = this.SourceRect;
                if (int32Rect1.X == 0 && int32Rect1.Y == 0 && (int32Rect1.Width == source.PixelWidth && int32Rect1.Height == source.PixelHeight))
                    int32Rect1 = Int32Rect.Empty;
                if (!int32Rect1.IsEmpty)
                {
                    CroppedBitmap croppedBitmap = new CroppedBitmap();
                    croppedBitmap.BeginInit();
                    BitmapSource bitmapSource = source;
                    croppedBitmap.Source = bitmapSource;
                    Int32Rect int32Rect2 = int32Rect1;
                    croppedBitmap.SourceRect = int32Rect2;
                    croppedBitmap.EndInit();
                    source = (BitmapSource)croppedBitmap;
                    if (this._isDownloading)
                        source.UnregisterDownloadEventSource();
                }
                int num1 = this.DecodePixelWidth;
                int num2 = this.DecodePixelHeight;
                if (num1 == 0 && num2 == 0)
                {
                    num1 = source.PixelWidth;
                    num2 = source.PixelHeight;
                }
                else if (num1 == 0)
                    num1 = source.PixelWidth * num2 / source.PixelHeight;
                else if (num2 == 0)
                    num2 = source.PixelHeight * num1 / source.PixelWidth;
                if (num1 != source.PixelWidth || num2 != source.PixelHeight || this.Rotation != Rotation.Rotate0)
                {
                    TransformedBitmap transformedBitmap = new TransformedBitmap();
                    transformedBitmap.BeginInit();
                    transformedBitmap.Source = source;
                    TransformGroup transformGroup = new TransformGroup();
                    if (num1 != source.PixelWidth || num2 != source.PixelHeight)
                    {
                        int pixelWidth = source.PixelWidth;
                        int pixelHeight = source.PixelHeight;
                        transformGroup.Children.Add((Transform)new ScaleTransform(1.0 * (double)num1 / (double)pixelWidth, 1.0 * (double)num2 / (double)pixelHeight));
                    }
                    if (this.Rotation != Rotation.Rotate0)
                    {
                        double angle = 0.0;
                        switch (this.Rotation)
                        {
                            case Rotation.Rotate0:
                                angle = 0.0;
                                break;
                            case Rotation.Rotate90:
                                angle = 90.0;
                                break;
                            case Rotation.Rotate180:
                                angle = 180.0;
                                break;
                            case Rotation.Rotate270:
                                angle = 270.0;
                                break;
                        }
                        transformGroup.Children.Add((Transform)new RotateTransform(angle));
                    }
                    transformedBitmap.Transform = (Transform)transformGroup;
                    transformedBitmap.EndInit();
                    source = (BitmapSource)transformedBitmap;
                    if (this._isDownloading)
                        source.UnregisterDownloadEventSource();
                }
                if ((this.CreateOptions & BitmapCreateOptions.IgnoreColorProfile) == BitmapCreateOptions.None && frame.ColorContexts != null && (frame.ColorContexts[0] != (ColorContext)null && frame.ColorContexts[0].IsValid) && source.Format.Format != PixelFormatEnum.Default)
                {
                    PixelFormat closestDuceFormat = BitmapSource.GetClosestDUCEFormat(source.Format, source.Palette);
                    bool flag1 = source.Format != closestDuceFormat;
                    ColorContext destinationColorContext;
                    try
                    {
                        destinationColorContext = new ColorContext(closestDuceFormat);
                    }
                    catch (NotSupportedException ex)
                    {
                        destinationColorContext = (ColorContext)null;
                    }
                    if (destinationColorContext != (ColorContext)null)
                    {
                        bool flag2 = false;
                        bool flag3 = false;
                        try
                        {
                            source = (BitmapSource)new ColorConvertedBitmap(source, frame.ColorContexts[0], destinationColorContext, closestDuceFormat);
                            if (this._isDownloading)
                                source.UnregisterDownloadEventSource();
                            flag2 = true;
                        }
                        catch (NotSupportedException ex)
                        {
                        }
                        catch (FileFormatException ex)
                        {
                            flag3 = true;
                        }
                        if (((flag2 ? 0 : (!flag3 ? 1 : 0)) & (flag1 ? 1 : 0)) != 0)
                        {
                            source = (BitmapSource)new ColorConvertedBitmap((BitmapSource)new FormatConvertedBitmap(source, closestDuceFormat, source.Palette, 0.0), frame.ColorContexts[0], destinationColorContext, closestDuceFormat);
                            if (this._isDownloading)
                                source.UnregisterDownloadEventSource();
                        }
                    }
                }
                if (this.CacheOption != BitmapCacheOption.None)
                {
                    try
                    {
                        source = (BitmapSource)new CachedBitmap(source, this.CreateOptions & ~BitmapCreateOptions.DelayCreation, this.CacheOption);
                        if (this._isDownloading)
                            source.UnregisterDownloadEventSource();
                    }
                    catch (Exception ex)
                    {
                        this.RecoverFromDecodeFailure(ex);
                        this.CreationCompleted = true;
                        return;
                    }
                }
                if (bitmapDecoder != null && this.CacheOption == BitmapCacheOption.OnLoad)
                    bitmapDecoder.CloseStream();
                else if (this.CacheOption != BitmapCacheOption.OnLoad)
                    this._finalSource = source;
                this.WicSourceHandle = source.WicSourceHandle;
                this.IsSourceCached = source.IsSourceCached;
                this.CreationCompleted = true;
                this.UpdateCachedSettings();
                if (this.IsDownloading)
                    return;
                this.InsertInCache(uri);
            }
        }

        private void UriCachePolicyPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._uriCachePolicy = e.NewValue as RequestCachePolicy;
        }

        private void UriSourcePropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._uriSource = e.NewValue as Uri;
        }

        private void StreamSourcePropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._streamSource = e.NewValue as Stream;
        }

        private void DecodePixelWidthPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._decodePixelWidth = (int)e.NewValue;
        }

        private void DecodePixelHeightPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._decodePixelHeight = (int)e.NewValue;
        }

        private void RotationPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._rotation = (Rotation)e.NewValue;
        }

        private void SourceRectPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._sourceRect = (Int32Rect)e.NewValue;
        }

        private void CreateOptionsPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            BitmapCreateOptions newValue = (BitmapCreateOptions)e.NewValue;
            this._createOptions = newValue;
            this.DelayCreation = (uint)(newValue & BitmapCreateOptions.DelayCreation) > 0U;
        }

        private void CacheOptionPropertyChangedHook(DependencyPropertyChangedEventArgs e)
        {
            if (e.IsASubPropertyChange)
                return;
            this._cacheOption = (BitmapCacheOption)e.NewValue;
        }

        private static object CoerceUriCachePolicy(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._uriCachePolicy;
            return value;
        }

        private static object CoerceUriSource(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._uriSource;
            return value;
        }

        private static object CoerceStreamSource(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._streamSource;
            return value;
        }

        private static object CoerceDecodePixelWidth(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._decodePixelWidth;
            return value;
        }

        private static object CoerceDecodePixelHeight(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._decodePixelHeight;
            return value;
        }

        private static object CoerceRotation(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._rotation;
            return value;
        }

        private static object CoerceSourceRect(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._sourceRect;
            return value;
        }

        private static object CoerceCreateOptions(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._createOptions;
            return value;
        }

        private static object CoerceCacheOption(DependencyObject d, object value)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            if (!bitmapImage._bitmapInit.IsInInit)
                return (object)bitmapImage._cacheOption;
            return value;
        }

        private void OnDownloadCompleted(object sender, EventArgs e)
        {
            this._isDownloading = false;
            this._decoder.DownloadProgress -= new EventHandler<DownloadProgressEventArgs>(this.OnDownloadProgress);
            this._decoder.DownloadCompleted -= new EventHandler(this.OnDownloadCompleted);
            this._decoder.DownloadFailed -= new EventHandler<ExceptionEventArgs>(this.OnDownloadFailed);
            if ((this.CreateOptions & BitmapCreateOptions.DelayCreation) != BitmapCreateOptions.None)
            {
                this.DelayCreation = true;
            }
            else
            {
                this.FinalizeCreation();
                this._needsUpdate = true;
                this.RegisterForAsyncUpdateResource();
                this.FireChanged();
            }
            this._downloadEvent.InvokeEvents((object)this, (EventArgs)null);
        }

        private void OnDownloadProgress(object sender, DownloadProgressEventArgs e)
        {
            this._progressEvent.InvokeEvents((object)this, e);
        }

        private void OnDownloadFailed(object sender, ExceptionEventArgs e)
        {
            this._isDownloading = false;
            this._decoder.DownloadProgress -= new EventHandler<DownloadProgressEventArgs>(this.OnDownloadProgress);
            this._decoder.DownloadCompleted -= new EventHandler(this.OnDownloadCompleted);
            this._decoder.DownloadFailed -= new EventHandler<ExceptionEventArgs>(this.OnDownloadFailed);
            this._failedEvent.InvokeEvents((object)this, e);
        }

        /// <summary>Crea un clon modificable de <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> y hace copias en profundidad de sus valores.</summary>
        /// <returns>Clon modificable del objeto actual. La propiedad <see cref="P:System.Windows.Freezable.IsFrozen" /> del objeto clonado es false aunque la propiedad <see cref="P:System.Windows.Freezable.IsFrozen" /> del origen sea true.</returns>
        public BitmapImage Clone()
        {
            return (BitmapImage)base.Clone();
        }

        /// <summary>Crea un clon modificable de este objeto <see cref="T:System.Windows.Media.Imaging.BitmapImage" /> y hace copias en profundidad de sus valores actuales. Las referencias de recursos, los enlaces de datos y las animaciones no se copian, pero sus valores actuales sí.</summary>
        /// <returns>Clon modificable del objeto actual. La propiedad <see cref="P:System.Windows.Freezable.IsFrozen" /> del objeto clonado es false aunque la propiedad <see cref="P:System.Windows.Freezable.IsFrozen" /> del origen sea true.</returns>
        public BitmapImage CloneCurrentValue()
        {
            return (BitmapImage)base.CloneCurrentValue();
        }

        private static void UriCachePolicyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.UriCachePolicyPropertyChangedHook(e1);
            DependencyProperty cachePolicyProperty = BitmapImage.UriCachePolicyProperty;
            bitmapImage.PropertyChanged(cachePolicyProperty);
        }

        private static void UriSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.UriSourcePropertyChangedHook(e1);
            DependencyProperty uriSourceProperty = BitmapImage.UriSourceProperty;
            bitmapImage.PropertyChanged(uriSourceProperty);
        }

        private static void StreamSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.StreamSourcePropertyChangedHook(e1);
            DependencyProperty streamSourceProperty = BitmapImage.StreamSourceProperty;
            bitmapImage.PropertyChanged(streamSourceProperty);
        }

        private static void DecodePixelWidthPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.DecodePixelWidthPropertyChangedHook(e1);
            DependencyProperty pixelWidthProperty = BitmapImage.DecodePixelWidthProperty;
            bitmapImage.PropertyChanged(pixelWidthProperty);
        }

        private static void DecodePixelHeightPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.DecodePixelHeightPropertyChangedHook(e1);
            DependencyProperty pixelHeightProperty = BitmapImage.DecodePixelHeightProperty;
            bitmapImage.PropertyChanged(pixelHeightProperty);
        }

        private static void RotationPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.RotationPropertyChangedHook(e1);
            DependencyProperty rotationProperty = BitmapImage.RotationProperty;
            bitmapImage.PropertyChanged(rotationProperty);
        }

        private static void SourceRectPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.SourceRectPropertyChangedHook(e1);
            DependencyProperty sourceRectProperty = BitmapImage.SourceRectProperty;
            bitmapImage.PropertyChanged(sourceRectProperty);
        }

        private static void CreateOptionsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.CreateOptionsPropertyChangedHook(e1);
            DependencyProperty createOptionsProperty = BitmapImage.CreateOptionsProperty;
            bitmapImage.PropertyChanged(createOptionsProperty);
        }

        private static void CacheOptionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BitmapImage bitmapImage = (BitmapImage)d;
            DependencyPropertyChangedEventArgs e1 = e;
            bitmapImage.CacheOptionPropertyChangedHook(e1);
            DependencyProperty cacheOptionProperty = BitmapImage.CacheOptionProperty;
            bitmapImage.PropertyChanged(cacheOptionProperty);
        }

        /// <summary>Obtiene o establece un valor que representa la directiva de almacenamiento en caché de las imágenes procedentes de un origen HTTP.</summary>
        /// <returns>Clase <see cref="T:System.Net.Cache.RequestCachePolicy" /> base del contexto actual. El valor predeterminado es null.</returns>
        [TypeConverter(typeof(RequestCachePolicyConverter))]
        public RequestCachePolicy UriCachePolicy
        {
            get
            {
                return (RequestCachePolicy)this.GetValue(BitmapImage.UriCachePolicyProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.UriCachePolicyProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el origen de la clase <see cref="T:System.Uri" /> de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>Origen de la clase <see cref="T:System.Uri" /> de la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es null.</returns>
        public Uri UriSource
        {
            get
            {
                return (Uri)this.GetValue(BitmapImage.UriSourceProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.UriSourceProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el origen de la secuencia de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>Origen de la secuencia de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es null.</returns>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Stream StreamSource
        {
            get
            {
                return (Stream)this.GetValue(BitmapImage.StreamSourceProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.StreamSourceProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el ancho, en píxeles, en el que se decodifica la imagen.</summary>
        /// <returns>Ancho, en píxeles, en el que se decodifica la imagen. El valor predeterminado es 0.</returns>
        public int DecodePixelWidth
        {
            get
            {
                return (int)this.GetValue(BitmapImage.DecodePixelWidthProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.DecodePixelWidthProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el alto, en píxeles, en el que se decodifica la imagen.</summary>
        /// <returns>Alto, en píxeles, en el que se decodifica la imagen. El valor predeterminado es 0.</returns>
        public int DecodePixelHeight
        {
            get
            {
                return (int)this.GetValue(BitmapImage.DecodePixelHeightProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.DecodePixelHeightProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el ángulo en el que gira <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>El giro que se usa para <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es <see cref="F:System.Windows.Media.Imaging.Rotation.Rotate0" />.</returns>
        public Rotation Rotation
        {
            get
            {
                return (Rotation)this.GetValue(BitmapImage.RotationProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.RotationProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el rectángulo que se utiliza como el origen de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>Rectángulo que se utiliza como el origen de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es <see cref="P:System.Windows.Int32Rect.Empty" />.</returns>
        public Int32Rect SourceRect
        {
            get
            {
                return (Int32Rect)this.GetValue(BitmapImage.SourceRectProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.SourceRectProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece la enumeración <see cref="T:System.Windows.Media.Imaging.BitmapCreateOptions" /> de una clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>La enumeración <see cref="T:System.Windows.Media.Imaging.BitmapCreateOptions" /> usada para esta clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es <see cref="F:System.Windows.Media.Imaging.BitmapCreateOptions.None" />.</returns>
        public BitmapCreateOptions CreateOptions
        {
            get
            {
                return (BitmapCreateOptions)this.GetValue(BitmapImage.CreateOptionsProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.CreateOptionsProperty, (object)value);
            }
        }

        /// <summary>Obtiene o establece el objeto <see cref="T:System.Windows.Media.Imaging.BitmapCacheOption" /> que se va a usar para esta instancia de <see cref="T:System.Windows.Media.Imaging.BitmapImage" />.</summary>
        /// <returns>La enumeración <see cref="T:System.Windows.Media.Imaging.BitmapCacheOption" /> que se usa para la clase <see cref="T:System.Windows.Media.Imaging.BitmapImage" />. El valor predeterminado es <see cref="F:System.Windows.Media.Imaging.BitmapCacheOption.Default" />.</returns>
        public BitmapCacheOption CacheOption
        {
            get
            {
                return (BitmapCacheOption)this.GetValue(BitmapImage.CacheOptionProperty);
            }
            set
            {
                this.SetValueInternal(BitmapImage.CacheOptionProperty, (object)value);
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            return (Freezable)new BitmapImage();
        }

        protected override void CloneCore(Freezable source)
        {
            BitmapImage otherBitmapImage = (BitmapImage)source;
            this.ClonePrequel(otherBitmapImage);
            base.CloneCore(source);
            this.ClonePostscript(otherBitmapImage);
        }

        protected override void CloneCurrentValueCore(Freezable source)
        {
            BitmapImage otherBitmapImage = (BitmapImage)source;
            this.ClonePrequel(otherBitmapImage);
            base.CloneCurrentValueCore(source);
            this.ClonePostscript(otherBitmapImage);
        }

        protected override void GetAsFrozenCore(Freezable source)
        {
            BitmapImage otherBitmapImage = (BitmapImage)source;
            this.ClonePrequel(otherBitmapImage);
            base.GetAsFrozenCore(source);
            this.ClonePostscript(otherBitmapImage);
        }

        protected override void GetCurrentValueAsFrozenCore(Freezable source)
        {
            BitmapImage otherBitmapImage = (BitmapImage)source;
            this.ClonePrequel(otherBitmapImage);
            base.GetCurrentValueAsFrozenCore(source);
            this.ClonePostscript(otherBitmapImage);
        }

        static BitmapImage()
        {
            Type ownerType = typeof(BitmapImage);
            BitmapImage.UriCachePolicyProperty = Animatable.RegisterProperty(nameof(UriCachePolicy), typeof(RequestCachePolicy), ownerType, (object)null, new PropertyChangedCallback(BitmapImage.UriCachePolicyPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceUriCachePolicy));
            BitmapImage.UriSourceProperty = Animatable.RegisterProperty(nameof(UriSource), typeof(Uri), ownerType, (object)null, new PropertyChangedCallback(BitmapImage.UriSourcePropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceUriSource));
            BitmapImage.StreamSourceProperty = Animatable.RegisterProperty(nameof(StreamSource), typeof(Stream), ownerType, (object)null, new PropertyChangedCallback(BitmapImage.StreamSourcePropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceStreamSource));
            BitmapImage.DecodePixelWidthProperty = Animatable.RegisterProperty(nameof(DecodePixelWidth), typeof(int), ownerType, (object)0, new PropertyChangedCallback(BitmapImage.DecodePixelWidthPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceDecodePixelWidth));
            BitmapImage.DecodePixelHeightProperty = Animatable.RegisterProperty(nameof(DecodePixelHeight), typeof(int), ownerType, (object)0, new PropertyChangedCallback(BitmapImage.DecodePixelHeightPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceDecodePixelHeight));
            BitmapImage.RotationProperty = Animatable.RegisterProperty(nameof(Rotation), typeof(Rotation), ownerType, (object)Rotation.Rotate0, new PropertyChangedCallback(BitmapImage.RotationPropertyChanged), new ValidateValueCallback(ValidateEnums.IsRotationValid), false, new CoerceValueCallback(BitmapImage.CoerceRotation));
            BitmapImage.SourceRectProperty = Animatable.RegisterProperty(nameof(SourceRect), typeof(Int32Rect), ownerType, (object)Int32Rect.Empty, new PropertyChangedCallback(BitmapImage.SourceRectPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceSourceRect));
            BitmapImage.CreateOptionsProperty = Animatable.RegisterProperty(nameof(CreateOptions), typeof(BitmapCreateOptions), ownerType, (object)BitmapCreateOptions.None, new PropertyChangedCallback(BitmapImage.CreateOptionsPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceCreateOptions));
            BitmapImage.CacheOptionProperty = Animatable.RegisterProperty(nameof(CacheOption), typeof(BitmapCacheOption), ownerType, (object)BitmapCacheOption.Default, new PropertyChangedCallback(BitmapImage.CacheOptionPropertyChanged), (ValidateValueCallback)null, false, new CoerceValueCallback(BitmapImage.CoerceCacheOption));
        }
    }
}
