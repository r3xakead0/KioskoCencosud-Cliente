﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Network
    {

        const int PORT_NO = 9876;

        /// <summary>
        /// Enviar comando al Kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <param name="command">Comando a enviar</param>
        /// <returns></returns>
        private string SendCommand(string hostname, string command)
        {
            try
            {
                string response = "";

                using (var client = new TcpClient())
                {

                    var result = client.BeginConnect(hostname, PORT_NO, null, null);
                    result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3));

                    if (client.Connected)
                    {
                        byte[] bytesToSend, bytesToRead;
                        NetworkStream nwStream;
                        int bytesRead = 0;

                        bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);

                        nwStream = client.GetStream();
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                        bytesToRead = new byte[client.ReceiveBufferSize];
                        bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                        string textToRead = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);

                        response = textToRead.Trim();

                        client.EndConnect(result);
                    }

                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reinicio del Kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public string Restart(string hostname)
        {
            try
            {
                string textToSend = "RESTART";
                string textToResponse = "";

                textToResponse = this.SendCommand(hostname, textToSend);

                return textToResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Estado del Kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public string Status(string hostname)
        {
            try
            {
                string textToSend = "STATUS";
                string textToResponse = "";

                textToResponse = this.SendCommand(hostname, textToSend);

                return textToResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
