﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Cupones.DataAccess
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Pangui
    {

        #region Construtores

        public Pangui()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public string[] CuponesSeriados(string nroBonus, string nroDoc, string[] cupones, string hostname, string tienda)
        {
            string[] cuponesSeriados = null;
            try
            {

                string strCupones = string.Join(",", cupones);
                string strFecha = DateTime.Now.ToString("yyyyMMddHHmmss");

                var proxyClient = new CuponesWSService();

                var proxyResult = proxyClient.getCuponesSeriados(nroBonus, strCupones, hostname, tienda, nroDoc, strFecha);
                if (proxyResult != null && proxyResult.Length > 0)
                {
                    int separatorAt = proxyResult.IndexOf("@@@");

                    string timespan = proxyResult.Substring(0, separatorAt);
                    string strData = proxyResult.Substring(separatorAt + 3);

                    string[] data = strData.Split('|');

                    cuponesSeriados = new string[data.Length];

                    for (int i = 0; i < data.Length; i++)
                    {
                        string[] cupon = data[i].Split(',');
                        cuponesSeriados[i] = cupon[i];
                    }
                }

                return cuponesSeriados;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
