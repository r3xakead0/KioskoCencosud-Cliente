﻿using System;
using System.Drawing;
using System.IO;

namespace Cupones.Common
{
    public class Banner : Base
    {

        private const string DIRECTORY = "Banners";

        /// <summary>
        /// Constructor
        /// </summary>
        public Banner()
        {

        }

        /// <summary>
        /// Obtener imagen de banner     
        /// </summary>
        /// <param name="path">Ruta relativa del archivo de imagen</param>
        /// <returns></returns>
        public Image GetImage(string path = "")
        {
            try
            {
                if (path.Trim().Length == 0 )
                    path = DIRECTORY + @"\BannerDefault.jpg";

                return this.GetImageFromFile(path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Banners
        /// </summary>
        private void ClearForder()
        {
            try
            {
                string pathDirLocal = Path.Combine("Cache", DIRECTORY);

                base.ClearForder(pathDirLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Banners
        /// </summary>
        public void DownloadFolder(bool overwrite = false)
        {
            try
            {
                string pathDirLocal = Path.Combine("Cache", DIRECTORY);

                if (overwrite)
                    this.ClearForder(pathDirLocal);

                string pathDirFtp = Path.Combine("Imagenes", DIRECTORY);

                base.DownloadFolder(pathDirFtp, pathDirLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
