﻿using System;
using System.IO;

namespace Cupones.Common
{
    public class Legal : Base
    {

        private string Company = "";

        /// <summary>
        /// Constructor
        /// </summary>
        public Legal(string company)
        {
            this.Company = company;
        }

        /// <summary>
        /// Descargar folder de imagenes de Banners
        /// </summary>
        public void DownloadFolder(bool overwrite = false)
        {
            try
            {
                string directory = "Legal";

                string pathDirLocal = Path.Combine("Cache", directory);

                if (overwrite)
                    base.ClearForder(pathDirLocal);

                string pathDirFtp = Path.Combine(directory);

                base.DownloadFolder(pathDirFtp, pathDirLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener texto de terminos
        /// </summary>
        /// <returns></returns>
        public string GetTermsAndConditions()
        {
            string strTerms = "";
            try
            {
                string filename = this.Company + ".txt";
                string directory = "Legal";
                string pathLegal = Path.Combine(directory, filename);

                strTerms = this.GetTextFromFile(pathLegal);

                return strTerms;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener texto de politicas
        /// </summary>
        /// <returns></returns>
        public string GetPoliciesAndPrivacy()
        {
            string strPolicies = "";
            try
            {
                string filename = "pp_" + this.Company + ".txt";
                string directory = "Legal";
                string pathLegal = Path.Combine(directory, filename);

                strPolicies = this.GetTextFromFile(pathLegal);

                return strPolicies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
