﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BarcodeLib;
using BE = Cupones.BusinessEntity;

namespace Cupones.Common
{
    public class Coupon : Base
    {
        #region Values
        private string Company = ""; //WONG o METRO
        #endregion

        #region Constants
        private const int descuentoLen = 7; //Maximo 7 caracteres para el descuento
        private const int mensajeLen = 22; //Maximo 22 caracteres para el mensaje
        private const int legalLen = 40; //Maximo 40 caracteres por linea para mensaje legal del cupon
        private const int legalSuperLen = 48; //Maximo 40 caracteres por linea para mensaje legal del super cupon
        #endregion

        #region Images
        private Image CouponNormal = null; //Imagen base del Cupon del tipo nomal
        private Image CouponDrink = null; //Imagen base del Cupon para bebidas alcoholicas
        private Image SuperCoupoGift = null; //Imagen base del Super Cupon del tipo Regalo
        private Image SuperCoupoNormal = null; //Imagen base del Super Cupon del tipo Normal
        private Image SuperCouponTime = null; //Imagen base del Super Cupon del tipo Tiempo
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public Coupon(string company)
        {
            try
            {
                this.Company = company;

                if (company.ToUpper().Equals("WONG"))
                {
                    this.CouponNormal = global::Cupones.Common.Properties.Resources.cupon_normal_wong;
                    this.CouponDrink = global::Cupones.Common.Properties.Resources.cupon_bebida_wong;
                    this.SuperCoupoGift = global::Cupones.Common.Properties.Resources.cupon_regalo_wong;
                    this.SuperCoupoNormal = global::Cupones.Common.Properties.Resources.cupon_super_wong;
                    this.SuperCouponTime = global::Cupones.Common.Properties.Resources.cupon_horario_wong;
                }
                else if (company.ToUpper().Equals("METRO"))
                {
                    this.CouponNormal = global::Cupones.Common.Properties.Resources.cupon_normal_metro;
                    this.CouponDrink = global::Cupones.Common.Properties.Resources.cupon_bebida_metro;
                    this.SuperCoupoGift = global::Cupones.Common.Properties.Resources.cupon_regalo_metro;
                    this.SuperCoupoNormal = global::Cupones.Common.Properties.Resources.cupon_super_metro;
                    this.SuperCouponTime = global::Cupones.Common.Properties.Resources.cupon_horario_metro;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Obtener la imagen minuatura del cupon
        /// </summary>
        /// <param name="cuopon">Objeto datos de cupon</param>
        /// <returns></returns>
        public Image ImagePreview(BE.Cupon cuopon)
        {
            Image imgCoupon = null;

            try
            {
                if (cuopon != null)
                {
                    #region Imagen del la Miniatura del Cupon
                    Image imgPreview = this.GetImageFromUrl(cuopon.ImagenPath);
                    //Image imgPreview = this.GetImageFromFtp(cuopon.ImagenPath);
                    int widthImage = 355;
                    int heightImage = 163;
                    var bmpPreview = new Bitmap(imgPreview, new Size(widthImage, heightImage)); // Ajustar tamaño de la imagen
                    cuopon.Imagen = (Image)(bmpPreview);
                    #endregion

                    #region Parrafo de Descuento del Cupon
                    string descuento = cuopon.Descuento.Trim();
                    descuento = (descuento.Length > descuentoLen ? descuento.Substring(1, descuentoLen) : descuento);
                    #endregion

                    #region Parrafo de Descripcion del Cupon
                    string mensaje01 = cuopon.Mensaje1.Trim();
                    mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                    string mensaje02 = cuopon.Mensaje2.Trim();
                    mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                    string mensaje03 = cuopon.Mensaje3.Trim();
                    mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                    #endregion

                    #region Generar Imagen Minuatura del Cupon

                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    using (Graphics graphics = Graphics.FromImage(imgPreview))
                    {
                        using (Font arialFont = new Font("Arial Black", 25))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Red, 230f, 30f, drawFormat);
                        }

                        using (Font arialFont = new Font("Arial Black", 13))
                        {
                            graphics.DrawString(mensaje01, arialFont, Brushes.Black, 230f, 70f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.Black, 230f, 90f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.Black, 230f, 110f, drawFormat);
                        }
                    }

                    #endregion

                    imgCoupon = imgPreview;
                }
                   
                return imgCoupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen minuatura del cupon (multicupon)
        /// </summary>
        /// <param name="cuopon">Objeto datos de cupon</param>
        /// <param name="check">Seleccionado o no</param>
        /// <returns></returns>
        public Image ImagePreviewMulti(BE.Cupon cuopon, bool check = false)
        {
            Image imgCoupon = null;

            try
            {

                if (cuopon != null)
                {
                    #region Imagen del la Miniatura del Cupon
                    Image imgPreview = this.GetImageFromUrl(cuopon.ImagenPath);
                    //Image imgPreview = this.GetImageFromFtp(cuopon.ImagenPath);
                    int widthImage = 355;
                    int heightImage = 163;
                    var bmpPreview = new Bitmap(imgPreview, new Size(widthImage, heightImage)); // Ajustar tamaño de la imagen
                    cuopon.Imagen = (Image)(bmpPreview);
                    #endregion

                    #region Parrafo de Descuento del Cupon
                    string descuento = cuopon.Descuento.Trim();
                    descuento = (descuento.Length > descuentoLen ? descuento.Substring(1, descuentoLen) : descuento);
                    #endregion

                    #region Parrafo de Descripcion del Cupon
                    string mensaje01 = cuopon.Mensaje1.Trim();
                    mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                    string mensaje02 = cuopon.Mensaje2.Trim();
                    mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                    string mensaje03 = cuopon.Mensaje3.Trim();
                    mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                    #endregion

                    #region Imagen de Check y Uncheak (Selecciono y No selecciono)
                    Bitmap bmpCheck = global::Cupones.Common.Properties.Resources.check; //Imagen de Check (Seleccionado)
                    Bitmap bmpUncheck = global::Cupones.Common.Properties.Resources.uncheck; //Imagen de UnCheck (No Seleccionado)
                    #endregion

                    #region Generar Imagen Minuatura del Cupon

                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    using (Graphics graphics = Graphics.FromImage(imgPreview))
                    {
                        using (Font arialFont = new Font("Arial Black", 25))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Red, 230f, 30f, drawFormat);
                        }

                        using (Font arialFont = new Font("Arial Black", 13))
                        {
                            graphics.DrawString(mensaje01, arialFont, Brushes.Black, 230f, 70f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.Black, 230f, 90f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.Black, 230f, 110f, drawFormat);
                        }

                        graphics.DrawImage((check ? bmpCheck : bmpUncheck), 285, 120, 27, 28);
                    }

                    #endregion

                    imgCoupon = imgPreview;
                }
                    
                return imgCoupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen de super cupon de horario
        /// </summary>
        /// <param name="superCoupon">Objeto datos de super cupon</param>
        /// <returns></returns>
        public Image ImagePreviewSuperTime(BE.Cupon superCoupon)
        {
            Image imgCoupon = null;

            try
            {
                #region Validar formato de imagen base
                if (this.SuperCouponTime == null)
                    return null;
                #endregion

                if (superCoupon != null)
                {

                    #region Imagen del Super Cupon
                    Image imgContenedorSuperCupon = this.SuperCouponTime;

                    Image imgContenidoSuperCupon = this.GetImageFromUrl(superCoupon.ImagenPath);
                    superCoupon.Imagen = imgContenidoSuperCupon;
                    #endregion

                    #region Parrafo de Descuento del Cupon
                    string descuento = superCoupon.Descuento.Trim();
                    descuento = (descuento.Length > descuentoLen ? descuento.Substring(1, descuentoLen) : descuento);
                    #endregion

                    #region Parrafo de Descripcion del Cupon
                    string mensaje01 = superCoupon.Mensaje1.Trim();
                    mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                    string mensaje02 = superCoupon.Mensaje2.Trim();
                    mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                    string mensaje03 = superCoupon.Mensaje3.Trim();
                    mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                    #endregion

                    #region Generar Imagen Minuatura del Cupon

                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    using (Graphics graphics = Graphics.FromImage(imgContenedorSuperCupon))
                    {

                        #region Imagen del Contenido

                        var bmpContenidoSuperCupon = new Bitmap(imgContenidoSuperCupon);
                        graphics.DrawImage(bmpContenidoSuperCupon, 90f, 330f, 145f, 208f);

                        #endregion

                        #region Titulo
                        using (Font arialFont = new Font("Verdana", 25))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Orange, 170f, 490f, drawFormat);

                        }
                        #endregion

                        #region Descripcion
                        using (Font arialFont = new Font("Verdana", 15))
                        {

                            graphics.DrawString(mensaje01, arialFont, Brushes.White, 170f, 540f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.White, 170f, 560f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.White, 170f, 580f, drawFormat);
                        }
                        #endregion
                    }

                    #endregion

                    imgCoupon = imgContenedorSuperCupon;
                }

                return imgCoupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen de super cupon de tiempo
        /// </summary>
        /// <param name="superCoupon">Objeto datos de super cupon</param>
        /// <returns></returns>
        public Image ImagePreviewSuperNormal(BE.Cupon superCoupon)
        {
            Image imgCoupon = null;

            try
            {
                #region Validar formato de imagen base
                if (this.SuperCoupoNormal == null)
                    return null;
                #endregion

                if (superCoupon != null)
                {

                    #region Imagen del Super Cupon
                    Image imgContenedorSuperCupon = this.SuperCoupoNormal;

                    Image imgContenidoSuperCupon = this.GetImageFromUrl(superCoupon.ImagenPath);
                    superCoupon.Imagen = imgContenidoSuperCupon;
                    #endregion

                    #region Parrafo de Descuento del Cupon
                    string descuento = superCoupon.Descuento.Trim();
                    descuento = (descuento.Length > descuentoLen ? descuento.Substring(1, descuentoLen) : descuento);
                    #endregion

                    #region Parrafo de Descripcion del Cupon
                    string mensaje01 = superCoupon.Mensaje1.Trim();
                    mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                    string mensaje02 = superCoupon.Mensaje2.Trim();
                    mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                    string mensaje03 = superCoupon.Mensaje3.Trim();
                    mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                    #endregion

                    #region Generar Imagen Minuatura del Cupon

                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    using (Graphics graphics = Graphics.FromImage(imgContenedorSuperCupon))
                    {

                        #region Imagen del Contenido

                        var bmpContenidoSuperCupon = new Bitmap(imgContenidoSuperCupon);
                        graphics.DrawImage(bmpContenidoSuperCupon, 20, 40, 290, 415);

                        #endregion

                        #region Titulo
                        using (Font arialFont = new Font("Arial Black", 40))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Orange, 170f, 400f, drawFormat);

                        }
                        #endregion

                        #region Sub Titulo
                        using (Font arialFont = new Font("Arial Black", 25))
                        {

                            graphics.DrawString(mensaje01, arialFont, Brushes.White, 170f, 480f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.White, 170f, 510f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.White, 170f, 540f, drawFormat);
                        }
                        #endregion

                        #region Descripcion
                        using (Font arialFont = new Font("Arial", 8))
                        {

                            string description = superCoupon.Descripcion ?? "";
                            description = description.ToUpper();

                            string[] lineDesc = Regex.Matches(description, ".{1," + legalSuperLen + "}")
                                                .Cast<Match>()
                                                .Select(m => m.Value)
                                                .ToArray();

                            int floaty = 664;
                            foreach (var line in lineDesc)
                            {
                                graphics.DrawString(line.Trim(), arialFont, Brushes.Black, 160f, floaty, drawFormat);
                                floaty += 15;
                            }
                        }
                        #endregion
                    }

                    #endregion

                    imgCoupon = imgContenedorSuperCupon;
                }

                return imgCoupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen de super cupon de regalo
        /// </summary>
        /// <param name="superCoupon">Objeto datos de super cupon</param>
        /// <returns></returns>
        public Image ImagePreviewSuperGift(BE.Cupon superCoupon)
        {
            Image imgCoupon = null;

            try
            {
                #region Validar formato de imagen base
                if (this.SuperCoupoGift == null)
                    return null;
                #endregion

                if (superCoupon != null)
                {
                    imgCoupon = this.SuperCouponTime;
                }

                return imgCoupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen de impresion del cupon
        /// </summary>
        /// <param name="company">Nombre de Ambito o Empresa</param>
        /// <param name="store">Codigo de Tienda</param>
        /// <param name="client">Objeto datos de cliente</param>
        /// <param name="cuopon">Objeto datos de cupon</param>
        /// <returns></returns>
        public Image ImageForPrint(BE.DetalleCupon cuopon)
        {
            Image impresionCupon = null;
            try
            {

                #region Validar formato de imagen base
                if (this.CouponNormal == null || this.CouponDrink == null)
                    return null;
                #endregion

                if (cuopon != null)
                {

                    #region Imagen del Cupon
                    Image imgCoupon = this.GetImageFromUrl(cuopon.ImagenPath);
                    //Image imgCoupon = this.GetImageFromFtp(cuopon.ImagenPath);
                    cuopon.Imagen = imgCoupon;
                    #endregion

                    #region Parrafo de Aviso Legal
                    string legal = cuopon.LegalMensaje.Trim();
                    string[] linelegal = Regex.Matches(legal, ".{1," + legalLen + "}")
                                        .Cast<Match>()
                                        .Select(m => m.Value)
                                        .ToArray();
                    #endregion

                    #region Parrafo de Descuento del Cupon
                    string descuento = cuopon.Descuento.Trim();
                    int descuentoSize = 55;
                    float descuentoY = 180;
                    if (descuento.Length > descuentoLen)
                    {
                        if (descuento.Length > descuentoLen * 2)
                        {
                            descuento = descuento.Substring(0, descuentoLen * 2);
                        }
                        descuentoSize = 25;
                        descuentoY = 200;
                    }
                    #endregion

                    #region Parrafo de Descripcion del Cupon
                    string mensaje01 = cuopon.Mensaje1.Trim();
                    mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                    string mensaje02 = cuopon.Mensaje2.Trim();
                    mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                    string mensaje03 = cuopon.Mensaje3.Trim();
                    mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                    #endregion

                    #region Imagen del Fondo de Ticket
                    Image imgBaseCoupon = null;
                    if (cuopon.LegalTipo == 1) //Bebida Alcoholicas
                        imgBaseCoupon = this.CouponDrink;
                    else //Normal
                        imgBaseCoupon = this.CouponNormal;
                    #endregion

                    #region Ajustar tamaño del Fondo de Ticket
                    int widthCoupon = 534;
                    int heightCoupon = 1230;
                    var tamanhoCupon = new Size(widthCoupon, heightCoupon);
                    imgBaseCoupon = (Image)(new Bitmap(imgBaseCoupon, tamanhoCupon));
                    #endregion

                    #region Imagen del Codigo de Barras
                    Image imgBarcode = null;
                    string codigoBarra = "";
                    using (var barcode = new Barcode())
                    {
                        if (cuopon.CodigoBarra.Trim().Length == 13)
                            codigoBarra = cuopon.CodigoBarra.Trim();
                        else
                            codigoBarra = new string('0', 13);

                        barcode.IncludeLabel = false;
                        imgBarcode = barcode.Encode(TYPE.CODE128, codigoBarra, Color.Black, Color.White, 400, 100);
                    }
                    #endregion

                    #region Formato de Texto
                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;
                    #endregion

                    using (Graphics graphics = Graphics.FromImage(imgBaseCoupon))
                    {

                        #region Cabecera
                        if (cuopon.LegalTipo == 1) //Bebida Alcoholicas
                        {
                            using (Font arialFont = new Font("Arial Black", descuentoSize))
                            {
                                graphics.DrawString(descuento, arialFont, Brushes.Black, 270f, 150f, drawFormat);

                            }
                            using (Font arialFont = new Font("Arial Black", 20))
                            {
                                graphics.DrawString(mensaje01, arialFont, Brushes.Black, 270f, 230f, drawFormat);
                                graphics.DrawString(mensaje02, arialFont, Brushes.Black, 270f, 265f, drawFormat);
                                graphics.DrawString(mensaje03, arialFont, Brushes.Black, 270f, 300f, drawFormat);
                            }
                        }
                        else //Normal
                        {
                            using (Font arialFont = new Font("Arial Black", descuentoSize))
                            {
                                graphics.DrawString(descuento, arialFont, Brushes.Black, 270f, descuentoY, drawFormat);

                            }
                            using (Font arialFont = new Font("Arial Black", 25))
                            {
                                graphics.DrawString(mensaje01, arialFont, Brushes.Black, 270f, 255f, drawFormat);
                                graphics.DrawString(mensaje02, arialFont, Brushes.Black, 270f, 290f, drawFormat);
                                graphics.DrawString(mensaje03, arialFont, Brushes.Black, 270f, 320f, drawFormat);
                            }
                        }

                        #endregion

                        #region Cuerpo

                        string mensajeCanjeTienda = "Canjéalo en la Tienda";
                        string mensajeCanjeOnline = "Canjéalo en " + cuopon.Ambito.ToUpper() + ".pe";
                        string mensajeOnlineCodigo = "Ingresando el código: ";

                        using (Font arialFont = new Font("Arial Black", 25))
                        {

                            int width = 250;
                            int height = 125;

                            if (cuopon.FlagCanal == 1) //Canje Tienda
                            {
                                graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 270f, 505f, drawFormat);
                                graphics.DrawImage(imgBarcode, 70, 550, 400, 100);
                                graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 650f, drawFormat);

                                // Create Point for upper-left corner of image.
                                Point ulCorner = new Point(400, 400);
                                int x = 147;
                                int y = 390;

                                // Draw image to screen.
                                graphics.DrawImage(cuopon.Imagen, x, y, width, height);

                                if (cuopon.FlagSeriado == 1)
                                {
                                    string numeroBonus = cuopon.BonusPrincipalSeriado.Trim();
                                    string bonusSeriado = this.SerialBonus(numeroBonus);

                                    graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 680f, drawFormat);
                                }
                            }
                            else if (cuopon.FlagCanal == 2) //Canje Online
                            {
                                graphics.DrawString(mensajeCanjeOnline, arialFont, Brushes.Black, 250f, 560f, drawFormat);
                                graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 250f, 590f, drawFormat);
                                graphics.DrawString(cuopon.CodigoOnline, arialFont, Brushes.Black, 250f, 620f, drawFormat);

                                // Create Point for upper-left corner of image.
                                Point ulCorner = new Point(400, 400);
                                int x = 147;
                                int y = 415;

                                // Draw image to screen.
                                graphics.DrawImage(cuopon.Imagen, x, y, width, height);

                                if (cuopon.FlagSeriado == 1)
                                {
                                    string numeroBonus = cuopon.BonusPrincipalSeriado.Trim();
                                    string bonusSeriado = this.SerialBonus(numeroBonus);

                                    graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 730f, drawFormat);
                                }
                            }
                            else if (cuopon.FlagCanal == 3) //Canje Tienda y online
                            {
                                if (cuopon.LegalTipo == 1) //Bebida Alcoholicas
                                {
                                    graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 270f, 420f, drawFormat);
                                    graphics.DrawImage(imgBarcode, 70, 465, 400, 100);
                                    graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 555f, drawFormat);

                                    graphics.DrawString("o " + mensajeCanjeOnline, arialFont, Brushes.Black, 270f, 620f, drawFormat);
                                    graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 270f, 650f, drawFormat);
                                    graphics.DrawString(cuopon.CodigoOnline, arialFont, Brushes.Black, 270f, 690f, drawFormat);

                                    // Create Point for upper-left corner of image.
                                    Point ulCorner = new Point(400, 400);
                                    int x = 147;
                                    int y = 310;

                                    // Draw image to screen.
                                    graphics.DrawImage(cuopon.Imagen, x, y, width, height);

                                    if (cuopon.FlagSeriado == 1)
                                    {
                                        string numeroBonus = cuopon.BonusPrincipalSeriado.Trim();
                                        string bonusSeriado = this.SerialBonus(numeroBonus);

                                        graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 590f, drawFormat);
                                    }
                                }
                                else //Normal
                                {
                                    graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 260f, 550f, drawFormat);

                                    graphics.DrawImage(imgBarcode, 70, 600, 400, 100);
                                    graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 690f, drawFormat);

                                    graphics.DrawString("o " + mensajeCanjeOnline, arialFont, Brushes.Black, 270f, 755f, drawFormat);
                                    graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 270f, 795f, drawFormat);
                                    graphics.DrawString(cuopon.CodigoOnline, arialFont, Brushes.Black, 270f, 825f, drawFormat);

                                    // Create Point for upper-left corner of image.
                                    Point ulCorner = new Point(400, 400);
                                    int x = 147;
                                    int y = 415;

                                    // Draw image to screen.
                                    graphics.DrawImage(cuopon.Imagen, x, y, width, height);

                                    if (cuopon.FlagSeriado == 1)
                                    {
                                        string numeroBonus = cuopon.BonusPrincipalSeriado.Trim();
                                        string bonusSeriado = this.SerialBonus(numeroBonus);

                                        graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 725f, drawFormat);
                                    }
                                }
                            }

                        }
                        #endregion

                        #region Pie - Descripcion Legal
                        using (Font arialFont = new Font("Arial Black", 12))
                        {

                            int floaty = 0;
                            if (cuopon.LegalTipo == 1) //Bebida Alcoholicas
                                floaty = 730;
                            else //Normal
                                floaty = 890;

                            foreach (var line in linelegal)
                            {
                                graphics.DrawString(line.Trim(), arialFont, Brushes.Black, 270f, floaty, drawFormat);
                                floaty += 20;
                            }
                        }
                        #endregion

                        #region Lateral Izquierdo
                        using (Font arialFont = new Font("Arial Black", 15))
                        {
                            float x = 20.0F;
                            float y = 310.0F;

                            SolidBrush drawBrush = new SolidBrush(Color.Black);
                            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                            graphics.DrawString(cuopon.Vigencia, arialFont, drawBrush, x, y, drawFormat);
                        }

                        string fecha = DateTime.Now.ToString("dd/MM/yyyy");
                        string tiendaFecha = cuopon.Tienda + " - " + fecha;
                        using (Font arialFont = new Font("Arial Black", 15))
                        {
                            float x = 20.0F;
                            float y = 700.0F;

                            SolidBrush drawBrush = new SolidBrush(Color.Black);
                            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                            graphics.DrawString(tiendaFecha, arialFont, drawBrush, x, y, drawFormat);
                        }
                        #endregion

                        #region Lateral Derecho

                        string docLateral = "";

                        if (cuopon.NroDocumento.Length > 7)
                        {
                            string tipDocLateral = (cuopon.TipoDocumento == 1 ? "D" : "B");
                            string nroDocLateral = cuopon.NroDocumento.Substring(0, 2) +
                                new String('*', cuopon.NroDocumento.Length - 4) +
                                cuopon.NroDocumento.Substring(cuopon.NroDocumento.Length - 2);

                            docLateral = tipDocLateral + "-" + nroDocLateral;
                        }

                        using (Font arialFont = new Font("Arial Black", 15))
                        {
                            float x = 480.0F;
                            float y = 310.0F;

                            SolidBrush drawBrush = new SolidBrush(Color.Black);
                            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                            graphics.DrawString(docLateral, arialFont, drawBrush, x, y, drawFormat);
                        }
                        #endregion

                    }

                    impresionCupon = imgBaseCoupon;
                }

                return impresionCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Oculta el numero del bonus
        /// </summary>
        /// <param name="numberBonus">Numero del Bonus</param>
        /// <returns></returns>
        private string SerialBonus(string numberBonus)
        {
            try
            {
                string bonusSeriado = "";

                string numeroBonus = numberBonus.Trim();
                if (numeroBonus.Length == 19)
                {
                    int cntVisibleDerecha = 3;
                    int cntVisibleIzquierda = 3;
                    int cntSeriado = numeroBonus.Length - cntVisibleDerecha - cntVisibleIzquierda;

                    bonusSeriado = numeroBonus.Substring(0, cntVisibleDerecha)
                                + new String('*', cntSeriado)
                                + numeroBonus.Substring(numeroBonus.Length - cntVisibleIzquierda);
                }

                return bonusSeriado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Descargar folder de imagenes
        /// </summary>
        private new void ClearForder(string directory)
        {
            try
            {
                string pathDirLocal = Path.Combine("Cache", directory);
                base.ClearForder(pathDirLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes
        /// </summary>
        private void DownloadFolder(string directory)
        {
            try
            {
                string pathDirLocal = Path.Combine("Cache", directory);
                string pathDirFtp = Path.Combine("Imagenes", directory);

                base.DownloadFolder(pathDirFtp, pathDirLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Mis Cupones
        /// </summary>
        public void DownloadFolderMisCupones(bool overwrite = false)
        {
            try
            {
                string directory = "MisCupones";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Super Cupones
        /// </summary>
        public void DownloadFolderSuperCupones(bool overwrite = false)
        {
            try
            {
                string directory = "SuperCupon";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Cupones de Marcas
        /// </summary>
        public void DownloadFolderCuponesMarcas(bool overwrite = false)
        {
            try
            {
                string directory = "CuponesMarca";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Cupones de Marcas
        /// </summary>
        public void DownloadFolderCuponesLugar(bool overwrite = false)
        {
            try
            {
                string directory = "CuponesLugar";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Cupones del Club W
        /// </summary>
        public void DownloadFolderCuponesClub(bool overwrite = false)
        {
            try
            {
                string directory = "ClubW";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Cupones de Marcas
        /// </summary>
        public void DownloadFolderCuponesBonus(bool overwrite = false)
        {
            try
            {
                string directory = "CuponesBonus";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Descargar folder de imagenes de Cupones de Marcas
        /// </summary>
        public void DownloadFolderCuponesComerciales(bool overwrite = false)
        {
            try
            {
                string directory = "CuponesComerciales";
                if (overwrite)
                    this.ClearForder(directory);
                this.DownloadFolder(directory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
