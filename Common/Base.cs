﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using FluentFTP;
using System.Configuration;

namespace Cupones.Common
{
    public class Base
    {

        private string folderCache = "";
        private string baseRoot = "";

        #region Propiedades
        public string BaseRoot { get => baseRoot; set => baseRoot = value; }
        public string FolderCache { get => folderCache; set => folderCache = value; }
        #endregion

        public Base()
        {
            folderCache = "Cache";
            baseRoot = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        /// <summary>
        /// Obtener credenciales para el servidor FTP
        /// </summary>
        /// <param name="hostname">Retorna el hostname o IP del FTP</param>
        /// <param name="username">Retorna el Uuuario del FTP</param>
        /// <param name="password">Retorna la clave del FTP</param>
        private void GetConfigFtp(out string hostname, out string username, out string password)
        {
            try
            {
                string host = ConfigurationManager.AppSettings["FtpHost"];
                string user = ConfigurationManager.AppSettings["FtpUser"];
                string pass = ConfigurationManager.AppSettings["FtpPass"];

                hostname = host;
                username = user;
                password = pass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Elimina la carpeta de la ubicación Local
        /// </summary>
        /// <param name="pathFolderLocal">Ruta absoluta de la carpeta origen en la ubicación Local. Ejm: C://Wong/Cache/Imagenes/MisCupones/</param>
        internal void ClearForder(string pathFolderLocal)
        {
            try
            {
                //Validar si existe la carpeta en la ubicación local, en caso exista se eliminará
                if (Directory.Exists(pathFolderLocal) == true)
                    Directory.Delete(pathFolderLocal, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Decarga la carpeta desde el servidor FTP la ubicación Local
        /// </summary>
        /// <param name="pathFolderFtp">Ruta relativa de la carpeta origen en el servidor FTP. Ejm: /Imagenes/MisCupones/</param>
        /// <param name="pathFolderLocal">Ruta absoluta de la carpeta origen en la ubicación Local. Ejm: C://Wong/Cache/Imagenes/MisCupones/</param>
        internal void DownloadFolder(string pathFolderFtp, string pathFolderLocal)
        {
            try
            {
                //Validar si existe la carpeta en la ubicación local, en caso no exista se creará
                if (Directory.Exists(pathFolderLocal) == false)
                    Directory.CreateDirectory(pathFolderLocal);

                this.GetConfigFtp(out string hostname, out string username, out string password);

                using (var ftp = new FtpClient())
                {

                    ftp.Encoding = Encoding.GetEncoding(1252);
                    ftp.Host = hostname;
                    ftp.Credentials = new NetworkCredential(username, password);

                    ftp.RetryAttempts = 3;

                    foreach (FtpListItem fileFtp in ftp.GetListing(pathFolderFtp))
                    {
                        if (fileFtp.Type == FtpFileSystemObjectType.File) //Solo archivos
                        {
                            string filename = fileFtp.Name;

                            string pathFileFtp = Path.Combine(pathFolderFtp, filename);
                            string pathFileLocal = Path.Combine(pathFolderLocal, filename);

                            if (File.Exists(pathFileLocal) == false)
                            {
                                //Descargar el archivo
                                ftp.DownloadFile(pathFileLocal, pathFileFtp, true, FtpVerify.Retry);
                            }
                            else
                            {
                                var fileInfo = new FileInfo(pathFileLocal);

                                long sizeFileLocal = fileInfo.Length;
                                long sizeFileFtp = ftp.GetFileSize(pathFileFtp);
                                
                                if (sizeFileLocal == 0) //Si el archivo local tiene tamaño 0 bytes
                                {
                                    //Descargar el archivo
                                    ftp.DownloadFile(pathFileLocal, pathFileFtp, true, FtpVerify.Retry);
                                }
                                else if (sizeFileLocal != sizeFileFtp) //Si el archivo local tiene diferente tamaño del archivo ftp
                                {
                                    //Descargar el archivo
                                    ftp.DownloadFile(pathFileLocal, pathFileFtp, true, FtpVerify.Retry);
                                }
                            }
                        }

                    }


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Elimina el archivo de la ubicación Local
        /// </summary>
        /// <param name="pathFileLocal">Ruta absoluta del destino del archivo en la ubicación Local. Ejm: C://Wong/Cache/Imagenes/MisCupones/kiosko_aceite_canola.png</param>
        internal void DeleteFile(string pathFileLocal)
        {
            try
            {
                //Validar si existe la carpeta en la ubicación local, en caso exista se eliminará
                if (File.Exists(pathFileLocal) == true)
                    File.Delete(pathFileLocal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Decarga el archivo desde el servidor FTP la ubicación Local
        /// </summary>
        /// <param name="pathFileFtp">Ruta relativa del origen del archivo en el servidor FTP. Ejm: /Imagenes/MisCupones/kiosko_aceite_canola.png</param>
        /// <param name="pathFileLocal">Ruta absoluta del destino del archivo en la ubicación Local. Ejm: C://Wong/Cache/Imagenes/MisCupones/kiosko_aceite_canola.png</param>
        private void DownloadFile(string pathFileFtp, string pathFileLocal)
        {
            try
            {

                //Validar si existe la carpeta en la ubicación local
                string pathDirLocal = Path.GetDirectoryName(pathFileLocal);
                string pathDirFtp = Path.GetDirectoryName(pathFileFtp);

                //En caso no exista la carpeta en la ubicación local se creará
                if (Directory.Exists(pathDirLocal) == false)
                    Directory.CreateDirectory(pathDirLocal);

                bool copyFile = false;
                if (File.Exists(pathFileLocal) == false) //No existe el archivo localmente
                {
                    copyFile = true;
                }
                else
                {
                    var fileInfo = new FileInfo(pathFileLocal);
                    long length = fileInfo.Length;

                    if (length == 0) //El archivo tiene un tamaño de 0 bytes
                        copyFile = true;
                }

                if (copyFile == true)
                {
                    
                    this.GetConfigFtp(out string hostname, out string username, out string password);

                    //Conectarse al FTP
                    using (var ftp = new FtpClient())
                    {
                        ftp.Encoding = Encoding.GetEncoding(1252);
                        ftp.Host = hostname;
                        ftp.Credentials = new NetworkCredential(username, password);

                        ftp.RetryAttempts = 3;

                        //Descargar el archivo
                        if (ftp.FileExists(pathFileFtp) == true) //Validar si existe el archivo en el FTP
                        {
                            ftp.DownloadFile(pathFileLocal, pathFileFtp, true, FtpVerify.Retry);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TEMPORAL
        internal Image GetImageFromUrl(string urlImage)
        {
            Image image = null;
            try
            {

                string filename = Path.GetFileName(urlImage);
                string directory = "";

                string urlDir = Path.GetDirectoryName(urlImage).Split('\\').Last();
                switch (urlDir.ToUpper())
                {
                    case "KIOSKO2":
                        directory = "MisCupones";
                        break;
                    case "CUPONMARCA":
                        directory = "CuponesMarca";
                        break;
                    case "CUPONLUGAR":
                        directory = "CuponesLugar";
                        break;
                    case "CLUBW":
                        directory = "ClubW";
                        break;
                    case "SUPERCUPON":
                        directory = "SuperCupon";
                        break;
                    default:
                        directory = urlDir;
                        break;
                }

                string pathImage = Path.Combine(directory, filename);
                image = this.GetImageFromFile(pathImage);

                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener la imagen desde una servidor ftp
        /// </summary>
        /// <param name="pathImage">ruta relativa de la imagen</param>
        /// <returns></returns>
        internal Image GetImageFromFile(string pathImage)
        {
            Image image = null;
            try
            {
                string directory = Path.GetDirectoryName(pathImage).Split('\\').Last();
                string filename = Path.GetFileName(pathImage);
                
                string pathAbsoluteFileLocal = Path.Combine(BaseRoot, FolderCache, directory, filename);
                string pathRelativeFileFtp = Path.Combine("Imagenes", directory, filename);

                this.DownloadFile(pathRelativeFileFtp, pathAbsoluteFileLocal);

                image = Image.FromFile(pathAbsoluteFileLocal);

                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener el texto desde una servidor ftp
        /// </summary>
        /// <param name="pathText">ruta relativa del archivo de texto</param>
        /// <returns></returns>
        internal string GetTextFromFile(string pathText)
        {
            string text = "";
            try
            {
                string directory = Path.GetDirectoryName(pathText).Split('\\').Last();
                string filename = Path.GetFileName(pathText);
                
                string pathAbsoluteFileLocal = Path.Combine(BaseRoot,FolderCache, directory, filename);
                string pathRelativeFileFtp = Path.Combine(directory, filename);

                this.DownloadFile(pathRelativeFileFtp, pathAbsoluteFileLocal);

                text = File.ReadAllText(pathAbsoluteFileLocal);

                return text;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
