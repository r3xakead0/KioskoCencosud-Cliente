﻿using MaterialSkin.Controls;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using COMMON = Cupones.Common;

namespace VisorCupones.Winform
{
    public partial class FrmCouponSuper : MaterialForm
    {

        #region "Singletons"

        private static FrmCouponSuper frmInstance = null;

        public static FrmCouponSuper Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmCouponSuper();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        private string ambito = "";
        private string tienda = "";
        private string nroDocumento = "";
        private int idGrupoCupon = 2;
        private int idTipoCupon = 0;

        private BE.Cupon beSuperCoupon = null;

        public FrmCouponSuper()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void Cupoun_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                var lstCompanias = new BL.Configuraciones().ListarCompanhias();
                this.cboAmbito.DataSource = lstCompanias;
                this.cboAmbito.DisplayMember = "Nombre";
                this.cboAmbito.ValueMember = "Codigo";

                #endregion

                #region Tipo Cupon

                DataTable dtTipo = new DataTable();
                dtTipo.Columns.Add("codigo", typeof(int));
                dtTipo.Columns.Add("nombre", typeof(string));

                DataRow drTipo = null;
                //drTipo = dtTipo.NewRow();
                //drTipo["codigo"] = 1;
                //drTipo["nombre"] = "Regalo";
                //dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 2;
                drTipo["nombre"] = "Normal";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 3;
                drTipo["nombre"] = "Horario";
                dtTipo.Rows.Add(drTipo);

                this.CargarComboBox(ref this.cboTipoCupon, dtTipo);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

                #region Tiendas

                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";

                #endregion

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void cboAmbito_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.beSuperCoupon == null)
                    return;

                this.beSuperCoupon.Id = int.Parse(this.txtCupon.Text);
                this.beSuperCoupon.Descuento = this.txtDescuento.Text;
                this.beSuperCoupon.Mensaje1 = this.txtMensaje01.Text;
                this.beSuperCoupon.Mensaje2 = this.txtMensaje02.Text;
                this.beSuperCoupon.Mensaje3 = this.txtMensaje03.Text;
                this.beSuperCoupon.ImagenPath = this.txtUrlImagen.Text;
                this.beSuperCoupon.Descripcion = this.txtDescripcion.Text;

                switch (this.idTipoCupon)
                {
                    case 1: //Regalo
                        this.picSuperCoupon.Image = null;
                        break;
                    case 2: //Super
                        this.picSuperCoupon.Image = this.ObtenerSuper();
                        break;
                    case 3: //Horario
                        this.picSuperCoupon.Image = this.ObtenerHorario();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
        
        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                this.ambito = this.cboAmbito.SelectedValue.ToString();
                this.tienda = this.cboTienda.SelectedValue.ToString();
                this.nroDocumento = this.txtNroDocumento.Text.Trim();
                this.idTipoCupon = int.Parse(this.cboTipoCupon.SelectedValue.ToString());

                this.MostrarSuperCupon(this.nroDocumento, this.idGrupoCupon, this.ambito, this.tienda, this.idTipoCupon);
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
        
        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void picCoupon_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.picSuperCoupon.Image != null)
                {
                   
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }


        public void MostrarSuperCupon(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            try
            {

                this.beSuperCoupon = null;

                var lstCoupons = new BL.Cupones().ListarCupones(nroDocumento, idGrupo, ambito, tienda, tipoCupon);

                if (lstCoupons != null)
                {
                    if (lstCoupons.Count == 1)
                    {
                        this.beSuperCoupon = lstCoupons[0];

                        this.txtCupon.Text = this.beSuperCoupon.Id.ToString();
                        this.txtDescuento.Text = this.beSuperCoupon.Descuento;
                        this.txtMensaje01.Text = this.beSuperCoupon.Mensaje1;
                        this.txtMensaje02.Text = this.beSuperCoupon.Mensaje2;
                        this.txtMensaje03.Text = this.beSuperCoupon.Mensaje3;
                        this.txtUrlImagen.Text = this.beSuperCoupon.ImagenPath;
                        this.txtDescripcion.Text = this.beSuperCoupon.Descripcion;

                        switch (tipoCupon)
                        {
                            case 1: //Regalo
                                this.picSuperCoupon.Image = null;
                                break;
                            case 2: //Super
                                this.picSuperCoupon.Image = this.ObtenerSuper();
                                break;
                            case 3: //Horario
                                this.picSuperCoupon.Image = this.ObtenerHorario();
                                break;
                            default:
                                break;
                        }

                        return;
                    }
                }

                this.txtCupon.Clear();
                this.txtDescuento.Clear();
                this.txtMensaje01.Clear();
                this.txtMensaje02.Clear();
                this.txtMensaje03.Clear();
                this.txtUrlImagen.Clear();
                this.txtDescripcion.Clear();

                this.picSuperCoupon.Image = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            try
            {
                cbo.DataSource = dt;
                cbo.DisplayMember = dt.Columns[1].Caption;
                cbo.ValueMember = dt.Columns[0].Caption;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Image ObtenerSuper()
        {
            try
            {
                var common = new COMMON.Coupon(this.ambito);
                return common.ImagePreviewSuperNormal(this.beSuperCoupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Image ObtenerHorario()
        {
            
            try
            {

                var common = new COMMON.Coupon(this.ambito);
                return common.ImagePreviewSuperTime(this.beSuperCoupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
