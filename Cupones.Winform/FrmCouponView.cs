﻿using MaterialSkin.Controls;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Imaging;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using COMMON = Cupones.Common;

namespace VisorCupones.Winform
{
    public partial class FrmCouponView : MaterialForm
    {

        #region "Singletons"

        private static FrmCouponView frmInstance = null;

        public static FrmCouponView Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmCouponView();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        private int tipoDocumento = 0;
        private string nroDocumento = "";
        private int nroCupon = 0;
        private int idGrupoCupon = 0;
        private int idTipoCupon = 0;

        private BE.DetalleCupon beCoupon = null;

        public FrmCouponView()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void Cupoun_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                var lstCompanias = new BL.Configuraciones().ListarCompanhias();
                this.cboAmbito.DataSource = lstCompanias;
                this.cboAmbito.DisplayMember = "Nombre";
                this.cboAmbito.ValueMember = "Codigo";

                #endregion

                #region Tiendas

                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";

                #endregion

                #region Grupo Cupon

                DataTable dtGrupo = new DataTable();
                dtGrupo.Columns.Add("codigo", typeof(int));
                dtGrupo.Columns.Add("nombre", typeof(string));

                DataRow drGrupo = null;
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 1;
                drGrupo["nombre"] = "Disponible";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 2;
                drGrupo["nombre"] = "Super";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 3;
                drGrupo["nombre"] = "Marca";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 4;
                drGrupo["nombre"] = "Lugar";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 5;
                drGrupo["nombre"] = "Ruleta";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 6;
                drGrupo["nombre"] = "Arbol";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 7;
                drGrupo["nombre"] = "Bonus";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 8;
                drGrupo["nombre"] = "Invitacion";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 9;
                drGrupo["nombre"] = "Comercial";
                dtGrupo.Rows.Add(drGrupo);

                this.CargarComboBox(ref this.cboGrupoCupon, dtGrupo);

                #endregion

                #region Tipo Cupon

                DataTable dtTipo = new DataTable();
                dtTipo.Columns.Add("codigo", typeof(int));
                dtTipo.Columns.Add("nombre", typeof(string));

                DataRow drTipo = null;
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 2;
                drTipo["nombre"] = "Normal";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 1;
                drTipo["nombre"] = "Regalo";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 3;
                drTipo["nombre"] = "Horario";
                dtTipo.Rows.Add(drTipo);

                this.CargarComboBox(ref this.cboTipoCupon, dtTipo);

                this.cboTipoCupon.Enabled = false;

                #endregion

                #region Tipo Legal

                DataTable dtTipoLegal = new DataTable();
                dtTipoLegal.Columns.Add("codigo", typeof(int));
                dtTipoLegal.Columns.Add("nombre", typeof(string));

                DataRow drTipoLegal = null;
                drTipoLegal = dtTipoLegal.NewRow();
                drTipoLegal["codigo"] = 0;
                drTipoLegal["nombre"] = "Normal";
                dtTipoLegal.Rows.Add(drTipoLegal);
                drTipoLegal = dtTipoLegal.NewRow();
                drTipoLegal["codigo"] = 1;
                drTipoLegal["nombre"] = "Bebida Alcoholicas";
                dtTipoLegal.Rows.Add(drTipoLegal);

                this.CargarComboBox(ref this.cboTipoLegal, dtTipoLegal);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

                #region Canal

                DataTable dtCanal = new DataTable();
                dtCanal.Columns.Add("codigo", typeof(int));
                dtCanal.Columns.Add("nombre", typeof(string));

                DataRow drCanal = null;
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 1;
                drCanal["nombre"] = "Tienda";
                dtCanal.Rows.Add(drCanal);
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 2;
                drCanal["nombre"] = "Online";
                dtCanal.Rows.Add(drCanal);
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 3;
                drCanal["nombre"] = "Tienda y Online";
                dtCanal.Rows.Add(drCanal);

                this.CargarComboBox(ref this.cboCanal, dtCanal);

                #endregion
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Image imgCoupon = null;

                if (this.beCoupon != null)
                {

                    this.beCoupon.TipoDocumento = this.cboTipoDocumento.SelectedValue.ToString().Equals("D") ? 1 : 2;
                    this.beCoupon.NroDocumento = this.txtNroDocumento.Text;
                    this.beCoupon.Descuento = this.txtDescuento.Text;
                    this.beCoupon.Mensaje1 = this.txtMensaje01.Text;
                    this.beCoupon.Mensaje2 = this.txtMensaje02.Text;
                    this.beCoupon.Mensaje3 = this.txtMensaje03.Text;
                    this.beCoupon.Ambito = this.cboAmbito.SelectedValue.ToString();
                    this.beCoupon.Tienda = this.cboTienda.SelectedValue.ToString();
                    this.beCoupon.LegalTipo = int.Parse(this.cboTipoLegal.SelectedValue.ToString());
                    this.beCoupon.LegalMensaje = this.txtMensajeLegal.Text;

                    this.beCoupon.ImagenPath = this.txtUrlImagen.Text;

                    this.beCoupon.CodigoBarra = this.txtCodigoBarra.Text;
                    this.beCoupon.FlagCanal = int.Parse(this.cboCanal.SelectedValue.ToString());
                    this.beCoupon.FlagSeriado = this.chkBonusSeriado.Checked == true ? 1 : 0;
                    this.beCoupon.BonusPrincipalSeriado = this.txtNumeroBonus.Text;
                    this.beCoupon.CodigoOnline = this.txtCodigoOnline.Text;
                    this.beCoupon.Vigencia = this.txtVigencia.Text;

                    if (this.cboGrupoCupon.SelectedValue.ToString() == "2")
                        this.beCoupon.Tipo = int.Parse(this.cboTipoCupon.SelectedValue.ToString());
                    else
                        this.beCoupon.Tipo = 2;

                    var commonCupon = new COMMON.Coupon(this.beCoupon.Ambito);
                    imgCoupon = commonCupon.ImageForPrint(this.beCoupon);
                }
     
                this.picCoupon.Image = imgCoupon;
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.beCoupon == null)
                    return;

                if (this.picCoupon == null)
                    return;

                var dialog = new SaveFileDialog();
                dialog.Filter = "Imagen JPG |*.jpg|Imagen PNG|*.png";
                dialog.Title = "Exportar imagen del cupon";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.picCoupon.Image.Save(dialog.FileName, ImageFormat.Png);
                }

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (this.txtNroCupon.Text.Trim().Length == 0)
                {
                    this.txtNroCupon.Focus();
                    MessageBox.Show("Ingrese el número de cupon", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                this.tipoDocumento = this.cboTipoDocumento.SelectedValue.ToString().Equals("D") ? 1 : 2;
                this.nroDocumento = this.txtNroDocumento.Text.Trim();
                this.nroCupon = int.Parse(this.txtNroCupon.Text.Trim());
                this.idGrupoCupon = int.Parse(this.cboGrupoCupon.SelectedValue.ToString());
                this.idTipoCupon = 0;
                if (this.cboTipoCupon.Enabled == true)
                    this.idTipoCupon = int.Parse(this.cboTipoCupon.SelectedValue.ToString());

                this.ObtenerCupon(tipoDocumento, nroDocumento, nroCupon, idGrupoCupon, idTipoCupon);

                if (this.beCoupon != null)
                {
                    this.beCoupon.Tienda = this.cboTienda.SelectedValue.ToString();
                    var commonCupon = new COMMON.Coupon(this.beCoupon.Ambito);
                    this.picCoupon.Image = commonCupon.ImageForPrint(this.beCoupon);
                }

                
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void cboGrupoCupon_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (this.cboGrupoCupon.SelectedValue.ToString() != "2")
                {
                    this.cboTipoCupon.Enabled = false;
                    this.cboTipoCupon.SelectedValue = "2";
                }
                else
                {
                    this.cboTipoCupon.Enabled = true;
                    this.cboTipoCupon.SelectedValue = "2";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void picCoupon_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.picCoupon.Image != null)
                {
                   
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            cbo.DataSource = dt;
            cbo.DisplayMember = dt.Columns[1].Caption;
            cbo.ValueMember = dt.Columns[0].Caption;
        }

        public void ObtenerCupon(int tipoDocumento, string nroDocumento, int nroCupon, int idGrupo, int tipoCupon)
        {
            try
            {
                this.beCoupon = new BL.Cupones().ObtenerDetalleCupon(nroDocumento, nroCupon, idGrupo, tipoCupon);
     
                if (this.beCoupon != null)
                {
                    this.beCoupon.TipoDocumento = tipoDocumento;
                    this.beCoupon.Tienda = "";

                    this.txtDescuento.Text = beCoupon.Descuento;
                    this.txtMensaje01.Text = beCoupon.Mensaje1;
                    this.txtMensaje02.Text = beCoupon.Mensaje2;
                    this.txtMensaje03.Text = beCoupon.Mensaje3;
                    this.cboAmbito.SelectedValue = beCoupon.Ambito;
                    this.cboTipoLegal.SelectedValue = beCoupon.LegalTipo;
                    this.txtMensajeLegal.Text = beCoupon.LegalMensaje;

                    string directory = Path.GetDirectoryName(beCoupon.ImagenPath).Split('\\').Last();
                    string filename = Path.GetFileName(beCoupon.ImagenPath);
                    this.txtUrlImagen.Text = Path.Combine(directory, filename);

                    this.txtCodigoBarra.Text = beCoupon.CodigoBarra;
                    this.cboCanal.SelectedValue = beCoupon.FlagCanal;
                    this.chkBonusSeriado.Checked = (beCoupon.FlagSeriado == 1);
                    this.txtNumeroBonus.Text = beCoupon.BonusPrincipalSeriado;
                    this.txtCodigoOnline.Text = beCoupon.CodigoOnline;
                    this.txtVigencia.Text = beCoupon.Vigencia;

                    int idTipoCupon = beCoupon.Tipo;
                    if (idTipoCupon == 0)
                        this.cboTipoCupon.SelectedValue = 2;
                    else
                    {
                        if (idTipoCupon > 0 && idTipoCupon < 2)
                            this.cboTipoCupon.SelectedValue = idTipoCupon;
                        else
                            this.cboTipoCupon.SelectedValue = 2;
                    }

                }
                else
                {
                    this.txtDescuento.Clear();
                    this.txtMensaje01.Clear();
                    this.txtMensaje02.Clear();
                    this.txtMensaje03.Clear();
                    this.cboAmbito.SelectedValue = "WONG";
                    this.cboTipoLegal.SelectedValue = "0";
                    this.txtMensajeLegal.Clear();
                    this.txtUrlImagen.Clear();
                    this.txtCodigoBarra.Clear();
                    this.cboCanal.SelectedValue = "1";
                    this.chkBonusSeriado.Checked = false;
                    this.txtNumeroBonus.Clear();
                    this.txtCodigoOnline.Clear();
                    this.txtVigencia.Clear();
                    this.cboTipoCupon.SelectedValue = 2;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboAmbito_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
