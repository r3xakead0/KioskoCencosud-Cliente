﻿namespace VisorCupones.Winform
{
    partial class FrmCouponSuper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCouponSuper));
            this.picSuperCoupon = new System.Windows.Forms.PictureBox();
            this.btnObtener = new System.Windows.Forms.Button();
            this.cboTipoCupon = new System.Windows.Forms.ComboBox();
            this.lblTipoDocumento = new System.Windows.Forms.Label();
            this.txtNroDocumento = new System.Windows.Forms.TextBox();
            this.cboTipoDocumento = new System.Windows.Forms.ComboBox();
            this.grpCouponGet = new System.Windows.Forms.GroupBox();
            this.cboTienda = new System.Windows.Forms.ComboBox();
            this.lblNroDocumento = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.cboAmbito = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpCouponDetail = new System.Windows.Forms.GroupBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.lblCupon = new System.Windows.Forms.Label();
            this.txtCupon = new System.Windows.Forms.TextBox();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.lblMensaje03 = new System.Windows.Forms.Label();
            this.txtMensaje03 = new System.Windows.Forms.TextBox();
            this.lblMensaje02 = new System.Windows.Forms.Label();
            this.txtMensaje02 = new System.Windows.Forms.TextBox();
            this.lblMensaje01 = new System.Windows.Forms.Label();
            this.txtMensaje01 = new System.Windows.Forms.TextBox();
            this.lblUrlImagen = new System.Windows.Forms.Label();
            this.txtUrlImagen = new System.Windows.Forms.TextBox();
            this.lblDescuento = new System.Windows.Forms.Label();
            this.txtDescuento = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSuperCoupon)).BeginInit();
            this.grpCouponGet.SuspendLayout();
            this.grpCouponDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // picSuperCoupon
            // 
            this.picSuperCoupon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picSuperCoupon.BackColor = System.Drawing.Color.White;
            this.picSuperCoupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSuperCoupon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picSuperCoupon.Location = new System.Drawing.Point(521, 76);
            this.picSuperCoupon.Name = "picSuperCoupon";
            this.picSuperCoupon.Size = new System.Drawing.Size(204, 498);
            this.picSuperCoupon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSuperCoupon.TabIndex = 0;
            this.picSuperCoupon.TabStop = false;
            this.picSuperCoupon.Click += new System.EventHandler(this.picCoupon_Click);
            // 
            // btnObtener
            // 
            this.btnObtener.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnObtener.Location = new System.Drawing.Point(384, 76);
            this.btnObtener.Name = "btnObtener";
            this.btnObtener.Size = new System.Drawing.Size(113, 21);
            this.btnObtener.TabIndex = 37;
            this.btnObtener.Text = "&Obtener";
            this.btnObtener.UseVisualStyleBackColor = true;
            this.btnObtener.Click += new System.EventHandler(this.btnObtener_Click);
            // 
            // cboTipoCupon
            // 
            this.cboTipoCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipoCupon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoCupon.FormattingEnabled = true;
            this.cboTipoCupon.Location = new System.Drawing.Point(99, 76);
            this.cboTipoCupon.Name = "cboTipoCupon";
            this.cboTipoCupon.Size = new System.Drawing.Size(132, 21);
            this.cboTipoCupon.TabIndex = 36;
            this.cboTipoCupon.TabStop = false;
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoDocumento.AutoSize = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(272, 24);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(67, 13);
            this.lblTipoDocumento.TabIndex = 25;
            this.lblTipoDocumento.Text = "Tipo Doc.";
            // 
            // txtNroDocumento
            // 
            this.txtNroDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNroDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNroDocumento.Location = new System.Drawing.Point(365, 49);
            this.txtNroDocumento.Name = "txtNroDocumento";
            this.txtNroDocumento.Size = new System.Drawing.Size(132, 21);
            this.txtNroDocumento.TabIndex = 24;
            this.txtNroDocumento.TabStop = false;
            this.txtNroDocumento.Leave += new System.EventHandler(this.txtNroDocumento_Leave);
            // 
            // cboTipoDocumento
            // 
            this.cboTipoDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDocumento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoDocumento.FormattingEnabled = true;
            this.cboTipoDocumento.Location = new System.Drawing.Point(365, 21);
            this.cboTipoDocumento.Name = "cboTipoDocumento";
            this.cboTipoDocumento.Size = new System.Drawing.Size(132, 21);
            this.cboTipoDocumento.TabIndex = 23;
            this.cboTipoDocumento.TabStop = false;
            // 
            // grpCouponGet
            // 
            this.grpCouponGet.Controls.Add(this.cboTienda);
            this.grpCouponGet.Controls.Add(this.lblNroDocumento);
            this.grpCouponGet.Controls.Add(this.label2);
            this.grpCouponGet.Controls.Add(this.lblEmpresa);
            this.grpCouponGet.Controls.Add(this.cboAmbito);
            this.grpCouponGet.Controls.Add(this.label1);
            this.grpCouponGet.Controls.Add(this.btnObtener);
            this.grpCouponGet.Controls.Add(this.lblTipoDocumento);
            this.grpCouponGet.Controls.Add(this.cboTipoCupon);
            this.grpCouponGet.Controls.Add(this.txtNroDocumento);
            this.grpCouponGet.Controls.Add(this.cboTipoDocumento);
            this.grpCouponGet.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponGet.Location = new System.Drawing.Point(12, 76);
            this.grpCouponGet.Name = "grpCouponGet";
            this.grpCouponGet.Size = new System.Drawing.Size(503, 110);
            this.grpCouponGet.TabIndex = 34;
            this.grpCouponGet.TabStop = false;
            this.grpCouponGet.Text = "Obtener Super Cupon";
            // 
            // cboTienda
            // 
            this.cboTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTienda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTienda.FormattingEnabled = true;
            this.cboTienda.Location = new System.Drawing.Point(99, 48);
            this.cboTienda.Name = "cboTienda";
            this.cboTienda.Size = new System.Drawing.Size(132, 21);
            this.cboTienda.TabIndex = 49;
            this.cboTienda.TabStop = false;
            // 
            // lblNroDocumento
            // 
            this.lblNroDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNroDocumento.AutoSize = true;
            this.lblNroDocumento.Location = new System.Drawing.Point(272, 51);
            this.lblNroDocumento.Name = "lblNroDocumento";
            this.lblNroDocumento.Size = new System.Drawing.Size(62, 13);
            this.lblNroDocumento.TabIndex = 48;
            this.lblNroDocumento.Text = "Nro. Doc";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "Tipo";
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(6, 24);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(64, 13);
            this.lblEmpresa.TabIndex = 44;
            this.lblEmpresa.Text = "Empresa";
            // 
            // cboAmbito
            // 
            this.cboAmbito.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboAmbito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmbito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAmbito.FormattingEnabled = true;
            this.cboAmbito.Location = new System.Drawing.Point(99, 21);
            this.cboAmbito.Name = "cboAmbito";
            this.cboAmbito.Size = new System.Drawing.Size(132, 21);
            this.cboAmbito.TabIndex = 45;
            this.cboAmbito.TabStop = false;
            this.cboAmbito.SelectionChangeCommitted += new System.EventHandler(this.cboAmbito_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Tienda";
            // 
            // grpCouponDetail
            // 
            this.grpCouponDetail.Controls.Add(this.txtDescripcion);
            this.grpCouponDetail.Controls.Add(this.lblDescripcion);
            this.grpCouponDetail.Controls.Add(this.lblCupon);
            this.grpCouponDetail.Controls.Add(this.txtCupon);
            this.grpCouponDetail.Controls.Add(this.btnVisualizar);
            this.grpCouponDetail.Controls.Add(this.lblMensaje03);
            this.grpCouponDetail.Controls.Add(this.txtMensaje03);
            this.grpCouponDetail.Controls.Add(this.lblMensaje02);
            this.grpCouponDetail.Controls.Add(this.txtMensaje02);
            this.grpCouponDetail.Controls.Add(this.lblMensaje01);
            this.grpCouponDetail.Controls.Add(this.txtMensaje01);
            this.grpCouponDetail.Controls.Add(this.lblUrlImagen);
            this.grpCouponDetail.Controls.Add(this.txtUrlImagen);
            this.grpCouponDetail.Controls.Add(this.lblDescuento);
            this.grpCouponDetail.Controls.Add(this.txtDescuento);
            this.grpCouponDetail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponDetail.Location = new System.Drawing.Point(12, 192);
            this.grpCouponDetail.Name = "grpCouponDetail";
            this.grpCouponDetail.Size = new System.Drawing.Size(503, 382);
            this.grpCouponDetail.TabIndex = 39;
            this.grpCouponDetail.TabStop = false;
            this.grpCouponDetail.Text = "Detalle del Super Cupon";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescripcion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(99, 184);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescripcion.Size = new System.Drawing.Size(398, 181);
            this.txtDescripcion.TabIndex = 40;
            this.txtDescripcion.TabStop = false;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Location = new System.Drawing.Point(6, 186);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(83, 13);
            this.lblDescripcion.TabIndex = 36;
            this.lblDescripcion.Text = "Descripción";
            // 
            // lblCupon
            // 
            this.lblCupon.AutoSize = true;
            this.lblCupon.Location = new System.Drawing.Point(6, 24);
            this.lblCupon.Name = "lblCupon";
            this.lblCupon.Size = new System.Drawing.Size(47, 13);
            this.lblCupon.TabIndex = 35;
            this.lblCupon.Text = "Cupon";
            // 
            // txtCupon
            // 
            this.txtCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCupon.Location = new System.Drawing.Point(99, 22);
            this.txtCupon.Name = "txtCupon";
            this.txtCupon.ReadOnly = true;
            this.txtCupon.Size = new System.Drawing.Size(279, 21);
            this.txtCupon.TabIndex = 34;
            this.txtCupon.TabStop = false;
            this.txtCupon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVisualizar.Location = new System.Drawing.Point(384, 22);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(113, 21);
            this.btnVisualizar.TabIndex = 33;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // lblMensaje03
            // 
            this.lblMensaje03.AutoSize = true;
            this.lblMensaje03.Location = new System.Drawing.Point(6, 132);
            this.lblMensaje03.Name = "lblMensaje03";
            this.lblMensaje03.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje03.TabIndex = 15;
            this.lblMensaje03.Text = "Mensaje 3";
            // 
            // txtMensaje03
            // 
            this.txtMensaje03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje03.Location = new System.Drawing.Point(99, 130);
            this.txtMensaje03.Name = "txtMensaje03";
            this.txtMensaje03.Size = new System.Drawing.Size(398, 21);
            this.txtMensaje03.TabIndex = 14;
            this.txtMensaje03.TabStop = false;
            // 
            // lblMensaje02
            // 
            this.lblMensaje02.AutoSize = true;
            this.lblMensaje02.Location = new System.Drawing.Point(6, 105);
            this.lblMensaje02.Name = "lblMensaje02";
            this.lblMensaje02.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje02.TabIndex = 13;
            this.lblMensaje02.Text = "Mensaje 2";
            // 
            // txtMensaje02
            // 
            this.txtMensaje02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje02.Location = new System.Drawing.Point(99, 103);
            this.txtMensaje02.Name = "txtMensaje02";
            this.txtMensaje02.Size = new System.Drawing.Size(398, 21);
            this.txtMensaje02.TabIndex = 12;
            this.txtMensaje02.TabStop = false;
            // 
            // lblMensaje01
            // 
            this.lblMensaje01.AutoSize = true;
            this.lblMensaje01.Location = new System.Drawing.Point(6, 78);
            this.lblMensaje01.Name = "lblMensaje01";
            this.lblMensaje01.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje01.TabIndex = 11;
            this.lblMensaje01.Text = "Mensaje 1";
            // 
            // txtMensaje01
            // 
            this.txtMensaje01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje01.Location = new System.Drawing.Point(99, 76);
            this.txtMensaje01.Name = "txtMensaje01";
            this.txtMensaje01.Size = new System.Drawing.Size(398, 21);
            this.txtMensaje01.TabIndex = 10;
            this.txtMensaje01.TabStop = false;
            // 
            // lblUrlImagen
            // 
            this.lblUrlImagen.AutoSize = true;
            this.lblUrlImagen.Location = new System.Drawing.Point(6, 159);
            this.lblUrlImagen.Name = "lblUrlImagen";
            this.lblUrlImagen.Size = new System.Drawing.Size(80, 13);
            this.lblUrlImagen.TabIndex = 7;
            this.lblUrlImagen.Text = "Url Imagen";
            // 
            // txtUrlImagen
            // 
            this.txtUrlImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrlImagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUrlImagen.Location = new System.Drawing.Point(99, 157);
            this.txtUrlImagen.Name = "txtUrlImagen";
            this.txtUrlImagen.Size = new System.Drawing.Size(398, 21);
            this.txtUrlImagen.TabIndex = 6;
            this.txtUrlImagen.TabStop = false;
            // 
            // lblDescuento
            // 
            this.lblDescuento.AutoSize = true;
            this.lblDescuento.Location = new System.Drawing.Point(6, 51);
            this.lblDescuento.Name = "lblDescuento";
            this.lblDescuento.Size = new System.Drawing.Size(75, 13);
            this.lblDescuento.TabIndex = 3;
            this.lblDescuento.Text = "Descuento";
            // 
            // txtDescuento
            // 
            this.txtDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescuento.Location = new System.Drawing.Point(99, 49);
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.Size = new System.Drawing.Size(398, 21);
            this.txtDescuento.TabIndex = 2;
            this.txtDescuento.TabStop = false;
            this.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FrmCouponSuper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 586);
            this.Controls.Add(this.grpCouponDetail);
            this.Controls.Add(this.grpCouponGet);
            this.Controls.Add(this.picSuperCoupon);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCouponSuper";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mostrar Super Cupon";
            this.Load += new System.EventHandler(this.Cupoun_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picSuperCoupon)).EndInit();
            this.grpCouponGet.ResumeLayout(false);
            this.grpCouponGet.PerformLayout();
            this.grpCouponDetail.ResumeLayout(false);
            this.grpCouponDetail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picSuperCoupon;
        private System.Windows.Forms.Label lblTipoDocumento;
        private System.Windows.Forms.TextBox txtNroDocumento;
        private System.Windows.Forms.ComboBox cboTipoDocumento;
        private System.Windows.Forms.ComboBox cboTipoCupon;
        private System.Windows.Forms.Button btnObtener;
        private System.Windows.Forms.GroupBox grpCouponGet;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ComboBox cboAmbito;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpCouponDetail;
        private System.Windows.Forms.Label lblCupon;
        private System.Windows.Forms.TextBox txtCupon;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.Label lblMensaje03;
        private System.Windows.Forms.TextBox txtMensaje03;
        private System.Windows.Forms.Label lblMensaje02;
        private System.Windows.Forms.TextBox txtMensaje02;
        private System.Windows.Forms.Label lblMensaje01;
        private System.Windows.Forms.TextBox txtMensaje01;
        private System.Windows.Forms.Label lblUrlImagen;
        private System.Windows.Forms.TextBox txtUrlImagen;
        private System.Windows.Forms.Label lblDescuento;
        private System.Windows.Forms.TextBox txtDescuento;
        private System.Windows.Forms.Label lblNroDocumento;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.ComboBox cboTienda;
    }
}