﻿namespace VisorCupones.Winform
{
    partial class FrmSuperCoupon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSuperCoupon));
            this.grpCouponList = new System.Windows.Forms.GroupBox();
            this.lblCoupon = new System.Windows.Forms.Label();
            this.grpCouponDetail = new System.Windows.Forms.GroupBox();
            this.chkExclusivoApp = new System.Windows.Forms.CheckBox();
            this.lblCupon = new System.Windows.Forms.Label();
            this.txtCupon = new System.Windows.Forms.TextBox();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.lblMensaje03 = new System.Windows.Forms.Label();
            this.txtMensaje03 = new System.Windows.Forms.TextBox();
            this.lblMensaje02 = new System.Windows.Forms.Label();
            this.txtMensaje02 = new System.Windows.Forms.TextBox();
            this.lblMensaje01 = new System.Windows.Forms.Label();
            this.txtMensaje01 = new System.Windows.Forms.TextBox();
            this.lblUrlImagen = new System.Windows.Forms.Label();
            this.txtUrlImagen = new System.Windows.Forms.TextBox();
            this.lblDescuento = new System.Windows.Forms.Label();
            this.txtDescuento = new System.Windows.Forms.TextBox();
            this.dgvCoupons = new System.Windows.Forms.DataGridView();
            this.picCoupon = new System.Windows.Forms.PictureBox();
            this.btnObtener = new System.Windows.Forms.Button();
            this.cboGrupoCupon = new System.Windows.Forms.ComboBox();
            this.lblDocumento = new System.Windows.Forms.Label();
            this.txtNroDocumento = new System.Windows.Forms.TextBox();
            this.cboTipoDocumento = new System.Windows.Forms.ComboBox();
            this.lblTipo = new System.Windows.Forms.Label();
            this.grpCouponFind = new System.Windows.Forms.GroupBox();
            this.cboTienda = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.cboAmbito = new System.Windows.Forms.ComboBox();
            this.lblTienda = new System.Windows.Forms.Label();
            this.grpCouponList.SuspendLayout();
            this.grpCouponDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoupons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCoupon)).BeginInit();
            this.grpCouponFind.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCouponList
            // 
            this.grpCouponList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCouponList.Controls.Add(this.lblCoupon);
            this.grpCouponList.Controls.Add(this.grpCouponDetail);
            this.grpCouponList.Controls.Add(this.dgvCoupons);
            this.grpCouponList.Controls.Add(this.picCoupon);
            this.grpCouponList.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponList.Location = new System.Drawing.Point(12, 193);
            this.grpCouponList.Name = "grpCouponList";
            this.grpCouponList.Size = new System.Drawing.Size(776, 395);
            this.grpCouponList.TabIndex = 1;
            this.grpCouponList.TabStop = false;
            this.grpCouponList.Text = "Lista de Cupones";
            // 
            // lblCoupon
            // 
            this.lblCoupon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCoupon.BackColor = System.Drawing.Color.Navy;
            this.lblCoupon.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoupon.ForeColor = System.Drawing.Color.White;
            this.lblCoupon.Location = new System.Drawing.Point(415, 202);
            this.lblCoupon.Name = "lblCoupon";
            this.lblCoupon.Size = new System.Drawing.Size(355, 21);
            this.lblCoupon.TabIndex = 39;
            this.lblCoupon.Text = "Miniatura Cupon";
            this.lblCoupon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpCouponDetail
            // 
            this.grpCouponDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCouponDetail.Controls.Add(this.chkExclusivoApp);
            this.grpCouponDetail.Controls.Add(this.lblCupon);
            this.grpCouponDetail.Controls.Add(this.txtCupon);
            this.grpCouponDetail.Controls.Add(this.btnVisualizar);
            this.grpCouponDetail.Controls.Add(this.lblMensaje03);
            this.grpCouponDetail.Controls.Add(this.txtMensaje03);
            this.grpCouponDetail.Controls.Add(this.lblMensaje02);
            this.grpCouponDetail.Controls.Add(this.txtMensaje02);
            this.grpCouponDetail.Controls.Add(this.lblMensaje01);
            this.grpCouponDetail.Controls.Add(this.txtMensaje01);
            this.grpCouponDetail.Controls.Add(this.lblUrlImagen);
            this.grpCouponDetail.Controls.Add(this.txtUrlImagen);
            this.grpCouponDetail.Controls.Add(this.lblDescuento);
            this.grpCouponDetail.Controls.Add(this.txtDescuento);
            this.grpCouponDetail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponDetail.Location = new System.Drawing.Point(6, 205);
            this.grpCouponDetail.Name = "grpCouponDetail";
            this.grpCouponDetail.Size = new System.Drawing.Size(403, 184);
            this.grpCouponDetail.TabIndex = 38;
            this.grpCouponDetail.TabStop = false;
            this.grpCouponDetail.Text = "Detalle de Cupon";
            // 
            // chkExclusivoApp
            // 
            this.chkExclusivoApp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkExclusivoApp.AutoSize = true;
            this.chkExclusivoApp.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkExclusivoApp.Location = new System.Drawing.Point(280, 51);
            this.chkExclusivoApp.Name = "chkExclusivoApp";
            this.chkExclusivoApp.Size = new System.Drawing.Size(117, 17);
            this.chkExclusivoApp.TabIndex = 36;
            this.chkExclusivoApp.TabStop = false;
            this.chkExclusivoApp.Text = "Exclusivo APP";
            this.chkExclusivoApp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkExclusivoApp.UseVisualStyleBackColor = true;
            // 
            // lblCupon
            // 
            this.lblCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCupon.AutoSize = true;
            this.lblCupon.Location = new System.Drawing.Point(6, 24);
            this.lblCupon.Name = "lblCupon";
            this.lblCupon.Size = new System.Drawing.Size(47, 13);
            this.lblCupon.TabIndex = 35;
            this.lblCupon.Text = "Cupon";
            // 
            // txtCupon
            // 
            this.txtCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCupon.Location = new System.Drawing.Point(99, 22);
            this.txtCupon.Name = "txtCupon";
            this.txtCupon.ReadOnly = true;
            this.txtCupon.Size = new System.Drawing.Size(173, 21);
            this.txtCupon.TabIndex = 34;
            this.txtCupon.TabStop = false;
            this.txtCupon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVisualizar.Location = new System.Drawing.Point(280, 22);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(117, 21);
            this.btnVisualizar.TabIndex = 33;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // lblMensaje03
            // 
            this.lblMensaje03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje03.AutoSize = true;
            this.lblMensaje03.Location = new System.Drawing.Point(6, 132);
            this.lblMensaje03.Name = "lblMensaje03";
            this.lblMensaje03.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje03.TabIndex = 15;
            this.lblMensaje03.Text = "Mensaje 3";
            // 
            // txtMensaje03
            // 
            this.txtMensaje03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje03.Location = new System.Drawing.Point(99, 130);
            this.txtMensaje03.Name = "txtMensaje03";
            this.txtMensaje03.Size = new System.Drawing.Size(298, 21);
            this.txtMensaje03.TabIndex = 14;
            this.txtMensaje03.TabStop = false;
            // 
            // lblMensaje02
            // 
            this.lblMensaje02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje02.AutoSize = true;
            this.lblMensaje02.Location = new System.Drawing.Point(6, 105);
            this.lblMensaje02.Name = "lblMensaje02";
            this.lblMensaje02.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje02.TabIndex = 13;
            this.lblMensaje02.Text = "Mensaje 2";
            // 
            // txtMensaje02
            // 
            this.txtMensaje02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje02.Location = new System.Drawing.Point(99, 103);
            this.txtMensaje02.Name = "txtMensaje02";
            this.txtMensaje02.Size = new System.Drawing.Size(298, 21);
            this.txtMensaje02.TabIndex = 12;
            this.txtMensaje02.TabStop = false;
            // 
            // lblMensaje01
            // 
            this.lblMensaje01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje01.AutoSize = true;
            this.lblMensaje01.Location = new System.Drawing.Point(6, 78);
            this.lblMensaje01.Name = "lblMensaje01";
            this.lblMensaje01.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje01.TabIndex = 11;
            this.lblMensaje01.Text = "Mensaje 1";
            // 
            // txtMensaje01
            // 
            this.txtMensaje01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje01.Location = new System.Drawing.Point(99, 76);
            this.txtMensaje01.Name = "txtMensaje01";
            this.txtMensaje01.Size = new System.Drawing.Size(298, 21);
            this.txtMensaje01.TabIndex = 10;
            this.txtMensaje01.TabStop = false;
            // 
            // lblUrlImagen
            // 
            this.lblUrlImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUrlImagen.AutoSize = true;
            this.lblUrlImagen.Location = new System.Drawing.Point(6, 159);
            this.lblUrlImagen.Name = "lblUrlImagen";
            this.lblUrlImagen.Size = new System.Drawing.Size(80, 13);
            this.lblUrlImagen.TabIndex = 7;
            this.lblUrlImagen.Text = "Url Imagen";
            // 
            // txtUrlImagen
            // 
            this.txtUrlImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrlImagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUrlImagen.Location = new System.Drawing.Point(99, 157);
            this.txtUrlImagen.Name = "txtUrlImagen";
            this.txtUrlImagen.Size = new System.Drawing.Size(298, 21);
            this.txtUrlImagen.TabIndex = 6;
            this.txtUrlImagen.TabStop = false;
            // 
            // lblDescuento
            // 
            this.lblDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescuento.AutoSize = true;
            this.lblDescuento.Location = new System.Drawing.Point(6, 51);
            this.lblDescuento.Name = "lblDescuento";
            this.lblDescuento.Size = new System.Drawing.Size(75, 13);
            this.lblDescuento.TabIndex = 3;
            this.lblDescuento.Text = "Descuento";
            // 
            // txtDescuento
            // 
            this.txtDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescuento.Location = new System.Drawing.Point(99, 49);
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.Size = new System.Drawing.Size(173, 21);
            this.txtDescuento.TabIndex = 2;
            this.txtDescuento.TabStop = false;
            this.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dgvCoupons
            // 
            this.dgvCoupons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCoupons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCoupons.Location = new System.Drawing.Point(6, 20);
            this.dgvCoupons.Name = "dgvCoupons";
            this.dgvCoupons.Size = new System.Drawing.Size(764, 179);
            this.dgvCoupons.TabIndex = 37;
            this.dgvCoupons.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCoupons_CellClick);
            // 
            // picCoupon
            // 
            this.picCoupon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.picCoupon.BackColor = System.Drawing.Color.White;
            this.picCoupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCoupon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picCoupon.Location = new System.Drawing.Point(415, 226);
            this.picCoupon.Name = "picCoupon";
            this.picCoupon.Size = new System.Drawing.Size(355, 163);
            this.picCoupon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCoupon.TabIndex = 36;
            this.picCoupon.TabStop = false;
            // 
            // btnObtener
            // 
            this.btnObtener.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnObtener.Location = new System.Drawing.Point(631, 74);
            this.btnObtener.Name = "btnObtener";
            this.btnObtener.Size = new System.Drawing.Size(113, 21);
            this.btnObtener.TabIndex = 37;
            this.btnObtener.Text = "&Mostrar";
            this.btnObtener.UseVisualStyleBackColor = true;
            this.btnObtener.Click += new System.EventHandler(this.btnObtener_Click);
            // 
            // cboGrupoCupon
            // 
            this.cboGrupoCupon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGrupoCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboGrupoCupon.FormattingEnabled = true;
            this.cboGrupoCupon.Location = new System.Drawing.Point(146, 20);
            this.cboGrupoCupon.Name = "cboGrupoCupon";
            this.cboGrupoCupon.Size = new System.Drawing.Size(132, 21);
            this.cboGrupoCupon.TabIndex = 35;
            this.cboGrupoCupon.TabStop = false;
            // 
            // lblDocumento
            // 
            this.lblDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocumento.AutoSize = true;
            this.lblDocumento.Location = new System.Drawing.Point(379, 23);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Size = new System.Drawing.Size(80, 13);
            this.lblDocumento.TabIndex = 25;
            this.lblDocumento.Text = "Documento";
            // 
            // txtNroDocumento
            // 
            this.txtNroDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNroDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNroDocumento.Location = new System.Drawing.Point(631, 20);
            this.txtNroDocumento.Name = "txtNroDocumento";
            this.txtNroDocumento.Size = new System.Drawing.Size(113, 21);
            this.txtNroDocumento.TabIndex = 24;
            this.txtNroDocumento.TabStop = false;
            this.txtNroDocumento.Leave += new System.EventHandler(this.txtNroDocumento_Leave);
            // 
            // cboTipoDocumento
            // 
            this.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDocumento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoDocumento.FormattingEnabled = true;
            this.cboTipoDocumento.Location = new System.Drawing.Point(493, 20);
            this.cboTipoDocumento.Name = "cboTipoDocumento";
            this.cboTipoDocumento.Size = new System.Drawing.Size(132, 21);
            this.cboTipoDocumento.TabIndex = 23;
            this.cboTipoDocumento.TabStop = false;
            // 
            // lblTipo
            // 
            this.lblTipo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(32, 23);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(47, 13);
            this.lblTipo.TabIndex = 22;
            this.lblTipo.Text = "Cupon";
            // 
            // grpCouponFind
            // 
            this.grpCouponFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCouponFind.Controls.Add(this.cboTienda);
            this.grpCouponFind.Controls.Add(this.lblEmpresa);
            this.grpCouponFind.Controls.Add(this.cboAmbito);
            this.grpCouponFind.Controls.Add(this.lblTienda);
            this.grpCouponFind.Controls.Add(this.btnObtener);
            this.grpCouponFind.Controls.Add(this.lblTipo);
            this.grpCouponFind.Controls.Add(this.lblDocumento);
            this.grpCouponFind.Controls.Add(this.txtNroDocumento);
            this.grpCouponFind.Controls.Add(this.cboTipoDocumento);
            this.grpCouponFind.Controls.Add(this.cboGrupoCupon);
            this.grpCouponFind.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponFind.Location = new System.Drawing.Point(12, 76);
            this.grpCouponFind.Name = "grpCouponFind";
            this.grpCouponFind.Size = new System.Drawing.Size(776, 111);
            this.grpCouponFind.TabIndex = 34;
            this.grpCouponFind.TabStop = false;
            this.grpCouponFind.Text = "Mostrar Cupones";
            // 
            // cboTienda
            // 
            this.cboTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTienda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTienda.FormattingEnabled = true;
            this.cboTienda.Location = new System.Drawing.Point(146, 74);
            this.cboTienda.Name = "cboTienda";
            this.cboTienda.Size = new System.Drawing.Size(132, 21);
            this.cboTienda.TabIndex = 43;
            this.cboTienda.TabStop = false;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(32, 50);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(64, 13);
            this.lblEmpresa.TabIndex = 40;
            this.lblEmpresa.Text = "Empresa";
            // 
            // cboAmbito
            // 
            this.cboAmbito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmbito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAmbito.FormattingEnabled = true;
            this.cboAmbito.Location = new System.Drawing.Point(146, 47);
            this.cboAmbito.Name = "cboAmbito";
            this.cboAmbito.Size = new System.Drawing.Size(132, 21);
            this.cboAmbito.TabIndex = 41;
            this.cboAmbito.TabStop = false;
            this.cboAmbito.SelectionChangeCommitted += new System.EventHandler(this.cboAmbito_SelectionChangeCommitted);
            // 
            // lblTienda
            // 
            this.lblTienda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTienda.AutoSize = true;
            this.lblTienda.Location = new System.Drawing.Point(32, 77);
            this.lblTienda.Name = "lblTienda";
            this.lblTienda.Size = new System.Drawing.Size(51, 13);
            this.lblTienda.TabIndex = 38;
            this.lblTienda.Text = "Tienda";
            // 
            // FrmSuperCoupon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.grpCouponFind);
            this.Controls.Add(this.grpCouponList);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSuperCoupon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mostrar Cupones";
            this.Load += new System.EventHandler(this.CouponList_Load);
            this.grpCouponList.ResumeLayout(false);
            this.grpCouponDetail.ResumeLayout(false);
            this.grpCouponDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoupons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCoupon)).EndInit();
            this.grpCouponFind.ResumeLayout(false);
            this.grpCouponFind.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grpCouponList;
        private System.Windows.Forms.Label lblDocumento;
        private System.Windows.Forms.TextBox txtNroDocumento;
        private System.Windows.Forms.ComboBox cboTipoDocumento;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox cboGrupoCupon;
        private System.Windows.Forms.Button btnObtener;
        private System.Windows.Forms.GroupBox grpCouponFind;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ComboBox cboAmbito;
        private System.Windows.Forms.Label lblTienda;
        private System.Windows.Forms.PictureBox picCoupon;
        private System.Windows.Forms.DataGridView dgvCoupons;
        private System.Windows.Forms.GroupBox grpCouponDetail;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.Label lblMensaje03;
        private System.Windows.Forms.TextBox txtMensaje03;
        private System.Windows.Forms.Label lblMensaje02;
        private System.Windows.Forms.TextBox txtMensaje02;
        private System.Windows.Forms.Label lblMensaje01;
        private System.Windows.Forms.TextBox txtMensaje01;
        private System.Windows.Forms.Label lblUrlImagen;
        private System.Windows.Forms.TextBox txtUrlImagen;
        private System.Windows.Forms.Label lblDescuento;
        private System.Windows.Forms.TextBox txtDescuento;
        private System.Windows.Forms.Label lblCupon;
        private System.Windows.Forms.TextBox txtCupon;
        private System.Windows.Forms.Label lblCoupon;
        private System.Windows.Forms.ComboBox cboTienda;
        private System.Windows.Forms.CheckBox chkExclusivoApp;
    }
}