﻿using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System;
using System.Globalization;
using System.Text;
using System.IO;
using System.Reflection;
using System.Linq;

namespace VisorCupones
{
    public class Util
    {

        //Titulo de la Aplicacion
        private static string TitleApplication = "Visor de Cupones";

        public static void PointerLoad(Form form)
        {
            form.Cursor = Cursors.WaitCursor;
        }

        public static void PointerReady(Form form)
        {
            form.Cursor = Cursors.Arrow;
        }

        #region Mensajes

        public static void ErrorMessage(string mensaje)
        {
            MessageBox.Show(mensaje, TitleApplication, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void CriticalMessage(string mensaje)
        {
            MessageBox.Show(mensaje, TitleApplication, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void InformationMessage(string mensaje)
        {
            MessageBox.Show(mensaje, TitleApplication, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool ConfirmationMessage(string mensaje)
        {

            DialogResult rpta = MessageBox.Show(mensaje, TitleApplication, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (rpta == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion

        public static void FormatDatagridview(ref DataGridView dgv)
        {
            dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            dgv.ReadOnly = true;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.AllowUserToAddRows = false;
            dgv.AllowUserToDeleteRows = false;
            dgv.MultiSelect = false;
            dgv.AllowDrop = false;
            dgv.RowHeadersVisible = false;
        }

        public static void AutoWidthColumn(ref DataGridView dgv, string columnName, bool scroll = true)
        {

            int width = dgv.Width - (scroll == true ? 20 : 3);

            if (dgv.RowHeadersVisible == true)
                width = width - dgv.RowHeadersWidth;

            for (int c = 0; c <= dgv.ColumnCount - 1; c++)
            {
                if (c != dgv.Columns[columnName].Index)
                {
                    if (dgv.Columns[c].Visible)
                    {
                        width = width - dgv.Columns[c].Width;
                    }
                }
            }

            dgv.Columns[columnName].Width = width;

        }

        /// <summary>
        /// Convertir ruta URL a la ruta FTP
        /// </summary>
        /// <param name="pathUrl">ruta de archivo</param>
        /// <returns></returns>
        public static string TranslateToPathFtp(string pathUrl)
        {

            string pathFtp = "";

            try
            {
                string directory = "";
                string filename = "";

                string urlDir = Path.GetDirectoryName(pathUrl).Split('\\').Last();
                switch (urlDir.ToUpper())
                {
                    case "KIOSKO2":
                        directory = "MisCupones";
                        break;
                    case "CUPONMARCA":
                        directory = "CuponesMarca";
                        break;
                    case "CUPONLUGAR":
                        directory = "CuponesLugar";
                        break;
                    case "CLUBW":
                        directory = "ClubW";
                        break;
                    case "SUPERCUPON":
                        directory = "SuperCupon";
                        break;
                    default:
                        directory = urlDir;
                        break;
                }

                filename = Path.GetFileName(pathUrl);

                pathFtp = Path.Combine("Imagenes", directory, filename).Replace("\\", "/");

                return pathFtp;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}