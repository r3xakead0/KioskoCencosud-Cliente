﻿namespace VisorCupones.Winform
{
    partial class FrmMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.btnCuponDiseno = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCuponMiniatura = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCuponSuper = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lblVersion = new System.Windows.Forms.Label();
            this.picIntro = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picIntro)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCuponDiseno
            // 
            this.btnCuponDiseno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCuponDiseno.Depth = 0;
            this.btnCuponDiseno.Location = new System.Drawing.Point(403, 135);
            this.btnCuponDiseno.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponDiseno.Name = "btnCuponDiseno";
            this.btnCuponDiseno.Primary = true;
            this.btnCuponDiseno.Size = new System.Drawing.Size(141, 24);
            this.btnCuponDiseno.TabIndex = 17;
            this.btnCuponDiseno.Text = "Diseño Cupon";
            this.btnCuponDiseno.UseVisualStyleBackColor = true;
            this.btnCuponDiseno.Click += new System.EventHandler(this.btnCuponDiseno_Click);
            // 
            // btnCuponMiniatura
            // 
            this.btnCuponMiniatura.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCuponMiniatura.Depth = 0;
            this.btnCuponMiniatura.Location = new System.Drawing.Point(403, 75);
            this.btnCuponMiniatura.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponMiniatura.Name = "btnCuponMiniatura";
            this.btnCuponMiniatura.Primary = true;
            this.btnCuponMiniatura.Size = new System.Drawing.Size(141, 24);
            this.btnCuponMiniatura.TabIndex = 18;
            this.btnCuponMiniatura.Text = "Miniatura Cupon";
            this.btnCuponMiniatura.UseVisualStyleBackColor = true;
            this.btnCuponMiniatura.Click += new System.EventHandler(this.btnCuponMiniatura_Click);
            // 
            // btnCuponSuper
            // 
            this.btnCuponSuper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCuponSuper.Depth = 0;
            this.btnCuponSuper.Location = new System.Drawing.Point(403, 105);
            this.btnCuponSuper.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponSuper.Name = "btnCuponSuper";
            this.btnCuponSuper.Primary = true;
            this.btnCuponSuper.Size = new System.Drawing.Size(141, 24);
            this.btnCuponSuper.TabIndex = 19;
            this.btnCuponSuper.Text = "Super Cupon";
            this.btnCuponSuper.UseVisualStyleBackColor = true;
            this.btnCuponSuper.Click += new System.EventHandler(this.btnCuponSuper_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(403, 318);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(141, 13);
            this.lblVersion.TabIndex = 45;
            this.lblVersion.Text = "VERSIÓN 0.0.0.0";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picIntro
            // 
            this.picIntro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picIntro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picIntro.Image = global::VisorCupones.Winform.Properties.Resources.intro;
            this.picIntro.Location = new System.Drawing.Point(12, 75);
            this.picIntro.Name = "picIntro";
            this.picIntro.Size = new System.Drawing.Size(385, 256);
            this.picIntro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picIntro.TabIndex = 20;
            this.picIntro.TabStop = false;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(556, 343);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.picIntro);
            this.Controls.Add(this.btnCuponSuper);
            this.Controls.Add(this.btnCuponMiniatura);
            this.Controls.Add(this.btnCuponDiseno);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visor de Cupones";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picIntro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponDiseno;
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponMiniatura;
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponSuper;
        private System.Windows.Forms.PictureBox picIntro;
        private System.Windows.Forms.Label lblVersion;
    }
}

