﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using COMMON = Cupones.Common;

namespace VisorCupones.Winform
{
    public partial class FrmSuperCoupon : MaterialForm
    {

        #region "Singletons"

        private static FrmSuperCoupon frmInstance = null;

        public static FrmSuperCoupon Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmSuperCoupon();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        private string Ambito = "";

        public FrmSuperCoupon()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void CouponList_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                var lstCompanias = new BL.Configuraciones().ListarCompanhias();
                this.cboAmbito.DataSource = lstCompanias;
                this.cboAmbito.DisplayMember = "Nombre";
                this.cboAmbito.ValueMember = "Codigo";

                #endregion

                #region Grupo Cupon

                DataTable dtGrupo = new DataTable();
                dtGrupo.Columns.Add("codigo", typeof(int));
                dtGrupo.Columns.Add("nombre", typeof(string));

                DataRow drGrupo = null;
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 1;
                drGrupo["nombre"] = "Disponible";
                dtGrupo.Rows.Add(drGrupo);
                //drGrupo = dtGrupo.NewRow();
                //drGrupo["codigo"] = 2;
                //drGrupo["nombre"] = "Super";
                //Grupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 3;
                drGrupo["nombre"] = "Marca";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 4;
                drGrupo["nombre"] = "Lugar";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 5;
                drGrupo["nombre"] = "Ruleta";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 6;
                drGrupo["nombre"] = "Arbol";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 7;
                drGrupo["nombre"] = "Bonus";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 8;
                drGrupo["nombre"] = "Invitacion";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 9;
                drGrupo["nombre"] = "Comercial";
                dtGrupo.Rows.Add(drGrupo);

                this.CargarComboBox(ref this.cboGrupoCupon, dtGrupo);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

                #region Tiendas

                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";

                #endregion

                var lstCoupons = new List<BE.Cupon>();
                var sorted = new SortableBindingList<BE.Cupon>(lstCoupons);
                this.dgvCoupons.DataSource = sorted;

                this.FormatCoupons();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboAmbito_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                var empresa = cboAmbito.SelectedValue.ToString().ToUpper();

                var lstTiendas = new BL.Configuraciones().ListarTiendas(empresa);

                this.cboTienda.DataSource = lstTiendas;
                this.cboTienda.DisplayMember = "Nombre";
                this.cboTienda.ValueMember = "Codigo";
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string nroDocumento = this.txtNroDocumento.Text.Trim();

                this.Ambito = this.cboAmbito.SelectedValue.ToString();
                string tienda = this.cboTienda.SelectedValue.ToString();
                int idGrupoCupon = int.Parse(this.cboGrupoCupon.SelectedValue.ToString());
                int idTipoCupon = 0;

                this.MostrarCupones(nroDocumento, idGrupoCupon, this.Ambito, tienda, idTipoCupon);
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvCoupons_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.dgvCoupons.CurrentRow != null)
                {
                    var coupon = (BE.Cupon)this.dgvCoupons.CurrentRow.DataBoundItem;
                    if (coupon != null)
                    {
                        this.picCoupon.Image = new COMMON.Coupon(this.Ambito).ImagePreview(coupon);
                        this.LoadCouponDetail(coupon);
                    }
                    else
                    {
                        this.picCoupon.Image = null;
                        this.LoadCouponDetail(null);
                    }
                }
                else
                {
                    this.picCoupon.Image = null;
                    this.LoadCouponDetail(null);
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCoupons.CurrentRow != null)
                {
                    var coupon = (BE.Cupon)this.dgvCoupons.CurrentRow.DataBoundItem;

                    if (coupon != null)
                    {
                        coupon.Descuento = this.txtDescuento.Text;
                        coupon.Mensaje1 = this.txtMensaje01.Text;
                        coupon.Mensaje2 = this.txtMensaje02.Text;
                        coupon.Mensaje3 = this.txtMensaje03.Text;
                        coupon.ImagenPath = this.txtUrlImagen.Text;

                        this.picCoupon.Image = new COMMON.Coupon(this.Ambito).ImagePreview(coupon);
                        this.LoadCouponDetail(coupon);
                    }
                    else
                    {
                        this.picCoupon.Image = null;
                        this.LoadCouponDetail(null);
                    }
                }
                else
                {
                    this.picCoupon.Image = null;
                    this.LoadCouponDetail(null);
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            cbo.DataSource = dt;
            cbo.DisplayMember = dt.Columns[1].Caption;
            cbo.ValueMember = dt.Columns[0].Caption;
        }

        public void MostrarCupones(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            try
            {
                var lstCoupons = new BL.Cupones().ListarCupones(nroDocumento, idGrupo, ambito, tienda, tipoCupon);

                for (int i = 0; i < lstCoupons.Count; i++)
                {
                    string directory = Path.GetDirectoryName(lstCoupons[i].ImagenPath).Split('\\').Last();
                    string filename = Path.GetFileName(lstCoupons[i].ImagenPath);
                    lstCoupons[i].ImagenPath = Path.Combine(directory, filename);
                }

                if (lstCoupons != null)
                {
                    var sorted = new SortableBindingList<BE.Cupon>(lstCoupons);
                    this.dgvCoupons.DataSource = sorted;
                }       
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatCoupons()
        {
            try
            {
                Util.FormatDatagridview(ref dgvCoupons);

                for (int i = 0; i < this.dgvCoupons.Columns.Count; i++)
                {
                    this.dgvCoupons.Columns[i].Visible = false;
                }
                this.dgvCoupons.Columns["CodCampania"].Visible = true;
                this.dgvCoupons.Columns["CodCampania"].HeaderText = "Cupon";
                this.dgvCoupons.Columns["CodCampania"].Width = 80;
                this.dgvCoupons.Columns["CodCampania"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Descuento"].Visible = true;
                this.dgvCoupons.Columns["Descuento"].HeaderText = "Descuento";
                this.dgvCoupons.Columns["Descuento"].Width = 80;
                this.dgvCoupons.Columns["Descuento"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dgvCoupons.Columns["Mensaje1"].Visible = true;
                this.dgvCoupons.Columns["Mensaje1"].HeaderText = "Mensaje 1";
                this.dgvCoupons.Columns["Mensaje1"].Width = 120;
                this.dgvCoupons.Columns["Mensaje1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Mensaje2"].Visible = true;
                this.dgvCoupons.Columns["Mensaje2"].HeaderText = "Mensaje 2";
                this.dgvCoupons.Columns["Mensaje2"].Width = 120;
                this.dgvCoupons.Columns["Mensaje2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Mensaje3"].Visible = true;
                this.dgvCoupons.Columns["Mensaje3"].HeaderText = "Mensaje 3";
                this.dgvCoupons.Columns["Mensaje3"].Width = 120;
                this.dgvCoupons.Columns["Mensaje3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["ImagenPath"].Visible = true;
                this.dgvCoupons.Columns["ImagenPath"].HeaderText = "Url Imagen";
                this.dgvCoupons.Columns["ImagenPath"].Width = 200;
                this.dgvCoupons.Columns["ImagenPath"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                Util.AutoWidthColumn(ref dgvCoupons, "ImagenPath");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadCouponDetail(BE.Cupon coupon = null)
        {
            try
            {
                if (coupon != null)
                {
                    this.txtCupon.Text = coupon.CodCampania.ToString();
                    this.txtDescuento.Text = coupon.Descuento;
                    this.txtMensaje01.Text = coupon.Mensaje1;
                    this.txtMensaje02.Text = coupon.Mensaje2;
                    this.txtMensaje03.Text = coupon.Mensaje3;
                    this.txtUrlImagen.Text = coupon.ImagenPath;
                    this.chkExclusivoApp.Checked = coupon.ExclusivoApp;
                }
                else
                {
                    this.txtCupon.Clear();
                    this.txtDescuento.Clear();
                    this.txtMensaje01.Clear();
                    this.txtMensaje02.Clear();
                    this.txtMensaje03.Clear();
                    this.txtUrlImagen.Clear();
                    this.chkExclusivoApp.Checked = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
