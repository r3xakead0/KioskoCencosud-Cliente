﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Windows.Forms;

namespace VisorCupones.Winform
{
    public partial class FrmMenu : MaterialForm
    {

        private readonly MaterialSkinManager materialSkinManager;

        public FrmMenu()
        {
            InitializeComponent();

            // Initialize MaterialSkinManager
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            
        }
        
        private void btnCuponMiniatura_Click(object sender, EventArgs e)
        {
            try
            {
                var min = FrmSuperCoupon.Instance();
                min.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnCuponDiseno_Click(object sender, EventArgs e)
        {
            try
            {
                var detail = FrmCouponView.Instance();
                detail.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnCuponSuper_Click(object sender, EventArgs e)
        {
            try
            {
                var super = FrmCouponSuper.Instance();
                super.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblVersion.Text = $"VERSIÓN { Application.ProductVersion }";
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
    }

}
