﻿namespace VisorCupones.Winform
{
    partial class FrmCouponView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCouponView));
            this.picCoupon = new System.Windows.Forms.PictureBox();
            this.grpCouponDetail = new System.Windows.Forms.GroupBox();
            this.cboTienda = new System.Windows.Forms.ComboBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.lblVigencia = new System.Windows.Forms.Label();
            this.txtVigencia = new System.Windows.Forms.TextBox();
            this.chkBonusSeriado = new System.Windows.Forms.CheckBox();
            this.lblCodigoOnline = new System.Windows.Forms.Label();
            this.txtCodigoOnline = new System.Windows.Forms.TextBox();
            this.lbloNumeroBonus = new System.Windows.Forms.Label();
            this.txtNumeroBonus = new System.Windows.Forms.TextBox();
            this.lblTienda = new System.Windows.Forms.Label();
            this.cboCanal = new System.Windows.Forms.ComboBox();
            this.lblCanal = new System.Windows.Forms.Label();
            this.lblCodigoBarra = new System.Windows.Forms.Label();
            this.txtCodigoBarra = new System.Windows.Forms.TextBox();
            this.lblMensaje03 = new System.Windows.Forms.Label();
            this.txtMensaje03 = new System.Windows.Forms.TextBox();
            this.lblMensaje02 = new System.Windows.Forms.Label();
            this.txtMensaje02 = new System.Windows.Forms.TextBox();
            this.lblMensaje01 = new System.Windows.Forms.Label();
            this.txtMensaje01 = new System.Windows.Forms.TextBox();
            this.lblMensajeLegal = new System.Windows.Forms.Label();
            this.txtMensajeLegal = new System.Windows.Forms.TextBox();
            this.lblUrlImagen = new System.Windows.Forms.Label();
            this.txtUrlImagen = new System.Windows.Forms.TextBox();
            this.cboAmbito = new System.Windows.Forms.ComboBox();
            this.lblAmbito = new System.Windows.Forms.Label();
            this.lblDescuento = new System.Windows.Forms.Label();
            this.txtDescuento = new System.Windows.Forms.TextBox();
            this.cboTipoLegal = new System.Windows.Forms.ComboBox();
            this.lblTipoLegal = new System.Windows.Forms.Label();
            this.btnObtener = new System.Windows.Forms.Button();
            this.cboTipoCupon = new System.Windows.Forms.ComboBox();
            this.cboGrupoCupon = new System.Windows.Forms.ComboBox();
            this.txtNroCupon = new System.Windows.Forms.TextBox();
            this.lblDocumento = new System.Windows.Forms.Label();
            this.txtNroDocumento = new System.Windows.Forms.TextBox();
            this.cboTipoDocumento = new System.Windows.Forms.ComboBox();
            this.lblCupon = new System.Windows.Forms.Label();
            this.grpCouponGet = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.picCoupon)).BeginInit();
            this.grpCouponDetail.SuspendLayout();
            this.grpCouponGet.SuspendLayout();
            this.SuspendLayout();
            // 
            // picCoupon
            // 
            this.picCoupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.picCoupon.BackColor = System.Drawing.Color.White;
            this.picCoupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCoupon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picCoupon.Location = new System.Drawing.Point(12, 76);
            this.picCoupon.Name = "picCoupon";
            this.picCoupon.Size = new System.Drawing.Size(267, 615);
            this.picCoupon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCoupon.TabIndex = 0;
            this.picCoupon.TabStop = false;
            this.picCoupon.Click += new System.EventHandler(this.picCoupon_Click);
            // 
            // grpCouponDetail
            // 
            this.grpCouponDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCouponDetail.Controls.Add(this.cboTienda);
            this.grpCouponDetail.Controls.Add(this.btnExportar);
            this.grpCouponDetail.Controls.Add(this.btnVisualizar);
            this.grpCouponDetail.Controls.Add(this.lblVigencia);
            this.grpCouponDetail.Controls.Add(this.txtVigencia);
            this.grpCouponDetail.Controls.Add(this.chkBonusSeriado);
            this.grpCouponDetail.Controls.Add(this.lblCodigoOnline);
            this.grpCouponDetail.Controls.Add(this.txtCodigoOnline);
            this.grpCouponDetail.Controls.Add(this.lbloNumeroBonus);
            this.grpCouponDetail.Controls.Add(this.txtNumeroBonus);
            this.grpCouponDetail.Controls.Add(this.lblTienda);
            this.grpCouponDetail.Controls.Add(this.cboCanal);
            this.grpCouponDetail.Controls.Add(this.lblCanal);
            this.grpCouponDetail.Controls.Add(this.lblCodigoBarra);
            this.grpCouponDetail.Controls.Add(this.txtCodigoBarra);
            this.grpCouponDetail.Controls.Add(this.lblMensaje03);
            this.grpCouponDetail.Controls.Add(this.txtMensaje03);
            this.grpCouponDetail.Controls.Add(this.lblMensaje02);
            this.grpCouponDetail.Controls.Add(this.txtMensaje02);
            this.grpCouponDetail.Controls.Add(this.lblMensaje01);
            this.grpCouponDetail.Controls.Add(this.txtMensaje01);
            this.grpCouponDetail.Controls.Add(this.lblMensajeLegal);
            this.grpCouponDetail.Controls.Add(this.txtMensajeLegal);
            this.grpCouponDetail.Controls.Add(this.lblUrlImagen);
            this.grpCouponDetail.Controls.Add(this.txtUrlImagen);
            this.grpCouponDetail.Controls.Add(this.cboAmbito);
            this.grpCouponDetail.Controls.Add(this.lblAmbito);
            this.grpCouponDetail.Controls.Add(this.lblDescuento);
            this.grpCouponDetail.Controls.Add(this.txtDescuento);
            this.grpCouponDetail.Controls.Add(this.cboTipoLegal);
            this.grpCouponDetail.Controls.Add(this.lblTipoLegal);
            this.grpCouponDetail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponDetail.Location = new System.Drawing.Point(285, 162);
            this.grpCouponDetail.Name = "grpCouponDetail";
            this.grpCouponDetail.Size = new System.Drawing.Size(503, 529);
            this.grpCouponDetail.TabIndex = 1;
            this.grpCouponDetail.TabStop = false;
            this.grpCouponDetail.Text = "Detalle de Cupon";
            // 
            // cboTienda
            // 
            this.cboTienda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTienda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTienda.FormattingEnabled = true;
            this.cboTienda.Location = new System.Drawing.Point(146, 54);
            this.cboTienda.Name = "cboTienda";
            this.cboTienda.Size = new System.Drawing.Size(325, 21);
            this.cboTienda.TabIndex = 44;
            this.cboTienda.TabStop = false;
            // 
            // btnExportar
            // 
            this.btnExportar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportar.Location = new System.Drawing.Point(261, 490);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(210, 23);
            this.btnExportar.TabIndex = 34;
            this.btnExportar.Text = "&Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(35, 490);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(210, 23);
            this.btnVisualizar.TabIndex = 33;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // lblVigencia
            // 
            this.lblVigencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVigencia.AutoSize = true;
            this.lblVigencia.Location = new System.Drawing.Point(32, 411);
            this.lblVigencia.Name = "lblVigencia";
            this.lblVigencia.Size = new System.Drawing.Size(62, 13);
            this.lblVigencia.TabIndex = 32;
            this.lblVigencia.Text = "Vigencia";
            // 
            // txtVigencia
            // 
            this.txtVigencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVigencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVigencia.Location = new System.Drawing.Point(146, 409);
            this.txtVigencia.Name = "txtVigencia";
            this.txtVigencia.Size = new System.Drawing.Size(325, 21);
            this.txtVigencia.TabIndex = 31;
            this.txtVigencia.TabStop = false;
            // 
            // chkBonusSeriado
            // 
            this.chkBonusSeriado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBonusSeriado.AutoSize = true;
            this.chkBonusSeriado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBonusSeriado.Location = new System.Drawing.Point(352, 464);
            this.chkBonusSeriado.Name = "chkBonusSeriado";
            this.chkBonusSeriado.Size = new System.Drawing.Size(119, 17);
            this.chkBonusSeriado.TabIndex = 30;
            this.chkBonusSeriado.TabStop = false;
            this.chkBonusSeriado.Text = "Bonus Seriado";
            this.chkBonusSeriado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBonusSeriado.UseVisualStyleBackColor = true;
            // 
            // lblCodigoOnline
            // 
            this.lblCodigoOnline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCodigoOnline.AutoSize = true;
            this.lblCodigoOnline.Location = new System.Drawing.Point(32, 165);
            this.lblCodigoOnline.Name = "lblCodigoOnline";
            this.lblCodigoOnline.Size = new System.Drawing.Size(96, 13);
            this.lblCodigoOnline.TabIndex = 29;
            this.lblCodigoOnline.Text = "Codigo Online";
            // 
            // txtCodigoOnline
            // 
            this.txtCodigoOnline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigoOnline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigoOnline.Location = new System.Drawing.Point(146, 163);
            this.txtCodigoOnline.Name = "txtCodigoOnline";
            this.txtCodigoOnline.Size = new System.Drawing.Size(325, 21);
            this.txtCodigoOnline.TabIndex = 28;
            this.txtCodigoOnline.TabStop = false;
            // 
            // lbloNumeroBonus
            // 
            this.lbloNumeroBonus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbloNumeroBonus.AutoSize = true;
            this.lbloNumeroBonus.Location = new System.Drawing.Point(32, 465);
            this.lbloNumeroBonus.Name = "lbloNumeroBonus";
            this.lbloNumeroBonus.Size = new System.Drawing.Size(101, 13);
            this.lbloNumeroBonus.TabIndex = 27;
            this.lbloNumeroBonus.Text = "Numero Bonus";
            // 
            // txtNumeroBonus
            // 
            this.txtNumeroBonus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumeroBonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumeroBonus.Location = new System.Drawing.Point(146, 463);
            this.txtNumeroBonus.Name = "txtNumeroBonus";
            this.txtNumeroBonus.Size = new System.Drawing.Size(200, 21);
            this.txtNumeroBonus.TabIndex = 26;
            this.txtNumeroBonus.TabStop = false;
            // 
            // lblTienda
            // 
            this.lblTienda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTienda.AutoSize = true;
            this.lblTienda.Location = new System.Drawing.Point(32, 57);
            this.lblTienda.Name = "lblTienda";
            this.lblTienda.Size = new System.Drawing.Size(51, 13);
            this.lblTienda.TabIndex = 21;
            this.lblTienda.Text = "Tienda";
            // 
            // cboCanal
            // 
            this.cboCanal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCanal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCanal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCanal.FormattingEnabled = true;
            this.cboCanal.Location = new System.Drawing.Point(146, 136);
            this.cboCanal.Name = "cboCanal";
            this.cboCanal.Size = new System.Drawing.Size(325, 21);
            this.cboCanal.TabIndex = 19;
            this.cboCanal.TabStop = false;
            // 
            // lblCanal
            // 
            this.lblCanal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCanal.AutoSize = true;
            this.lblCanal.Location = new System.Drawing.Point(32, 139);
            this.lblCanal.Name = "lblCanal";
            this.lblCanal.Size = new System.Drawing.Size(43, 13);
            this.lblCanal.TabIndex = 18;
            this.lblCanal.Text = "Canal";
            // 
            // lblCodigoBarra
            // 
            this.lblCodigoBarra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCodigoBarra.AutoSize = true;
            this.lblCodigoBarra.Location = new System.Drawing.Point(32, 438);
            this.lblCodigoBarra.Name = "lblCodigoBarra";
            this.lblCodigoBarra.Size = new System.Drawing.Size(111, 13);
            this.lblCodigoBarra.TabIndex = 17;
            this.lblCodigoBarra.Text = "Codigo de Barra";
            // 
            // txtCodigoBarra
            // 
            this.txtCodigoBarra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigoBarra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigoBarra.Location = new System.Drawing.Point(146, 436);
            this.txtCodigoBarra.Name = "txtCodigoBarra";
            this.txtCodigoBarra.Size = new System.Drawing.Size(325, 21);
            this.txtCodigoBarra.TabIndex = 16;
            this.txtCodigoBarra.TabStop = false;
            // 
            // lblMensaje03
            // 
            this.lblMensaje03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje03.AutoSize = true;
            this.lblMensaje03.Location = new System.Drawing.Point(32, 384);
            this.lblMensaje03.Name = "lblMensaje03";
            this.lblMensaje03.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje03.TabIndex = 15;
            this.lblMensaje03.Text = "Mensaje 3";
            // 
            // txtMensaje03
            // 
            this.txtMensaje03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje03.Location = new System.Drawing.Point(146, 382);
            this.txtMensaje03.Name = "txtMensaje03";
            this.txtMensaje03.Size = new System.Drawing.Size(325, 21);
            this.txtMensaje03.TabIndex = 14;
            this.txtMensaje03.TabStop = false;
            // 
            // lblMensaje02
            // 
            this.lblMensaje02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje02.AutoSize = true;
            this.lblMensaje02.Location = new System.Drawing.Point(32, 357);
            this.lblMensaje02.Name = "lblMensaje02";
            this.lblMensaje02.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje02.TabIndex = 13;
            this.lblMensaje02.Text = "Mensaje 2";
            // 
            // txtMensaje02
            // 
            this.txtMensaje02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje02.Location = new System.Drawing.Point(146, 355);
            this.txtMensaje02.Name = "txtMensaje02";
            this.txtMensaje02.Size = new System.Drawing.Size(325, 21);
            this.txtMensaje02.TabIndex = 12;
            this.txtMensaje02.TabStop = false;
            // 
            // lblMensaje01
            // 
            this.lblMensaje01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensaje01.AutoSize = true;
            this.lblMensaje01.Location = new System.Drawing.Point(32, 330);
            this.lblMensaje01.Name = "lblMensaje01";
            this.lblMensaje01.Size = new System.Drawing.Size(73, 13);
            this.lblMensaje01.TabIndex = 11;
            this.lblMensaje01.Text = "Mensaje 1";
            // 
            // txtMensaje01
            // 
            this.txtMensaje01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensaje01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensaje01.Location = new System.Drawing.Point(146, 328);
            this.txtMensaje01.Name = "txtMensaje01";
            this.txtMensaje01.Size = new System.Drawing.Size(325, 21);
            this.txtMensaje01.TabIndex = 10;
            this.txtMensaje01.TabStop = false;
            // 
            // lblMensajeLegal
            // 
            this.lblMensajeLegal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensajeLegal.AutoSize = true;
            this.lblMensajeLegal.Location = new System.Drawing.Point(32, 192);
            this.lblMensajeLegal.Name = "lblMensajeLegal";
            this.lblMensajeLegal.Size = new System.Drawing.Size(100, 13);
            this.lblMensajeLegal.TabIndex = 9;
            this.lblMensajeLegal.Text = "Mensaje Legal";
            // 
            // txtMensajeLegal
            // 
            this.txtMensajeLegal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensajeLegal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensajeLegal.Location = new System.Drawing.Point(146, 190);
            this.txtMensajeLegal.Multiline = true;
            this.txtMensajeLegal.Name = "txtMensajeLegal";
            this.txtMensajeLegal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMensajeLegal.Size = new System.Drawing.Size(325, 105);
            this.txtMensajeLegal.TabIndex = 8;
            this.txtMensajeLegal.TabStop = false;
            // 
            // lblUrlImagen
            // 
            this.lblUrlImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUrlImagen.AutoSize = true;
            this.lblUrlImagen.Location = new System.Drawing.Point(32, 111);
            this.lblUrlImagen.Name = "lblUrlImagen";
            this.lblUrlImagen.Size = new System.Drawing.Size(80, 13);
            this.lblUrlImagen.TabIndex = 7;
            this.lblUrlImagen.Text = "Url Imagen";
            // 
            // txtUrlImagen
            // 
            this.txtUrlImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrlImagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUrlImagen.Location = new System.Drawing.Point(146, 109);
            this.txtUrlImagen.Name = "txtUrlImagen";
            this.txtUrlImagen.Size = new System.Drawing.Size(325, 21);
            this.txtUrlImagen.TabIndex = 6;
            this.txtUrlImagen.TabStop = false;
            // 
            // cboAmbito
            // 
            this.cboAmbito.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboAmbito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmbito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAmbito.FormattingEnabled = true;
            this.cboAmbito.Location = new System.Drawing.Point(146, 27);
            this.cboAmbito.Name = "cboAmbito";
            this.cboAmbito.Size = new System.Drawing.Size(325, 21);
            this.cboAmbito.TabIndex = 5;
            this.cboAmbito.TabStop = false;
            this.cboAmbito.SelectionChangeCommitted += new System.EventHandler(this.cboAmbito_SelectionChangeCommitted);
            // 
            // lblAmbito
            // 
            this.lblAmbito.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAmbito.AutoSize = true;
            this.lblAmbito.Location = new System.Drawing.Point(32, 30);
            this.lblAmbito.Name = "lblAmbito";
            this.lblAmbito.Size = new System.Drawing.Size(53, 13);
            this.lblAmbito.TabIndex = 4;
            this.lblAmbito.Text = "Ambito";
            // 
            // lblDescuento
            // 
            this.lblDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescuento.AutoSize = true;
            this.lblDescuento.Location = new System.Drawing.Point(32, 303);
            this.lblDescuento.Name = "lblDescuento";
            this.lblDescuento.Size = new System.Drawing.Size(75, 13);
            this.lblDescuento.TabIndex = 3;
            this.lblDescuento.Text = "Descuento";
            // 
            // txtDescuento
            // 
            this.txtDescuento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescuento.Location = new System.Drawing.Point(146, 301);
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.Size = new System.Drawing.Size(325, 21);
            this.txtDescuento.TabIndex = 2;
            this.txtDescuento.TabStop = false;
            // 
            // cboTipoLegal
            // 
            this.cboTipoLegal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipoLegal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoLegal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoLegal.FormattingEnabled = true;
            this.cboTipoLegal.Location = new System.Drawing.Point(146, 82);
            this.cboTipoLegal.Name = "cboTipoLegal";
            this.cboTipoLegal.Size = new System.Drawing.Size(325, 21);
            this.cboTipoLegal.TabIndex = 1;
            this.cboTipoLegal.TabStop = false;
            // 
            // lblTipoLegal
            // 
            this.lblTipoLegal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoLegal.AutoSize = true;
            this.lblTipoLegal.Location = new System.Drawing.Point(32, 85);
            this.lblTipoLegal.Name = "lblTipoLegal";
            this.lblTipoLegal.Size = new System.Drawing.Size(74, 13);
            this.lblTipoLegal.TabIndex = 0;
            this.lblTipoLegal.Text = "Tipo Legal";
            // 
            // btnObtener
            // 
            this.btnObtener.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnObtener.Location = new System.Drawing.Point(388, 47);
            this.btnObtener.Name = "btnObtener";
            this.btnObtener.Size = new System.Drawing.Size(83, 21);
            this.btnObtener.TabIndex = 37;
            this.btnObtener.Text = "&Obtener";
            this.btnObtener.UseVisualStyleBackColor = true;
            this.btnObtener.Click += new System.EventHandler(this.btnObtener_Click);
            // 
            // cboTipoCupon
            // 
            this.cboTipoCupon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTipoCupon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoCupon.FormattingEnabled = true;
            this.cboTipoCupon.Location = new System.Drawing.Point(388, 20);
            this.cboTipoCupon.Name = "cboTipoCupon";
            this.cboTipoCupon.Size = new System.Drawing.Size(83, 21);
            this.cboTipoCupon.TabIndex = 36;
            this.cboTipoCupon.TabStop = false;
            // 
            // cboGrupoCupon
            // 
            this.cboGrupoCupon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGrupoCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboGrupoCupon.FormattingEnabled = true;
            this.cboGrupoCupon.Location = new System.Drawing.Point(146, 20);
            this.cboGrupoCupon.Name = "cboGrupoCupon";
            this.cboGrupoCupon.Size = new System.Drawing.Size(132, 21);
            this.cboGrupoCupon.TabIndex = 35;
            this.cboGrupoCupon.TabStop = false;
            this.cboGrupoCupon.SelectionChangeCommitted += new System.EventHandler(this.cboGrupoCupon_SelectionChangeCommitted);
            // 
            // txtNroCupon
            // 
            this.txtNroCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNroCupon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNroCupon.Location = new System.Drawing.Point(284, 20);
            this.txtNroCupon.Name = "txtNroCupon";
            this.txtNroCupon.Size = new System.Drawing.Size(99, 21);
            this.txtNroCupon.TabIndex = 34;
            this.txtNroCupon.TabStop = false;
            // 
            // lblDocumento
            // 
            this.lblDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocumento.AutoSize = true;
            this.lblDocumento.Location = new System.Drawing.Point(32, 50);
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Size = new System.Drawing.Size(80, 13);
            this.lblDocumento.TabIndex = 25;
            this.lblDocumento.Text = "Documento";
            // 
            // txtNroDocumento
            // 
            this.txtNroDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNroDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNroDocumento.Location = new System.Drawing.Point(284, 47);
            this.txtNroDocumento.Name = "txtNroDocumento";
            this.txtNroDocumento.Size = new System.Drawing.Size(99, 21);
            this.txtNroDocumento.TabIndex = 24;
            this.txtNroDocumento.TabStop = false;
            this.txtNroDocumento.Leave += new System.EventHandler(this.txtNroDocumento_Leave);
            // 
            // cboTipoDocumento
            // 
            this.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDocumento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoDocumento.FormattingEnabled = true;
            this.cboTipoDocumento.Location = new System.Drawing.Point(146, 47);
            this.cboTipoDocumento.Name = "cboTipoDocumento";
            this.cboTipoDocumento.Size = new System.Drawing.Size(132, 21);
            this.cboTipoDocumento.TabIndex = 23;
            this.cboTipoDocumento.TabStop = false;
            // 
            // lblCupon
            // 
            this.lblCupon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCupon.AutoSize = true;
            this.lblCupon.Location = new System.Drawing.Point(32, 23);
            this.lblCupon.Name = "lblCupon";
            this.lblCupon.Size = new System.Drawing.Size(47, 13);
            this.lblCupon.TabIndex = 22;
            this.lblCupon.Text = "Cupon";
            // 
            // grpCouponGet
            // 
            this.grpCouponGet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCouponGet.Controls.Add(this.btnObtener);
            this.grpCouponGet.Controls.Add(this.lblCupon);
            this.grpCouponGet.Controls.Add(this.lblDocumento);
            this.grpCouponGet.Controls.Add(this.cboTipoCupon);
            this.grpCouponGet.Controls.Add(this.txtNroDocumento);
            this.grpCouponGet.Controls.Add(this.cboTipoDocumento);
            this.grpCouponGet.Controls.Add(this.cboGrupoCupon);
            this.grpCouponGet.Controls.Add(this.txtNroCupon);
            this.grpCouponGet.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCouponGet.Location = new System.Drawing.Point(285, 76);
            this.grpCouponGet.Name = "grpCouponGet";
            this.grpCouponGet.Size = new System.Drawing.Size(503, 80);
            this.grpCouponGet.TabIndex = 34;
            this.grpCouponGet.TabStop = false;
            this.grpCouponGet.Text = "Obtener Cupon";
            // 
            // FrmCouponView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 703);
            this.Controls.Add(this.grpCouponGet);
            this.Controls.Add(this.grpCouponDetail);
            this.Controls.Add(this.picCoupon);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCouponView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impresión de Cupon";
            this.Load += new System.EventHandler(this.Cupoun_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picCoupon)).EndInit();
            this.grpCouponDetail.ResumeLayout(false);
            this.grpCouponDetail.PerformLayout();
            this.grpCouponGet.ResumeLayout(false);
            this.grpCouponGet.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picCoupon;
        private System.Windows.Forms.GroupBox grpCouponDetail;
        private System.Windows.Forms.ComboBox cboTipoLegal;
        private System.Windows.Forms.Label lblTipoLegal;
        private System.Windows.Forms.TextBox txtDescuento;
        private System.Windows.Forms.Label lblDescuento;
        private System.Windows.Forms.ComboBox cboAmbito;
        private System.Windows.Forms.Label lblAmbito;
        private System.Windows.Forms.Label lblUrlImagen;
        private System.Windows.Forms.TextBox txtUrlImagen;
        private System.Windows.Forms.Label lblMensajeLegal;
        private System.Windows.Forms.TextBox txtMensajeLegal;
        private System.Windows.Forms.Label lblMensaje01;
        private System.Windows.Forms.TextBox txtMensaje01;
        private System.Windows.Forms.Label lblMensaje03;
        private System.Windows.Forms.TextBox txtMensaje03;
        private System.Windows.Forms.Label lblMensaje02;
        private System.Windows.Forms.TextBox txtMensaje02;
        private System.Windows.Forms.Label lblCodigoBarra;
        private System.Windows.Forms.TextBox txtCodigoBarra;
        private System.Windows.Forms.ComboBox cboCanal;
        private System.Windows.Forms.Label lblCanal;
        private System.Windows.Forms.Label lblTienda;
        private System.Windows.Forms.Label lblDocumento;
        private System.Windows.Forms.TextBox txtNroDocumento;
        private System.Windows.Forms.ComboBox cboTipoDocumento;
        private System.Windows.Forms.Label lblCupon;
        private System.Windows.Forms.Label lbloNumeroBonus;
        private System.Windows.Forms.TextBox txtNumeroBonus;
        private System.Windows.Forms.Label lblCodigoOnline;
        private System.Windows.Forms.TextBox txtCodigoOnline;
        private System.Windows.Forms.CheckBox chkBonusSeriado;
        private System.Windows.Forms.Label lblVigencia;
        private System.Windows.Forms.TextBox txtVigencia;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.ComboBox cboGrupoCupon;
        private System.Windows.Forms.TextBox txtNroCupon;
        private System.Windows.Forms.ComboBox cboTipoCupon;
        private System.Windows.Forms.Button btnObtener;
        private System.Windows.Forms.GroupBox grpCouponGet;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.ComboBox cboTienda;
    }
}