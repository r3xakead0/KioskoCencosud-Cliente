﻿namespace VisorCupones.WebApi.Models
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class DetalleCupon
    {
        public int Codigo { get; set; }
        public string NroDocumento { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string LegalMensaje { get; set; }
        public string CodigoBarra { get; set; }
        public string CodigoOnline { get; set; }
        public int LegalTipo { get; set; } // 0 = Normal | 1 = Bebida Alcoholicas
        public int Canal { get; set; } // 1 = Canje Tienda | 2 = Canje Online | 3 = Canje Tienda y online
        public int BonusSeriado { get; set; } // 0 = No | 1 = Si
        public string NumeroBonus { get; set; } 
        public string Vigencia { get; set; }
        public string ImagenFtp { get; set; }
        public string ImagenImpresion { get; set; }
    }
}
