﻿namespace VisorCupones.WebApi.Models
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class GrupoCupon
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
