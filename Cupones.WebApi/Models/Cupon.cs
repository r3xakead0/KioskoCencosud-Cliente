﻿namespace VisorCupones.WebApi.Models
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Cupon
    {
        public int Codigo { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string ImagenFtp { get; set; }
        public string Compania { get; set; }
        public int Tipo { get; set; }
        public string MensajeLegal { get; set; }
    }
}
