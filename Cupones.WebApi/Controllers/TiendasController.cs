﻿using System.Collections.Generic;
using System.Web.Http;
using VisorCupones.WebApi.Models;
using BL = Cupones.BusinessLogic;

namespace VisorCupones.WebApi.Controllers
{
    public class TiendasController : ApiController
    {
        [HttpGet]
        [Route("api/tiendas")]
        public IEnumerable<Tienda> GetAllTiendas()
        {
            var lstTiendas = new List<Tienda>();

            var lstBeTiendas = new BL.Configuraciones().ListarTiendas();

            foreach (var beTienda in lstBeTiendas)
            {
                var tienda = new Tienda();

                tienda.Codigo = beTienda.Codigo;
                tienda.Nombre = beTienda.Nombre;

                lstTiendas.Add(tienda);
            }

            return lstTiendas;
        }

        [HttpGet]
        [Route("api/tiendas/{compania}")]
        public IEnumerable<Tienda> GetTiendas(string compania)
        {
            var lstTiendas = new List<Tienda>();

            var lstBeTiendas = new BL.Configuraciones().ListarTiendas(compania.ToUpper());

            foreach (var beTienda in lstBeTiendas)
            {
                var tienda = new Tienda();

                tienda.Codigo = beTienda.Codigo;
                tienda.Nombre = beTienda.Nombre;

                lstTiendas.Add(tienda);
            }

            return lstTiendas;
        }
    }
}
