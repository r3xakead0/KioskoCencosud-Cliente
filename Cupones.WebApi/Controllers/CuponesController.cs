﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Http;
using VisorCupones.WebApi.Models;
using BL = Cupones.BusinessLogic;
using BE = Cupones.BusinessEntity;
using COMMON = Cupones.Common;
using System;

namespace VisorCupones.WebApi.Controllers
{
    public class CuponesController : ApiController
    {

        [HttpGet]
        [Route("api/cupones/canales")]
        public IEnumerable<CanalCupon> GetCanalesCupones()
        {
            var lstCanalesCupon = new List<CanalCupon>();

            lstCanalesCupon.Add(new CanalCupon() { Id = 1, Nombre = "Canje Tienda" });
            lstCanalesCupon.Add(new CanalCupon() { Id = 2, Nombre = "Canje Online" });
            lstCanalesCupon.Add(new CanalCupon() { Id = 3, Nombre = "Canje Tienda y Online" });

            return lstCanalesCupon;
        }

        [HttpGet]
        [Route("api/cupones/legales")]
        public IEnumerable<LegalCupon> GetLegalesCupones()
        {
            var lstLegalesCupones = new List<LegalCupon>();

            lstLegalesCupones.Add(new LegalCupon() { Id = 0, Nombre = "Normal" });
            lstLegalesCupones.Add(new LegalCupon() { Id = 1, Nombre = "Bebida Alcoholicas" });

            return lstLegalesCupones;
        }

        [HttpGet]
        [Route("api/cupones/grupos")]
        public IEnumerable<GrupoCupon> GetGruposCupones()
        {
            var lstGruposCupones = new List<GrupoCupon>();

            lstGruposCupones.Add(new GrupoCupon() { Id = 1, Nombre = "Disponible" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 2, Nombre = "Super" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 3, Nombre = "Marca" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 4, Nombre = "Lugar" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 5, Nombre = "Ruleta" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 6, Nombre = "Arbol" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 7, Nombre = "Bonus" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 8, Nombre = "Invitacion" });
            lstGruposCupones.Add(new GrupoCupon() { Id = 9, Nombre = "Comercial" });
            
            return lstGruposCupones;
        }

        [HttpGet]
        [Route("api/cupones/tipos")]
        public IEnumerable<TipoCupon> GetGruposTipos(int idGrupo)
        {
            var lstTiposCupones = new List<TipoCupon>();

            switch (idGrupo)
            {
                case 2: //Super
                    lstTiposCupones.Add(new TipoCupon() { Id = 1, Nombre = "Regalo" });
                    lstTiposCupones.Add(new TipoCupon() { Id = 2, Nombre = "Normal" });
                    lstTiposCupones.Add(new TipoCupon() { Id = 3, Nombre = "Horario" });
                    break;
                default:
                    lstTiposCupones.Add(new TipoCupon() { Id = 0, Nombre = "Ninguna" });
                    break;
            }

            return lstTiposCupones;
        }

        [HttpPost]
        [Route("api/cupones/previa")]
        public string GetPreview(Cupon cupon)
        {
            string imagePreview = "";

            if (cupon != null)
            {
                var beCupon = new BE.Cupon();

                beCupon.Id = cupon.Codigo;
                beCupon.Descuento = cupon.Descuento == null ? "" : cupon.Descuento;
                beCupon.Mensaje1 = cupon.Mensaje1 == null ? "" : cupon.Mensaje1;
                beCupon.Mensaje2 = cupon.Mensaje2 == null ? "" : cupon.Mensaje2;
                beCupon.Mensaje3 = cupon.Mensaje3 == null ? "" : cupon.Mensaje3;
                beCupon.ImagenPath = cupon.ImagenFtp == null ? "" : cupon.ImagenFtp;
                beCupon.Descripcion = cupon.MensajeLegal == null ? "" : cupon.MensajeLegal;

                imagePreview = this.GeneratePreview(beCupon);
            }

            return imagePreview;
        }

        [HttpPost]
        [Route("api/cupones/previasuper")]
        public string GetPreviewSuper(Cupon cupon)
        {
            string imagePreview = "";

            if (cupon != null)
            {
                string compania = cupon.Compania == null ? "" : cupon.Compania;
                int tipo = cupon.Tipo;

                var beCupon = new BE.Cupon();

                beCupon.Id = cupon.Codigo;
                beCupon.Descuento = cupon.Descuento == null ? "" : cupon.Descuento;
                beCupon.Mensaje1 = cupon.Mensaje1 == null ? "" : cupon.Mensaje1;
                beCupon.Mensaje2 = cupon.Mensaje2 == null ? "" : cupon.Mensaje2;
                beCupon.Mensaje3 = cupon.Mensaje3 == null ? "" : cupon.Mensaje3;
                beCupon.ImagenPath = cupon.ImagenFtp == null ? "" : cupon.ImagenFtp;
                beCupon.Descripcion = cupon.MensajeLegal == null ? "" : cupon.MensajeLegal;

                imagePreview = this.GeneratePreviewSuper(compania, tipo, beCupon);
            }

            return imagePreview;
        }

        [HttpPost]
        [Route("api/cupones/impresion")]
        public string GetPrint(DetalleCupon cupon)
        {
            
            var compania = cupon.Compania == null ? "" : cupon.Compania;
            var codCupon = cupon.Codigo;

            var beCupon = new BE.DetalleCupon();

            beCupon.TipoDocumento = 0;
            beCupon.NroDocumento = cupon.NroDocumento;
            beCupon.Descuento = cupon.Descuento == null ? "" : cupon.Descuento;
            beCupon.Mensaje1 = cupon.Mensaje1 == null ? "" : cupon.Mensaje1;
            beCupon.Mensaje2 = cupon.Mensaje2 == null ? "" : cupon.Mensaje2;
            beCupon.Mensaje3 = cupon.Mensaje3 == null ? "" : cupon.Mensaje3;
            beCupon.Ambito = compania;
            beCupon.Tienda = cupon.Tienda == null ? "" : cupon.Tienda;
            beCupon.LegalMensaje = cupon.LegalMensaje == null ? "" : cupon.LegalMensaje;
            beCupon.ImagenPath = cupon.ImagenFtp == null ? "" : cupon.ImagenFtp;
            beCupon.CodigoBarra = cupon.CodigoBarra == null ? "" : cupon.CodigoBarra;
            beCupon.CodigoOnline = cupon.CodigoOnline == null ? "" : cupon.CodigoOnline;
            beCupon.LegalTipo = cupon.LegalTipo;
            beCupon.FlagCanal = cupon.Canal;
            beCupon.FlagSeriado = cupon.BonusSeriado;
            beCupon.BonusPrincipalSeriado = cupon.NumeroBonus == null ? "" : cupon.NumeroBonus;
            beCupon.Vigencia = cupon.Vigencia == null ? "" : cupon.Vigencia;

            string imagePreview = this.GeneratePrint(codCupon, beCupon);

            return imagePreview;
        }

        [HttpGet]
        [Route("api/cupones/mostrar")]
        public IEnumerable<Cupon> GetCupones(string nroDoc, string compania, string tienda, int idGrupo, int tipoCupon)
        {
            var lstCupones = new List<Cupon>();

            nroDoc = nroDoc == null ? "" : nroDoc.ToUpper();
            compania = compania == null ? "" : compania.ToUpper();
            tienda = tienda == null ? "" : tienda.ToUpper();

            var lstBeCupones = new BL.Cupones().ListarCupones(nroDoc, idGrupo, compania, tienda, tipoCupon);

            foreach (var beCupon in lstBeCupones)
            {
                var cupon = new Cupon();

                cupon.Codigo = beCupon.CodCampania;
                cupon.Descuento = beCupon.Descuento;
                cupon.Mensaje1 = beCupon.Mensaje1;
                cupon.Mensaje2 = beCupon.Mensaje2;
                cupon.Mensaje3 = beCupon.Mensaje3;
                cupon.ImagenFtp = this.GetPath(beCupon.ImagenPath);
                cupon.Compania = compania;
                cupon.Tipo = tipoCupon;
                cupon.MensajeLegal = beCupon.Descripcion;

                lstCupones.Add(cupon);
            }

            return lstCupones;
        }

        [HttpGet]
        [Route("api/cupones/obtener")]
        public IHttpActionResult GetCupon(string nroDoc, int codCupon, int idGrupo, int tipoCupon)
        {
            DetalleCupon cupon = null;

            var beCupon = new BL.Cupones().ObtenerDetalleCupon(nroDoc, codCupon, idGrupo, tipoCupon);

            if (beCupon != null)
            {
                cupon = new DetalleCupon();

                cupon.Codigo = codCupon;
                cupon.NroDocumento = beCupon.NroDocumento;
                cupon.Compania = beCupon.Ambito;
                cupon.Tienda = beCupon.Tienda;
                cupon.Descuento = beCupon.Descuento;
                cupon.Mensaje1 = beCupon.Mensaje1;
                cupon.Mensaje2 = beCupon.Mensaje2;
                cupon.Mensaje3 = beCupon.Mensaje3;
                cupon.LegalMensaje = beCupon.LegalMensaje;
                cupon.ImagenFtp = this.GetPath(beCupon.ImagenPath);
                cupon.CodigoBarra = beCupon.CodigoBarra;
                cupon.CodigoOnline = beCupon.CodigoOnline;
                cupon.LegalTipo = beCupon.LegalTipo;
                cupon.Canal = beCupon.FlagCanal;
                cupon.BonusSeriado = beCupon.FlagSeriado;
                cupon.NumeroBonus = beCupon.BonusPrincipalSeriado;
                cupon.Vigencia = beCupon.Vigencia;
                cupon.ImagenImpresion = this.GeneratePrint(codCupon, beCupon);
            }
            else
            {
                return NotFound();
            }

            return Ok(cupon);
        }

        //Temporal
        private string GetPath(string url)
        {
            string filename = Path.GetFileName(url);
            string directory = "";

            string urlDir = Path.GetDirectoryName(url).Split('\\').Last();
            switch (urlDir.ToUpper())
            {
                case "KIOSKO2":
                    directory = "MisCupones";
                    break;
                case "CUPONMARCA":
                    directory = "CuponesMarca";
                    break;
                case "CUPONLUGAR":
                    directory = "CuponesLugar";
                    break;
                case "CLUBW":
                    directory = "ClubW";
                    break;
                case "SUPERCUPON":
                    directory = "SuperCupon";
                    break;
                default:
                    directory = urlDir;
                    break;
            }

            return $"/Imagenes/{directory}/{filename}";
        }

        /// <summary>
        /// Obtener url de la imagen previa del cupon generado
        /// </summary>
        /// <param name="cupon">Datos del Cupon</param>
        /// <returns></returns>
        private string GeneratePreview(BE.Cupon cupon)
        {
            string compania = ""; //No es necesario para vista previa
            string filename = "";

            var commonCoupon = new COMMON.Coupon(compania);

            commonCoupon.BaseRoot = HttpContext.Current.Server.MapPath("~/App_Data");
            commonCoupon.FolderCache = "Imagenes";

            Image imgPreview = commonCoupon.ImagePreview(cupon);
            if (imgPreview != null)
            {
                filename = $"/Shared/Generated/Preview/{cupon.Id.ToString()}.png";

                string fullPath = HttpContext.Current.Server.MapPath("~");
                string fullFileName = $"{fullPath}{filename.Replace("/","\\")}";

                string directory = Path.GetDirectoryName(fullFileName);
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }
                else
                {
                    if (File.Exists(fullFileName))
                        File.Delete(fullFileName);
                }

                imgPreview.Save(fullFileName, ImageFormat.Png);
                imgPreview.Dispose();
            }

            return filename;
        }

        private string GeneratePreviewSuper(string compania, int tipoCupon, BE.Cupon cupon)
        {
            string filename = "";

            var commonCoupon = new COMMON.Coupon(compania);

            commonCoupon.BaseRoot = HttpContext.Current.Server.MapPath("~/App_Data");
            commonCoupon.FolderCache = "Imagenes";

            Image imgPreview = null;

            switch (tipoCupon)
            {
                case 1: //Regalo
                    imgPreview = commonCoupon.ImagePreviewSuperGift(cupon);
                    break;
                case 2: //Normal
                    imgPreview = commonCoupon.ImagePreviewSuperNormal(cupon);
                    break;
                case 3: //Horario
                    imgPreview = commonCoupon.ImagePreviewSuperTime(cupon);
                    break;
                default: //Regalo
                    break;
            }

            if (imgPreview != null)
            {
                filename = $"/Shared/Generated/Preview/{cupon.Id.ToString()}.png";

                string fullPath = HttpContext.Current.Server.MapPath("~");
                string fullFileName = $"{fullPath}{filename.Replace("/", "\\")}";

                string directory = Path.GetDirectoryName(fullFileName);
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }
                else
                {
                    if (File.Exists(fullFileName))
                        File.Delete(fullFileName);
                }

                imgPreview.Save(fullFileName, ImageFormat.Png);
                imgPreview.Dispose();
            }

            return filename;
        }


        /// <summary>
        /// Obtener url de la imagen de impresión del cupon generado
        /// </summary>
        /// <param name="codCupon">Codigo del Cupon</param>
        /// <param name="cupon">Datos del Cupon</param>
        /// <returns></returns>
        private string GeneratePrint(int codCupon, BE.DetalleCupon cupon)
        {
            string compania = cupon.Ambito;
            string filename = "";

            var commonCoupon = new COMMON.Coupon(compania);

            commonCoupon.BaseRoot = HttpContext.Current.Server.MapPath("~/App_Data");
            commonCoupon.FolderCache = "Imagenes";

            Image imgPreview = commonCoupon.ImageForPrint(cupon);
            if (imgPreview != null)
            {
                filename = $"/Shared/Generated/Print/{codCupon.ToString()}.png";

                string fullPath = HttpContext.Current.Server.MapPath("~");
                string fullFileName = $"{fullPath}{filename.Replace("/", "\\")}";

                string directory = Path.GetDirectoryName(fullFileName);
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }
                else
                {
                    if (File.Exists(fullFileName))
                        File.Delete(fullFileName);
                }
                
                imgPreview.Save(fullFileName, ImageFormat.Png);
                imgPreview.Dispose();
            }

            return filename;
        }

    }
}
