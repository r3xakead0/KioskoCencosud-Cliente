﻿using System.Collections.Generic;
using System.Web.Http;
using VisorCupones.WebApi.Models;
using BL = Cupones.BusinessLogic;

namespace VisorCupones.WebApi.Controllers
{
    public class ClientesController : ApiController
    {
        [HttpGet]
        [Route("api/clientes/documentos")]
        public IEnumerable<TipoDocumento> GetTiposDocumentos()
        {
            var lstTiposDocumentos = new List<TipoDocumento>();

            lstTiposDocumentos.Add(new TipoDocumento() { Id = 1, Codigo = "D", Nombre = "DNI" });
            lstTiposDocumentos.Add(new TipoDocumento() { Id = 2, Codigo = "B", Nombre = "BONUS" });

            return lstTiposDocumentos;
        }
    }
}
