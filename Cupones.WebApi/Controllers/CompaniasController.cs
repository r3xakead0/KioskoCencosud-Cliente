﻿using System.Collections.Generic;
using System.Web.Http;
using VisorCupones.WebApi.Models;
using BL = Cupones.BusinessLogic;

namespace VisorCupones.WebApi.Controllers
{
    public class CompaniasController : ApiController
    {
        [HttpGet]
        [Route("api/companias")]
        public IEnumerable<Compania> GetAllCompanias()
        {
            var lstCompanias = new List<Compania>();

            var lstBeCompanias = new BL.Configuraciones().ListarCompanhias();

            foreach (var beCompania in lstBeCompanias)
            {
                var compania = new Compania();

                compania.Codigo = beCompania.Codigo;
                compania.Nombre = beCompania.Nombre;

                lstCompanias.Add(compania);
            }

            return lstCompanias;
        }
    }
}
