﻿namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Configuracion
    {
        #region Servidor de Email
        public string EmailServidor { get; set; }
        public int EmailPuerto { get; set; }
        public string EmailCuenta { get; set; }
        public string EmailClave { get; set; }
        #endregion

        #region Ticketera
        public string Compania { get; set; } 
        public string Tienda { get; set; }
        public string Hostname { get; set; }
        public string Ubicacion { get; set; }
        public string AplicacionClave { get; set; } 
        #endregion

        #region Servicio Tecnico
        public string TecnicoEmail { get; set; }
        public string TecnicoTelefono { get; set; } 
        #endregion
        
    }
}
