﻿namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class PerfilTienda 
    {
        public string TiendaId { get; set; }
        public string Ambito { get; set; }

        public int OpcionMisCupones { get; set; }
        public int OpcionMiSuperCupon { get; set; }
        public int OpcionMarcasSeleccionadas { get; set; }
        public int OpcionArbolDeCupones { get; set; }
        public int OpcionCuponesDelLugar { get; set; }
        public int OpcionRuletaDeCupones { get; set; }
        public int OpcionMisPuntosBonus { get; set; }
        public int OpcionMisInvitaciones { get; set; }
        public int OpcionCuponesComerciales { get; set; }
}
}
