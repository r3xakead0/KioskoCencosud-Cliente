﻿using System.Drawing;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Banner
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ImagenPath { get; set; }
        public Image Imagen { get; set; }
        public int Orden { get; set; }
        public int Prioridad { get; set; }
        public int Tiempo { get; set; }
        public int Contador { get; set; } = 0;
    }
}
