﻿using System;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public enum Opcion : int
    {
        Inactivo = 0,
        Activo = 1,
        Nuevo = 2,
        ActivoNuevo = 3
    }

}
