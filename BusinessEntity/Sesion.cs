﻿using System;
using System.Collections.Generic;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Sesion
    {

        private Guid _id;
        private DateTime _inicio;
        private DateTime? _fin;
        private string _nroDocumento;
        private int _tipoDocumento; // 0 = NO DEFINIDO | 1 = DNI | 2 = BONUS

        public bool OpcionMisCupones { get; set; }
        public bool OpcionMiSuperCupon { get; set; }
        public bool OpcionMarcasSeleccionadas { get; set; }
        public bool OpcionArbolDeCupones { get; set; }
        public bool OpcionCuponesDelLugar { get; set; }
        public bool OpcionRuletaDeCupones { get; set; }
        public bool OpcionMisPuntosBonus { get; set; }
        public bool OpcionMisInvitaciones { get; set; }
        public bool OpcionCuponesComerciales { get; set; }

        public List<int> CuponesImpresos { get; set; }
        public List<int> CuponesVistos { get; set; }
        public List<int> BannersVistos { get; set; }

        public Guid Id
        {
            get
            {
                return _id;
            }
        }

        public DateTime Inicio
        {
            get
            {
                return _inicio;
            }
        }

        public DateTime? Fin
        {
            get
            {
                return _fin;
            }
            set
            {
                this._fin = value;
            }
        }

        public string NroDocumento
        {
            get
            {
                return _nroDocumento;
            }
        }

        public int TipoDocumento
        {
            get
            {
                return _tipoDocumento;
            }
        }

        public Sesion(string nroDocumento, int tipoDocumento, DateTime fecha)
        {
            this._id = Guid.NewGuid();
            this._inicio = fecha;
            this._fin = null;
            this._nroDocumento = nroDocumento;
            this._tipoDocumento = (tipoDocumento < 1 && tipoDocumento > 2) ? 0 : tipoDocumento;

            this.OpcionMisCupones = false;
            this.OpcionMiSuperCupon = false;
            this.OpcionMarcasSeleccionadas = false;
            this.OpcionArbolDeCupones = false;
            this.OpcionCuponesDelLugar = false;
            this.OpcionRuletaDeCupones = false;
            this.OpcionMisPuntosBonus = false;
            this.OpcionMisInvitaciones = false;
            this.OpcionCuponesComerciales = false;

            this.CuponesImpresos = new List<int>();
            this.CuponesVistos = new List<int>();
            this.BannersVistos = new List<int>();
        }
    }
}

