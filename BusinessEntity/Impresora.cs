﻿namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Impresora
    {
        public int ImpresoraId { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }

        public int MargenIzquierdoX { get; set; }
        public int MargenSuperiorY { get; set; }
        public int Ancho { get; set; }
        public int Alto { get; set; }
    }
}
