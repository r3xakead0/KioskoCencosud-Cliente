﻿using System;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Perfil
    {

        public Perfil()
        {
            OpcionMisCupones = Opcion.Inactivo;
            OpcionMiSuperCupon = Opcion.Inactivo;
            OpcionMarcasSeleccionadas = Opcion.Inactivo;
            OpcionArbolDeCupones = Opcion.Inactivo;
            OpcionCuponesDelLugar = Opcion.Inactivo;
            OpcionRuletaDeCupones = Opcion.Inactivo;
            OpcionMisPuntosBonus = Opcion.Inactivo;
            OpcionMisInvitaciones = Opcion.Inactivo;
            OpcionCuponesComerciales = Opcion.Inactivo;

            FechaIniBanner = DateTime.Now;
            FechaFinBanner = DateTime.Now;
            RutaImagenBannerTienda = string.Empty;

            MargenIzquierdoX = 0;
            MargenSuperiorY = 0;
            Ancho = 0;
            Alto = 0;
        }
        
        #region Opciones del Kiosko

        public Opcion OpcionMisCupones { get; set; } 
        public Opcion OpcionMiSuperCupon { get; set; }
        public Opcion OpcionMarcasSeleccionadas { get; set; }
        public Opcion OpcionArbolDeCupones { get; set; }
        public Opcion OpcionCuponesDelLugar { get; set; }
        public Opcion OpcionRuletaDeCupones { get; set; }
        public Opcion OpcionMisPuntosBonus { get; set; }
        public Opcion OpcionMisInvitaciones { get; set; }
        public Opcion OpcionCuponesComerciales { get; set; }

        #endregion

        #region Banner del Kiosko

        public DateTime FechaIniBanner { get; set; }
        public DateTime FechaFinBanner { get; set; }
        public string RutaImagenBannerTienda { get; set; } 

        #endregion

        #region Configuracion de hoja para la impresora del Kiosko

        public int MargenIzquierdoX { get; set; }
        public int MargenSuperiorY { get; set; } 
        public int Ancho { get; set; } 
        public int Alto { get; set; } 

        #endregion

    }
}
