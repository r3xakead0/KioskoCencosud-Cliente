﻿namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Cliente
    {
        public string NroDocumento { get; set; }
        public int TipoDocumento { get; set; } //Tipo del documento del Cliente (1 = DNI | 2 = OTRO)
        public string NompleCompleto { get; set; }
        public int CuponesAsignadosMetro { get; set; } = 0;
        public int CuponesAsignadosWong { get; set; } = 0;
    }
}
