﻿using System;
using System.Drawing;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class Cupon
    {
        public int Id { get; set; }
        public int CodCampania { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string ImagenPath { get; set; }
        public Image Imagen { get; set; }
        public string Descripcion { get; set; }
        public int Prioridad { get; set; }
        public int CantidadCuponesDisponibles { get; set; }
        public int Redimidos { get; set; }
        public DateTime? FechaHoraRedencion { get; set; }
        public bool ExclusivoApp { get; set; }
    }
}
