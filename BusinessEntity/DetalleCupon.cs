﻿using System.Drawing;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class DetalleCupon
    {
        public int TipoDocumento { get; set; } // Tipo del documento del Cliente (1 = DNI | 2 = OTRO)
        public string NroDocumento { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Ambito { get; set; }
        public string Tienda { get; set; }
        public string LegalMensaje { get; set; }
        public string ImagenPath { get; set; }
        public Image Imagen { get; set; }
        public string CodigoBarra { get; set; }
        public int LegalTipo { get; set; } // 0 = Normal | 1 = Bebida Alcoholicas
        public int FlagCanal { get; set; } // 1 = Canje Tienda | 2 = Canje Online | 3 = Canje Tienda y online
        public int FlagSeriado { get; set; } // 0 = No | 1 = Si
        public string BonusPrincipalSeriado { get; set; } //Numero de Bonus Principal
        public string BonusTitularSeriado { get; set; } //Numero de Bonus Titular
        public string CodigoOnline { get; set; }
        public string Vigencia { get; set; }
        public int Tipo { get; set; }
        public int CodEmision { get; set; } 
    }
}
