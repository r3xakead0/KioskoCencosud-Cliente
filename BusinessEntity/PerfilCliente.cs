﻿namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class PerfilCliente 
    {
        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }

        public string TiendaAdmitidas { get; set; }
        public string Ambito { get; set; }

        public bool OpcionMisCupones { get; set; }
        public bool OpcionMiSuperCupon { get; set; }
        public bool OpcionMarcasSeleccionadas { get; set; }
        public bool OpcionArbolDeCupones { get; set; }
        public bool OpcionCuponesDelLugar { get; set; }
        public bool OpcionRuletaDeCupones { get; set; }
        public bool OpcionMisPuntosBonus { get; set; }
        public bool OpcionMisInvitaciones { get; set; }
        public bool OpcionCuponesComerciales { get; set; }
    }
}
