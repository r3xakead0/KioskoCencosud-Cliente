﻿using System;

namespace Cupones.BusinessEntity
{
    /// <summary>
    /// Author : Afu Tse
    /// Email : rusvi_rus@hotmail.com
    /// </summary>
    public class PerfilKiosko 
    {
        public int TiendaKioskoId { get; set; }
        public string TiendaId { get; set; }
        public string Ambito { get; set; }

        public string HostName { get; set; }
        public string DireccionIP { get; set; }
        public string Zona { get; set; }

        #region Impresora
        public int ImpresoraId { get; set; }
        public Impresora Impresora { get; set; }
        #endregion

        #region Banner
        public DateTime FechaIniBanner { get; set; }
        public DateTime FechaFinBanner { get; set; }
        public string RutaImagenBannerTienda { get; set; }
        #endregion
    }
}
