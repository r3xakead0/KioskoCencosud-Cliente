﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cupones.DataEntity
{
    public class JsonToEntity
    {

        #region Configuracion

        public static ConfiguracionEntity ObtenerConfiguracion(ConfiguracionJson json)
        {
            ConfiguracionEntity entity = null;
            try
            {
                entity = new ConfiguracionEntity();

                entity.Compania = json.Compania;
                entity.Tienda = json.Tienda;
                entity.Hostname = json.Hostname;
                entity.Ubicacion = json.Ubicacion;
                entity.LongPapel = json.LongPapel;
                entity.LongCupon = json.LongCupon;
                entity.TotImpresiones = json.TotImpresiones;
                entity.RestImpresiones = json.RestImpresiones;
                entity.TecnicoTelefono = json.Telefono;
                entity.TecnicoEmail = json.Email;
                entity.EmailServidor = json.Servidor;
                entity.EmailPuerto = json.Puerto;
                entity.EmailCuenta = json.EmailSalida;
                entity.EmailClave = json.Pass;
                entity.AplicacionClave = json.PassAplicacion;

                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Cupon

        public static DetalleCuponEntity ObtenerCupon(DetalleCuponJson json)
        {
            DetalleCuponEntity entity = null;
            try
            {
                entity = new DetalleCuponEntity();

                entity.Descuento = json.Descuento;
                entity.Mensaje1 = json.Mensaje1;
                entity.Mensaje2 = json.Mensaje2;
                entity.Mensaje3 = json.Mensaje3;
                entity.Ambito = json.Ambito;
                entity.TipoLegal = int.Parse(json.TipoLegal);
                entity.MensajeLegal = json.MensajeLegal;
                entity.Imagen = json.Imagen;
                entity.CodigoBarra = json.CodigoBarra;
                entity.CodigoCanal = int.Parse(json.CodigoCanal);
                entity.FlagSeriado = int.Parse(json.FlagSeriado);
                entity.BonusSeriado = json.BonusSeriado;
                entity.CodigoOnline = json.CodigoOnline;
                entity.Vigencia = json.Vigencia;
                entity.Tipo = int.Parse(json.Tipo);

                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Cliente

        public static ValidarEntity ValidarCliente(ValidarJson json)
        {
            ValidarEntity entity = null;
            try
            {
                entity = new ValidarEntity();

                entity.Valido = json.Valido.Equals("1");
                entity.EsCumpleanhos = json.EsCumpleanhos.Equals("1");
                entity.CuponCumpleanhos = json.CuponCumpleanhos.Equals("1");

                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ClienteEntity ObtenerCliente(ClienteJson json)
        {
            ClienteEntity entity = null;
            try
            {
                entity = new ClienteEntity();

                entity.TipoDocumento = json.TipoDocumento;
                entity.NumeroDocumento = json.NumeroDocumento;
                entity.Nombres = json.Nombres;

                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
