﻿namespace Cupones.DataEntity
{
    public class PerfilTiendaJson
    {
        public string TiendaId { get; set; }
        public string Ambito { get; set; }
        public string OpcionMisCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionMiSuperCupon { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionMarcasSeleccionadas { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionArbolDeCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionCuponesDelLugar { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionRuletaDeCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionMisPuntosBonus { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionMisInvitaciones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public string OpcionCuponesComerciales { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
    }

    public class PerfilClienteJson
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string TiendaAdmitidas { get; set; }
        public string Ambito { get; set; }
        public string OpcionMisCupones { get; set; }
        public string OpcionMiSuperCupon { get; set; }
        public string OpcionMarcasSeleccionadas { get; set; }
        public string OpcionArbolDeCupones { get; set; }
        public string OpcionCuponesDelLugar { get; set; }
        public string OpcionRuletaDeCupones { get; set; }
        public string OpcionMisPuntosBonus { get; set; }
        public string OpcionMisInvitaciones { get; set; }
        public string OpcionCuponesComerciales { get; set; }
    }

    public class PerfilKioskoJson
    {
        public string TiendaKioskoId { get; set; }
        public string TiendaId { get; set; }
        public string Ambito { get; set; }
        public string HostName { get; set; }
        public string DireccionIP { get; set; }
        public string Zona { get; set; }
        public string ImagenBanner { get; set; }
        public string FechaIniBanner { get; set; }
        public string FechaFinBanner { get; set; }
        public string ImpresoraId { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string MargenIzquierdoX { get; set; }
        public string MargenSuperiorY { get; set; }
        public string Ancho { get; set; }
        public string Alto { get; set; }
    }

}
