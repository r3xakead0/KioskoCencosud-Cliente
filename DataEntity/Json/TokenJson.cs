﻿namespace Cupones.DataEntity
{
    public class TokenJson
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}
