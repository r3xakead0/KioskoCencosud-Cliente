﻿namespace Cupones.DataEntity

{

    public class BannerJson
    {
        public int IdBanner { get; set; }
        public string Nombre { get; set; }
        public string Imagen { get; set; }
        public int Orden { get; set; }
        public int Prioridad { get; set; }
        public int Tiempo { get; set; }
    }

}
