﻿namespace Cupones.DataEntity
{

    public class CompanhiaJson
    {
        public string Compania { get; set; }
    }

    public class TiendaJson
    {
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string Hostname { get; set; }
    }

    public class ClienteJson
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombres { get; set; }
        public int CuponesAsignadosMetro { get; set; }
        public int CuponesAsignadosWong { get; set; }
    }

    public class ValidarJson
    {
        public string Valido { get; set; }
        public string EsCumpleanhos { get; set; }
        public string CuponCumpleanhos { get; set; }
        public string FechaHoraConsulta { get; set; }
    }

    public class RegistradoJson
    {
        public string Registrado { get; set; }
    }

}
