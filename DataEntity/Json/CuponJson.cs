﻿namespace Cupones.DataEntity
{
    public class CuponJson
    {
        public string IdKiosko { get; set; }
        public string CodCampania { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Imagen { get; set; }
        public string Descripcion { get; set; }
        public string Prioridad { get; set; }
        public string Disponibles { get; set; }
        public string Redimidos { get; set; }
        public string FechaRedencion { get; set; }
        public string HoraRedencion { get; set; }
        public string ExclusivoApp { get; set; }
    }

    public class DetalleCuponJson
    { 
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Ambito { get; set; }
        public string TipoLegal { get; set; }
        public string MensajeLegal { get; set; }
        public string Imagen { get; set; }
        public string CodigoBarra { get; set; }
        public string CodigoCanal { get; set; }
        public string FlagSeriado { get; set; }
        public string BonusSeriado { get; set; } //public string BonusPrincipalSeriado { get; set; } 
        public string BonusTitularSeriado { get; set; }
        public string CodigoOnline { get; set; }
        public string Vigencia { get; set; }
        public string Tipo { get; set; }
        public int CodEmision { get; set; }
    }

}
