﻿namespace Cupones.DataEntity

{

    public class ConfiguracionJson
    {
        public string Compania { get; set; }

        public string Tienda { get; set; }
        public string Hostname { get; set; }
        public string Ubicacion { get; set; }

        public int LongPapel { get; set; }
        public int LongCupon { get; set; }
        public int TotImpresiones { get; set; }
        public int RestImpresiones { get; set; }

        public string Telefono { get; set; }
        public string Email { get; set; }

        public string Servidor { get; set; }
        public int Puerto { get; set; }
        public string EmailSalida { get; set; }
        public string Pass { get; set; }

        public string PassAplicacion { get; set; }
    }

}
