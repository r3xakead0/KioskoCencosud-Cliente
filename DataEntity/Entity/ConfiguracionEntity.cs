﻿namespace Cupones.DataEntity
{

    public class ConfiguracionEntity
    {
        public string Compania { get; set; }

        public string Tienda { get; set; }
        public string Hostname { get; set; }
        public string Ubicacion { get; set; }

        public int LongPapel { get; set; }
        public int LongCupon { get; set; }
        public int TotImpresiones { get; set; }
        public int RestImpresiones { get; set; }

        public string TecnicoTelefono { get; set; }
        public string TecnicoEmail { get; set; }

        public string EmailServidor { get; set; }
        public int EmailPuerto { get; set; }
        public string EmailCuenta { get; set; }
        public string EmailClave { get; set; }

        public string AplicacionClave { get; set; }
    }

}
