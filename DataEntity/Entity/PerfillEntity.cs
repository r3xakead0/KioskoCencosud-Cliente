﻿namespace Cupones.DataEntity
{
    public class PerfilTiendaEntity
    {
        public string TiendaId { get; set; }
        public string Ambito { get; set; }
        public int OpcionMisCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionMiSuperCupon { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionMarcasSeleccionadas { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionArbolDeCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionCuponesDelLugar { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionRuletaDeCupones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionMisPuntosBonus { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionMisInvitaciones { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
        public int OpcionCuponesComerciales { get; set; } // 0 = Inactivo | 1 = Activo | 2 = Nuevo y Activo
    }

    public class PerfilClienteEntity
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string TiendaAdmitidas { get; set; }
        public string Ambito { get; set; }
        public bool OpcionMisCupones { get; set; }
        public bool OpcionMiSuperCupon { get; set; }
        public bool OpcionMarcasSeleccionadas { get; set; }
        public bool OpcionArbolDeCupones { get; set; }
        public bool OpcionCuponesDelLugar { get; set; }
        public bool OpcionRuletaDeCupones { get; set; }
        public bool OpcionMisPuntosBonus { get; set; }
        public bool OpcionMisInvitaciones { get; set; }
        public bool OpcionCuponesComerciales { get; set; }
    }

    public class PerfilKioskoEntity
    {
        public int TiendaKioskoId { get; set; }
        public string TiendaId { get; set; }
        public string Ambito { get; set; }
        public string HostName { get; set; }
        public string DireccionIP { get; set; }
        public string Zona { get; set; }
        public string ImagenBanner { get; set; }
        public string FechaIniBanner { get; set; }
        public string FechaFinBanner { get; set; }
        public int ImpresoraId { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int MargenIzquierdoX { get; set; }
        public int MargenSuperiorY { get; set; }
        public int Ancho { get; set; }
        public int Alto { get; set; }
    }

}
