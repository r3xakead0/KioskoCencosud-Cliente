﻿namespace Cupones.DataEntity
{
    public class CuponEntity
    {
        public int IdKiosko { get; set; }
        public int CodCampania { get; set; }
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Imagen { get; set; }
        public string Descripcion { get; set; }
        public int Prioridad { get; set; }
        public int CantidadDisponibles { get; set; }
        public int CantidadRedimidos { get; set; }
        public string FechaRedencion { get; set; }
        public string HoraRedencion { get; set; }
        public bool ExclusivoApp { get; set; }
    }

    public class DetalleCuponEntity
    {
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Ambito { get; set; }
        public int TipoLegal { get; set; }
        public string MensajeLegal { get; set; }
        public string Imagen { get; set; }
        public string CodigoBarra { get; set; }
        public int CodigoCanal { get; set; }
        public int FlagSeriado { get; set; }
        public string BonusPrincipalSeriado { get; set; }
        public string BonusTitularSeriado { get; set; }
        public string CodigoOnline { get; set; }
        public string Vigencia { get; set; }
        public int Tipo { get; set; }
        public int CodEmision { get; set; }
    
    }

}
