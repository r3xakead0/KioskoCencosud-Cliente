﻿using System;

namespace Cupones.DataEntity
{

    public class KioskoEntity
    {
        public string Empresa { get; set; }
        public string Tienda { get; set; } 
        public string Hostname { get; set; } 
        public string Ip { get; set; }
        public bool Conexion { get; set; } 
        public bool Encendido { get; set; }
        public string Version { get; set; }
        public string Sesion { get; set; }
        public DateTime FechaHora { get; set; }
    }

}
