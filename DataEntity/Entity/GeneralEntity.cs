﻿namespace Cupones.DataEntity
{

    public class CompanhiaEntity
    {
        public string Compania { get; set; }
    }

    public class TiendaEntity
    {
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string Hostname { get; set; }
    }

    public class ClienteEntity
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombres { get; set; }
        public int CuponesAsignadosMetro { get; set; }
        public int CuponesAsignadosWong { get; set; }
    }

    public class ValidarEntity
    {
        public bool Valido { get; set; }
        public bool EsCumpleanhos { get; set; }
        public bool CuponCumpleanhos { get; set; }
        public System.DateTime FechaHoraConsulta { get; set; }
    }
}
